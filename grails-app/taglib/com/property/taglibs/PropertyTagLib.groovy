package com.property.taglibs

import com.model.AppConstants
import com.model.security.Menu

class PropertyTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "property"
    def springSecurityService
    def menuService

    def pageHeader = { attrs, body ->
        out << """<h1>${attrs.title}<small>${attrs.description}</small></h1>"""
    }

    def alert = { attrs, body ->
        def msg = attrs.message
        def type = attrs.type
        def title = attrs.title
        def t = """"""
        def icon

        if(type=='success') icon = "check"
        else if(type=='danger') icon = "ban"
        else icon = type

        if(msg!=null && msg.trim().length()>0){
            t += """<div class="alert alert-${type} alert-dismissible">"""
            t += """<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>"""
            if(title!=null && title.trim().length()>0) {
                t += """<h4><i class="icon fa fa-${icon}"></i>${title}</h4>"""
            }
            t += msg
            t += "</div>"

            out << t
        }
    }

    def menus = { attrs, body ->
        if(springSecurityService.principal==null || springSecurityService.principal.getAuthorities()==null){
            out << ""
        } else {
            def roles = session.getAttribute(AppConstants.SESSION_ROLE)
            if (roles == null || roles.size() == 0) {
                roles = springSecurityService.principal.getAuthorities().collect { it.authority.toString() }
                session.setAttribute(AppConstants.SESSION_ROLE, roles)
            }
            def menuBody = ""
            def menus = menuService.getParentMenuList(roles)
            def selectedMenu = menuService.getSelectedMenu(session.getAttribute(AppConstants.SESSION_MENU_ID))
            menus.each { menu ->
                if (!menu.hasSubMenu) {
                    menuBody += renderMenu(selectedMenu, menu)
                } else {
                    def submenus = menuService.getSubmenuList(menu, roles, springSecurityService.principal.id)
                    if (submenus.size > 0) {
                        menuBody += renderSubMenus(selectedMenu, menu, submenus)
                    } else {
                        menuBody += renderMenu(selectedMenu, menu)
                    }
                }
            }
            out << menuBody
        }
    }

    def renderMenu(selectedMenu, menu){
        def tmp = ""
        if(selectedMenu.id == menu.id){
            tmp += """<li class="active">"""
        } else {
            tmp += """<li>"""
        }
        def icon = "fa-link"
        if(menu.icon!=null){
            icon = menu.icon
        }
        tmp += """<a href="${g.createLink(controller:menu.controllerPath,action:menu.actionName)}"><i class="fa ${icon}"></i><span>${menu.name}<span></a>"""
        tmp += """</li>"""
        return tmp
    }

    def renderSubMenus(selectedMenu, menu, submenus){
        def tmp = ""
        if(selectedMenu.parentMenu!=null && selectedMenu.parentMenu.id == menu.id){
            tmp += """<li class="treeview menu-open active">"""
        } else {
            tmp += """<li class="treeview">"""
        }
        def icon = "fa-link"
        if(menu.icon!=null){
            icon = menu.icon
        }
        tmp += """<a href="#"><i class="fa ${icon}"></i> <span>${menu.name}</span>"""
        tmp += """<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>"""
        tmp += """<ul class="treeview-menu">"""
        submenus.each { submenu ->
            if(submenu.id == selectedMenu.id){
                tmp += """<li class="active">"""
            } else {
                tmp += """<li>"""
            }
            tmp += """<a href="${g.createLink(controller:submenu.controllerPath,action:submenu.actionName)}"><span style="padding-left:20px">${submenu.name}<span></a>"""
            tmp += """</li>"""
        }
        tmp += """</ul></li>"""
        return tmp
    }
}
