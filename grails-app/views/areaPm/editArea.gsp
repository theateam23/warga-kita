<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edit Area</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Area Properti" description="Tambah dan Edit Area Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="areaPm" action="index"><i class="fa fa-gear"></i> Area</g:link></li>
            <li class="active">Edit Area</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <g:form action="confirmEdit" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${area.propName}" />
                            <p class="form-control-static">${area.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${area.id}" />
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${area.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfAddress" class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="text" name="address" class="form-control" id="tfAddress" maxlength="200" placeholder="Address" value="${area.address}">
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Total Sub Area:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="text" name="totalSubArea" class="form-control" maxlength="20" placeholder="Total Sub Area" value="${area.totalSubArea}">--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label for="tfTotalUnit" class="col-sm-2 control-label">Jumlah Unit:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="text" name="totalUnit" class="form-control" id="tfTotalUnit" maxlength="20" placeholder="Developer" value="${area.totalUnit}" required>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                </div>
                <div class="box-footer">
                    <g:link controller="areaPm" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#form1').validate({
                rules: {
                    totalSubArea: {
                        greaterThanZero: true
                    },
                    totalUnit: {
                        required: true,
                        greaterThanZero: true
                    }
                },
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>

</body>
</html>