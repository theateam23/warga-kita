<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Area Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Area Properti" description="Tambah dan Edit Area Properti" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-gear"></i> Area</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Area</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="areaPm" action="createArea" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Area Properti</g:link>

            <table class="table table-striped table-bordered table-hover" id="dataTables-areas" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="propInfoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Detail Property</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="propName"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Developer:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="propDeveloper"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="propAddress"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Properti Manager:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="propPmName"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="clear:both;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var detailAreaUrl = '${g.createLink(controller:'areaPm',action:'detailArea')}';
    var detailPropUrl = '${g.createLink(controller:'areaPm',action:'detailPropertyJSON')}';

    $(document).ready(function(){
        $('#dataTables-areas').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'areaPm',action:'areaListDatatable')}',
            "aoColumns": [
                { "mData": "name", "sTitle":"Nama", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailAreaUrl+"/"+full.id+"'>"+full.name+"</a>";
                        }
                },
//                { "mData": "address", "sTitle":"Alamat" },
//                { "mData": "totalSubArea", "sTitle":"Total Sub Area" },
//                { "mData": "totalUnit", "sTitle":"Total Unit" },
                { "mData": "property", "sTitle":"Properti", "mRender":
                        function(data, type, full){
                            return "<a href='#' onclick=doDetailProp('" + full.propId + "')>"+full.propName+"</a>";
                        }
                }
            ]
        });
    });

    function doDetailProp(propId){
        $.ajax({
            type: 'GET',
            url: detailPropUrl,
            data: {id: propId},
            success: (function (output) {
                if (output.result) {
                    $('#propName').html(output.name);
                    $('#propDeveloper').html(output.developer);
                    $('#propAddress').html(output.address);
                    $('#propPmName').html(output.pmName);
                    $('#propInfoModal').modal('show');
                } else {
                    vex.dialog.alert({message: output.message});
                }
            }),
            error: (function (data) {
                vex.dialog.alert({message: "System Error"})
            }),
            complete: (function (data) {
            })
        });
    }

</script>
</body>
</html>