<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${area.stage} ${area.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Area Properti" description="Tambah dan Edit Area Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="areaPm" action="index"><i class="fa fa-gear"></i> Area</g:link></li>
            <li>${area.action}</li>
            <li class="active">${area.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${area.stage}</h3>
            </div>
            <g:form action="${area.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${area.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${area.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${area.propName}" />
                            <p class="form-control-static">${area.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${area.id}" />
                            <input type="hidden" name="name" value="${area.name}" />
                            <p class="form-control-static">${area.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="address" value="${area.address}" />
                            <p class="form-control-static">${area.address}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Total Sub Area:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="hidden" name="totalSubArea" value="${area.totalSubArea}" />--}%
                            %{--<p class="form-control-static">${area.totalSubArea}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Jumlah Unit:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="hidden" name="totalUnit" value="${area.totalUnit}" />--}%
                            %{--<p class="form-control-static">${area.totalUnit}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                </div>
                <div class="box-footer">
                    <g:if test="${area.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" class="btn btn-info pull-right" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="areaPm" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>