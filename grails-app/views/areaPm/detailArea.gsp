<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Area</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Area Properti" description="Tambah dan Edit Area Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="areaPm" action="index"><i class="fa fa-gear"></i> Area</g:link></li>
            <li class="active">Detail Area</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${area.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${area.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${area.address}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Total Sub Area:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${area.totalSubArea}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Jumlah Unit:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${area.totalUnit}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="areaPm" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="areaPm" action="editArea" params="[id:area.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="areaPm" action="deleteArea" method="POST">
        <input type="hidden" name="id" value="${area.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data properti ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>