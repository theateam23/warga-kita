<!doctype html>
<html>
    <head>
        <title>Error</title>
        <meta name="layout" content="main">
    </head>
    <body>
        <section class="content-header">
            <property:pageHeader title="Error" description="404" />
        </section>

        <section class="content container-fluid">
            <ul class="errors">
                <li>Error: Page Not Found (404)</li>
                <li>Path: ${request.forwardURI}</li>
            </ul>
        </section>
    </body>
</html>
