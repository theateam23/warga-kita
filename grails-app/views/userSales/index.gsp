<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Manage Sales</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pengaturan Team Sales" description="Tambah dan Edit Team Sales" />
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Manage Sales</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Properti Sales</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <g:link controller="userSales" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Sales</g:link>

                <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%">

                </table>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </section>

    <script>
        var detailUserUrl = '${g.createLink(controller:'userSales',action:'detail')}';

        $(document).ready(function(){
            $('#dataTables-users').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bFilter": false,
                "bServerSide": true,
                "bProcessing": true,
                "sPaginationType": "simple_numbers",
                "sAjaxSource": '${g.createLink(controller:'userSales',action:'listDatatable')}',
                "columnDefs": [
                    { "orderable": false, "targets": 3 },
                    { "orderable": false, "targets": 4 },
                    { "orderable": false, "targets": 5 }
                ],
                "aoColumns": [
                    { "mData": "username", "sTitle":"Email", "mRender":
                            function(data, type, full){
                                return "<a href='"+detailUserUrl+"/"+full.id+"'>"+full.username+"</a>";
                            }},
                    { "mData": "name", "sTitle":"Nama" },
                    { "mData": "phoneNo", "sTitle":"No Telepon" },
                    { "mData": "totalDealProposed", "sTitle":"Deal Proposed" },
                    { "mData": "totalDealSecured", "sTitle":"Deal Secured" },
                    { "mData": "totalRevenue", "sTitle":"Total Revenue" }
                ]
            });
        });
    </script>
</body>
</html>