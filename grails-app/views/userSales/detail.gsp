<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Sales</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pengaturan Team Sales" description="Tambah dan Edit Team Sales" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i> <g:link controller="userSales" action="index"> Manage Sales</g:link></li>
            <li class="active">Detail Sales</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.username}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.phoneNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Bank:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.bankName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Rekening Bank:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.bankAccount}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Deal Proposed:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.totalDealProposed}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Secured Deal:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.totalDealSecured}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Revenue:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.totalRevenue}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-header with-border">
                <h3 class="box-title">Secured Property Deals</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="dataTables-property" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Developer</th>
                            <th>Address</th>
                            <th>Total Unit</th>
                            <th>Fee</th>
                            <th>Expiry Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${propList}" var="obj">
                            <tr>
                                <td>${obj.name}</td>
                                <td>${obj.developer}</td>
                                <td>${obj.address}</td>
                                <td>${obj.totalUnit}</td>
                                <td>${obj.fee}</td>
                                <td>${obj.expiryDate}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <g:link controller="userSales" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="userSales" action="edit" params="[id:user.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <g:link controller="userSales" action="resetPassword" params="[id:user.id]" class="btn btn-warning"><i class="fa fa-save"></i> Reset Password</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="userSales" action="delete" method="POST">
        <input type="hidden" name="id" value="${user.id}" />
    </g:form>

    <script>
        $(document).ready(function(){
            $('#dataTables-property').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bPaginate": true,
                "bFilter": false,
                "bInfo": false
            });
        });

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data user ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>