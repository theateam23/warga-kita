<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edit Sales</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pengaturan Team Sales" description="Tambah dan Edit Team Sales" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i> <g:link controller="userSales" action="index"> Manage Sales</g:link></li>
            <li class="active">Edit Sales</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <g:form action="confirmEdit" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${user.id}" />
                            <input type="hidden" name="username" value="${user.username}" />
                            <p class="form-control-static">${user.username}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" maxlength="200" name="name" class="form-control" id="tfName" placeholder="Name" value="${user.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfPhoneNo" class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <input type="number" maxlength="20" name="phoneNo" class="form-control" id="tfPhoneNo" placeholder="Phone No" value="${user.phoneNo}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Bank:</label>
                        <div class="col-sm-8">
                            <input maxlength="200" name="bankName" class="form-control" placeholder="Bank Name" value="${user.bankName}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Rekening Bank:</label>
                        <div class="col-sm-8">
                            <input maxlength="200" name="bankAccount" class="form-control" placeholder="Bank Account" value="${user.bankAccount}">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="userSales" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
           $('#form1').validate({
               submitHandler: function(form) {
                   $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                   form.submit();
               }
           });
        });
    </script>
</body>
</html>