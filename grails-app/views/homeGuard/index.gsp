<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home Guard</title>
</head>
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)}
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)}
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<body>
<section class="content-header">
    <property:pageHeader title="Laporan Jaga Rumah" description="Daftar Kegiatan Patroli Jaga Rumah" />
    <ol class="breadcrumb">
        <li>Report</li>
        <li class="active"><i class="fa fa-lock"></i> Home Guard</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Jaga Rumah</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="form-horizontal col-md-7">
                <g:form action="index" method="GET" style="margin-bottom: 20px;" name="form1">
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Properti:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${propName}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="guardDate" />
                                <input type="hidden" name="startDate" id="startDate" value="${startDate}"/>
                                <input type="hidden" name="endDate" id="endDate" value="${endDate}"/>
                            </div>
                        </div>
                    </div>
                </g:form>

                <div class="form-group">
                    <div class="pull-right" id="downloadDiv">
                        <button type="button" onclick="submitDownload()" class="btn btn-primary"><i class="fa fa-download"></i> Download Report</button>
                        <button type="button" onclick="submitSearch()" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </div>

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-members" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="imageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Image</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12" id="imageContainer">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="clear:both;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#guardDate').daterangepicker({
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": "  to  "
            },
            maxDate: new Date(),
            startDate: moment(),
            endDate: moment()
        }, function(start, end, label) {
            $("#startDate").val(start._d.getTime());
            $("#endDate").val(end._d.getTime());
        });

        var ps = $('#startDate').val();
        var pe = $('#endDate').val();
        var drp = $("#guardDate").data('daterangepicker');

        if(ps==null || ps==''){
            $("#startDate").val(drp.startDate._d.getTime());
        } else {
            drp.startDate = moment(new Date(parseInt(ps)));
        }
        if(pe==null || pe=='') {
            $("#endDate").val(drp.endDate._d.getTime());
        } else {
            drp.endDate = moment(moment(new Date(parseInt(pe))));
        }
        drp.updateElement();

        $('#dataTables-members').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'homeGuard',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propId", "value": $('#propertyList').val() } );
                aoData.push( { "name": "startDate", "value": $('#startDate').val() } );
                aoData.push( { "name": "endDate", "value": $('#endDate').val() } );
            },
            "aoColumns": [
                { "mData": "date", "sTitle":"Tanggal" },
                { "mData": "unitName", "sTitle":"Nama Unit" },
                { "mData": "checkDate", "sTitle":"Waktu Patroli" },
                { "mData": "checkBy", "sTitle":"Patroli Keamanan" },
                { "mData": "img", "sTitle":"Gambar Diupload", "mRender":
                        function(data, type, full){
                            if(full.imageFileId!=null && full.imageFileId!="") {
                                return "<a href='#' onclick=showImage('" + full.imageFileId + "')>"+full.imageFileName+"</a>";
                            } else {
                                return "-";
                            }
                        }
                }
            ]
        });
    });

    function submitSearch(){
        $('#form1').attr("action",'${g.createLink(controller:'homeGuard',action:'index')}');
        $('#form1').submit();
    }

    function submitDownload(){
        $('#form1').attr("action",'${g.createLink(controller:'homeGuard',action:'downloadReport')}');
        $('#form1').submit();
    }


    function showImage(imageFileId){
        $('#imageContainer').html('<img src="${createLink(controller:'homeGuard',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
        $('#imageModal').modal('show');
    }

</script>
</body>
</html>
