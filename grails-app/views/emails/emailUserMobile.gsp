Hi,
<br/>
<br/>
Kami ingin menginformasikan bahwa ada registrasi user mobile yang menunggu persetujuan anda.
<br/> Berikut informasi tentang mereka:
<br/> Email: ${userMobile.email}
<br/> Nama: ${userMobile.name}
<br/> No Telepon: ${userMobile.phoneNo}
<br/> Nama Unit: ${userMobile.unitName}
<br/> Nama Area: ${userMobile.areaName}
<br/>

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

<hr>

Hi,
<br/>
<br/>
We would like to notify you that there is new user mobile registration pending for your approval
<br/> Below is the basic information about them:
<br/> Email: ${userMobile.email}
<br/> Name: ${userMobile.name}
<br/> Phone No: ${userMobile.phoneNo}
<br/> Unit Name: ${userMobile.unitName}
<br/> Area Name: ${userMobile.areaName}
<br/>

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

