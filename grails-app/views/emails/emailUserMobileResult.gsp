Hi,
<br/>
<br/>
 Kami ingin menginformasikan bahwa registrasi user mobile anda sudah disetujui.
<br/> Anda bisa langsung login, menggunakan autentikasi yang sudah anda daftarkan sebelumnya.
<br/> Berikut informasi tentang anda:
<br/> Email: ${userMobile.email}
<br/> Nama: ${userMobile.name}
<br/> No Telepon: ${userMobile.phoneNo}
<br/> Nama Unit: ${userMobile.unitName}
<br/> Nama Area: ${userMobile.areaName}
<br/>

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

<hr>

Hi,
<br/>
<br/>
 We would like to notify you that your user mobile registration request is approve.
<br/> You can directly login using authentication that you register before.
<br/> Below is the basic information about you:
<br/> Email: ${userMobile.email}
<br/> Name: ${userMobile.name}
<br/> Phone No: ${userMobile.phoneNo}
<br/> Unit Name: ${userMobile.unitName}
<br/> Area Name: ${userMobile.areaName}
<br/>

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

