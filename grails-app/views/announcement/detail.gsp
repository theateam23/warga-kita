<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Announcement</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Property Announcement" description="Publish and Edit Announcement" />
        <ol class="breadcrumb">
            <li>Information</li>
            <li><g:link controller="announcement" action="index"><i class="fa fa-info"></i> Announcement</g:link></li>
            <li class="active">Detail Announcement</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${announcement.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${announcement.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Periode Waktu:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${announcement.periodDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Target:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <g:each in="${targetNameList}" var="obj">
                                    ${obj}<br/>
                                </g:each>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${announcement.imageFileId}')">${announcement.imageFileName}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="announcement" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="announcement" action="edit" params="[id:announcement.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="announcement" action="delete" method="POST">
        <input type="hidden" name="id" value="${announcement.id}" />
    </g:form>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'announcement',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus pengumuman ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>