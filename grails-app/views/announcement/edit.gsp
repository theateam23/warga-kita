<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Edit Announcement</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Property Announcement" description="Publish and Edit Announcement" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="announcement" action="index"><i class="fa fa-info"></i> Announcement</g:link></li>
            <li class="active">Edit Announcement</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <div class="form-horizontal">
                <div class="box-body">
                    <g:form action="confirmEdit" method="POST" name="announcementForm" onsubmit="return false">
                        <property:alert message="${flash.message}" type="success"/>
                        <property:alert message="${flash.error}" type="danger"/>
                        <input type="hidden" value="${announcement.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${announcement.imageFileName}" name="imageFileName" id="imageFileName" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${announcement.propName}" />
                                <p class="form-control-static">${announcement.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfTitle" class="col-sm-2 control-label">Judul:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="${announcement.id}" />
                                <input type="text" name="title" class="form-control" id="tfTitle" maxlength="200" placeholder="Title" value="${announcement.title}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Periode Waktu:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="periodDate" />
                                    <input type="hidden" name="periodStart" id="periodStart" value="${announcement.periodStart}"/>
                                    <input type="hidden" name="periodEnd" id="periodEnd" value="${announcement.periodEnd}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Target:</label>
                            <div class="col-sm-8">
                                <g:each in="${roleList}" var="role">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="target" value="${role.id}" ${announcement.target?.contains(role.id)?"checked":""}>
                                            ${role.name}
                                        </label>
                                    </div>
                                </g:each>
                                <div id="target-error-msg"></div>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport" accept="image/*">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${announcement.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Recommended image resolution: 475 x 144, maximum file size 2 MB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="announcement" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
                </div>
            </div>
        </div>
    </section>

    <script>
        var fileUploaded = ${announcement.imageFileId==null || announcement.imageFileId==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${announcement.imageFileId==null || announcement.imageFileId==''?"false":"true"};

        $(document).ready(function(){
            $('#periodDate').daterangepicker({
                "locale": {
                    "format": "YYYY-MM-DD",
                    "separator": "  to  "
                },
                startDate: moment(),
                endDate: moment().add(6, 'days')
            }, function(start, end, label) {
                $("#periodStart").val(start._d.getTime());
                $("#periodEnd").val(end._d.getTime());
            });

            var ps = $('#periodStart').val();
            var pe = $('#periodEnd').val();
            var drp = $("#periodDate").data('daterangepicker');

            if(ps==null || ps==''){
                $("#periodStart").val(drp.startDate._d.getTime());
            } else {
                drp.startDate = moment(new Date(parseInt(ps)));
            }
            if(pe==null || pe=='') {
                $("#periodEnd").val(drp.endDate._d.getTime());
            } else {
                drp.endDate = moment(moment(new Date(parseInt(pe))));
            }
            drp.updateElement();

            $("#announcementForm").validate({
                rules: {
                    'target': {
                        required: true
                    }
                },
                messages: {
                    'target': {
                        required: "This field is required"
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "target" ){
                        $('#target-error-msg').html('');
                        error.appendTo('#target-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "target" ){
                        $('#target-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });
            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            if($('#announcementForm').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['announcementForm'].submit();
                } else {
                    vex.dialog.alert({
                        message: "You have to upload an image file."
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
