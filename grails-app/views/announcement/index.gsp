<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Announcement Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Property Announcement" description="Publish and Edit Announcement" />
    <ol class="breadcrumb">
        <li>Information</li>
        <li class="active"><i class="fa fa-info"></i> Announcement</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Announcement</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="announcement" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Announcement</g:link>

            <table class="table table-striped table-bordered table-hover" id="dataTables-announcement" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="imageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Image</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12" id="imageContainer">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="clear:both;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var detailUrl = '${g.createLink(controller:'announcement',action:'detail')}';
    var showImageUrl = '${g.createLink(controller:'announcement',action:'showImage')}';

    $(document).ready(function(){
        $('#dataTables-announcement').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'announcement',action:'listDatatable')}',
            "columnDefs": [
                { "orderable": false, "targets": 4 },
                { "orderable": false, "targets": 5 }
            ],
            "aoColumns": [
                { "mData": "periodStart", "sTitle":"Awal Periode", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.periodStart+"</a>";
                        }
                },
                { "mData": "periodEnd", "sTitle":"Akhir Periode" },
                { "mData": "propName", "sTitle":"Properti" },
                { "mData": "title", "sTitle":"Judul", "sWidth": "20%"},
                { "mData": "target", "sTitle":"Target", "sWidth": "15%"},
                { "mData": "imageFileId", "sTitle":"Gambar", "sWidth": "15%", "mRender":
                        function(data, type, full){
                            if(full.imageFileId!=null && full.imageFileId!=''){
                                return "<a href='#' onclick=showImage('" + full.imageFileId + "')>"+full.imageFileName+"</a>";
                            } else {
                                return "-";
                            }
                        }
                },
                { "mData": "createdDate", "sTitle":"Tanggal Dibuat" }
            ]
        });
    });

    function showImage(imageFileId){
        $('#imageContainer').html('<img src="${createLink(controller:'announcement',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
        $('#imageModal').modal('show');
    }

</script>
</body>
</html>