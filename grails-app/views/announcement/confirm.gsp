<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${announcement.stage} ${announcement.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Property Announcement" description="Publish and Edit Announcement" />
        <ol class="breadcrumb">
            <li>Information</li>
            <li><g:link controller="announcement" action="index"><i class="fa fa-info"></i> Announcement</g:link></li>
            <li>${announcement.action}</li>
            <li class="active">${announcement.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${announcement.stage}</h3>
            </div>
            <g:form action="${announcement.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${announcement.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${announcement.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propId" value="${announcement.propId}" />
                            <p class="form-control-static">${announcement.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${announcement.id}" />
                            <input type="hidden" name="title" value="${announcement.title}" />
                            <p class="form-control-static">${announcement.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Periode Waktu:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="periodStart" value="${announcement.periodStart}" />
                            <input type="hidden" name="periodEnd" value="${announcement.periodEnd}" />
                            <p class="form-control-static">${announcement.periodDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Target:</label>
                        <div class="col-sm-8">
                            <g:each in="${announcement.target}" var="obj">
                                <input type="hidden" name="target" value="${obj}" />
                            </g:each>
                            <p class="form-control-static">
                                <g:each in="${targetNameList}" var="obj">
                                    ${obj}<br/>
                                </g:each>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="imageFileId" value="${announcement.imageFileId}" />
                            <input type="hidden" name="imageFileName" value="${announcement.imageFileName}" />
                            <g:if test="${announcement.stage == 'Confirm'}">
                                <p class="form-control-static">${announcement.imageFileName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${announcement.imageFileId}')">${announcement.imageFileName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${announcement.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" class="btn btn-info pull-right" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });">Submit</button>
                    </g:if>
                    <g:else>
                        <g:link controller="announcement" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'announcement',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>