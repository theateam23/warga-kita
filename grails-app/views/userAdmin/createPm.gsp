<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Tambah Property Manager</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Manager" description="Tambag dan Edit Properti Manager" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i> Manage User</li>
            <li><g:link controller="userAdmin" action="index"> Property Manager</g:link></li>
            <li class="active">Create Property Manager</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Properti Manager</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label for="tfUsername" class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="email" maxlength="50" name="username" class="form-control" id="tfUsername" placeholder="Email" value="${user.username}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" maxlength="200" name="name" class="form-control" id="tfName" placeholder="Name" value="${user.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfPhoneNo" class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <input type="number" maxlength="20" name="phoneNo" class="form-control" id="tfPhoneNo" placeholder="Phone No" value="${user.phoneNo}">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="userAdmin" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $("#form1").validate({
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>
</body>
</html>