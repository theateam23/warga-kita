<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Property Manager</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Manager" description="Tambag dan Edit Properti Manager" />
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Manage Property Manager</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Properti Manager</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <g:link controller="userAdmin" action="createPm" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Property Manager</g:link>

                <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%">

                </table>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </section>

    <script>
        var detailUserUrl = '${g.createLink(controller:'userAdmin',action:'detailUser')}';

        $(document).ready(function(){
            $('#dataTables-users').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bFilter": false,
                "bServerSide": true,
                "bProcessing": true,
                "sPaginationType": "simple_numbers",
                "sAjaxSource": '${g.createLink(controller:'userAdmin',action:'pmListDatatable')}',
                "fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "more_data", "value": "my_value" } );
                },
                "aoColumns": [
                    { "mData": "username", "sTitle":"Email", "mRender":
                            function(data, type, full){
                                return "<a href='"+detailUserUrl+"/"+full.id+"'>"+full.username+"</a>";
                            }},
                    { "mData": "name", "sTitle":"Nama" },
                    { "mData": "phoneNo", "sTitle":"No Telepon" }
                ]
            });
        });
    </script>
</body>
</html>