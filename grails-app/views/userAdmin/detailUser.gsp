<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Property Manager</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Manager" description="Tambag dan Edit Properti Manager" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i> Manage User</li>
            <li><g:link controller="userAdmin" action="index"> Property Manager</g:link></li>
            <li class="active">Detail Property Manager</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.username}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.phoneNo}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-header with-border">
                <h3 class="box-title">Property Detail</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="dataTables-property" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Developer</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${propertyList}" var="obj">
                            <tr>
                                <td>${obj.name}</td>
                                <td>${obj.developer}</td>
                                <td>${obj.address}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <g:link controller="userAdmin" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="userAdmin" action="editPm" params="[id:user.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <g:link controller="userAdmin" action="resetPassword" params="[id:user.id]" class="btn btn-warning"><i class="fa fa-save"></i> Reset Password</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="userAdmin" action="deletePm" method="POST">
        <input type="hidden" name="id" value="${user.id}" />
    </g:form>

    <script>
        $(document).ready(function(){
            $('#dataTables-property').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false
            });
        });

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus user ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>