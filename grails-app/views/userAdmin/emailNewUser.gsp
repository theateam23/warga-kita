Hi, ${userName}
<br/>
<br/>
Selamat Datang di Warga Kita! Kami sudah membuatkan akun untuk anda.
<br/> Silahkan gunakan informasi ini untuk login di website kami:
<br/> Username: your email
<br/> Password: ${password}
<br/>
<br/> Anda bisa mengubah password anda setelah anda login di <a href="https://www.warga-kita.com">website</a>  kami.

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

<hr>

Hi, ${userName}
<br/>
<br/>
Welcome to Warga Kita! We have created a new account for you.
<br/> Please use this information to sign in to our website:
<br/> Username: your email
<br/> Password: ${password}
<br/>
<br/> You can change your password once you sign in to our <a href="https://www.warga-kita.com">website</a>.

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

