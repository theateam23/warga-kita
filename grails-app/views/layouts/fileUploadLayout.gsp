<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/08/2015
  Time: 16:17
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:layoutTitle default="Welcome"/></title>

    <asset:stylesheet src="blueimp-fileupload/jquery.fileupload.css"/>
    <asset:stylesheet src="blueimp-fileupload/jquery.fileupload-ui.css"/>
    <g:layoutHead/>
</head>

<body>
    <g:pageProperty name="page.fileUploadContent"/>

    <asset:javascript src="blueimp-fileupload/vendor/jquery.ui.widget.js"/>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <asset:javascript src="blueimp-fileupload/jquery.iframe-transport.js"/>
    <!-- The basic File Upload plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload.js"/>
    <!-- The File Upload processing plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-process.js"/>
    <!-- The File Upload image preview & resize plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-image.js"/>
    <!-- The File Upload audio preview plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-audio.js"/>
    <!-- The File Upload video preview plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-video.js"/>
    <!-- The File Upload validation plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-validate.js"/>
    <!-- The File Upload user interface plugin -->
    <asset:javascript src="blueimp-fileupload/jquery.fileupload-ui.js"/>

</body>
</html>