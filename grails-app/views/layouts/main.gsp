<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="google-site-verification" content="CenNB8DvXA3dsR7EojDQwXMGi64CL9_NJpNynqo1dGo" />
    <title><g:layoutTitle default="Warga Kita"/></title>
    <asset:link rel="icon" href="wargaku_logo_only.png" type="image/x-ico" />

    <asset:stylesheet src="bootstrap/bootstrap.min.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <asset:stylesheet src="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"/>
    <asset:stylesheet src="adminlte/skin-purple-light.css"/>
    <asset:stylesheet src="datatables/dataTables.bootstrap.min.css"/>
    <asset:stylesheet src="select2/select2.min.css"/>
    <asset:stylesheet src="vex/vex.css"/>
    <asset:stylesheet src="vex/vex-theme-os.css"/>
    <asset:stylesheet src="daterangepicker/daterangepicker.css"/>
    <asset:stylesheet src="adminlte/AdminLTE.css"/>
    <asset:stylesheet src="custom.css"/>
    <asset:javascript src="jquery-3.3.1.min.js"/>
    <asset:javascript src="bootstrap/bootstrap.min.js"/>
    <asset:javascript src="jquery.blockUI.js"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <g:layoutHead/>
</head>

<body class="hold-transition skin-purple-light layout-boxed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <g:link controller="dashboard" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <asset:image src="wargakita_logo_box.png" alt="User Image" style="max-height:50px;padding-right: 5px; padding-bottom: 5px"/>
            </span>
        </g:link>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <sec:ifAnyGranted roles='ROLE_PM,ROLE_PA'>
                        <!-- Tasks Menu -->
                        <li class="dropdown tasks-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag-o"></i>
                                <span class="label label-danger taskCount" id="totalCount1">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have <span class="taskCount" id="totalCount2"></span> approval waiting.</li>
                                <li class="footer">
                                    <g:link controller="userApproval">Tampilkan semua <span class="taskCount" id="userCount"></span> permintaan user mobile</g:link>
                                </li>
                                <li class="footer" id="facilityCountBox">
                                    <g:link controller="manageFacilityBooking">Tampilkan semua <span class="taskCount" id="bookingCount"></span> permintaan pesan fasilitas</g:link>
                                </li>
                            </ul>
                        </li>
                    </sec:ifAnyGranted>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="${createLink(controller:'profile',action: 'profilePic')}" class="user-image" alt="User Image" />
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${sec.loggedInUserInfo(field: 'fullName')}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="${createLink(controller:'profile',action: 'profilePic')}" class="img-circle" alt="User Image" />
                                <p>
                                    ${sec.loggedInUserInfo(field: 'fullName')} - ${sec.loggedInUserInfo(field: 'roleName')}
                                    <small>Member since ${sec.loggedInUserInfo(field: 'createdDate')}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            %{--<li class="user-body">--}%
                                %{--<div class="row">--}%
                                    %{--<div class="col-xs-4 text-center">--}%
                                        %{--<a href="#">-</a>--}%
                                    %{--</div>--}%
                                    %{--<div class="col-xs-4 text-center">--}%

                                    %{--</div>--}%
                                    %{--<div class="col-xs-4 text-center">--}%
                                        %{--<a href="#">-</a>--}%
                                    %{--</div>--}%
                                %{--</div>--}%
                                %{--<!-- /.row -->--}%
                            %{--</li>--}%
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <g:link controller="profile" class="btn btn-default btn-flat">Profil</g:link>
                                </div>
                                <div style="float:left;margin:0 20px">
                                    <g:link controller="profile" action="changePassword" class="btn btn-default btn-flat">Ganti Password</g:link>
                                </div>
                                <div class="pull-right">
                                    <g:link controller="logout" class="btn btn-default btn-flat">Sign out</g:link>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="${createLink(controller:'profile',action: 'profilePic')}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>${sec.loggedInUserInfo(field: 'fullName')}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> ${sec.loggedInUserInfo(field: 'roleName')}</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            %{--<form action="#" method="get" class="sidebar-form">--}%
                %{--<div class="input-group">--}%
                    %{--<input type="text" name="q" class="form-control" placeholder="Cari ...">--}%
                    %{--<span class="input-group-btn">--}%
                        %{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}%
                        %{--</button>--}%
                    %{--</span>--}%
                %{--</div>--}%
            %{--</form>--}%
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Daftar Menu</li>
                <!-- Optionally, you can add icons to the links -->
                <property:menus/>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <g:layoutBody/>
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Warga Kita
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="#">Equalsmark</a>.</strong> All rights reserved.
    </footer>

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<asset:javascript src="adminlte/adminlte.min.js"/>
<asset:javascript src="datatables/datatables.min.js"/>
<asset:javascript src="datatables/dataTables.bootstrap.min.js"/>
<asset:javascript src="vex/vex.combined.min.js"/>
<asset:javascript src="select2/select2.min.js"/>
<asset:javascript src="validator/jquery.validate.min.js"/>
<asset:javascript src="validator/additional-methods.min.js"/>
<asset:javascript src="main.js"/>
<asset:javascript src="daterangepicker/moment.min.js"/>
<asset:javascript src="daterangepicker/daterangepicker.js"/>

<script>
    vex.defaultOptions.className = 'vex-theme-os';
    <sec:ifAnyGranted roles='ROLE_PM,ROLE_PA'>
    var getTaskCountUrl = '${g.createLink(controller:'profile',action:'getTaskCount')}';

    $(document).ready(function(){
        $.ajax({
            type: 'GET',
            url: getTaskCountUrl,
            success: (function (output) {
                if (output.result) {
                    $('#totalCount1').html(output.totalCount);
                    $('#totalCount2').html(output.totalCount);
                    $('#userCount').html(output.userCount);
                    if(output.hasFacility) {
                        $('#bookingCount').html(output.bookingCount);
                    } else {
                        $('#facilityCountBox').hide();
                    }
                } else {
                    $('#totalCount1').html("0");
                    $('#totalCount2').html("0");
                    $('#userCount').html("0");
                    $('#bookingCount').html("0");
                }
            }),
            error: (function (data) {
                $('#totalCount1').html("0");
                $('#totalCount2').html("0");
                $('#userCount').html("0");
                $('#bookingCount').html("0");
            }),
            complete: (function (data) {
            })
        });
    });
    </sec:ifAnyGranted>

    $(window).on("unload", function(e) {
        $.unblockUI();
    });
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>