<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="ionicons/css/ionicons.min.css" />
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Halaman Utama" description="" />
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-body">
                <div class="row">
                    <g:each in="${bulletList}" var="obj">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-${obj.color}">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="${obj.icon}"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" ${obj.fontSize?"style=font-size:${obj.fontSize}":""}>${obj.count}</div>
                                            <div>${obj.description}</div>
                                        </div>
                                    </div>
                                </div>
                                <g:if test="${obj.controller!=null}">
                                    <g:link controller="${obj.controller}" action="${obj.action}">
                                        <div class="panel-footer">
                                            <span class="pull-left">${obj.linkText}</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </g:link>
                                </g:if>
                                <g:else>
                                    <div class="panel-footer">
                                        <span class="pull-left">&nbsp;</span>
                                        <div class="clearfix"></div>
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </g:each>
                </div>
            </div>
        </div>
    </section>
</body>
</html>