<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Tambah Property Admin</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Admin" description="Pengaturan Properti Admin" />
        <ol class="breadcrumb">
            <li>User Management</li>
            <li><g:link controller="userPa" action="index"><i class="fa fa-users"></i> Property Admin</g:link></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Properti Admin</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label for="tfUsername" class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="email" name="username" class="form-control" id="tfUsername" maxlength="200" placeholder="Email" value="${user.username}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${user.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfPhoneNo" class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <input type="number" name="phoneNo" class="form-control" id="tfPhoneNo" maxlength="200" placeholder="Phone No" value="${user.phoneNo}">
                        </div>
                    </div>
                    %{--<div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${user.propName}" />
                            <p class="form-control-static">${user.propName}</p>
                        </div>
                    </div>--}%
                </div>
                <div class="box-footer">
                    <g:link controller="userPa" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#form1').validate({
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>
</body>
</html>