<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Property Admin</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Property Manager" description="Manage Property Admin" />
        <ol class="breadcrumb">
            <li>User Management</li>
            <li><g:link controller="userPa" action="index"><i class="fa fa-users"></i> Property Admin</g:link></li>
            <li class="active">Detail</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.username}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.phoneNo}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <g:each in="${userPropertyList}" var="obj">
                                    ${obj}<br/>
                                </g:each>
                            </p>
                        </div>
                    </div>--}%
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="userPa" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="userPa" action="editPa" params="[id:user.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <g:link controller="userPa" action="resetPassword" params="[id:user.id]" class="btn btn-warning"><i class="fa fa-save"></i> Reset Password</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="userPa" action="deletePa" method="POST">
        <input type="hidden" name="id" value="${user.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data user ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>