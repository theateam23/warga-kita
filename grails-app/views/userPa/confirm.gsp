<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${user.stage} ${user.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Admin" description="Pengaturan Properti Admin" />
        <ol class="breadcrumb">
            <li>User Management</li>
            <li><g:link controller="userPa" action="index"><i class="fa fa-users"></i> Property Admin</g:link></li>
            <li>${user.action}</li>
            <li class="active">${user.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${user.stage}</h3>
            </div>
            <g:form action="${user.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${user.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${user.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${user.id}" />
                            <input type="hidden" name="username" value="${user.username}" />
                            <p class="form-control-static">${user.username}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="name" value="${user.name}" />
                            <p class="form-control-static">${user.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="phoneNo" value="${user.phoneNo}" />
                            <p class="form-control-static">${user.phoneNo}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${user.propName}" />
                            <p class="form-control-static">${user.propName}</p>
                        </div>
                    </div>--}%
                </div>
                <div class="box-footer">
                    <g:if test="${user.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="userPa" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>