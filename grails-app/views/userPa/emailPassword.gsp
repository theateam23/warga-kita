Hi, ${userName}
<br/>
<br/>
Anda (atau orang berpura-pura menjadi anda) meminta mereset password.
<br/> Abaikan email ini, jika anda tidak melakukannya; Tidak ada perubahan yang terjadi.
<br/> Jika anda yang melakukannya, maka klik <a href="${tokenLink}">here</a> untuk mereset password anda.

<br/>
<br/>
Regards,<br/>

Warga Kita Admin

<hr>

Hi, ${userName}
<br/>
<br/>
You (or someone pretending to be you) requested tht your password be reset.
<br/> If you didn't make this request then ignore the email; no changes have been made.
<br/> If you did make the request, then click <a href="${tokenLink}">here</a>  to reset your password.

<br/>
<br/>
Regards,<br/>

Warga Kita Admin