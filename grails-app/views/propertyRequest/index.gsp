<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Request Form</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Surat Keterangan" description="Tambah dan Edit Surat Keterangan" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-clipboard"></i> Description Form</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Surat Keterangan</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="propertyRequest" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Surat Keterangan</g:link>
            </div>

            <br style="clear:both"/>

            %{--<div class="form-horizontal col-md-7">--}%
                %{--<g:form action="index" method="GET" style="margin-bottom: 20px;" name="form1">--}%
                    %{--<div class="form-group">--}%
                        %{--<label for="propertyList" class="col-sm-3 control-label">Properti:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${propName}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Submit Tanggal:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<div class="input-group">--}%
                                %{--<div class="input-group-addon">--}%
                                    %{--<i class="fa fa-calendar"></i>--}%
                                %{--</div>--}%
                                %{--<input type="text" class="form-control pull-right" id="submitDate" />--}%
                                %{--<input type="hidden" name="startDate" id="startDate" value="${startDate}"/>--}%
                                %{--<input type="hidden" name="endDate" id="endDate" value="${endDate}"/>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                %{--</g:form>--}%

                %{--<div class="form-group">--}%
                    %{--<div class="pull-right" id="downloadDiv">--}%
                        %{--<button type="button" onclick="submitSearch()" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>--}%
                    %{--</div>--}%
                %{--</div>--}%
            %{--</div>--}%

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-requests" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<g:form name="formDelete" controller="propertyRequest" action="delete" method="POST">
</g:form>

<script>
    var detailUrl = '${g.createLink(controller:'propertyRequest',action:'detail')}';

    $(document).ready(function(){
//        $('#submitDate').daterangepicker({
//            "locale": {
//                "format": "YYYY-MM-DD",
//                "separator": "  to  "
//            },
//            startDate: moment(),
//            endDate: moment()
//        }, function(start, end, label) {
//            $("#startDate").val(start._d.getTime());
//            $("#endDate").val(end._d.getTime());
//        });
//
//        var ps = $('#startDate').val();
//        var pe = $('#endDate').val();
//        var drp = $("#submitDate").data('daterangepicker');
//
//        if(ps==null || ps==''){
//            $("#startDate").val(drp.startDate._d.getTime());
//        } else {
//            drp.startDate = moment(new Date(parseInt(ps)));
//        }
//        if(pe==null || pe=='') {
//            $("#endDate").val(drp.endDate._d.getTime());
//        } else {
//            drp.endDate = moment(moment(new Date(parseInt(pe))));
//        }
//        drp.updateElement();
        $('#dataTables-requests').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'propertyRequest',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propId", "value": $('#propertyList').val() } );
                aoData.push( { "name": "startDate", "value": $('#startDate').val() } );
                aoData.push( { "name": "endDate", "value": $('#endDate').val() } );
            },
            "aoColumns": [
                { "mData": "date", "sTitle":"Tanggal Dibuat", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.date+"</a>";
                        }
                },
//                { "mData": "property", "sTitle":"Properti" },
                { "mData": "title", "sTitle":"Judul" },
//                { "mData": "description", "sTitle":"Keterangan" },
                { "mData": "fileName", "sTitle":"Nama File" }
            ]
        });
    });

    function confirmDelete(id){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk menghapus surat keterangan ini?',
            callback: function (value) {
                if (value) {
                    $('#formDelete').html('<input type="hidden" value="'+id+'" name="id" />');
                    $('#formDelete').submit();
                }
            }
        });
    }

    function submitSearch(){
        $('#form1').attr("action",'${g.createLink(controller:'propertyRequest',action:'index')}');
        $('#form1').submit();
    }

</script>
</body>
</html>