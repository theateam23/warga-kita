<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Request Form</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Surat Keterangan" description="Tambah dan Edit Surat Keterangan" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="propertyRequest" action="index"><i class="fa fa-clipboard"></i> Description Form</g:link></li>
            <li class="active">Detail Request Form</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Request Form:</label>
                        <div class="col-sm-8" style="padding-top: 7px">
                            <g:if test="${detail.fileId!=null && detail.fileId!=""}">
                                <g:link action="downloadFile" id="${detail.fileId}">${detail.fileName}</g:link>
                            </g:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="propertyRequest" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="propertyRequest" action="edit" params="[id:detail.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="propertyRequest" action="delete" method="POST">
        <input type="hidden" name="id" value="${detail.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus surat keterangan ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>