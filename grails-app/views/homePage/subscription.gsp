<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Warga Kita</title>
    <asset:link rel="icon" href="wargaku_logo_only.png" type="image/x-ico" />

    <asset:javascript src="jquery-3.3.1.min.js"/>
    <asset:stylesheet src="homepage/style.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>
<body>
<div class="content">
    <div id="header-wrapper" class="form-wrapper">
        <div id="navbar">
            <div id="logo"></div>
            <ul class="nav-items">
                <li class="nav-item">
                    <g:link action="index">Halaman Utama</g:link>
                </li>
                <li class="nav-item active">
                    <g:link action="subscription">Minat Berlangganan</g:link>
                </li>
                <li class="nav-item">
                    <g:link action="reseller">Peluang Reseller</g:link>
                </li>
                <li class="nav-item">
                    <sec:ifLoggedIn>
                        <g:link condivoller="dashboard">Dashboard</g:link>
                    </sec:ifLoggedIn>
                    <sec:ifNotLoggedIn>
                        <g:link condivoller="dashboard">Sign In</g:link>
                    </sec:ifNotLoggedIn>
                </li>
            </ul>
        </div>
        <div id="banner" class="form-banner">
            <div id="formsubscription" class="sections" style="background-image: url(${assetPath(src: 'homepage/characters_bg.png')});background-size:cover">
                <div style="clear:both;padding: 50px 8%">
                    <g:form action="submitSubscription">
                        <div class="form-table">
                            <div class="form-group">
                                <div class="label-col"><label>Nama Lengkap:</label></div>
                                <div class="input-col"><input class="input-text" type="text" name="name" maxlength="200"/></div>
                            </div>
                            <div class="form-group">
                                <div class="label-col"><label>Email:</label></div>
                                <div class="input-col"><input class="input-text" type="text" name="email" maxlength="200"/></div>
                            </div>
                            <div class="form-group">
                                <div class="label-col"><label>No Handphone:</label></div>
                                <div class="input-col"><input class="input-text" type="text" name="phoneNo" maxlength="200"/></div>
                            </div>
                            <div class="form-group">
                                <div class="label-col"><label>Nama Properti:</label></div>
                                <div class="input-col"><input class="input-text" type="text" name="property" maxlength="200"/></div>
                            </div>
                            <div class="form-group">
                                <div class="label-col"><label>Keperluan:</label></div>
                                <div class="input-col"><textarea class="input-text" name="reason" maxlength="1000" rows="5" style="resize: none"></textarea></div>
                            </div>
                            <div class="form-group">
                                <div class="label-col"><label></label></div>
                                <div class="input-col"><button class="btn btn-submit" type="submit">Kirim</button></div>
                            </div>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
    <div id="footer" class="form-footer">
        <div class="footer-item">
            <p>Tujuan Kami:</p>
            <p><i class="fa fa-paper-plane"></i> Kami ingin membantu warga untuk lebih mendapatkan rasa aman, nyaman dan komunikatif ketika berada di lingkungannya sendiri.</p>
        </div>
        <div class="footer-item">
            <p>Ikuti Kami:</p>
            <p><i class="fa fa-facebook-square"></i> Like Facebook Kami</p>
            <p><i class="fa fa-instagram"></i> Love Instagram Kami</p>
            <p><i class="fa fa-twitter"></i> Follow Twitter Kami</p>
        </div>
        <div class="footer-item">
            <p>Alamat Kami:</p>
            <p><i class="fa fa-map-marker"></i> &nbsp;Equalsmark Team, Sawangan Depok, Jawa Barat, 16510</p>
            <p><i class="fa fa-phone"></i> +62 856-1035-388</p>
            <p><i class="fa fa-envelope"></i> support@warga-kita.com</p>
        </div>
    </div>
    <div id="copyright">
        Copyright © 2018 Equalsmark. All rights reserved.
    </div>
</div>

<script>
    var msg = "${flash.message?:""}";
    $(document).ready(function(){
        if(msg!=""){
            alert(msg);
        }
    });

</script>

</body>
</html>
