<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="CenNB8DvXA3dsR7EojDQwXMGi64CL9_NJpNynqo1dGo" />
    <title>Warga Kita</title>
    <asset:link rel="icon" href="wargaku_logo_only.png" type="image/x-ico" />

    <asset:stylesheet src="homepage/style.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>
<body>
<div class="content">
    <div id="header-wrapper">
        <div id="navbar">
            <div id="logo"></div>
            <ul class="nav-items">
                <li class="nav-item active">
                    <g:link action="index">Halaman Utama</g:link>
                </li>
                <li class="nav-item">
                    <g:link action="subscription">Minat Berlangganan</g:link>
                </li>
                <li class="nav-item">
                    <g:link action="reseller">Peluang Reseller</g:link>
                </li>
                <li class="nav-item">
                    <sec:ifLoggedIn>
                        <g:link controller="dashboard">Dashboard</g:link>
                    </sec:ifLoggedIn>
                    <sec:ifNotLoggedIn>
                        <g:link controller="dashboard">Sign In</g:link>
                    </sec:ifNotLoggedIn>
                </li>
            </ul>
        </div>
        <div id="banner">
            <div class="promotional">
                <p>Aplikasi warga, perumahan, dan apartemen yang membantu dan mengamankan lingkungan anda, dengan memberikan kemudahan akses antara warga yang satu dengan lainnya.</p>
                <div class="btn-div">
                    <a href="https://play.google.com/store/apps/details?id=com.equalsmark.wargaku" class="btn-gplay"></a>
                    <a href="https://itunes.apple.com/us/app/warga-kita/id1415444023" class="btn-apstr"></a>
                </div>
            </div>
            <asset:image src="homepage/content_apps_login.png" alt="" id="content-app-img" />
        </div>
    </div>
    <div class="item-section">
        <div class="item-image">
            <h1>Produk dan Fitur</h1>
            <asset:image src="homepage/content_apps_communication.png" alt="" />
        </div>
        <div class="item-text">
            <h2>Komunikasi</h2>
            <p>
                Dengan adanya fitur "sebar info", akan memudahkan pihak terkait dalam menginformasikan berita terbaru yang terkait dengan lingkungan anda, bahkan fitur ini juga bisa dipakai untuk mengundang sekaligus mengingatkan warga untuk hadir.
            </p>
            <h2>Formulir</h2>
            <p>
                Memudahkan warga untuk meminta dan mencari tahu informasi yang dibutuhkan dalam meminta surat keterangan, seperti Surat Domisili, Surat Ijin Usaha,  ataupun surat keterangan yang lainnya.
            </p>
            <h2>Kritik dan Saran</h2>
            <p>
                Memberikan saran dan kritikan kepada pihak terkait secara cepat melalui aplikasi.
            </p>
        </div>
    </div>
    <div class="item-section">
        <div class="item-image">
            <h1>&#8203;</h1>
            <asset:image src="homepage/content_apps_information.png" alt="" />
        </div>
        <div class="item-text">
            <h2>Informasi</h2>
            <p>
                Memberikan kemudahan untuk mengakses informasi untuk dibutuhkan dari info tempat umum, kuliner, jasa, sewa, hobi, toko terdekat dan lainnya.
            </p>
            <h2>Keamanan</h2>
            <p>
                Melindungi anda dan anggota keluarga serta membantu dalam kondisi butuh bantuan dengan cara menyediakan 3 fitur
            <ul>
                <li>
                    <span>Tombol Darurat</span>
                    <p>Dalam keadaan darurat anda bisa langsung terhubung dengan pihak keamanan.</p>
                </li>
                <li>
                    <span>Daftar Penghuni</span>
                    <p>Dengan memberikan data akurat orang-orang yang tinggal, memudahkan pihak keamanan untuk mengantisipasi hal-hal buruk.</p>
                </li>
                <li>
                    <span>Daftar Kendaraan</span>
                    <p>Dengan memberikan informasi terkait kendaraan yang dipakai, juga akan memudahkan pihak keamanan untuk membatasi akses lingkungan anda.</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="item-section">
        <div class="item-image">
            <h1>&#8203;</h1>
            <asset:image src="homepage/content_apps_booking_facility.png" alt="" />
        </div>
        <div class="item-text">
            <h2>Pesan Fasilitas</h2>
            <p>
                Memudahkan warga untuk cek dan memesan fasilitas yang disediakan pihak terkait atau milik warga sendiri.
            </p>
            <h2>Daftar Pengunjung</h2>
            <p>
                Memberikan data dan informasi yang akurat untuk pihak keamanan dalam menangani pengunjung yang datang.
            </p>
            <h2>Jaga Rumah</h2>
            <p>
                Memberikan penjagaan unit secara maksimal karena data hanya akan bisa diakses oleh pihak yang berwenang saja.
            </p>
            <asset:image src="homepage/content_apps_footer.png" alt="" />
        </div>
    </div>
    <div class="item-section">
        <div class="item-image special-case">
            <asset:image src="homepage/content_pricelist.png" alt="" />
        </div>
        <div class="item-text special-case">
            <h2>Pilih dan sesuaikan kebutuhan berlangganan untuk properti anda</h2>
            <p style="padding: 40px;">
                Jangan ragu untuk kontak kami, jika anda ingin konsultasi atau ingin memiliki sistem sendiri dengan branding sendiri.
            </p>
            <div class="flavor">
                Gratis berlangganan untuk 1 bulan pertama.
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="footer-item">
            <p>Tujuan Kami:</p>
            <p><i class="fa fa-paper-plane"></i> Kami ingin membantu warga untuk lebih mendapatkan rasa aman, nyaman dan komunikatif ketika berada di lingkungannya sendiri.</p>
        </div>
        <div class="footer-item">
            <p>Ikuti Kami:</p>
            <p><i class="fa fa-facebook-square"></i> Like Facebook Kami</p>
            <p><i class="fa fa-instagram"></i> Love Instagram Kami</p>
            <p><i class="fa fa-twitter"></i> Follow Twitter Kami</p>
        </div>
        <div class="footer-item">
            <p>Alamat Kami:</p>
            <p><i class="fa fa-map-marker"></i> &nbsp;Equalsmark Team, Sawangan Depok, Jawa Barat, 16510</p>
            <p><i class="fa fa-phone"></i> +62 856-1035-388</p>
            <p><i class="fa fa-envelope"></i> support@warga-kita.com</p>
        </div>
    </div>
    <div id="copyright">
        Copyright © 2018 Equalsmark. All rights reserved.
    </div>
</div>
</body>
</html>
