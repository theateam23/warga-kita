<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${member.stage} ${member.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Member Maintenance" description="Tambah dan Edit Member" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="memberPm" action="index"><i class="fa fa-user"></i> Resident</g:link></li>
            <li>${member.action}</li>
            <li class="active">${member.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${member.stage}</h3>
            </div>
            <g:form action="${member.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${member.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${member.action}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${member.propName}" />
                            <p class="form-control-static">${member.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="areaId" value="${member.areaId}" />
                            <p class="form-control-static">${member.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="unitId" value="${member.unitId}" />
                            <p class="form-control-static">${member.unitName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${member.id}" />
                            <input type="hidden" name="name" value="${member.name}" />
                            <p class="form-control-static">${member.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Lahir:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="dateOfBirth" value="${member.dateOfBirth}" />
                            <p class="form-control-static">${member.dateOfBirth}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tempat Lahir:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="placeOfBirth" value="${member.placeOfBirth}" />
                            <p class="form-control-static">${member.placeOfBirth}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pekerjaan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="profession" value="${member.profession}" />
                            <p class="form-control-static">${member.profession}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID KTP:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="idKTP" value="${member.idKTP}" />
                            <p class="form-control-static">${member.idKTP}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="email" value="${member.email}" />
                            <p class="form-control-static">${member.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="address" value="${member.address}" />
                            <p class="form-control-static">${member.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="status" value="${member.status}" />
                            <p class="form-control-static">${member.status}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis Kelamin:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="genderId" value="${member.genderId}" />
                            <p class="form-control-static">${member.genderName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pendidikan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="education" value="${member.education}" />
                            <p class="form-control-static">${member.education}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hobi:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="hobby" value="${member.hobby}" />
                            <p class="form-control-static">${member.hobby}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="description" value="${member.description}" />
                            <p class="form-control-static">${member.description}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Relationship:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="hidden" name="relationship" value="${member.relationship}" />--}%
                            %{--<p class="form-control-static">${member.relationship}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="imageFileId" value="${member.imageFileId}" />
                            <input type="hidden" name="imageFileName" value="${member.imageFileName}" />
                            <g:if test="${member.stage == 'Confirm'}">
                                <p class="form-control-static">${member.imageFileName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${member.imageFileId}')">${member.imageFileName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${member.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="memberPm" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'memberPm',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>