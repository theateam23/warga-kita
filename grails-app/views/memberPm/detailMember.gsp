<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Member</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Member Maintenance" description="Tambah dan Edit Member" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="memberPm" action="index"><i class="fa fa-user"></i> Resident</g:link></li>
            <li class="active">Detail Member</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${member.id}" />
                            <p class="form-control-static">${member.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Area:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.unitName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Lahir:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.dateOfBirth}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tempat Lahir:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.placeOfBirth}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pekerjaan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.profession}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID KTP:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.idKTP}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.status}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis Kelamin:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.gender}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pendidikan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.education}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hobi:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.hobby}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${member.description}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Relationship:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${member.relationship}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${member.imageFileId}')">${member.imageFileName}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="memberPm" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="memberPm" action="editMember" params="[id:member.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="memberPm" action="deleteMember" method="POST">
        <input type="hidden" name="id" value="${member.id}" />
    </g:form>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'memberPm',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data penghuni ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>