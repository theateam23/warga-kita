<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Edit Member</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Laporan Penghuni" description="Tampilkan dan Download Data Penghuni" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="memberPm" action="index"><i class="fa fa-user"></i> Resident</g:link></li>
            <li class="active">Edit Member</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <div class="form-horizontal">
                <div class="box-body">
                    <g:form action="confirmEdit" method="POST" name="form1" onsubmit="return false">
                        <property:alert message="${flash.message}" type="success"/>
                        <property:alert message="${flash.error}" type="danger"/>
                        <input type="hidden" value="${member.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${member.imageFileName}" name="imageFileName" id="imageFileName" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="${member.id}" />
                                <input type="hidden" name="propName" value="${member.propName}" />
                                <p class="form-control-static">${member.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">area:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="areaName" value="${member.areaName}" />
                                <p class="form-control-static">${member.areaName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Unit:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="unitName" value="${member.unitName}" />
                                <p class="form-control-static">${member.unitName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${member.name}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Lahir:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="dateOfBirth" class="form-control" id="tfDateOfBirth" value="${member.dateOfBirth}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tempat Lahir:</label>
                            <div class="col-sm-8">
                                <input type="text" name="placeOfBirth" class="form-control" id="tfPlaceOfBirth" maxlength="200" placeholder="Place of Birth" value="${member.placeOfBirth}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pekerjaan:</label>
                            <div class="col-sm-8">
                                <input type="text" name="profession" class="form-control" id="tfProfession" maxlength="200" placeholder="Profession" value="${member.profession}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ID KTP:</label>
                            <div class="col-sm-8">
                                <input type="text" name="idKTP" class="form-control" id="tfIdKTP" maxlength="200" placeholder="KTP" value="${member.idKTP}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-8">
                                <input type="text" name="email" class="form-control" id="tfEmail" maxlength="200" placeholder="Email" value="${member.email}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alamat:</label>
                            <div class="col-sm-8">
                                <input type="text" name="address" class="form-control" id="tfAddress" maxlength="200" placeholder="Address" value="${member.address}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status:</label>
                            <div class="col-sm-8">
                                <g:select id="statusMemberList" name='status' value="${member.status}"
                                          from='${statusMemberList}' class="form-control select2"></g:select>
                                <div id="status-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Kelamin:</label>
                            <div class="col-sm-8">
                                <g:select id="genderList" name='genderId' value="${member.genderId}"
                                          from='${genderList}' class="form-control select2" optionKey="code" optionValue="name"></g:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pendidikan:</label>
                            <div class="col-sm-8">
                                <input type="text" name="education" class="form-control" id="tfEducation" maxlength="200" placeholder="Education" value="${member.education}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Hobi:</label>
                            <div class="col-sm-8">
                                <input type="text" name="hobby" class="form-control" id="tfHobby" maxlength="200" placeholder="Hobby" value="${member.hobby}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Keterangan:</label>
                            <div class="col-sm-8">
                                <input type="text" name="description" class="form-control" id="tfDescription" maxlength="200" placeholder="Description" value="${member.description}">
                            </div>
                        </div>
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-2 control-label">Relationship:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<g:select id="memberRelationList" name='relationship' value="${member.relationship}"--}%
                                          %{--from='${memberRelationList}' class="form-control select2" optionKey="code" optionValue="name"></g:select>--}%
                                %{--<div id="relationship-error-msg"></div>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="uploadImage">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport" accept="image/*">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${member.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="memberPm" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
                </div>
            </div>
        </div>
    </section>


    <script>
        var fileUploaded = ${member.imageFileId==null || member.imageFileId==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${member.imageFileId==null || member.imageFileId==''?"false":"true"};

        $(document).ready(function(){
            $("#memberForm").validate({
                rules: {
                    'status': {
                        required: true
                    },
                    'relationship': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "status" ){
                        $('#status-error-msg').html('');
                        error.appendTo('#status-error-msg');
                    } else if (element.attr("name") == "relationship" ){
                        $('#relationship-error-msg').html('');
                        error.appendTo('#relationship-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "status" ){
                        $('#status-error-msg').html('');
                    } else if ($(element).attr("name") == "relationship" ){
                        $('#relationship-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });

            $('#statusMemberList').select2();
            $('#memberRelationList').select2();
            $('#genderList').select2();

            $('#tfDateOfBirth').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "locale": {
                    "format": "YYYY-MM-DD"
                },
                autoUpdateInput: false
            }, function(chosen_date) {
                $('#tfDateOfBirth').val(chosen_date.format('YYYY-MM-DD'));
            });

            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            if($('#form1').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['form1'].submit();
                } else {
                    vex.dialog.confirm({
                        message: "You haven't uploaded any image file. Continue?",
                        callback: function (value) {
                            if (value) {
                                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                                document.forms['form1'].submit();
                            }
                        }
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
