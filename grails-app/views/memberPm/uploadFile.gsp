<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Upload File</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Laporan Penghuni" description="Tampilkan dan Download Data Penghuni" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="memberPm" action="index"><i class="fa fa-user"></i> Resident</g:link></li>
            <li class="active">Upload File</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="pull-right">
                    <g:link controller="memberPm" action="downloadTemplate" class="btn btn-primary"><i class="fa fa-download"></i> Download Template</g:link>
                </div>

                <br style="clear:both"/>

                <div class="form-horizontal">
                    <g:form action="confirmUpload" method="post" controller="memberPm" name="memberForm" onsubmit="return false">
                        <input type="hidden" value="${fileImportId}" name="fileImportId" id="fileImportId" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${propName}" />
                                <p class="form-control-static">${propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="areaList" class="col-sm-2 control-label">Area:</label>
                            <div class="col-sm-8">
                                <g:select id="areaList" name='areaId'
                                          from='${areaList}' class="form-control select2"
                                          optionKey="id" optionValue="name"></g:select>
                                <div id="area-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="areaList" class="col-sm-2 control-label">Unit:</label>
                            <div class="col-sm-8">
                                <select id="unitList" name='unitId' class="form-control select2"></select>
                                <div id="unitId-error-msg"></div>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">File Import:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult"></label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="memberPm" action="index" class="btn btn-default">Batal</g:link>
                <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Lanjutkan</button>
            </div>
        </div>
    </section>

    <script>
        var enableContinue = false;
        var f = null;
        var isErr = false;
        var isChangeFile = ${fileImportId==null || fileImportId==""?"false":"true"};
        var unitByAreaUrl = '${g.createLink(controller:'memberPm', action:'unitByAreaJSON')}';

        $(function () {
            $('#memberForm').validate({
                rules: {
                    'propId': {
                        required: true
                    },
                    'areaId': {
                        required: true
                    },
                    'unitId': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                        error.appendTo('#unitId-error-msg');
                    } else if (element.attr("name") == "areaId" ){
                        $('#area-error-msg').html('');
                        error.appendTo('#area-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                    } else if ($(element).attr("name") == "areaId" ){
                        $('#area-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });

            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                enableContinue = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        enableContinue = false;
                        $('#fileImportId').val('');
                    } else {
                        enableContinue = true;
                        $('#fileImportId').val(data.result.fileId);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });

            $('#propertyList').select2({
                placeholder: "Select Property",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            }).on("change",function(){
                updateareaList();
            });

            $('#areaList').select2({
                placeholder: "Select area",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            }).on("change",function(){
                updateUnitList();
            });

            $('#unitList').select2({
                placeholder: "Select Unit",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            });
            updateUnitList();
        });

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#fileImportId').val("");
            isChangeFile = false;
            enableContinue = false;
        }

        function updateUnitList(){
            $('#unitList').html('');

            var area = $('#areaList');
            if(area.val()!='' && area.val()!=null) {
                $.ajax({
                    type: 'GET',
                    url: unitByAreaUrl,
                    data: {areaId: area.val()},
                    success: (function (output) {
                        if (output.result) {
                            $.each(output.data, function() {
                                var newOption = new Option(this.name, this.id, false, false);
                                $('#unitList').append(newOption);
                            });
                            var unitIdVal = $('#unitIdVal').val();
                            if(unitIdVal!=''){
                                $('#unitList').val(unitIdVal).trigger('change');
                            }
                        } else {
                            vex.dialog.alert({message: output.message});
                        }
                    }),
                    error: (function (data) {
                        vex.dialog.alert({message: "System Error"})
                    }),
                    complete: (function (data) {
                    })
                });
            }
        }

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function submitForm() {
            if($('#memberForm').valid()) {
                if (enableContinue) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['memberForm'].submit();
                } else {
                    vex.dialog.alert({
                        message: 'Please upload a file'
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
