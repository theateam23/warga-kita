<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Laporan Penghuni" description="Tampilkan dan Download Data Penghuni" />
    <ol class="breadcrumb">
        <li><i class="fa fa-user"></i> Member Maintenance</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Member from File</h3>
        </div>
        <g:form action="confirmImport" class="form-horizontal" method="POST">
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <input type="hidden" name="fileImportId" value="${fileImportId}" />
                <table class="table table-striped table-bordered table-hover" id="dataTables-members" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Line</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Relationship</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${memberList}" status="i" var="obj">
                            <tr>
                                <td>${obj.cnt}</td>
                                <td>${obj.name}</td>
                                <td>${obj.status}</td>
                                <td>${obj.relationship}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="box-footer">
                    <div class="pull-right">
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Konfirmasi</button>
                    </div>
                </div>

            </div>
        </g:form>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#dataTables-members').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "sPaginationType": "simple_numbers"
        });
    });

</script>
</body>
</html>