<!doctype html>
<html>
    <head>
        <title>Error</title>
        <meta name="layout" content="main">
    </head>
    <body>
        <section class="content-header">
            <property:pageHeader title="Error" description="500" />
        </section>

        <section class="content container-fluid">
            <ul class="errors">
                <li>An error has occurred</li>
                <li>Exception: ${exception}</li>
                <li>Message: ${message}</li>
                <li>Path: ${path}</li>
            </ul>
        </section>
    </body>
</html>
