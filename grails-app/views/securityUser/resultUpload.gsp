<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Upload Result</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="User Keamanan" description="Tambah dan Edit User Keamanan" />
    <ol class="breadcrumb">
        <li>User Management</li>
        <li><g:link controller="securityUser" action="index"><i class="fa fa-users"></i> Security User</g:link></li>
        <li>Upload File</li>
        <li class="active">Result</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Hasil Upload Dari File</h3>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Line</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>ID KTP</th>
                            <th>Tanggal Lahir</th>
                            <th>No Telepon</th>
                            <th>Jenis Kelamin</th>
                            <th>Pesan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${userList}" status="i" var="obj">
                            <tr>
                                <td>${obj.cnt}</td>
                                <td>${obj.name}</td>
                                <td>${obj.email}</td>
                                <td>${obj.idKTP}</td>
                                <td>${obj.dob}</td>
                                <td>${obj.phoneNo}</td>
                                <td>${obj.gender}</td>
                                <td>${obj.message}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="box-footer">
                    <g:link controller="securityUser" action="index" class="btn btn-info pull-right">Selesai</g:link>
                </div>

            </div>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#dataTables-users').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "sPaginationType": "simple_numbers"
        });
    });

</script>
</body>
</html>