<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Security User</title>
    <style>
        .no-padding{
            padding:0
        }
    </style>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="User Keamanan" description="Tambah dan Edit User Keamanan" />
    <ol class="breadcrumb">
        <li>User Management</li>
        <li class="active"><i class="fa fa-users"></i> Security User</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar User Mobile Keamanan</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="securityUser" action="create" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah User</g:link>
                <g:link controller="securityUser" action="uploadFile" class="btn btn-primary"><i class="fa fa-upload"></i> Upload File</g:link>
            </div>

            <br style="clear:both" />

            <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<g:form name="form1" controller="securityUser" action="setAsDefault" method="POST">
</g:form>

<g:form name="form2" controller="securityUser" action="resetFromDefault" method="POST">
</g:form>

<script>
    var detailUserUrl = '${g.createLink(controller:'securityUser',action:'detail')}';

    $(document).ready(function(){
        $('#dataTables-users').DataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'securityUser',action:'listDatatable')}',
            "columnDefs": [
                { "orderable": false, "targets": 4 }
            ],
            "aoColumns": [
                { "mData": "email", "sTitle":"Email", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUserUrl+"/"+full.id+"'>"+full.email+"</a>";
                        }
                },
                { "mData": "name", "sTitle":"Nama" },
                { "mData": "phoneNo", "sTitle":"No Telepon" },
                { "mData": "isDefaultDisplay", "sTitle":"Petugas Utama" },
                { "mData": "id", "sTitle":"", "mRender":
                        function(data, type, full){
                            if(full.isDefault=='Y'){
                                return '<button class="btn btn-info" onclick="confirmReset(\'' + full.id + '\')">Reset</button>';
                            } else {
                                return '<button class="btn btn-info" onclick="confirmSetDefault(\'' + full.id + '\')">Jadikan Utama</button>';
                            }
                        }
                }
            ]
        });
    });

    function confirmSetDefault(id){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk menjadikan petugas ini sebagai petugas utama?',
            callback: function (value) {
                if (value) {
                    $('#form1').html('<input type="hidden" value="'+id+'" name="id" />');
                    $('#form1').submit();
                }
            }
        });
    }

    function confirmReset(id){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk membatalkan status petugas utama untuk petugas ini?',
            callback: function (value) {
                if (value) {
                    $('#form2').html('<input type="hidden" value="'+id+'" name="id" />');
                    $('#form2').submit();
                }
            }
        });
    }

</script>
</body>
</html>