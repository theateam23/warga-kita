<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Security User</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="User Keamanan" description="Tambah dan Edit User Keamanan" />
        <ol class="breadcrumb">
            <li>User Management</li>
            <li><g:link controller="securityUser" action="index"><i class="fa fa-users"></i> Security User</g:link></li>
            <li class="active">Detail Mobile Security User</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Posisi:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID KTP:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.idKTP}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Lahir:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.dob}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">No telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.phoneNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis Kelamin:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${user.genderName}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <g:link controller="securityUser" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="securityUser" action="edit" params="[id:user.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="securityUser" action="delete" method="POST">
        <input type="hidden" name="id" value="${user.id}" />
    </g:form>

    <script>
        $(document).ready(function(){
            $('#dataTables-unit').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false
            });
        });
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus user keamanan ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>