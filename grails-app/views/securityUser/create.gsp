<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Tambah Security User</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="User Keamanan" description="Tambah dan Edit User Keamanan" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i>User Management</li>
            <li><g:link controller="securityUser" action="index"> Security User</g:link></li>
            <li class="active">Create Security User</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah User Keamanan</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${user.propName}" />
                            <p class="form-control-static">${user.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${user.id}" />
                            <input type="email" name="email" class="form-control" id="tfEmail" maxlength="200" placeholder="Email" value="${user.email}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${user.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-3 control-label">Posisi:</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" class="form-control" maxlength="20" placeholder="Title" value="${user.title}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID KTP:</label>
                        <div class="col-sm-8">
                            <input type="text" name="idKTP" class="form-control" maxlength="200" placeholder="ID Card No" value="${user.idKTP}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Lahir:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="dob" id="tfDob" class="form-control" value="${user.dob}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfPhoneNo" class="col-sm-3 control-label">No telepon:</label>
                        <div class="col-sm-8">
                            <input type="number" name="phoneNo" class="form-control" id="tfPhoneNo" maxlength="20" placeholder="Phone No" value="${user.phoneNo}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis Kelamin:</label>
                        <div class="col-sm-8">
                            <g:select id="genderList" from='${genderList}' class="form-control select2" value="${user.genderId}"
                                      name="genderId" optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <g:link controller="securityUser" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        var unitByAreaUrl = '${g.createLink(controller:'securityUser', action:'unitByAreaJSON')}';

        $(document).ready(function(){
            $("#form1").validate({
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });

            $('#genderList').select2();

            $('#tfDob').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "locale": {
                    "format": "YYYY-MM-DD"
                },
                autoUpdateInput: false
            }, function(chosen_date) {
                $('#tfDob').val(chosen_date.format('YYYY-MM-DD'));
            });
        });
    </script>
</body>
</html>