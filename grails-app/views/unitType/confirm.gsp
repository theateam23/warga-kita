<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${unitType.stage} ${unitType.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Tipe Unit Properti" description="Tambah dan Edit Tipe Unit Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="unitType" action="index"><i class="fa fa-gear"></i> Unit Type</g:link></li>
            <li>${unitType.action}</li>
            <li class="active">${unitType.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${unitType.stage}</h3>
            </div>
            <g:form action="${unitType.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${unitType.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${unitType.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${unitType.id}" />
                            <input type="hidden" name="code" value="${unitType.code}" />
                            <p class="form-control-static">${unitType.code}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="name" value="${unitType.name}" />
                            <p class="form-control-static">${unitType.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="description" value="${unitType.description}" />
                            <p class="form-control-static">${unitType.description}</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${unitType.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="unitType" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>