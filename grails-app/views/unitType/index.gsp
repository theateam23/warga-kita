<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Unit Type Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Tipe Unit Properti" description="Tambah dan Edit Tipe Unit Properti" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-gear"></i> Unit Type</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Tipe Unit</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="unitType" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Tipe Unit Properti</g:link>

            <table class="table table-striped table-bordered table-hover" id="dataTables-uts" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'unitType',action:'detail')}';

    $(document).ready(function(){
        $('#dataTables-uts').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'unitType',action:'listDatatable')}',
            "aoColumns": [
                { "mData": "code", "sTitle":"Kode", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.code+"</a>";
                        }
                },
                { "mData": "name", "sTitle":"Nama" },
                { "mData": "description", "sTitle":"Keterangan" }
            ]
        });
    });

</script>
</body>
</html>