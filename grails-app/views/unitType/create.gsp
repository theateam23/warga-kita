<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Tambah Unit Type</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Tipe Unit Properti" description="Tambah dan Edit Tipe Unit Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="unitType" action="index"><i class="fa fa-gear"></i> Unit Type</g:link></li>
            <li class="active">Create Unit Type</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Tipe Unit</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label for="tfCode" class="col-sm-2 control-label">Kode:</label>
                        <div class="col-sm-8">
                            <input type="text" name="code" class="form-control" id="tfCode" maxlength="200" placeholder="Code" value="${unitType.code}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${unitType.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfDescription" class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="text" name="description" class="form-control" id="tfDescription" maxlength="200" placeholder="Description" value="${unitType.description}">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="unitType" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#form1').validate({
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>
</body>
</html>