<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Unit Type</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Tipe Unit Properti" description="Tambah dan Edit Tipe Unit Properti" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="unitType" action="index"><i class="fa fa-gear"></i> Unit Type</g:link></li>
            <li class="active">Detail Unit Type</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${unitType.code}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${unitType.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${unitType.description}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="unitType" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="unitType" action="edit" params="[id:unitType.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="unitType" action="delete" method="POST">
        <input type="hidden" name="id" value="${unitType.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data tipe unit ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>