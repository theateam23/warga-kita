<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Confirm Upload</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="User Mobile" description="Tambah dan Edit User Mobile" />
    <ol class="breadcrumb">
        <li>User Management</li>
        <li><g:link controller="userMobile" action="index"><i class="fa fa-users"></i> Mobile User</g:link></li>
        <li>Upload File</li>
        <li class="active">Confirm</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar User Dari File Upload</h3>
        </div>
        <g:form action="submitImport" class="form-horizontal" method="POST">
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
            </div>
            <div class="box-body">
                <input type="hidden" name="fileImportId" value="${fileImportId}" />
                <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Baris</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Telepon</th>
                            <th>Status</th>
                            <th>Nama Unit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${userList}" status="i" var="obj">
                            <tr>
                                <td>${obj.cnt}</td>
                                <td>${obj.name}</td>
                                <td>${obj.email}</td>
                                <td>${obj.phoneNo}</td>
                                <td>${obj.status}</td>
                                <td>${obj.unit}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="box-footer">
                    <g:link controller="userMobile" action="uploadFile" class="btn btn-default">Kembali</g:link>
                    <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                </div>

            </div>
        </g:form>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#dataTables-users').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "sPaginationType": "simple_numbers"
        });
    });

</script>
</body>
</html>