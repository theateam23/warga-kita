<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${user.stage} ${user.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="User Mobile" description="Tambah dan Edit User Mobile" />
        <ol class="breadcrumb">
            <li>User Management</li>
            <li><g:link controller="userMobile" action="index"><i class="fa fa-users"></i> Mobile User</g:link></li>
            <li>${user.action}</li>
            <li class="active">${user.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${user.stage}</h3>
            </div>
            <g:form action="${user.actionType=='add'?'submitAdd':'submitEdit'}" method="POST">
                <div class="box-body" id="testBody">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${user.propName}" />
                                <p class="form-control-static">${user.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="${user.id}" />
                                <input type="hidden" name="email" value="${user.email}" />
                                <p class="form-control-static">${user.email}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="name" value="${user.name}" />
                                <p class="form-control-static">${user.name}</p>
                            </div>
                        </div>
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">Judul:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="hidden" name="title" value="${user.title}" />--}%
                                %{--<p class="form-control-static">${user.title}</p>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">ID KTP:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="hidden" name="idKTP" value="${user.idKTP}" />--}%
                                %{--<p class="form-control-static">${user.idKTP}</p>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">Tanggal Lahir:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="hidden" name="dob" value="${user.dob}" />--}%
                                %{--<p class="form-control-static">${user.dob}</p>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                        <div class="form-group">
                            <label class="col-sm-3 control-label">No telepon:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="phoneNo" value="${user.phoneNo}" />
                                <p class="form-control-static">${user.phoneNo}</p>
                            </div>
                        </div>
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">Jenis Kelamin:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="hidden" name="genderId" value="${user.genderId}" />--}%
                                %{--<p class="form-control-static">${user.genderName}</p>--}%
                            %{--</div>--}%
                        %{--</div>--}%
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Area:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="areaId" value="${user.areaId}" />
                                <p class="form-control-static">${user.areaName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Unit:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="unitId" value="${user.unitId}" />
                                <p class="form-control-static">${user.unitName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Sub Area:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="subArea" value="${user.subArea}" />
                                <p class="form-control-static">${user.subArea}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="roleId" value="${user.roleId}" />
                                <p class="form-control-static">${user.roleName}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${user.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="userMobile" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#dataTables-unit').dataTable({
                "language": {
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Berikutnya"
                    },
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "emptyTable": "Data tidak ditemukan"
                },
                "bLengthChange": false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false
            });
        });
    </script>
</body>
</html>