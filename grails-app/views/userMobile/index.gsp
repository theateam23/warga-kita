<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Mobile User</title>
    <style>
        .no-padding{
            padding:0
        }
    </style>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="User Mobile" description="Tambah dan Edit User Mobile" />
    <ol class="breadcrumb">
        <li>User Management</li>
        <li class="active"><i class="fa fa-users"></i> Mobile User</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Mobile User</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="userMobile" action="createUser" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah User</g:link>
                <g:link controller="userMobile" action="uploadFile" class="btn btn-primary"><i class="fa fa-upload"></i> Upload File</g:link>
            </div>

            <br style="clear:both" />

            <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUserUrl = '${g.createLink(controller:'userMobile',action:'detailUser')}';

    $(document).ready(function(){
        $('#dataTables-users').DataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'userMobile',action:'listDatatable')}',
            "aoColumns": [
                { "mData": "email", "sTitle":"Email", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUserUrl+"/"+full.id+"'>"+full.email+"</a>";
                        }
                },
                { "mData": "name", "sTitle":"Nama" },
                { "mData": "area", "sTitle":"Area" },
//                { "mData": "subArea", "sTitle":"Sub Area" },
                { "mData": "unit", "sTitle":"Unit" },
                { "mData": "role", "sTitle":"Status" }
//                { "mData": "createdDate", "sTitle":"Tanggal Dibuat" }
            ]
        });
    });

</script>
</body>
</html>