<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edit Mobile User</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Mobile User" description="Tambah dan Edit User Mobile" />
        <ol class="breadcrumb">
            <li><i class="fa fa-users"></i>User Management</li>
            <li><g:link controller="userMobile" action="index"> Mobile User</g:link></li>
            <li class="active">Edit Mobile User</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <g:form action="confirmEdit" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${user.propName}" />
                            <p class="form-control-static">${user.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${user.id}" />
                            <input type="email" name="email" class="form-control" id="tfEmail" maxlength="200" placeholder="Email" value="${user.email}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${user.name}" required>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label for="tfName" class="col-sm-3 control-label">Judul:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="text" name="title" class="form-control" placeholder="Title" value="${user.title}" required>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">ID KTP:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<input type="text" name="idKTP" class="form-control" placeholder="ID Card No" value="${user.idKTP}">--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Tanggal Lahir:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<div class="input-group">--}%
                                %{--<div class="input-group-addon">--}%
                                    %{--<i class="fa fa-calendar"></i>--}%
                                %{--</div>--}%
                                %{--<input type="text" name="dob" id="tfDob" class="form-control" value="${user.dob}">--}%
                            %{--</div>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label for="tfPhoneNo" class="col-sm-3 control-label">No telepon:</label>
                        <div class="col-sm-8">
                            <input type="number" name="phoneNo" class="form-control" id="tfPhoneNo" maxlength="20" placeholder="Phone No" value="${user.phoneNo}">
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Jenis Kelamin:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<g:select id="genderList" from='${genderList}' class="form-control select2" value="${user.genderId}"--}%
                                      %{--name="genderId" optionKey="id" optionValue="name"></g:select>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Area:</label>
                        <div class="col-sm-8">
                            <g:select id="areaList" from='${areaList}' class="form-control select2" value="${user.areaId}"
                                      name="areaId" optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unitId" class="col-sm-3 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <input type="hidden" id="unitIdVal" value="${user.unitId}" />
                            <select id="unitId" name='unitId' class="form-control select2"></select>
                            <div id="unitId-error-msg"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" id="tfSubArea" name="subArea" value="${user.subArea}" />
                            <p class="form-control-static" id="subAreaP">${user.subArea}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status:</label>
                        <div class="col-sm-8">
                            <g:select id="roleList" from='${roleList}' class="form-control select2" value="${user.roleId}"
                                      name="roleId" optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <g:link controller="userMobile" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        var unitByAreaUrl = '${g.createLink(controller:'userMobile', action:'unitByAreaJSON')}';
        var subUserRoleName = '${com.model.AppConstants.ROLE_SUB_USER}';
        $(document).ready(function(){
            $.validator.addMethod("validateUnitRole", function(value, element){
                var unitIdVal = $('#unitIdVal').val();
                var selectedUnit = $('#unitId :selected');
                var roleName = $('#roleList :selected').text();

                if(unitIdVal == selectedUnit.val()){
                    return true;
                }
                var tenant = selectedUnit.data("tenant");
                var owner = selectedUnit.data("owner");
                if((tenant==true && roleName != subUserRoleName) || (owner==true && roleName != subUserRoleName)){
                    return false;
                }
                return true;
            }, "Unit sudah memiliki penyewa atau pemilik.");

            $("#form1").validate({
                rules: {
                    'unitId': {
                        required: true,
                        validateUnitRole: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                        error.appendTo('#unitId-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                },
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });

            $('#areaList').select2({
                placeholder: "Select Area",
                escapeMarkup: function (m) { return m; }
            }).on("change",function(){
                updateUnitList();
            });

            $('#genderList').select2();
            $('#unitId').select2({
                "placeholder": "Select Unit"
            }).on("change",function(){
                $('#subAreaP').html('');
                $('#unitId').valid();
                updateSubArea();
            });
            $('#roleList').select2({
                "placeholder": "Select Role"
            });

            $('#tfDob').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "locale": {
                    "format": "YYYY-MM-DD"
                },
                autoUpdateInput: false
            }, function(chosen_date) {
                $('#tfDob').val(chosen_date.format('YYYY-MM-DD'));
            });
            updateUnitList();
        });

        function updateUnitList(){
            $('#unitId').html('');

            var area = $('#areaList');
            if(area.val()!='' && area.val()!=null) {
                $.ajax({
                    type: 'GET',
                    url: unitByAreaUrl,
                    data: {areaId: area.val(), umpId:'${user.id}'},
                    success: (function (output) {
                        if (output.result) {
                            $.each(output.data, function() {
                                var newOption = new Option(this.name, this.id, false, false);
                                newOption.setAttribute("data-owner", this.hasOwner);
                                newOption.setAttribute("data-tenant", this.hasTenant);
                                newOption.setAttribute("data-subarea", this.subArea);
                                $('#unitId').append(newOption);
                            });
                            var unitIdVal = $('#unitIdVal').val();
                            if(unitIdVal!=''){
                                $('#unitId').val(unitIdVal).trigger('change');
                            }
                        } else {
                            vex.dialog.alert({message: output.message});
                        }
                    }),
                    error: (function (data) {
                        vex.dialog.alert({message: "System Error"})
                    }),
                    complete: (function (data) {
                    })
                });
            }
        }

        function updateSubArea(){
            var selectedUnit = $('#unitId :selected');
            var subArea = selectedUnit.data("subarea");
            $('#subAreaP').html(subArea);
            $('#tfSubArea').val(subArea);
        }
    </script>
</body>
</html>