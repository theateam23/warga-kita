<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Manage Booking</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Persetujuan Fasilitas Umum" description="Setuju dan Tolak Permintaan Fasilitas Umum" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li class="active"><i class="fa fa-address-card"></i> Manage Booking</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pesanan Fasilitas Umum</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="facilityBooking" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Pesanan Fasilitas Umum</g:link>

            <br style="clear:both" />

            <g:form action="index" class="form-horizontal" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label for="facilityList" class="col-sm-2 control-label">Fasilitas:</label>
                    <div class="col-sm-8">
                        <g:select id="facilityList" name='facilityId' value="${facilityId}"
                                  noSelection="${['':'Tampilkan Semua']}"
                                  from='${facilityList}' class="form-control select2"
                                  optionKey="id" optionValue="name"></g:select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="statusList" class="col-sm-2 control-label">Status:</label>
                    <div class="col-sm-8">
                        <g:select id="statusList" name='status' value="${status}"
                                  noSelection="${['':'Tampilkan Semua']}"
                                  from='${statusList}' class="form-control select2"></g:select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tfCode" class="col-sm-2 control-label">Kode:</label>
                    <div class="col-sm-6">
                        <input type="text" name="code" class="form-control" id="tfCode" placeholder="Code" value="${code}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </g:form>

            <table class="table table-striped table-bordered table-hover" id="dataTables-bookings" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<g:form name="formApprove" action="approve" method="POST">
    <input type="hidden" name="facilityId" value="${facilityId}" />
    <input type="hidden" name="status" value="${status}" />
    <input type="hidden" name="code" value="${code}" />
</g:form>

<g:form name="formCancel" action="cancel" method="POST">
    <input type="hidden" name="facilityId" value="${facilityId}" />
    <input type="hidden" name="status" value="${status}" />
    <input type="hidden" name="code" value="${code}" />
</g:form>

<g:form name="formReject" action="reject" method="POST">
    <input type="hidden" name="facilityId" value="${facilityId}" />
    <input type="hidden" name="status" value="${status}" />
    <input type="hidden" name="code" value="${code}" />
</g:form>

<script>
    var detailBookingUrl = '${g.createLink(controller:'manageFacilityBooking',action:'detail')}';
    var statusNew = '${com.model.AppConstants.BOOKING_STATUS_NEW}';
    var statusOpen = '${com.model.AppConstants.BOOKING_STATUS_OPEN}';

    $(document).ready(function(){
        $('#facilityList').select2();
        $('#statusList').select2();
        $('#dataTables-bookings').DataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'manageFacilityBooking',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "facilityId", "value": $('#facilityList').val() } );
                aoData.push( { "name": "status", "value": $('#statusList').val() } );
                aoData.push( { "name": "code", "value": $('#tfCode').val() } );
            },
            "aoColumns": [
                { "mData": "code", "sTitle":"Kode" , "mRender":
                        function(data, type, full){
                            return "<a href='"+detailBookingUrl+"/"+full.id+"'>"+full.code+"</a>";
                        }
                },
                { "mData": "facilityName", "sTitle":"Fasilitas" },
                { "mData": "bookDate", "sTitle":"Tanggal Pesanan" },
                { "mData": "bookTime", "sTitle":"Waktu" },
                { "mData": "userName", "sTitle":"Nama" },
                { "mData": "status", "sTitle":"Status" },
                { "mData": "id", "sTitle":"", "mRender":
                        function(data, type, full){
                            if(full.status == statusNew){
                                return '<button class="btn btn-info" onclick="onApproveClick(\''+full.id+'\',\''+full.code+'\')"><i class="fa fa-check"></i> Setuju</button> ' +
                                        '<button class="btn btn-warning" onclick="onRejectClick(\''+full.id+'\',\''+full.code+'\')"><i class="fa fa-times"></i> Tolak</button>';
                            } else if(full.status == statusOpen){
                                return '<button class="btn btn-warning" onclick="onCancelClick(\''+full.id+'\',\''+full.code+'\')"><i class="fa fa-times"></i> Batal</button>';
                            } else {
                                return "";
                            }
                        }
                }
            ]
        });
    });

    function onApproveClick(id, code){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Menyetujui permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $('#formApprove').append(
                            $('<input>').attr('type', 'hidden').attr('name', 'id').val(id)
                    );
                    $('#formApprove').submit();
                }
            }
        });
    }

    function onCancelClick(id, code){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Membatalkan permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $('#formCancel').append(
                            $('<input>').attr('type', 'hidden').attr('name', 'id').val(id)
                    );
                    $('#formCancel').submit();
                }
            }
        });
    }

    function onRejectClick(id, code){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Menolak permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $('#formReject').append(
                            $('<input>').attr('type', 'hidden').attr('name', 'id').val(id)
                    );
                    $('#formReject').submit();
                }
            }
        });
    }
</script>
</body>
</html>