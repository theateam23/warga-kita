<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Booking</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Persetujuan Fasilitas Umum" description="Setuju dan Tolak Permintaan Fasilitas Umum" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li class="active"><i class="fa fa-address-card"></i> Detail Booking</li>
    </ol>
</section>


<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Detail</h3>
        </div>
        <div class="box-body" id="testBody">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.code}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Fasilitas:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.facilityName}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.bookDate}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Waktu:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.bookTime}</p>
                    </div>
                </div>
                <g:if test="${detail.fromMobile}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.unit}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No HP:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.phoneNo}</p>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.name}</p>
                        </div>
                    </div>
                </g:else>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.description}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.status}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <g:link controller="manageFacilityBooking" action="index" class="btn btn-default">Kembali</g:link>
            <div class="pull-right">
                <g:if test="${detail.isNew}">
                    <button type="button" onclick="onApproveClick()" class="btn btn-primary"><i class="fa fa-check"></i> Setujui</button>
                    <button type="button" onclick="onRejectClick()" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                </g:if>
                <g:if test="${detail.isOpen}">
                    <button type="button" onclick="onCancelClick()" class="btn btn-danger"><i class="fa fa-times"></i> Batal</button>
                </g:if>
            </div>
        </div>
    </div>
</section>

<g:form name="formApprove" action="approve" method="POST">
    <input type="hidden" name="id" value="${detail.id}" />
</g:form>

<g:form name="formCancel" action="cancel" method="POST">
    <input type="hidden" name="id" value="${detail.id}" />
</g:form>

<g:form name="formReject" action="reject" method="POST">
    <input type="hidden" name="id" value="${detail.id}" />
</g:form>

<script>
    var code = '${detail.code}';
    function onApproveClick(){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Menyetujui permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    $('#formApprove').submit();
                }
            }
        });
    }

    function onCancelClick(){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Membatalkan permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    $('#formCancel').submit();
                }
            }
        });
    }

    function onRejectClick(){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk Menolak permintaan dengan kode: '+code+'?',
            callback: function (value) {
                if (value) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    $('#formReject').submit();
                }
            }
        });
    }
</script>
</body>
</html>