<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Broadcast Message</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Sebar Pengumuman" description="Sebar dan Pasang Pengingat Pengumuman" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li><g:link action="index"><i class="fa fa-envelope"></i> Broadcast Message</g:link></li>
        <li class="active">Create</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pengumuman</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="broadcastMessage" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Buat Pengumuman</g:link>
            </div>

            <br style="clear: both"/>

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Area:</label>
                    <div class="col-sm-8">
                        <input type="text" id="areaName" name="areaName" class="form-control" maxlength="200" placeholder="Area Name" value="${areaName}">
                    </div>
                </div>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-search"></i> Cari</button>
            </g:form>

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-objs" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'broadcastMessage',action:'detail')}';

    $(document).ready(function(){
        $('#dataTables-objs').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'broadcastMessage',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "areaName", "value": $('#areaName').val() } );
            },
            "aoColumns": [
                { "mData": "createdDate", "sTitle":"Tanggal Dibuat", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.createdDate+"</a>";
                        }
                },
                { "mData": "title", "sTitle":"Judul" },
                { "mData": "reminderDate", "sTitle":"Tanggal Pengingat" },
                { "mData": "target", "sTitle":"Tujuan" },
                { "mData": "createdBy", "sTitle":"Dibuat Oleh" }
            ],
            "order": [[0,'desc']]
        });
    });
</script>
</body>
</html>