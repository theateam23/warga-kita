<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Broadcast Message</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Sebar Pengumuman" description="Sebar dan Pasang Pengingat Pengumuman " />
        <ol class="breadcrumb">
            <li>Activity</li>
            <li><g:link action="index"><i class="fa fa-envelope"></i> Broadcast Message</g:link></li>
            <li class="active">Detail</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pengumuman</h3>
            </div>
            <g:form action="submit" class="form-horizontal" method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tujuan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${obj.target}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${obj.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pesan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${obj.content}</p>
                        </div>
                    </div>
                    <input type="hidden" name="hasReminder" value="${obj.hasReminder}" />
                    <g:if test="${obj.hasReminder}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Waktu Diingatkan:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="reminderDate" value="${obj.reminderDate}" />
                                <p class="form-control-static">${obj.reminderDate}</p>
                            </div>
                        </div>
                    </g:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Dibuat oleh:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${obj.createdBy} pada ${obj.createdDate}</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:actionSubmit action="index" class="btn btn-default" value="Back" />
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>