<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Broadcast Message</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Sebar Pengumuman" description="Sebar dan Pasang Pengingat Pengumuman " />
        <ol class="breadcrumb">
            <li>Activity</li>
            <li><g:link action="index"><i class="fa fa-envelope"></i> Broadcast Message</g:link></li>
            <li>Create</li>
            <li class="active">Confirm</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Konfirmasi</h3>
            </div>
            <g:form action="submit" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${obj.propName}" />
                            <p class="form-control-static">${obj.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tujuan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="areaId" value="${obj.areaId}" />
                            <input type="hidden" name="target" value="${obj.target}" />
                            <p class="form-control-static">${obj.target}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="broadcastTitle" value="${obj.broadcastTitle}" />
                            <p class="form-control-static">${obj.broadcastTitle}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pesan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="broadcastMessage" value="${obj.broadcastMessage}" />
                            <p class="form-control-static">${obj.broadcastMessage}</p>
                        </div>
                    </div>
                    <input type="hidden" name="hasReminder" value="${obj.hasReminder}" />
                    <g:if test="${obj.hasReminder=="Y"}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Waktu Diingatkan:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="reminderDate" value="${obj.reminderDate}" />
                                <p class="form-control-static">${obj.reminderDate}</p>
                            </div>
                        </div>
                    </g:if>
                </div>
                <div class="box-footer">
                    <g:actionSubmit action="index" class="btn btn-default" value="Batal" />
                    <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Sebar Pengumuman</button>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>