<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Broadcast Message</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Sebar Pengumuman" description="Sebar dan Pasang Pengingat Pengumuman " />
    <ol class="breadcrumb">
        <li>Aktivity</li>
        <li><g:link action="index"><i class="fa fa-envelope"></i> Broadcast Message</g:link></li>
        <li class="active">Create</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Buat Pesan</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="form-horizontal col-md-7">
                <g:form action="confirm" method="POST" style="margin-bottom: 20px;" name="form1">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${propName}" />
                            <p class="form-control-static">${propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="areaList" class="col-sm-4 control-label">Tujuan:</label>
                        <div class="col-sm-8">
                            <g:select id="areaList" name='areaId' value="${areaId}"
                                      noSelection="${['':'Semua Area']}"
                                      from='${areaList}' class="form-control select2"
                                      optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <input type="text" name="broadcastTitle" class="form-control" placeholder="Title" maxlength="200" value="${broadcastTitle}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Pesan:</label>
                        <div class="col-sm-8">
                            <textarea rows="4" cols="50" name="broadcastMessage" class="form-control" maxlength="1000" required>${broadcastMessage}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4">
                            <div class="checkbox col-sm-6">
                                <label>
                                    <input type="checkbox" id="cbReminder" name="hasReminder" value="Y" ${hasReminder=="Y"?"checked":""} onchange="toggleReminderDate()">
                                    Pasang pengingat
                                </label>
                            </div>
                            <div class="input-group col-sm-6" style="margin-top: 10px" id="reminderDateDiv">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="reminderDate" class="form-control" id="tfReminderDate" value="${reminderDate}" required>
                            </div>
                            <div class="col-sm-offset-6" id="reminderDate-error-msg"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="pull-right" id="downloadDiv">
                            <button type="submit" class="btn btn-primary">Konfirmasi</button>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#areaList').select2();

        $.validator.addMethod( "hasReminder", function(value, element) {
            if($('#cbReminder').is(":checked")){
                return true;
            } else {
                return false;
            }
        }, "Reminder date is required");

        $('#form1').validate({
            rules: {
                'reminderDate': {
                    hasReminder: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "reminderDate" ){
                    $('#reminderDate-error-msg').html('');
                    error.appendTo('#reminderDate-error-msg');
                } else {
                    error.insertAfter(element);
                }
            },
            success: function(label,element) {
                if ($(element).attr("name") == "reminderDate" ){
                    $('#reminderDate-error-msg').html('');
                } else {
                    $(label).remove();
                }
            },
            submitHandler: function(form) {
                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                form.submit();
            }
        });
        $('#tfReminderDate').daterangepicker({
            timePicker: true,
            singleDatePicker: true,
            showDropdowns: true,
            "locale": {
                "format": "YYYY-MM-DD hh:mm A"
            }
        }, function(chosen_date) {
            $('#tfReminderDate').val(chosen_date.format('YYYY-MM-DD hh:mm A'));
            $('#reminderDate-error-msg').html('');
        });
        toggleReminderDate();
    });

    function toggleReminderDate(){
        var reminderDt = $('#tfReminderDate');
        var checked = $('#cbReminder').is(":checked");
        if(checked){
            reminderDt.prop("disabled",false);
        } else {
            reminderDt.prop("disabled",true);
            $('#reminderDate-error-msg').html('');
        }
    }
</script>
</body>
</html>