<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Visitor</title>

    <asset:stylesheet src="fullcalendar/fullcalendar.min.css" />
    <asset:stylesheet src="fullcalendar/fullcalendar.print.min.css" media="print"/>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pesan Fasilitas Umum" description="Tambah Pesanan Fasilitas Umum" />
        <ol class="breadcrumb">
            <li>Activity</li>
            <li><g:link controller="facilityBooking" action="index"><i class="fa fa-address-book"></i> Facility Booking</g:link></li>
            <li class="active">Booking</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Pesanan di: ${facility.name}</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-horizontal">
                    <div id="dayEvents" style="max-width: 90%;margin: 0 auto;"></div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="facilityBooking" action="index" class="btn btn-default">Kembali</g:link>
            </div>
        </div>
    </section>

    <g:form name="formBooking" controller="facilityBooking" action="confirmBooking" method="POST">
        <input type="hidden" name="id" value="${facility.id}" />
        <input type="hidden" name="startDate" value="${facility.startDate}" />
        <input type="hidden" name="endDate" value="${facility.endDate}" />
        <input type="hidden" name="start" id="thStart" value="" />
        <input type="hidden" name="end" id="thEnd" value="" />
        <input type="hidden" name="description" id="thDescription" value="" />
    </g:form>

    <asset:javascript src="fullcalendar/moment.min.js" />
    <asset:javascript src="fullcalendar/fullcalendar.min.js" />

    <script>
        var paramDate = ${facility.startDate?:"null"};
        var facilityId = '${facility.id}';
        var tzoffset = ${tzoffset?:'0'};

        $(document).ready(function(){
            $('#dayEvents').fullCalendar({
                header: {
                    left: '',
                    center: 'title',
                    right: ''
                },
                slotDuration: "01:00",
                defaultView: 'agendaDay',
                navLinks: false, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "${g.createLink(controller: 'facilityBooking', action: 'getEvents')}",
                        dataType: 'json',
                        data: {
                            // our hypothetical feed requires UNIX timestamps
                            start: (start.unix()-tzoffset)*1000,
                            end: (end.unix()-tzoffset)*1000,
                            id:facilityId
                        },
                        success: function(doc) {
                            callback(doc);
                        }
                    });
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end) {
                    confirmBooking((start.unix()-tzoffset)*1000, (end.unix()-tzoffset)*1000);
                }
            });

            if(paramDate!=null) {
                $('#dayEvents').fullCalendar('gotoDate', moment(paramDate));
            }
        });

        function confirmBooking(start, end){
            vex.dialog.open({
                message: 'Keterangan Pesanan:',
                input: [
                    '<input name="description" type="text" maxlength="200" placeholder="Booking Description"/>'
                ].join(''),
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Lanjutkan' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'Batal' })
                ],
                callback: function (data) {
                    if (data) {
                        if(data.description!=null){
                            $('#thDescription').val(data.description);
                        }
                        $('#thStart').val(start);
                        $('#thEnd').val(end);
                        $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                        $('#formBooking').submit();
                    }
                }
            })
        }
    </script>
</body>
</html>