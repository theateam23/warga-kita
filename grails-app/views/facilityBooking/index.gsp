<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Facility Booking</title>

    <asset:stylesheet src="fullcalendar/fullcalendar.min.css" />
    <asset:stylesheet src="fullcalendar/fullcalendar.print.min.css" media="print"/>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Pesan Fasilitas Umum" description="Tambah Pesanan Fasilitas Umum" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li class="active"><i class="fa fa-address-book"></i> Facility Booking</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pesanan</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:form action="index" class="form-horizontal" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Fasilitas:</label>
                    <div class="col-sm-8">
                        <g:select id="facilityList" name='facilityId' value="${facilityId}"
                                  from='${facilityList}' class="form-control select2"
                                  optionKey="id" optionValue="name"></g:select>
                        <button type="submit" class="btn btn-info">Tampilkan</button>
                    </div>
                </div>
            </g:form>

            <div id="facilitySchedule" style="max-width: 90%;margin: 0 auto;"></div>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<asset:javascript src="fullcalendar/moment.min.js" />
<asset:javascript src="fullcalendar/fullcalendar.min.js" />

<script>
    var detailUserUrl = '${g.createLink(controller:'visitor',action:'detail')}';
    var tzoffset = ${tzoffset?:'0'};

    $(document).ready(function(){
        $('#facilityList').select2();

        $('#facilitySchedule').fullCalendar({
            header: {
                left: 'prev today',
                center: 'title',
                right: 'next'
            },
            defaultDate: moment(),
            navLinks: false, // can click day/week names to navigate views
            editable: false,
            eventLimit: true, // allow "more" link when too many events
            events: function(start, end, timezone, callback) {
                $.ajax({
                    url: "${g.createLink(controller: 'facilityBooking', action: 'getEvents')}",
                    dataType: 'json',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: (start.unix()-tzoffset)*1000,
                        end: (end.unix()-tzoffset)*1000,
                        id: $('#facilityList').val()
                    },
                    success: function(doc) {
                        callback(doc);
                    }
                });
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
                var s = (start.unix()-tzoffset)*1000;
                var e = (end.unix()-tzoffset)*1000;
                window.location.href = "${g.createLink(controller: 'facilityBooking', action: 'booking')}?id="+$('#facilityList').val()+"&start="+s+"&end="+e;
            },
            eventClick: function(event, jsEvent, view) {
                var s = (event.start.unix()-tzoffset)*1000;
                var e = (event.end.unix()-tzoffset)*1000;
                window.location.href = "${g.createLink(controller: 'facilityBooking', action: 'booking')}?id="+$('#facilityList').val()+"&start="+s+"&end="+e;
            }
        });
    });

</script>
</body>
</html>