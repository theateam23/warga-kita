<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${facilityBooking.stage} ${facilityBooking.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pesan Fasilitas Umum" description="Tambah Pesanan Fasilitas Umum" />
        <ol class="breadcrumb">
            <li>Activity</li>
            <li><g:link controller="facilityBooking" action="index"><i class="fa fa-address-book"></i> Facility Booking</g:link></li>
            <li>${facilityBooking.action}</li>
            <li class="active">${facilityBooking.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${facilityBooking.stage}</h3>
            </div>
            <g:form action="submitBooking" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${facilityBooking.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${facilityBooking.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Fasilitas:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${facilityBooking.id}" />
                            <p class="form-control-static">${facilityBooking.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="startDate" value="${facilityBooking.startDate}" />
                            <input type="hidden" name="endDate" value="${facilityBooking.endDate}" />
                            <p class="form-control-static">${facilityBooking.strStartDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Start Time:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="start" value="${facilityBooking.start}" />
                            <p class="form-control-static">${facilityBooking.strStartTime}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">End Time:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="end" value="${facilityBooking.end}" />
                            <p class="form-control-static">${facilityBooking.strEndTime}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="description" value="${facilityBooking.description}" />
                            <p class="form-control-static">${facilityBooking.description}</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${facilityBooking.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="facilityBooking" action="index" params="[facilityId:facilityBooking.id]" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>