<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 18/07/2015
  Time: 0:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="errorLayout"/>
    <title>Forbidden</title>
</head>

<body>
    <h1>403</h1>
    <h3 class="font-bold">You are not authorized to access this page</h3>

    <div class="error-desc">
        If you feel you should have access to this page, please contact your admin or supervisor<br/>
        You can go back to main page by clicking
        <br/><g:link controller="dashboard" action="index" class="btn btn-success m-t">this</g:link>
        <br/>Or you might want to
        <br/><g:link controller="logout" class="btn btn-success m-t">logout</g:link>
    </div>
</body>
</html>
