<!doctype html>
<html>
<head>
    <title>Warga Kita</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <asset:link rel="icon" href="wargaku_logo_only.png" type="image/x-ico" />
    <asset:stylesheet src="bootstrap/bootstrap.min.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <asset:stylesheet src="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"/>
    <asset:stylesheet src="animate/animate.css"/>
    <asset:stylesheet src="css-hamburgers/hamburgers.min.css"/>
    <asset:stylesheet src="animsition/animsition.min.css"/>
    <asset:stylesheet src="auth/main.css"/>
    <asset:stylesheet src="auth/util.css"/>
</head>
<body class="bg-boxed">
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" action='/login/authenticate' method='POST' id='loginForm'>
                <div class="w-full text-center" style="background-color: #1CA2B3">
                    <asset:image src="wargaku_logo_big.png" style="max-height:300px" />
                </div>

                <div class="login-form">
                    <span class="login100-form-title p-b-15">
                        Login untuk melanjutkan
                    </span>

                    <div class="w-full text-center p-t-5 p-b-5">
                        <span class="txt4">
                            &nbsp;<g:if test='${flash.message}'>${flash.message}</g:if>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Username is required">
                        <input class="input100" type="text" name="username" id="username" />
                        <span class="focus-input100"></span>
                        <span class="label-input100">Username</span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="password" id="password" />
                        <span class="focus-input100"></span>
                        <span class="label-input100">Password</span>
                    </div>

                    <div class="flex-sb-m w-full p-t-3 p-b-32">
                        %{--<div class="contact100-form-checkbox">--}%
                            %{--<input class="input-checkbox100" type="checkbox" name="remember-me" id='remember_me' name='${rememberMeParameter}' <g:if test='${hasCookie}'>checked='checked'</g:if>>--}%
                            %{--<label class="label-checkbox100" for="remember_me">--}%
                                %{--Remember me--}%
                            %{--</label>--}%
                        %{--</div>--}%

                        <div>
                            <span class="txt1">
                                Lupa
                            </span>

                            <g:link controller="auth" action="forgotPassword" class="txt2">
                                password anda?
                            </g:link>
                        </div>
                    </div>

                    <div class="container-login100-form-btn">
                        <input type="submit" id="submit" class="login100-form-btn" value="Sign in" />
                    </div>
                </div>
            </form>

            <div class="login100-more" style="background-image: url(${assetPath(src:'bg-01.jpg')});"></div>
        </div>
    </div>
</div>

<asset:javascript src="jquery-3.3.1.min.js"/>
<asset:javascript src="animsition/animsition.min.js"/>
<asset:javascript src="bootstrap/popper.min.js"/>
<asset:javascript src="auth/main.js"/>

</body>
</html>
