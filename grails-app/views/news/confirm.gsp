<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${propertyNews.stage} ${propertyNews.action}</title>
    <asset:stylesheet src="summernote/summernote.css"/>
    <asset:stylesheet src="ionicons/css/ionicons.min.css" />
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Informasi Properti" description="Edit dan Update Informasi" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="propertyNews" action="index"><i class="fa fa-info"></i> News</g:link></li>
            <li>${propertyNews.action}</li>
            <li class="active">${propertyNews.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${propertyNews.stage}</h3>
            </div>
            <g:form action="${propertyNews.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${propertyNews.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${propertyNews.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${propertyNews.propName}" />
                            <p class="form-control-static">${propertyNews.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="email" value="${propertyNews.email}" />
                            <p class="form-control-static">${propertyNews.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="phoneNo" value="${propertyNews.phoneNo}" />
                            <p class="form-control-static">${propertyNews.phoneNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SMS:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="sms" value="${propertyNews.sms}" />
                            <p class="form-control-static">${propertyNews.sms}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="address" value="${propertyNews.address}" />
                            <p class="form-control-static">${propertyNews.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${propertyNews.id}" />
                            <input type="hidden" name="title" value="${propertyNews.title}" />
                            <p class="form-control-static">${propertyNews.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kata Pencarian:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="subtitle" value="${propertyNews.subtitle}" />
                            <p class="form-control-static">${propertyNews.subtitle}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Info Singkat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="summary" value="${propertyNews.summary}" />
                            <p class="form-control-static">${propertyNews.summary}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kategori:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="category" value="${propertyNews.category}" />
                            <p class="form-control-static"><i class="${propertyNews.categoryIcon}"></i>&nbsp;&nbsp;&nbsp;${propertyNews.categoryName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8" style="padding-top:0.5em">
                            <input type="hidden" name="description" value="${propertyNews.description}" />
                            <div id="smDesc">
                                <g:applyCodec encodeAs="none">
                                    ${propertyNews.description}
                                </g:applyCodec>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Target:</label>
                        <div class="col-sm-8">
                            <g:each in="${propertyNews.target}" var="obj">
                                <input type="hidden" name="target" value="${obj}" />
                            </g:each>
                            <p class="form-control-static">
                                <g:each in="${targetNameList}" var="obj">
                                    ${obj}<br/>
                                </g:each>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="imageFileId" value="${propertyNews.imageFileId}" />
                            <input type="hidden" name="imageFileName" value="${propertyNews.imageFileName}" />
                            <g:if test="${propertyNews.stage == 'Confirm'}">
                                <p class="form-control-static">${propertyNews.imageFileName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${propertyNews.imageFileId}')">${propertyNews.imageFileName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${propertyNews.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="news" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <asset:javascript src="summernote/summernote.js"/>

    <script>
        $(document).ready(function(){
            $('#smDesc').summernote();
            $('#smDesc').summernote('destroy');
        });

        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'news',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>