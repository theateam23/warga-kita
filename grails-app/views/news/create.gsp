<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Tambah News</title>

    <asset:stylesheet src="summernote/summernote.css"/>
    <asset:stylesheet src="ionicons/css/ionicons.min.css" />

    <style>
        .fa{
            font-size: 12px;
        }
    </style>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Informasi Properti" description="Edit dan Update Informasi" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="news" action="index"><i class="fa fa-info"></i> News</g:link></li>
            <li class="active">Create News</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Informasi</h3>
            </div>
            <div class="form-horizontal">
                <div class="box-body">
                    <g:form action="confirmAdd" method="POST" name="newsForm" onsubmit="return false">
                        <property:alert message="${flash.message}" type="success"/>
                        <property:alert message="${flash.error}" type="danger"/>
                        <input type="hidden" value="${propertyNews.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${propertyNews.imageFileName}" name="imageFileName" id="imageFileName" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${propertyNews.propName}" />
                                <p class="form-control-static">${propertyNews.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfEmail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-8">
                                <input type="text" name="email" class="form-control" id="tfEmail" placeholder="Email" value="${propertyNews.email}" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfPhoneNo" class="col-sm-2 control-label">Telepon:</label>
                            <div class="col-sm-8">
                                <input type="number" name="phoneNo" class="form-control" id="tfPhoneNo" placeholder="Telepon" value="${propertyNews.phoneNo}" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfSms" class="col-sm-2 control-label">SMS:</label>
                            <div class="col-sm-8">
                                <input type="number" name="sms" class="form-control" id="tfSms" placeholder="SMS" value="${propertyNews.sms}" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfAddress" class="col-sm-2 control-label">Alamat:</label>
                            <div class="col-sm-8">
                                <input type="text" name="address" class="form-control" id="tfAddress" placeholder="Alamat" value="${propertyNews.address}" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfTitle" class="col-sm-2 control-label">Judul:</label>
                            <div class="col-sm-8">
                                <input type="text" name="title" class="form-control" id="tfTitle" placeholder="Title" value="${propertyNews.title}" maxlength="200" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfSubtitle" class="col-sm-2 control-label">Kata Pencarian:</label>
                            <div class="col-sm-8">
                                <input type="text" name="subtitle" class="form-control" id="tfSubtitle" placeholder="Keywords" value="${propertyNews.subtitle}" maxlength="200" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfSummary" class="col-sm-2 control-label">Info Singkat:</label>
                            <div class="col-sm-8">
                                <input type="text" name="summary" class="form-control" id="tfSummary" placeholder="Summary" value="${propertyNews.summary}" maxlength="500" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kategori:</label>
                            <div class="col-sm-8">
                                <select name="category" id="categoryList" class="form-control">
                                    <g:each in="${categoryList}" var="cat">
                                        <option value="${cat.id}" ${cat.id == propertyNews.category?"selected":""} data-icon="${cat.icon}">${cat.name}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Keterangan:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="description" id="thDescription" />
                                <div id="smDesc">
                                    <g:applyCodec encodeAs="none">
                                    ${propertyNews.description}
                                    </g:applyCodec>
                                </div>
                                <div id="description-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Target:</label>
                            <div class="col-sm-8">
                                <g:each in="${roleList}" var="role">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="target" value="${role.id}" ${propertyNews.target?.contains(role.id)?"checked":""}>
                                            ${role.name}
                                        </label>
                                    </div>
                                </g:each>
                                <div id="target-error-msg"></div>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport" accept="image/*">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${propertyNews.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 100 KB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="news" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
                </div>
            </div>
        </div>
    </section>

    <asset:javascript src="summernote/summernote.js"/>

    <script>
        var fileUploaded = ${propertyNews.imageFileId==null || propertyNews.imageFileId==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${propertyNews.imageFileId==null || propertyNews.imageFileId==''?"false":"true"};

        $(document).ready(function(){
            $("#categoryList").select2({
                templateResult: formatState,
                templateSelection: formatState
            });

            function formatState (opt) {
                if (!opt.id) {
                    return opt.text;
                }
                var opticon = $(opt.element).data('icon');
                if(!opticon){
                    return opt.text;
                } else {
                    var $opt = $(
                            '<span><i class="' + opticon + '"></i>&nbsp;&nbsp;&nbsp;' + opt.text + '</span>'
                    );
                    return $opt;
                }
            }

            $('#smDesc').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['codeview']
                ],
                height: 200,
                disableResizeEditor: true
            });

            $("#newsForm").validate({
                ignore: ".note-editable",
                rules: {
                    'target': {
                        required: true
                    },
                    'description': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "target" ){
                        $('#target-error-msg').html('');
                        error.appendTo('#target-error-msg');
                    } else if (element.attr("name") == "description" ){
                        $('#description-error-msg').html('');
                        error.appendTo('#description-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "target" ){
                        $('#target-error-msg').html('');
                    } else if ($(element).attr("name") == "description" ){
                        $('#description-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });
            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 102400) {
                        vex.dialog.alert({
                            message: "Ukuran file terlalu besar."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            $('#thDescription').val($('#smDesc').summernote('code'));
            if($('#newsForm').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['newsForm'].submit();
                } else {
                    vex.dialog.alert({
                        message: "You haven't uploaded any image file"
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
