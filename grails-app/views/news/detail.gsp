<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail News</title>
    <asset:stylesheet src="summernote/summernote.css"/>
    <asset:stylesheet src="ionicons/css/ionicons.min.css" />
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Informasi Properti" description="Edit dan Update Informasi" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="news" action="index"><i class="fa fa-info"></i> News</g:link></li>
            <li class="active">Detail News</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.phoneNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SMS:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.sms}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.title}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kata Pencarian:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.subtitle}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Info Singkat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propertyNews.summary}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kategori:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><i class="${propertyNews.categoryIcon}"></i>&nbsp;&nbsp;&nbsp;${propertyNews.category}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8" style="padding-top: 0.5em">
                            <div id="smDesc">
                                <g:applyCodec encodeAs="none">
                                    ${propertyNews.description}
                                </g:applyCodec>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Target:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <g:each in="${targetNameList}" var="obj">
                                    ${obj}<br/>
                                </g:each>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${propertyNews.imageFileId}')">${propertyNews.imageFileName}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="news" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="news" action="edit" params="[id:propertyNews.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="news" action="delete" method="POST">
        <input type="hidden" name="id" value="${propertyNews.id}" />
    </g:form>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <asset:javascript src="summernote/summernote.js"/>

    <script>
        $(document).ready(function(){
            $('#smDesc').summernote();
            $('#smDesc').summernote('destroy');
        });
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'news',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data informasi ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>