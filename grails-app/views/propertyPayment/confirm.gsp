<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${detail.stage} ${detail.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pembayaran Properti" description="Pendaftaran Pembayaran Properti" />
        <ol class="breadcrumb">
            <li><g:link controller="propertyPayment" action="index"><i class="fa fa-money"></i> Pembayaran Properti</g:link></li>
            <li>${detail.action}</li>
            <li class="active">${detail.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${detail.stage}</h3>
            </div>
            <g:form action="submitAdd" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${detail.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${detail.action}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propId" value="${detail.propId}" />
                            <p class="form-control-static">${detail.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.totalUnit}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Jatuh Tempo Lama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.oldExpiryDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Sales:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.salesName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Pembayaran:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="paymentAmount" value="${detail.paymentAmount}" />
                            <p class="form-control-static">${detail.paymentAmount}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Komisi Sales:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.salesCommission}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Jatuh Tempo Baru:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="expiryDate" value="${detail.expiryDate}" />
                            <p class="form-control-static">${detail.expiryDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="description" value="${detail.description}" />
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Dokumen:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="attachment" value="${detail.attachment}" />
                            <input type="hidden" name="attachmentName" value="${detail.attachmentName}" />
                            <g:if test="${detail.stage == 'Confirm'}">
                                <p class="form-control-static">${detail.attachmentName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><g:link action="downloadFile" id="${detail.attachment}">${detail.attachmentName}</g:link></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${detail.stage == 'Confirm'}">
                        <g:actionSubmit controller="propertyPayment" action="create" class="btn btn-default" value="Back" />
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="propertyPayment" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>