<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Pembayaran</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Pembayaran Properti" description="Pendaftaran Pembayaran Properti" />
        <ol class="breadcrumb">
            <li><g:link controller="propertyPayment" action="index"><i class="fa fa-money"></i> Pembayaran Properti</g:link></li>
            <li class="active">Detail Pembayaran</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.totalUnit}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Sales:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.salesName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Pembayaran:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.paymentAmount}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Komisi Sales:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.salesCommission}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Jatuh Tempo:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.expiryDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Dokumen:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="attachment" value="${detail.attachment}" />
                            <input type="hidden" name="attachmentName" value="${detail.attachmentName}" />
                            <g:if test="${detail.stage == 'Confirm'}">
                                <p class="form-control-static">${detail.attachmentName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><g:link action="downloadFile" id="${detail.attachment}">${detail.attachmentName}</g:link></p>
                            </g:else>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Dibuat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.createdOn}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="propertyPayment" action="index" class="btn btn-default">Kembali</g:link>
            </div>
        </div>
    </section>
</body>
</html>