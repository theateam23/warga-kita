<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Laporan Pembayaran Properti</title>
    <asset:stylesheet src="bootstrap-datepicker/bootstrap-datepicker.min.css"/>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Pembayaran Properti" description="Pendaftaran Pembayaran Properti" />
    <ol class="breadcrumb">
        <li><g:link controller="propertyPayment" action="index"><i class="fa fa-money"></i> Pembayaran Properti</g:link></li>
        <li class="active">Laporan</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Laporan Pembayaran Properti</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <br style="clear:both" />

            <g:form action="downloadReport" class="form-horizontal" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Bulan:</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="reportMonth" class="form-control" id="tfReportMonth" value="${reportMonth}" required>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <g:actionSubmit controller="propertyPayment" action="index" class="btn btn-default" value="Kembali" />
                    <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right"><i class="fa fa-download"></i> Download</button>
                </div>
            </g:form>
        </div>
    </div>
</section>

<asset:javascript src="bootstrap-datepicker/bootstrap-datepicker.min.js"/>

<script>

    $(document).ready(function(){
        $('#tfReportMonth').datepicker({
            format: "M yyyy",
            minViewMode: 1
        });
    });

</script>
</body>
</html>