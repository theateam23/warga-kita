<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Buat Pembayaran</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Pembayaran Properti" description="Pendaftaran Pembayaran Properti" />
        <ol class="breadcrumb">
            <li><g:link controller="propertyPayment" action="index"><i class="fa fa-money"></i> Pembayaran Properti</g:link></li>
            <li class="active">Buat Pembayaran</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Buat Pembayaran</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-horizontal">
                    <g:form action="confirmAdd" method="POST" name="form1" onsubmit="return false">
                        <input type="hidden" value="${detail.attachment}" name="attachment" id="attachment" />
                        <input type="hidden" value="${detail.attachmentName}" name="attachmentName" id="attachmentName" />

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <g:select id="propList" name='propId' value="${detail.propId}"
                                          from='${propList}' class="form-control select2"
                                          optionKey="id" optionValue="name"></g:select>
                            </div>
                        </div>
                        <div class="form-group propDiv">
                            <label class="col-sm-3 control-label">Alamat:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="propAddress"></p>
                            </div>
                        </div>
                        <div class="form-group propDiv">
                            <label class="col-sm-3 control-label">Jumlah Unit:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="propTotalUnit"></p>
                            </div>
                        </div>
                        <div class="form-group propDiv">
                            <label class="col-sm-3 control-label">Tanggal Jatuh Tempo:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="propExpiryDate"></p>
                            </div>
                        </div>
                        <div class="form-group propDiv">
                            <label class="col-sm-3 control-label">Nama Sales:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="propSalesName"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jumlah Pembayaran:</label>
                            <div class="col-sm-8">
                                <input type="number" name="paymentAmount" class="form-control" maxlength="20" placeholder="Jumlah Pembayaran" value="${detail.paymentAmount}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tanggal Jatuh Tempo Baru:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="expiryDate" class="form-control" id="tfExpiryDate" value="${detail.expiryDate}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Keterangan:</label>
                            <div class="col-sm-8">
                                <textarea rows="4" cols="50" name="description" class="form-control" maxlength="300">${detail.description}</textarea>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Dokumen:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport">
                                        </span>
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${detail.attachmentName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-3 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="propertyPayment" action="index" class="btn btn-default">Batal</g:link>
                <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
            </div>
        </div>
    </section>

    <script>
        var detailPropertyUrl = '${g.createLink(controller:'propertyPayment',action:'detailPropertyJSON')}';

        var fileUploaded = ${detail.attachment==null || detail.attachment==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${detail.attachment==null || detail.attachment==''?"false":"true"};

        $(function () {
            $('#tfExpiryDate').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "locale": {
                    "format": "YYYY-MM-DD"
                }
            });
            $('#form1').validate({
                rules: {
                    paymentAmount: {
                        required: true,
                        greaterThanZero: true
                    }
                }
            });
            $('#propList').select2().on("change", function(){
                togglePropDiv();
            });
            togglePropDiv();

            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#attachment').val('');
                        $('#attachmentName').val('');
                    } else {
                        fileUploaded = true;
                        $('#attachment').val(data.result.fileId);
                        $('#attachmentName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#attachment').val("");
            $('#attachmentName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function togglePropDiv(){
            var propId = $('#propList').val();
            $('.propDiv').hide();
            if(propId!='' && propId!=null) {
                $.ajax({
                    type: 'GET',
                    url: detailPropertyUrl,
                    data: { propId: propId },
                    success: (function (output) {
                        if (output.result) {
                            $('#propAddress').html(output.address);
                            $('#propTotalUnit').html(output.totalUnit);
                            $('#propExpiryDate').html(output.expiryDate);
                            $('#propSalesName').html(output.salesName);
                            $('.propDiv').show();
                        } else {
                            vex.dialog.alert({message: output.message});
                        }
                    }),
                    error: (function (data) {
                        vex.dialog.alert({message: "System Error"})
                    }),
                    complete: (function (data) {
                    })
                });
            }
        }

        function submitForm() {
            if($('#form1').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['form1'].submit();
                } else {
                    vex.dialog.confirm({
                        message: "Anda belum mengupload dokumen apapun. Lanjut?",
                        callback: function (value) {
                            if (value) {
                                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                                document.forms['form1'].submit();
                            }
                        }
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
