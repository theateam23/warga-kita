<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Pembayaran Properti</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Pembayaran Properti" description="Pendaftaran Pembayaran Properti" />
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-money"></i> Pembayaran Properti</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pembayaran</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="propertyPayment" action="create" class="btn btn-primary"><i class="fa fa-plus"></i> Buat Pembayaran</g:link>
                <g:link controller="propertyPayment" action="report" class="btn btn-primary"><i class="fa fa-list"></i> &nbsp;Laporan</g:link>
            </div>

            <br style="clear:both" />

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Properti:</label>
                    <div class="col-sm-8">
                        <input type="text" name="propName" id="tfPropName" class="form-control" placeholder="Nama Properti" value="${propName}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Sales:</label>
                    <div class="col-sm-8">
                        <input type="text" name="salesName" id="tfSalesName" class="form-control" placeholder="Nama Sales" value="${salesName}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="createdDate" />
                            <input type="hidden" name="startDate" id="startDate" value="${startDate}"/>
                            <input type="hidden" name="endDate" id="endDate" value="${endDate}"/>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-search"></i> Cari</button>
            </g:form>

            <br style="clear: both;" />

            <table class="table table-striped table-bordered table-hover" id="dataTables-uts" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'propertyPayment',action:'detail')}';

    $(document).ready(function(){
        $('#createdDate').daterangepicker({
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": "  to  "
            },
            maxDate: new Date(),
            startDate: moment(),
            endDate: moment()
        }, function(start, end, label) {
            $("#startDate").val(start._d.getTime());
            $("#endDate").val(end._d.getTime());
        });

        var ps = $('#startDate').val();
        var pe = $('#endDate').val();
        var drp = $("#createdDate").data('daterangepicker');

        if(ps==null || ps==''){
            $("#startDate").val(drp.startDate._d.getTime());
        } else {
            drp.startDate = moment(new Date(parseInt(ps)));
        }
        if(pe==null || pe=='') {
            $("#endDate").val(drp.endDate._d.getTime());
        } else {
            drp.endDate = moment(moment(new Date(parseInt(pe))));
        }
        drp.updateElement();

        $('#dataTables-uts').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'propertyPayment',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propName", "value": $('#tfPropName').val() } );
                aoData.push( { "name": "salesName", "value": $('#tfSalesName').val() } );
                aoData.push( { "name": "startDate", "value": $('#startDate').val() } );
                aoData.push( { "name": "endDate", "value": $('#endDate').val() } );
            },
            "aoColumns": [
                { "mData": "id", "sTitle":"Tanggal Buat", "mRender":
                    function(data, type, full){
                        return "<a href='"+detailUrl+"/"+full.id+"'>"+full.createdDate+"</a>";
                    }},
                { "mData": "propertyName", "sTitle":"Nama Properti" },
                { "mData": "salesName", "sTitle":"Nama Sales" },
                { "mData": "paymentAmount", "sTitle":"Jumlah Pembayaran" },
                { "mData": "salesCommission", "sTitle":"Komisi Sales" },
                { "mData": "expiryDate", "sTitle":"Tanggal Jatuh Tempo" }
            ]
        });
    });

</script>
</body>
</html>