<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Visitor</title>
    <style>
        .no-padding{
            padding:0
        }
    </style>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Laporan Pengunjung" description="Tampilkan dan Download Laporan Pengunjung" />
    <ol class="breadcrumb">
        <li>Report</li>
        <li class="active"><i class="fa fa-users"></i> Visitor</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pengunjung</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;" name="form1">

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="visitDate" />
                            <input type="hidden" name="startDate" id="startDate" value="${startDate}"/>
                            <input type="hidden" name="endDate" id="endDate" value="${endDate}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="pull-right" id="downloadDiv">
                        <button type="button" onclick="submitDownload()" class="btn btn-primary"><i class="fa fa-download"></i> Download Laporan</button>
                        <button type="button" onclick="submitSearch()" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </g:form>


            <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUserUrl = '${g.createLink(controller:'visitor',action:'detail')}';

    $(document).ready(function(){
        $('#visitDate').daterangepicker({
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": "  to  "
            },
            startDate: moment(),
            endDate: moment().add(6,'days')
        }, function(start, end, label) {
            $("#startDate").val(start._d.getTime());
            $("#endDate").val(end._d.getTime());
        });

        var ps = $('#startDate').val();
        var pe = $('#endDate').val();
        var drp = $("#visitDate").data('daterangepicker');

        if(ps==null || ps==''){
            $("#startDate").val(drp.startDate._d.getTime());
        } else {
            drp.startDate = moment(new Date(parseInt(ps)));
        }
        if(pe==null || pe=='') {
            $("#endDate").val(drp.endDate._d.getTime());
        } else {
            drp.endDate = moment(moment(new Date(parseInt(pe))));
        }
        drp.updateElement();

        $('#dataTables-users').DataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'visitor',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "startDate", "value": $('#startDate').val() } );
                aoData.push( { "name": "endDate", "value": $('#endDate').val() } );
            },
            "aoColumns": [
                { "mData": "visitDate", "sTitle":"Tanggal Kunjung", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUserUrl+"/"+full.id+"'>"+full.visitDate+"</a>";
                        }
                },
                { "mData": "name", "sTitle":"Nama" },
//                { "mData": "phoneNo", "sTitle":"No Telepon" },
//                { "mData": "vehicleNo", "sTitle":"Plat Nomor" },
                { "mData": "status", "sTitle":"Status" }
            ]
        });
    });

    function submitSearch(){
        $('#form1').attr("action",'${g.createLink(controller:'visitor',action:'index')}');
        $('#form1').submit();
    }

    function submitDownload(){
        $('#form1').attr("action",'${g.createLink(controller:'visitor',action:'downloadReport')}');
        $('#form1').submit();
    }

</script>
</body>
</html>