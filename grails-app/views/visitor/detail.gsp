<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Visitor</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Visitor" description="View Visitor Log" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="visitor" action="index"><i class="fa fa-users"></i> Visitor</g:link></li>
            <li class="active">Detail Visitor</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${visitor.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${visitor.phoneNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vehicle No:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${visitor.vehicleNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Visit Tanggal:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${visitor.visitDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${visitor.status}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Picture:</label>
                        <div class="col-sm-8" style="padding-top: 10px">
                            <img src="${g.createLink(controller:'visitor', action:'showImage', params:[id:visitor.id])}" style="max-height: 150px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="visitor" action="index" class="btn btn-default">Kembali</g:link>
            </div>
        </div>
    </section>
</body>
</html>