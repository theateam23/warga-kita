<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Vehicle</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Vehicle Maintenance" description="Tambah dan Edit Vehicle" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="vehiclePm" action="index"><i class="fa fa-car"></i> Vehicle</g:link></li>
            <li class="active">Detail Vehicle</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${vehicle.id}" />
                            <p class="form-control-static">${vehicle.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.unitName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Plate No:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.plateNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vehicle Type:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.vehicleType}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Brand:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.brand}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Color:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.color}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tax Due Tanggal:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${vehicle.taxDueDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${vehicle.imageFileId}')">${vehicle.imageFileName}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="vehiclePm" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="vehiclePm" action="editVehicle" params="[id:vehicle.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="vehiclePm" action="deleteVehicle" method="POST">
        <input type="hidden" name="id" value="${vehicle.id}" />
    </g:form>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'vehiclePm',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }

        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data kendaraan ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>