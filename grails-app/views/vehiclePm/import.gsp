<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Vehicle Maintenance</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Vehicle Maintenance" description="Tambah, Edit, Upload File Vehicle" />
        <ol class="breadcrumb">
            <li><i class="fa fa-car"></i> Vehicle Maintenance</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">File Import:</label>
                        <g:form action="importFile" method="post" controller="vehiclePm" name="vehicleForm" onsubmit="return false">
                            <input type="hidden" value="${fileImportId}" name="fileImportId" id="fileImportId" />
                        </g:form>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult"></label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="button" onclick="submitForm()" class="btn btn-primary"><i class="fa fa-save"></i> Lanjutkan</button>
                </div>
            </div>
        </div>
    </section>

    <script>
        var enableContinue = false;
        var f = null;
        var isErr = false;
        var isChangeFile = ${fileImportId==null || fileImportId==''?"false":"true"};
        $(function () {
            if (isChangeFile) {
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function () {
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                enableContinue = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size + "b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if (data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width', '0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                },
                done: function (e, data) {
                    if (data.result.result == false) {
                        isErr = true;
                        enableContinue = false;
                        $('#fileImportId').val('');
                    } else {
                        enableContinue = true;
                        $('#fileImportId').val(data.result.fileId);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut("slow", function () {
                        if (isErr) {
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail: function (e, data) {
                    isErr = true;
                    $('#progress').fadeOut("slow", function () {
                        if (isErr) {
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#fileImportId').val("");
            enableContinue = false;
        }

        function submitForm() {
            if($('#vehicleForm').valid()) {
                if (enableContinue) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['vehicleForm'].submit();
                } else {
                    vex.dialog.alert({
                        message: 'Please upload a file'
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
