<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Tambah Vehicle</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Vehicle Maintenance" description="Tambah, Edit, Upload File Vehicle" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="vehiclePm" action="index"><i class="fa fa-car"></i> Vehicle</g:link></li>
            <li class="active">Create Vehicle</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Kendaraah</h3>
            </div>
            <div class="form-horizontal">
                <div class="box-body">
                    <g:form action="confirmAdd" method="POST" name="form1" onsubmit="return false">
                        <property:alert message="${flash.message}" type="success"/>
                        <property:alert message="${flash.error}" type="danger"/>
                        <input type="hidden" value="${vehicle.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${vehicle.imageFileName}" name="imageFileName" id="imageFileName" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${vehicle.propName}" />
                                <p class="form-control-static">${vehicle.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="areaList" class="col-sm-2 control-label">Area:</label>
                            <div class="col-sm-8">
                                <g:select id="areaList" name='areaId' value="${vehicle.areaId}"
                                          from='${areaList}' class="form-control select2"
                                          optionKey="id" optionValue="name"></g:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="unitList" class="col-sm-2 control-label">Unit:</label>
                            <div class="col-sm-8">
                                <input type="hidden" id="unitIdVal" value="${vehicle.unitId}" />
                                <select id="unitList" name='unitId' class="form-control select2"></select>
                                <div id="unitId-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Plate No:</label>
                            <div class="col-sm-8">
                                <input type="text" name="plateNo" class="form-control" id="tfPlateNo" maxlength="200" placeholder="Plate No" value="${vehicle.plateNo}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Vehicle Type:</label>
                            <div class="col-sm-8">
                                <g:select id="vehicleTypeList" name='vehicleType' value="${vehicle.vehicleType}"
                                          from='${vehicleTypeList}' class="form-control select2" optionKey="code" optionValue="name"></g:select>
                                <div id="vehicleType-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Brand:</label>
                            <div class="col-sm-8">
                                <input type="text" name="brand" class="form-control" id="tfBrand" maxlength="200" placeholder="Brand" value="${vehicle.brand}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Color:</label>
                            <div class="col-sm-8">
                                <input type="text" name="color" class="form-control" id="tfColor" maxlength="200" placeholder="Color" value="${vehicle.color}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tax Due Tanggal:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="taxDueDate" class="form-control" id="tfTaxDueDate" value="${vehicle.taxDueDate}" required>
                                </div>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="uploadImage">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport" accept="image/*">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${vehicle.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="vehiclePm" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
                </div>
            </div>
        </div>
    </section>

    <script>
        var unitByAreaUrl = '${g.createLink(controller:'vehiclePm', action:'unitByAreaJSON')}';
        var fileUploaded = ${vehicle.imageFileId==null || vehicle.imageFileId==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${vehicle.imageFileId==null || vehicle.imageFileId==''?"false":"true"};

        $(document).ready(function(){
            $("#vehicleForm").validate({
                rules: {
                    'unitId': {
                        required: true
                    },
                    'vehicleType': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                        error.appendTo('#unitId-error-msg');
                    } else if (element.attr("name") == "vehicleType" ){
                        $('#vehicleType-error-msg').html('');
                        error.appendTo('#status-error-msg');
                    } else {
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("name") == "unitId" ){
                        $('#unitId-error-msg').html('');
                    } else if ($(element).attr("name") == "vehicleType" ){
                        $('#vehicleType-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });

            $('#areaList').select2({
                placeholder: "Select area",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            }).on("change",function(){
                updateUnitList();
            });

            $('#unitList').select2({
                placeholder: "Select Unit",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            });

            $('#vehicleTypeList').select2();

            $('#tfTaxDueDate').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "locale": {
                    "format": "YYYY-MM-DD"
                }
            });

            updateUnitList();

            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });
        });

        function updateUnitList(){
            $('#unitList').html('');
            var area = $('#areaList');
            if(area.val()!='' && area.val()!=null) {
                $.ajax({
                    type: 'GET',
                    url: unitByAreaUrl,
                    data: {areaId: area.val()},
                    success: (function (output) {
                        if (output.result) {
                            $.each(output.data, function() {
                                var newOption = new Option(this.name, this.id, false, false);
                                $('#unitList').append(newOption);
                            });
                            var unitIdVal = $('#unitIdVal').val();
                            if(unitIdVal!=''){
                                $('#unitList').val(unitIdVal).trigger('change');
                            }
                        } else {
                            vex.dialog.alert({message: output.message});
                        }
                    }),
                    error: (function (data) {
                        vex.dialog.alert({message: "System Error"})
                    }),
                    complete: (function (data) {
                    })
                });
            }
        }

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            if($('#form1').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['form1'].submit();
                } else {
                    vex.dialog.confirm({
                        message: "You haven't uploaded any image file. Continue?",
                        callback: function (value) {
                            if (value) {
                                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                                document.forms['form1'].submit();
                            }
                        }
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
