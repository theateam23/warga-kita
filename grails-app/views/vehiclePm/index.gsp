<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Vehicle Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Laporan Kendaraan" description="Tampilkan dan Download Laporan Kendaraan" />
    <ol class="breadcrumb">
        <li>Report</li>
        <li class="active"><i class="fa fa-car"></i> Vehicle</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Kendaraan</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                %{--<g:link controller="vehiclePm" action="createVehicle" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Vehicle</g:link>--}%
                %{--<g:link controller="vehiclePm" action="uploadFile" class="btn btn-primary"><i class="fa fa-upload"></i> Upload File</g:link>--}%
            </div>

            <br style="clear:both"/>

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;" name="form1">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Properti:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${propName}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="areaList" class="col-sm-2 control-label">Area:</label>
                    <div class="col-sm-8">
                        <g:select id="areaList" name='areaId' value="${areaId}"
                                  noSelection="${['':'Tampilkan Semua']}"
                                  from='${areaList}' class="form-control select2"
                                  optionKey="id" optionValue="name"></g:select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="unitList" class="col-sm-2 control-label">Unit:</label>
                    <div class="col-sm-8">
                        <input type="hidden" id="unitIdVal" value="${unitId}"/>
                        <select id="unitList" name='unitId' class="form-control select2"></select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="pull-right" id="downloadDiv">
                        <button type="button" onclick="submitDownload()" class="btn btn-primary"><i class="fa fa-download"></i> Download Laporan</button>
                        <button type="button" onclick="submitSearch()" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </g:form>

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-vehicles" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailVehicleUrl = '${g.createLink(controller:'vehiclePm',action:'detailVehicle')}';
    var unitByAreaUrl = '${g.createLink(controller:'vehiclePm',action:'unitByAreaJSON')}';

    $(document).ready(function(){
        $('#areaList').select2().on("change",function(){
            updateUnitList();
        });
        $('#unitList').select2();
        $('#dataTables-vehicles').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'vehiclePm',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "areaId", "value": $('#areaList').val() } );
                aoData.push( { "name": "unitId", "value": $('#unitIdVal').val() } );
            },
            "aoColumns": [
//                { "mData": "plateNo", "sTitle":"Plate No", "mRender":
//                        function(data, type, full){
//                            return "<a href='"+detailVehicleUrl+"/"+full.id+"'>"+full.plateNo+"</a>";
//                        }
//                },
                { "mData": "plateNo", "sTitle":"Plat Nomor" },
                { "mData": "areaName", "sTitle":"Area" },
                { "mData": "unitName", "sTitle":"Unit" },
                { "mData": "vehicleType", "sTitle":"Jenis Kendaraan" },
                { "mData": "taxDueDate", "sTitle":"Masa Berlaku STNK" }
            ]
        });

        updateUnitList();
    });

    function updateUnitList(){
        $('#unitList').html('');
        var newOption = new Option("Tampilkan semua", "", false, false);
        $('#unitList').append(newOption);
        var area = $('#areaList');
        if(area.val()!='' && area.val()!=null) {
            $.ajax({
                type: 'GET',
                url: unitByAreaUrl,
                data: {areaId: area.val()},
                success: (function (output) {
                    if (output.result) {
                        $.each(output.data, function() {
                            var newOption = new Option(this.name, this.id, false, false);
                            $('#unitList').append(newOption);
                        });
                        var unitIdVal = $('#unitIdVal').val();
                        if(unitIdVal!=''){
                            $('#unitList').val(unitIdVal).trigger('change');
                        }
                    } else {
                        vex.dialog.alert({message: output.message});
                    }
                }),
                error: (function (data) {
                    vex.dialog.alert({message: "System Error"})
                }),
                complete: (function (data) {
                })
            });
        }
    }

    function submitSearch(){
        $('#form1').attr("action",'${g.createLink(controller:'vehiclePm',action:'index')}');
        $('#form1').submit();
    }

    function submitDownload(){
        $('#form1').attr("action",'${g.createLink(controller:'vehiclePm',action:'downloadReport')}');
        $('#form1').submit();
    }
</script>
</body>
</html>