<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${vehicle.stage} ${vehicle.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Vehicle Maintenance" description="Tambah dan Edit Vehicle" />
        <ol class="breadcrumb">
            <li>Report</li>
            <li><g:link controller="vehiclePm" action="index"><i class="fa fa-car"></i> Vehicle</g:link></li>
            <li>${vehicle.action}</li>
            <li class="active">${vehicle.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${vehicle.stage}</h3>
            </div>
            <g:form action="${vehicle.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${vehicle.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${vehicle.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${vehicle.propName}" />
                            <p class="form-control-static">${vehicle.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="areaId" value="${vehicle.areaId}" />
                            <p class="form-control-static">${vehicle.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="unitId" value="${vehicle.unitId}" />
                            <p class="form-control-static">${vehicle.unitName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Plate No:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${vehicle.id}" />
                            <input type="hidden" name="plateNo" value="${vehicle.plateNo}" />
                            <p class="form-control-static">${vehicle.plateNo}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vehicle Type:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="vehicleType" value="${vehicle.vehicleType}" />
                            <p class="form-control-static">${vehicle.vehicleType}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Brand:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="brand" value="${vehicle.brand}" />
                            <p class="form-control-static">${vehicle.brand}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Color:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="color" value="${vehicle.color}" />
                            <p class="form-control-static">${vehicle.color}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tax Due Tanggal:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="taxDueDate" value="${vehicle.taxDueDate}" />
                            <p class="form-control-static">${vehicle.taxDueDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="imageFileId" value="${vehicle.imageFileId}" />
                            <input type="hidden" name="imageFileName" value="${vehicle.imageFileName}" />
                            <g:if test="${vehicle.stage == 'Confirm'}">
                                <p class="form-control-static">${vehicle.imageFileName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${vehicle.imageFileId}')">${vehicle.imageFileName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${vehicle.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="vehiclePm" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'vehiclePm',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>