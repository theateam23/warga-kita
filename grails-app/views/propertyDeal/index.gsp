<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Property Deal</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Properti Prospek" description="Usulan dan Pengajuan Properti Prospek" />
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-stamp"></i> Property Lead</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Properti Prospek</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="propertyDeal" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Ajukan Prospek Baru</g:link>

            <br style="clear:both" />

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Properti:</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="tfName" class="form-control" placeholder="Name" value="${name}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status:</label>
                    <div class="col-sm-8">
                        <g:select id="statusList" name='status' value="${status}"
                                  noSelection="${['':'All']}"
                                  from='${statusList}' class="form-control select2"
                                  optionKey="id" optionValue="name"></g:select>
                    </div>
                </div>

                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-search"></i> Cari</button>
            </g:form>

            <br style="clear: both;" />

            <table class="table table-striped table-bordered table-hover" id="dataTables-uts" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'propertyDeal',action:'detail')}';

    $(document).ready(function(){
        $('#statusList').select2();
        $('#dataTables-uts').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'propertyDeal',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "name", "value": $('#tfName').val() } );
                aoData.push( { "name": "status", "value": $('#statusList').val() } );
            },
            "aoColumns": [
                { "mData": "name", "sTitle":"Properti", "mRender":
                    function(data, type, full){
                        return "<a href='"+detailUrl+"/"+full.id+"'>"+full.name+"</a>";
                    }},
                { "mData": "address", "sTitle":"Alamat" },
                { "mData": "developer", "sTitle":"Developer" },
//                { "mData": "propertyType", "sTitle":"Tipe" },
                { "mData": "status", "sTitle":"Status" },
//                { "mData": "startDate", "sTitle":"Tanggal Mulai" },
//                { "mData": "dayCount", "sTitle":"Hitung Hari" }
            ]
        });
    });

</script>
</body>
</html>