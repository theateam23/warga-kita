<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Property Deal</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Prospek" description="Usulan dan Pengajuan Properti Prospek" />
        <ol class="breadcrumb">
            <li><g:link controller="propertyDeal" action="index"><i class="fa fa-stamp"></i> Property Lead</g:link></li>
            <li class="active">Detail Property Lead</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${detail.id}" />
                            <p class="form-control-static">${detail.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Developer:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.developer}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tipe:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.propertyType}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Created:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.createdDate} by ${detail.createdBy}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.status}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar Properti:</label>
                        <div class="col-sm-8" style="padding-top: 7px">
                            <g:if test="${detail.attachment!=null && detail.attachment!=""}">
                                <g:link action="downloadFile" id="${detail.attachment}">${detail.attachmentName}</g:link>
                            </g:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="propertyDeal" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:if test="${detail.canTake}">
                        <g:link controller="propertyDeal" action="takeDeal" params="[id:detail.id]" class="btn btn-primary"><i class="fa fa-hand-o-up"></i> Ambil Prospek</g:link>
                    </g:if>
                    <g:if test="${detail.canEdit}">
                        <g:link controller="propertyDeal" action="edit" params="[id:detail.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Ubah</g:link>
                    </g:if>
                    <g:if test="${detail.canSubmit}">
                        <g:link controller="propertyDeal" action="submitDeal" params="[id:detail.id]" class="btn btn-primary"><i class="fa fa-upload"></i> Ajukan Prospek </g:link>
                    </g:if>
                    <g:if test="${detail.canRelease}">
                        <g:link controller="propertyDeal" action="releaseDeal" params="[id:detail.id]" class="btn btn-info"><i class="fa fa-undo"></i> Lepas Prospek </g:link>
                    </g:if>
                    <g:if test="${detail.canApprove}">
                        <g:link controller="propertyDeal" action="approveDeal" params="[id:detail.id]" class="btn btn-info"><i class="fa fa-thumbs-up"></i> Setujui Prospek</g:link>
                    </g:if>
                    <g:if test="${detail.canDelete}">
                        <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </g:if>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="propertyDeal" action="delete" method="POST">
        <input type="hidden" name="id" value="${detail.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus deal properti ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>