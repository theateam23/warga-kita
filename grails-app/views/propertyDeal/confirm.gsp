<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${detail.stage} ${detail.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Properti Prospek" description="Usulan dan Pengajuan Properti Prospek" />
        <ol class="breadcrumb">
            <li><g:link controller="propertyDeal" action="index"><i class="fa fa-stamp"></i> Property Lead</g:link></li>
            <li>${detail.action}</li>
            <li class="active">${detail.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${detail.stage}</h3>
            </div>
            <g:form action="${detail.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${detail.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${detail.action}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${detail.id}" />
                            <input type="hidden" name="name" value="${detail.name}" />
                            <p class="form-control-static">${detail.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Developer:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="developer" value="${detail.developer}" />
                            <p class="form-control-static">${detail.developer}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="address" value="${detail.address}" />
                            <p class="form-control-static">${detail.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tipe:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propertyTypeId" value="${detail.propertyTypeId}" />
                            <p class="form-control-static">${detail.propertyTypeName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Keterangan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="description" value="${detail.description}" />
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Gambar Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="attachment" value="${detail.attachment}" />
                            <input type="hidden" name="attachmentName" value="${detail.attachmentName}" />
                            <g:if test="${detail.stage == 'Confirm'}">
                                <p class="form-control-static">${detail.attachmentName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><g:link action="downloadFile" id="${detail.attachment}">${detail.attachmentName}</g:link></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${detail.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="propertyDeal" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>