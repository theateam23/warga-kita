<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Approve Deal</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Properti Prospek" description="Usulan dan Pengajuan Properti Prospek" />
    <ol class="breadcrumb">
        <li><g:link controller="propertyDeal" action="index"><i class="fa fa-stamp"></i> Property Lead</g:link></li>
        <li class="active">Approve Lead</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Approval Information</h3>
        </div>
        <g:form action="confirmApproveDeal" class="form-horizontal" method="POST" name="form1">
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Properti:</label>
                    <div class="col-sm-8">
                        <input type="hidden" name="id" value="${detail.id}" />
                        <p class="form-control-static">${detail.name}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Developer:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.developer}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.address}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tipe:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.propertyType}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.description}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Created:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.createdDate} by ${detail.createdBy}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${detail.status}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Upload Gambar Properti:</label>
                    <div class="col-sm-8" style="padding-top: 7px">
                        <g:if test="${detail.attachment!=null && detail.attachment!=""}">
                            <g:link action="downloadFile" id="${detail.attachment}">${detail.attachmentName}</g:link>
                        </g:if>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Code:</label>
                    <div class="col-sm-8">
                        <input type="text" name="code" class="form-control" placeholder="Code" value="${detail.code}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jumlah Unit:</label>
                    <div class="col-sm-8">
                        <input type="number" name="totalUnit" class="form-control" placeholder="Total Unit" value="${detail.totalUnit}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Fee:</label>
                    <div class="col-sm-8">
                        <input type="number" name="fee" class="form-control" placeholder="Fee" value="${detail.fee}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Expiry Tanggal:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="expiryDate" class="form-control" id="tfExpiryDate" value="${detail.expiryDate}" required>
                        </div>
                        <div id="expiryDate-error-msg"></div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link action="index" class="btn btn-default">Batal</g:link>
                <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
            </div>
        </g:form>
    </div>
</section>
<script>
    $(document).ready(function() {
        $("#form1").validate({
            rules: {
                'expiryDate': {
                    required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "expiryDate" ){
                    $('#expiryDate-error-msg').html('');
                    error.appendTo('#expiryDate-error-msg');
                } else {
                    error.insertAfter(element);
                }
            },
            success: function(label,element) {
                if ($(element).attr("name") == "expiryDate" ){
                    $('#expiryDate-error-msg').html('');
                } else {
                    $(label).remove();
                }
            },
            submitHandler: function(form) {
                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                form.submit();
            }
        });
        $('#tfExpiryDate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            "locale": {
                "format": "YYYY-MM-DD"
            },
            autoUpdateInput: false
        }, function(chosen_date) {
            $('#tfExpiryDate').val(chosen_date.format('YYYY-MM-DD'));
        });
    });
</script>
</body>
</html>