<!doctype html>
<html>
<head>
    <title>Property Manager</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <asset:stylesheet src="bootstrap/bootstrap.min.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <asset:stylesheet src="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"/>
    <asset:stylesheet src="animate/animate.css"/>
    <asset:stylesheet src="css-hamburgers/hamburgers.min.css"/>
    <asset:stylesheet src="animsition/animsition.min.css"/>
    <asset:stylesheet src="auth/main.css"/>
    <asset:stylesheet src="auth/util.css"/>
</head>
<body class="bg-boxed">
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form reg100-form">
                    <span class="login100-form-title p-b-25">
                        SIGN UP
                    </span>

                    <div class="w-full text-center p-t-5 p-b-5">
                        <span class="txt4">
                            &nbsp;<g:if test='${flash.message}'>${flash.message}</g:if>
                        </span>
                    </div>

                    <div class="wrap-inputr100 validate-input" data-validate="Name is required">
                        <span class="label-inputr100">Name</span>
                        <input class="inputr100" type="text" name="name" placeholder="Name">
                        <span class="focus-inputr100"></span>
                    </div>

                    <div class="wrap-inputr100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <span class="label-inputr100">Email</span>
                        <input class="inputr100" type="text" name="email" placeholder="Email addess">
                        <span class="focus-inputr100"></span>
                    </div>

                    <div class="wrap-inputr100 validate-input" data-validate="Username is required">
                        <span class="label-inputr100">Username</span>
                        <input class="inputr100" type="text" name="username" placeholder="Username">
                        <span class="focus-inputr100"></span>
                    </div>

                    <div class="wrap-inputr100 validate-input" data-validate = "Password is required">
                        <span class="label-inputr100">Password</span>
                        <input class="inputr100" type="password" name="pass" placeholder="*************">
                        <span class="focus-inputr100"></span>
                    </div>

                    <div class="wrap-inputr100 validate-input" data-validate = "Confirm Password is required">
                        <span class="label-inputr100">Confirm Password</span>
                        <input class="inputr100" type="password" name="repeat-pass" placeholder="*************">
                        <span class="focus-inputr100"></span>
                    </div>

                    <div class="flex-m w-full p-b-33">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                <span class="txt1">
                                    I agree to the
                                    <a href="#" class="txt2 hov1">
                                        Terms of User
                                    </a>
                                </span>
                            </label>
                        </div>


                    </div>

                    <div class="container-reg100-form-btn p-b-31">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn">
                                Sign Up
                            </button>
                        </div>

                        <g:link controller="dashboard" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
                            Sign In
                            <i class="fa fa-long-arrow-right m-l-5"></i>
                        </g:link>
                    </div>
                </form>

                <div class="login100-more" style="background-image: url(${assetPath(src:'bg-01.jpg')});"></div>
            </div>
        </div>
    </div>

    <asset:javascript src="jquery-3.3.1.min.js"/>
    <asset:javascript src="animsition/animsition.min.js"/>
    <asset:javascript src="bootstrap/popper.min.js"/>
    <asset:javascript src="auth/register.js"/>
</body>
</html>
