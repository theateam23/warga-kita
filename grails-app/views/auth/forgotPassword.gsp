<!doctype html>
<html>
<head>
    <title>Warga Kita</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <asset:link rel="icon" href="wargaku_logo_only.png" type="image/x-ico" />
    <asset:stylesheet src="bootstrap/bootstrap.min.css"/>
    <asset:stylesheet src="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <asset:stylesheet src="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"/>
    <asset:stylesheet src="animate/animate.css"/>
    <asset:stylesheet src="css-hamburgers/hamburgers.min.css"/>
    <asset:stylesheet src="animsition/animsition.min.css"/>
    <asset:stylesheet src="auth/main.css"/>
    <asset:stylesheet src="auth/util.css"/>
</head>
<body class="bg-boxed">
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <g:form class="login100-form validate-form" action="resetPassword">
                <div class="w-full text-center" style="background-color: #26A69A">
                    <asset:image src="wargaku_logo_only.png" style="max-height:300px" />
                </div>
                <div class="login-form">
                    <span class="login100-form-title p-b-15">
                        Lupa Password
                    </span>

                    <div class="w-full text-center p-t-5 p-b-5">
                        <span class="txt4">
                            &nbsp;<g:if test='${flash.message}'>${flash.message}</g:if>
                        </span>
                    </div>

                    <div class="w-full p-t-10 p-b-10">
                        <span class="txt5">
                            Masukan email anda, dan kami akan mengirim tautan untuk mereset password anda.
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="input100" type="text" name="email">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Reset Password saya
                        </button>
                    </div>

                    <div class="w-full text-center p-t-30 p-b-20">
                        <span class="txt1">
                            Anda sudah punya akun?&nbsp;
                        </span>
                        <g:link controller="auth" class="txt3">
                            Sign In
                        </g:link>
                    </div>
                </div>
            </g:form>

            <div class="login100-more" style="background-image: url(${assetPath(src:'bg-01.jpg')});"></div>
        </div>
    </div>
</div>

<asset:javascript src="jquery-3.3.1.min.js"/>
<asset:javascript src="animsition/animsition.min.js"/>
<asset:javascript src="bootstrap/popper.min.js"/>
<asset:javascript src="auth/forgotPassword.js"/>
</body>
</html>
