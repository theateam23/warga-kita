<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Mobile User</title>
    <style>
        .no-padding{
            padding:0
        }
    </style>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Persetujuan User Mobile" description="Setuju dan Tolak User Mobile" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li class="active"><i class="fa fa-users"></i> Approval Mobile User</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Permintaan Mobile User</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            %{--<div class="form-horizontal" style="margin-bottom: 20px;">--}%
                %{--<div class="form-group">--}%
                    %{--<label class="col-sm-2 control-label">Properti:</label>--}%
                    %{--<div class="col-sm-8">--}%
                        %{--<p class="form-control-static">${propName}</p>--}%
                    %{--</div>--}%
                %{--</div>--}%
            %{--</div>--}%

            <br style="clear:both;"/>

            <div class="form-group">
                <div class="pull-right">
                    <button type="button" onclick="confirmApprove()" class="btn btn-info"><i class="fa fa-check"></i> Setuju</button>
                    <button type="button" onclick="confirmReject()" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                </div>
            </div>

            <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<g:form name="formApprove" controller="userApproval" action="approveUser" method="POST">

</g:form>

<g:form name="formReject" controller="userApproval" action="rejectUser" method="POST">

</g:form>

<script>
    var tableUser;
    var userIds = [];

    $(document).ready(function(){
        tableUser = $('#dataTables-users').DataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'userApproval',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propId", "value": $('#propertyList').val() } );
            },
            'columnDefs': [{
                'targets': 0,
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    if(userIds.indexOf(data)!=-1){
                        return '<input type="checkbox" name="id[]" onchange="updateUserId(this,\''+data+'\')" checked="checked" value="' + $('<div/>').text(data).html() + '">';
                    } else {
                        return '<input type="checkbox" name="id[]" onchange="updateUserId(this,\''+data+'\')" value="' + $('<div/>').text(data).html() + '">';
                    }
                }
            }],
            'order': [[1, 'asc']],
            "aoColumns": [
                { "mData": "id", "sTitle":'<input type="checkbox" value="1" id="cbAll" data-toggle="tooltip" title="Select all users on this page only">' },
                { "mData": "email", "sTitle":"Email" },
                { "mData": "fullName", "sTitle":"Nama" },
                { "mData": "area", "sTitle":"Area" },
                { "mData": "subArea", "sTitle":"Sub Area" },
                { "mData": "unitName", "sTitle":"Unit" },
                { "mData": "role", "sTitle":"Status" },
                { "mData": "createdDate", "sTitle":"Tanggal Dibuat" }
            ]
        });

        $('#cbAll').on('click', function(){
            var rows = tableUser.rows({ 'search': 'applied' }).nodes();
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
            updateUserIds();
        });

        $('#dataTables-users tbody').on('change', 'input[type="checkbox"]', function(){
            if(!this.checked){
                var el = $('#cbAll').get(0);
                if(el && el.checked && ('indeterminate' in el)){
                    $(el).prop('checked',false);
                }
            }
        });
        tableUser.on('draw.dt', function() {
            $('#cbAll').prop("checked",false);
        });
        $('[data-toggle="tooltip"]').tooltip();
    });

    function updateUserIds(){
        tableUser.$('input[type="checkbox"]').each(function(){
            if($(this).prop("checked")){
                if(userIds.indexOf(this.value)==-1){
                    userIds.push(this.value);
                }
            } else {
                if(userIds.indexOf(this.value)!=-1){
                    userIds.splice($.inArray(this.value, userIds),1);
                }
            }
        });
    }

    function updateUserId(el,userId){
        if($(el).prop("checked")){
            if(userIds.indexOf(userId)==-1){
                userIds.push(userId);
            }
        } else {
            if(userIds.indexOf(userId)!=-1){
                userIds.splice($.inArray(userId, userIds),1);
            }
        }
    }

    function confirmApprove(){
        if(userIds.length==0){
            vex.dialog.alert({message: 'Tidak ada user yang dipilih'});
        } else {
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menyetujui user-user ini?',
                callback: function (value) {
                    if (value) {
                        $.each(userIds,function(){
                            $('#formApprove').append(
                                    $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', 'userId')
                                            .val(this)
                            );
                        });
                        $('#formApprove').submit();
                    }
                }
            });
        }
    }

    function confirmReject(){
        if(userIds.length==0){
            vex.dialog.alert({message: 'Tidak ada user yang dipilih'});
        } else {
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menolak user-user ini?',
                callback: function (value) {
                    if (value) {
                        $.each(userIds,function(){
                            $('#formReject').append(
                                    $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', 'userId')
                                            .val(this)
                            );
                        });
                        $('#formReject').submit();
                    }
                }
            });
        }
    }

</script>
</body>
</html>