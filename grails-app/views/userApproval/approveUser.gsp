<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Upload Result</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Persetujuan User Mobile" description="Setuju dan Tolak User Mobile" />
    <ol class="breadcrumb">
        <li>Activity</li>
        <li><g:link action="index"><i class="fa fa-users"></i> Approval Mobile User</g:link></li>
        <li class="active">Result</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Permintaan User Mobile</h3>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <table class="table table-striped table-bordered table-hover" id="dataTables-users" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Area</th>
                            <th>Sub Area</th>
                            <th>Unit</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${result}" status="i" var="obj">
                            <tr>
                                <td>${obj.email}</td>
                                <td>${obj.name}</td>
                                <td>${obj.area}</td>
                                <td>${obj.subArea}</td>
                                <td>${obj.unitName}</td>
                                <td>${obj.role}</td>
                                <td>${obj.status}</td>
                                <td>${obj.message}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="box-footer">
                    <g:link action="index" class="btn btn-info pull-right">Selesai</g:link>
                </div>

            </div>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#dataTables-users').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "sPaginationType": "simple_numbers"
        });
    });

</script>
</body>
</html>