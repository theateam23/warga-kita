<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Property Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Properti" description="Tambah dan Edit Properti" />
    <ol class="breadcrumb">
        <li><i class="fa fa-gear"></i> Maintenance</li>
        <li class="active">Property</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Properti</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="propertyAdmin" action="createProperty" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Property</g:link>

            <table class="table table-striped table-bordered table-hover" id="dataTables-props" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="pmInfoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle">Detail Property Manager</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="pmEmail"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="pmName"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">No Telepon:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="pmPhoneNo"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="clear:both;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var detailPropUrl = '${g.createLink(controller:'propertyAdmin',action:'detailProperty')}';
    var detailPmUrl = '${g.createLink(controller:'propertyAdmin',action:'detailPmJSON')}';

    $(document).ready(function(){
        $('#dataTables-props').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'propertyAdmin',action:'propertyListDatatable')}',
            "aoColumns": [
                { "mData": "name", "sTitle":"Nama", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailPropUrl+"/"+full.id+"'>"+full.name+"</a>";
                        }
                },
                { "mData": "developer", "sTitle":"Developer" },
                { "mData": "address", "sTitle":"Alamat" },
                { "mData": "propertyManager", "sTitle":"Property Manager", "mRender":
                        function(data, type, full){
                            return "<a href='#' onclick=doDetailPm('" + full.pmId + "')>"+full.pmName+"</a>";
                        }
                }
            ]
        });
    });

    function doDetailPm(pmId){
        $.ajax({
            type: 'GET',
            url: detailPmUrl,
            data: {id: pmId},
            success: (function (output) {
                if (output.result) {
                    $('#pmEmail').html(output.username);
                    $('#pmName').html(output.name);
                    $('#pmPhoneNo').html(output.phoneNo);
                    $('#pmInfoModal').modal('show');
                } else {
                    vex.dialog.alert({message: output.message});
                }
            }),
            error: (function (data) {
                vex.dialog.alert({message: "System Error"})
            }),
            complete: (function (data) {
            })
        });
    }

</script>
</body>
</html>