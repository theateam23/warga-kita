<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Property</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Properti" description="Tambah dan Edit Properti" />
        <ol class="breadcrumb">
            <li><i class="fa fa-gear"></i> Maintenance</li>
            <li><g:link controller="propertyAdmin" action="index"> Property</g:link></li>
            <li class="active">Detail Property</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Code:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.code}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Developer:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.developer}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Property Type:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.propertyTypeName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Unit:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.totalUnit}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti Manager:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${property.pmText}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Security Phone No:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<g:if test="${property.securityPhoneNo!=null}">--}%
                                %{--<p class="form-control-static">--}%
                                    %{--<g:each in="${property.securityPhoneNo.split(";")}" var="obj">--}%
                                        %{--${obj}<br/>--}%
                                    %{--</g:each>--}%
                                %{--</p>--}%
                            %{--</g:if>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Security Emails:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<g:if test="${property.securityEmails!=null}">--}%
                                %{--<p class="form-control-static">--}%
                                    %{--<g:each in="${property.securityEmails.split(";")}" var="obj">--}%
                                        %{--${obj}<br/>--}%
                                    %{--</g:each>--}%
                                %{--</p>--}%
                            %{--</g:if>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mobile Background:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${property.mobileBackground}')">${property.mobileBackgroundName}</a></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Web Login Background:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${property.webLoginBackground}')">${property.webLoginBackgroundName}</a></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Logo:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static"><a href="#" onclick="showImage('${property.logo}')">${property.logoName}</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="propertyAdmin" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="propertyAdmin" action="editProperty" params="[id:property.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <g:form name="formDelete" controller="propertyAdmin" action="deleteProperty" method="POST">
        <input type="hidden" name="id" value="${property.id}" />
    </g:form>

    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data properti ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }

        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'propertyAdmin',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>