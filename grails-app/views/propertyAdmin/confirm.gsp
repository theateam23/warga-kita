<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${property.stage} ${property.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Properti" description="Tambah dan Edit Properti" />
        <ol class="breadcrumb">
            <li><i class="fa fa-gear"></i> Maintenance</li>
            <li><g:link controller="propertyAdmin" action="index"> Property</g:link></li>
            <li>${property.action}</li>
            <li class="active">${property.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${property.stage}</h3>
            </div>
            <g:form action="${property.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${property.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${property.action}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${property.id}" />
                            <input type="hidden" name="name" value="${property.name}" />
                            <p class="form-control-static">${property.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Code:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="code" value="${property.code}" />
                            <p class="form-control-static">${property.code}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Developer:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="developer" value="${property.developer}" />
                            <p class="form-control-static">${property.developer}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alamat:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="address" value="${property.address}" />
                            <p class="form-control-static">${property.address}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Property Type:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propertyTypeId" value="${property.propertyTypeId}" />
                            <p class="form-control-static">${property.propertyTypeName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Unit:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="totalUnit" value="${property.totalUnit}" />
                            <p class="form-control-static">${property.totalUnit}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti Manager:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="pmId" value="${property.pmId}" />
                            <input type="hidden" name="pmText" value="${property.pmText}" />
                            <p class="form-control-static">${property.pmText}</p>
                        </div>
                    </div>
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Security Phone No:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<g:if test="${property.securityPhoneNo!=null}">--}%
                                %{--<input type="hidden" name="securityPhoneNo" value="${property.securityPhoneNo}" />--}%
                                %{--<p class="form-control-static">--}%
                                    %{--<g:each in="${property.securityPhoneNo.split(";")}" var="obj">--}%
                                        %{--${obj}<br/>--}%
                                    %{--</g:each>--}%
                                %{--</p>--}%
                            %{--</g:if>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-3 control-label">Security Emails:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<g:if test="${property.securityEmails!=null}">--}%
                                %{--<input type="hidden" name="securityEmails" value="${property.securityEmails}" />--}%
                                %{--<p class="form-control-static">--}%
                                    %{--<g:each in="${property.securityEmails.split(";")}" var="obj">--}%
                                        %{--${obj}<br/>--}%
                                    %{--</g:each>--}%
                                %{--</p>--}%
                            %{--</g:if>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mobile Background:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="mobileBackground" value="${property.mobileBackground}" />
                            <input type="hidden" name="mobileBackgroundName" value="${property.mobileBackgroundName}" />
                            <g:if test="${property.stage == 'Confirm'}">
                                <p class="form-control-static">${property.mobileBackgroundName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${property.mobileBackground}')">${property.mobileBackgroundName}</a></p>
                            </g:else>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Web Login Background:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="webLoginBackground" value="${property.webLoginBackground}" />
                            <input type="hidden" name="webLoginBackgroundName" value="${property.webLoginBackgroundName}" />
                            <g:if test="${property.stage == 'Confirm'}">
                                <p class="form-control-static">${property.webLoginBackgroundName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${property.webLoginBackground}')">${property.webLoginBackgroundName}</a></p>
                            </g:else>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Logo:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="logo" value="${property.logo}" />
                            <input type="hidden" name="logoName" value="${property.logoName}" />
                            <g:if test="${property.stage == 'Confirm'}">
                                <p class="form-control-static">${property.logoName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${property.logo}')">${property.logoName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${property.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="propertyAdmin" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'propertyAdmin',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>