<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <meta name="layout" content="main"/>
    <title>Edit Property</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Data Properti" description="Tambah dan Edit Properti" />
        <ol class="breadcrumb">
            <li><i class="fa fa-gear"></i> Maintenance</li>
            <li><g:link controller="propertyAdmin" action="index"> Property</g:link></li>
            <li class="active">Edit Property</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah</h3>
            </div>
            <div class="form-horizontal">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>

                    <g:form action="confirmEdit" method="POST" name="form1" onsubmit="return false">
                        <input type="hidden" value="${property.mobileBackground}" name="mobileBackground" id="mobileBackground" />
                        <input type="hidden" value="${property.mobileBackgroundName}" name="mobileBackgroundName" id="mobileBackgroundName" />
                        <input type="hidden" value="${property.webLoginBackground}" name="webLoginBackground" id="webLoginBackground" />
                        <input type="hidden" value="${property.webLoginBackgroundName}" name="webLoginBackgroundName" id="webLoginBackgroundName" />
                        <input type="hidden" value="${property.logo}" name="logo" id="logo" />
                        <input type="hidden" value="${property.logoName}" name="logoName" id="logoName" />

                        <div class="form-group">
                            <label for="tfName" class="col-sm-3 control-label">Nama:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="${property.id}" />
                                <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${property.name}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Code:</label>
                            <div class="col-sm-8">
                                <input type="text" name="code" class="form-control" maxlength="200" placeholder="Code" value="${property.code}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfDeveloper" class="col-sm-3 control-label">Developer:</label>
                            <div class="col-sm-8">
                                <input type="text" name="developer" class="form-control" id="tfDeveloper" maxlength="200" placeholder="Developer" value="${property.developer}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfAddress" class="col-sm-3 control-label">Alamat:</label>
                            <div class="col-sm-8">
                                <input type="text" name="address" class="form-control" id="tfAddress" maxlength="200" placeholder="Address" value="${property.address}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="propertyTypeList" class="col-sm-3 control-label">Property Type:</label>
                            <div class="col-sm-8">
                                <g:select id="propertyTypeList" name='propertyTypeId' value="${property.propertyTypeId}"
                                          from='${propertyTypeList}' class="form-control select2"
                                          optionKey="id" optionValue="name"></g:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jumlah Unit:</label>
                            <div class="col-sm-8">
                                <input type="number" name="totalUnit" class="form-control" maxlength="20" placeholder="Total Unit" value="${property.totalUnit}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pmList" class="col-sm-3 control-label">Properti Manager:</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" id="pmList" style="width: 400px;"></select>
                                <input type="hidden" id="pmId" name="pmId" value="${property.pmId}" required/>
                                <input type="hidden" id="pmText" name="pmText" value="${property.pmText}" />
                            </div>
                        </div>
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">Security Phone Numbers:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="text" name="securityPhoneNo" class="form-control" maxlength="500" placeholder="Security Phone No" value="${property.securityPhoneNo}">--}%
                                %{--* separate with ; for example: 08080123456;0808654321--}%
                            %{--</div>--}%
                        %{--</div>--}%
                        %{--<div class="form-group">--}%
                            %{--<label class="col-sm-3 control-label">Security Emails:</label>--}%
                            %{--<div class="col-sm-8">--}%
                                %{--<input type="text" name="securityEmails" class="form-control" maxlength="500" placeholder="Security Email" value="${property.securityEmails}">--}%
                                %{--* separate with ; for example: user1@mail.com;user2@mail.com--}%
                            %{--</div>--}%
                        %{--</div>--}%
                    </g:form>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mobile Background:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnTextmb"></span>
                                            <input type="file" name="fileImportmb" id="fileImportmb">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResultmb">${property.mobileBackgroundName}</label>
                                        <input type="button" id="buttonUploadmb" class="btn btn-warning" onclick="submitFile('mb')" value="Upload File"/>
                                        <input type="button" id="buttonDeletemb" class="btn btn-danger" onclick="removeFile('mb')" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progressmb">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-3 upload-tooltip">Ukuran maksimum file adalah 100 KB</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Web Login Background:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnTextlb"></span>
                                            <input type="file" name="fileImportlb" id="fileImportlb">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResultlb">${property.webLoginBackgroundName}</label>
                                        <input type="button" id="buttonUploadlb" class="btn btn-warning" onclick="submitFile('lb')" value="Upload File"/>
                                        <input type="button" id="buttonDeletelb" class="btn btn-danger" onclick="removeFile('lb')" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progresslb">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-3 upload-tooltip">Ukuran maksimum file adalah 200 KB</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Logo:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnTextlogo"></span>
                                            <input type="file" name="fileImportlogo" id="fileImportlogo">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResultlogo">${property.logoName}</label>
                                        <input type="button" id="buttonUploadlogo" class="btn btn-warning" onclick="submitFile('logo')" value="Upload File"/>
                                        <input type="button" id="buttonDeletelogo" class="btn btn-danger" onclick="removeFile('logo')" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progresslogo">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-3 upload-tooltip">Ukuran maksimum file adalah 200 KB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="propertyAdmin" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
                </div>
            </div>
        </div>
    </section>

    <script>
        var fileUploadedmb = ${property.mobileBackground==null || property.mobileBackground==''?"false":"true"};
        var fileUploadedlb = ${property.webLoginBackground==null || property.webLoginBackground==''?"false":"true"};
        var fileUploadedlogo = ${property.logo==null || property.logo==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFilemb = ${property.mobileBackground==null || property.mobileBackground==''?"false":"true"};
        var isChangeFilelb = ${property.webLoginBackground==null || property.webLoginBackground==''?"false":"true"};
        var isChangeFilelogo = ${property.logo==null || property.logo==''?"false":"true"};
        var pmListUrl = '${g.createLink(controller:'propertyAdmin',action:'pmListJSON')}';

        $(document).ready(function(){
            $('#form1').validate({
                ignore: [],
                rules: {
                    totalUnit: {
                        greaterThanZero: true
                    }
                }
            });
            $('#propertyTypeList').select2();
            $('#pmList').select2({
                placeholder: "Select Property Manager",
                allowClear: true,
                ajax: {
                    url: pmListUrl,
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page,
                            page_limit: 7,
                            pmId: '${property.pmId}'
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 7) < data.totalCount
                            }
                        };
                    }
                },
                escapeMarkup: function (m) { return m; }
            }).on("change",function(){
                var detail = $('#pmList').select2('data');
                if(detail.length>0){
                    $('#pmId').val(detail[0].id);
                    $('#pmText').val(detail[0].text);
                } else {
                    $('#pmId').val('');
                    $('#pmText').val('');
                }
                $('#form1').validate().element("#pmId");
            });
            initDroplist();

            if(isChangeFilemb){
                $('#btnTextmb').html("Ganti File");
                $('#buttonDeletemb').show();
            } else {
                $('#btnTextmb').html("Pilih File");
                $('#buttonDeletemb').hide();
            }
            if(isChangeFilelb){
                $('#btnTextlb').html("Ganti File");
                $('#buttonDeletelb').show();
            } else {
                $('#btnTextlb').html("Pilih File");
                $('#buttonDeletelb').hide();
            }
            if(isChangeFilelogo){
                $('#btnTextlogo').html("Ganti File");
                $('#buttonDeletelogo').show();
            } else {
                $('#btnTextlogo').html("Pilih File");
                $('#buttonDeletelogo').hide();
            }

            $('#buttonUploadmb').hide();
            $('#buttonUploadlb').hide();
            $('#buttonUploadlogo').hide();
            $('#progressmb').hide();
            $('#progresslb').hide();
            $('#progresslogo').hide();
            $("#fileImportmb").on("change", function(){
                isErr = false;
                $('#uploadResultmb').removeClass("success");
                $('#uploadResultmb').removeClass("error");
                fileUploadedmb = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResultmb').html(content);
            });
            $("#fileImportlb").on("change", function(){
                isErr = false;
                $('#uploadResultlb').removeClass("success");
                $('#uploadResultlb').removeClass("error");
                fileUploadedlb = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResultlb').html(content);
            });
            $("#fileImportlogo").on("change", function(){
                isErr = false;
                $('#uploadResultlogo').removeClass("success");
                $('#uploadResultlogo').removeClass("error");
                fileUploadedlogo = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResultlogo').html(content);
            });

            var codes = [];
            codes.push('mb');
            codes.push('lb');
            codes.push('logo');
            $.each(codes,function(){
                var cd = this;
                var imageFileId = "", imageFileName = "", maxFileSize = 0;
                if(cd=='mb') {
                    imageFileId = 'mobileBackground';
                    imageFileName = 'mobileBackgroundName';
                    maxFileSize = 102400;
                } else if(cd=='lb'){
                    imageFileId = 'webLoginBackground';
                    imageFileName = 'webLoginBackgroundName';
                    maxFileSize = 204800;
                } else if(cd=='logo'){
                    imageFileId = 'logo';
                    imageFileName = 'logoName';
                    maxFileSize = 204800;
                }

                $('#fileImport'+cd).fileupload({
                    singleFileUploads: true,
                    context: $('#fileImport'+cd)[0],
                    add: function (e, data) {
                        if(data.originalFiles[0]['size'] > maxFileSize) {
                            vex.dialog.alert({
                                message: "Ukuran file terlalu besar."
                            });
                        } else {
                            $('#progress'+cd+' .progress-bar').css('width','0%');
                            $('#buttonUpload'+cd).show();
                            f = data;
                        }
                    },
                    progress: function(e, data){
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress'+cd+' .progress-bar').css('width',progress + '%');
                    },
                    done: function (e, data) {
                        if(data.result.result==false){
                            isErr = true;
                            $('#'+imageFileId).val('');
                            $('#'+imageFileName).val('');
                            setFileUploaded(cd, false);
                        } else {
                            setFileUploaded(cd, true);
                            $('#'+imageFileId).val(data.result.fileId);
                            $('#'+imageFileName).val(data.result.fileName);
                        }
                        $('#btnText'+cd).html("Ganti File");
                        $('#progress'+cd).fadeOut( "slow", function() {
                            if(isErr){
                                $('#uploadResult'+cd).append(" - Upload Error");
                                $('#uploadResult'+cd).removeClass("success");
                                $('#uploadResult'+cd).addClass("error");
                            } else {
                                $('#uploadResult'+cd).append(" - Upload Completed");
                                $('#uploadResult'+cd).removeClass("error");
                                $('#uploadResult'+cd).addClass("success");
                                $('#buttonDelete'+cd).show();
                            }
                        });
                    },
                    fail:function(e, data){
                        var isErr = true;
                        $('#progress'+cd).fadeOut( "slow", function() {
                            if(isErr){
                                $('#uploadResult'+cd).append(" - Upload Error");
                                $('#uploadResult'+cd).removeClass("success");
                                $('#uploadResult'+cd).addClass("error");
                            } else {
                                $('#uploadResult'+cd).append(" - Upload Completed");
                                $('#uploadResult'+cd).removeClass("error");
                                $('#uploadResult'+cd).addClass("success");
                                $('#buttonDelete'+cd).show();
                            }
                        });
                    }

                });
            });
        });

        function setFileUploaded(cd, val){
            if(cd=='mb') {
                fileUploadedmb = val;
            } else if(cd=='lb'){
                fileUploadedlb = val;
            } else if(cd=='logo'){
                fileUploadedlogo = val;
            }
        }

        function removeFile(cd){
            var imageFileId = "", imageFileName = "";
            if(cd=='mb') {
                imageFileId = 'mobileBackground';
                imageFileName = 'mobileBackgroundName';
                fileUploadedmb = false;
                isChangeFilemb = false;
            } else if(cd=='lb'){
                imageFileId = 'webLoginBackground';
                imageFileName = 'webLoginBackgroundName';
                fileUploadedlb = false;
                isChangeFilelb = false;
            } else if(cd=='logo'){
                imageFileId = 'logo';
                imageFileName = 'logoName';
                fileUploadedlogo = false;
                isChangeFilelogo = false;
            }
            $('#buttonDelete'+cd).hide();
            $("#fileImport"+cd).val('');
            $('#btnText'+cd).html("Pilih File");
            $('#uploadResult'+cd).html("");
            $('#'+imageFileId).val("");
            $('#'+imageFileName).val("");
        }

        function initDroplist(){
            var val = $('#pmId').val();
            if(val!=''){
                var option = new Option($('#pmText').val(), val, true, true);
                $('#pmList').append(option).trigger('change');
            }
        }

        function submitFile(cd){
            $('#buttonUpload'+cd).hide();
            $('#progress'+cd).show();
            f.submit();
        }

        function submitForm() {
            if($('#form1').valid()){
                if(fileUploadedmb && fileUploadedlb && fileUploadedlogo) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['form1'].submit();
                } else {
                    vex.dialog.confirm({
                        message: "You haven't uploaded any image file. Continue?",
                        callback: function (value) {
                            if (value) {
                                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                                document.forms['form1'].submit();
                            }
                        }
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
