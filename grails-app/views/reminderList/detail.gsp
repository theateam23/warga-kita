<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Reminder</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Daftar Pengingat" description="Tambah dan Ubah Pengingat" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="reminderList" action="index"><i class="fa fa-info"></i> Reminder List</g:link></li>
            <li class="active">Detail Reminder</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Deskripsi:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.description}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ingatkan setiap tanggal:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.reminderDay}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pesan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static preline">${detail.message}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.status}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Dibuat oleh:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.createdBy} on ${detail.createdDate}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Diubah oleh:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${detail.updatedBy} on ${detail.updatedDate}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="reminderList" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="reminderList" action="edit" params="[id:detail.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Ubah</g:link>
                    <g:if test="${detail.isActive=="Y"}">
                        <button type="button" onclick="confirmAction('mendeaktivasi')" class="btn btn-danger"><i class="fa fa-times"></i> Nonaktifkan</button>
                    </g:if>
                    <g:else>
                        <button type="button" onclick="confirmAction('mengaktivasi')" class="btn btn-danger"><i class="fa fa-check"></i> Aktivasi</button>
                    </g:else>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDeactivate" controller="reminderList" action="deactivate" method="POST">
        <input type="hidden" name="id" value="${detail.id}" />
    </g:form>

    <g:form name="formActivate" controller="reminderList" action="activate" method="POST">
        <input type="hidden" name="id" value="${detail.id}" />
    </g:form>

    <script>
        function confirmAction(msg){
            vex.dialog.confirm({
                message: 'Apakah anda yaking ingin '+msg+' reminder ini?',
                callback: function (value) {
                    if (value) {
                        if(msg=='mendeaktivasi'){
                            $('#formDeactivate').submit();
                        } else {
                            $('#formActivate').submit();
                        }
                    }
                }
            });
        }
    </script>
</body>
</html>
