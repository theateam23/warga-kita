<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Add Reminder</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Daftar Pengingat" description="Tambah dan Ubah Pengingat" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="reminderList" action="index"><i class="fa fa-info"></i> Reminder List</g:link></li>
            <li class="active">Add Reminder</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Pengingat</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${detail.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfDescription" class="col-sm-3 control-label">Deskripsi:</label>
                        <div class="col-sm-8">
                            <input type="text" name="description" class="form-control" id="tfDescription" maxlength="200" placeholder="Description" value="${detail.description}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ingatkan setiap tanggal:</label>
                        <div class="col-sm-8">
                            <input type="number" name="reminderDay" placeholder="1-28" class="form-control" maxlength="2" value="${detail.reminderDay}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pesan:</label>
                        <div class="col-sm-8">
                            <textarea rows="4" cols="50" name="message" class="form-control" placeholder="Message" maxlength="500" required>${detail.message}</textarea>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="reminderList" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#form1').validate({
                rules: {
                    reminderDay: {
                        required: true,
                        greaterThanZero: true
                    },
                    deadline: {
                        required: true,
                        greaterThanZero: true
                    }
                },
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>
</body>
</html>
