<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Reminder List</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Daftar Pengingat" description="Tambah dan Ubah Pengingat" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-info"></i> Reminder List</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pengingat</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <g:link controller="reminderList" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Pengingat</g:link>

            <table class="table table-striped table-bordered table-hover" id="dataTables-obj" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'reminderList',action:'detail')}';

    $(document).ready(function(){
        $('#dataTables-obj').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'reminderList',action:'listDatatable')}',
            "aoColumns": [
                { "mData": "name", "sTitle":"Nama", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.name+"</a>";
                        }
                },
                { "mData": "description", "sTitle":"Deskripsi" },
                { "mData": "createdDate", "sTitle":"Dibuat Tanggal" },
                { "mData": "createdBy", "sTitle":"Dibuat Oleh" },
                { "mData": "status", "sTitle":"Status" }
            ]
        });
    });

</script>
</body>
</html>
