<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Confirm Upload</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Unit Properti" description="Tambah, Edit, Upload File Unit" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li><g:link controller="unitPm" action="index"><i class="fa fa-gear"></i> Unit</g:link></li>
        <li>Upload File</li>
        <li class="active">Confirm</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar of Unit from File</h3>
        </div>
        <g:form action="submitImport" class="form-horizontal" method="POST">
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="areaId" value="${areaId}" />
                            <p class="form-control-static">${areaName}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <input type="hidden" name="fileImportId" value="${fileImportId}" />
                <table class="table table-striped table-bordered table-hover" id="dataTables-units" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Line</th>
                            <th>Name</th>
                            <th>Sub Area</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${unitList}" status="i" var="obj">
                            <tr>
                                <td>${obj.cnt}</td>
                                <td>${obj.name}</td>
                                <td>${obj.subArea}</td>
                                <td>${obj.unitType}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="box-footer">
                    <g:link controller="unitPm" action="uploadFile" class="btn btn-default">Kembali</g:link>
                    <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                </div>

            </div>
        </g:form>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $('#dataTables-units').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "sPaginationType": "simple_numbers"
        });
    });

</script>
</body>
</html>