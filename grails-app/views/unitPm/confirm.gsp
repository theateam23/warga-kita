<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${unit.stage} ${unit.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Unit Maintenance" description="Tambah dan Edit Unit" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="unitPm" action="index"><i class="fa fa-gear"></i> Unit</g:link></li>
            <li>${unit.action}</li>
            <li class="active">${unit.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${unit.stage}</h3>
            </div>
            <g:form action="${unit.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${unit.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${unit.action}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${unit.propName}" />
                            <p class="form-control-static">${unit.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="areaId" value="${unit.areaId}" />
                            <p class="form-control-static">${unit.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${unit.id}" />
                            <input type="hidden" name="name" value="${unit.name}" />
                            <p class="form-control-static">${unit.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sub Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="subArea" value="${unit.subArea}" />
                            <p class="form-control-static">${unit.subArea}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tipe Unit:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="unitTypeId" value="${unit.unitTypeId}" />
                            <p class="form-control-static">${unit.unitTypeName}</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${unit.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="unitPm" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>
</body>
</html>