<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Tambah Unit</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Unit Properti" description="Tambah, Edit, Upload File Unit" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="unitPm" action="index"><i class="fa fa-gear"></i> Unit</g:link></li>
            <li class="active">Create Unit</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Unit</h3>
            </div>
            <g:form action="confirmAdd" class="form-horizontal" method="POST" name="form1">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success"/>
                    <property:alert message="${flash.error}" type="danger"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${unit.propName}" />
                            <p class="form-control-static">${unit.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="areaList" class="col-sm-3 control-label">Area:</label>
                        <div class="col-sm-8">
                            <g:select id="areaList" name='areaId' value="${unit.areaId}"
                                      from='${areaList}' class="form-control select2"
                                      optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tfName" class="col-sm-3 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${unit.name}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Area:</label>
                        <div class="col-sm-8">
                            <input type="text" name="subArea" class="form-control" maxlength="200" placeholder="Sub Area" value="${unit.subArea}">
                        </div>
                        <label class="col-sm-offset-3 upload-tooltip">misal: Lantai 1, Blok A dll</label>
                    </div>
                    <div class="form-group">
                        <label for="unitTypeList" class="col-sm-3 control-label">Tipe Unit:</label>
                        <div class="col-sm-8">
                            <g:select id="unitTypeList" name='unitTypeId' value="${unit.unitTypeId}"
                                      from='${unitTypeList}' class="form-control select2"
                                      optionKey="id" optionValue="name"></g:select>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="unitPm" action="index" class="btn btn-default">Batal</g:link>
                    <button type="submit" class="btn btn-info pull-right">Konfirmasi</button>
                </div>
            </g:form>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $('#areaList').select2({
                placeholder: "Select area",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            });

            $('#unitTypeList').select2({
                placeholder: "Select Unit Type",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            });

            $('#form1').validate({
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }
            });
        });
    </script>
</body>
</html>