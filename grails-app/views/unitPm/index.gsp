<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Unit Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Unit Properti" description="Tambah, Edit, Upload File Unit" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-gear"></i> Unit</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Unit</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="unitPm" action="createUnit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Unit</g:link>
                <g:link controller="unitPm" action="uploadFile" class="btn btn-primary"><i class="fa fa-upload"></i> Upload File</g:link>
            </div>

            <br style="clear:both"/>

            <g:form action="index" class="form-horizontal col-md-7" method="GET" style="margin-bottom: 20px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Properti:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">${propName}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="areaList" class="col-sm-2 control-label">Area:</label>
                    <div class="col-sm-8">
                        <g:select id="areaList" name='areaId' value="${areaId}"
                                  noSelection="${['':'Tampilkan Semua']}"
                                  from='${areaList}' class="form-control select2"
                                  optionKey="id" optionValue="name"></g:select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="unitTypeList" class="col-sm-2 control-label">Tipe Unit:</label>
                    <div class="col-sm-8">
                        <g:select id="unitTypeList" name='unitTypeId' value="${unitTypeId}"
                                  noSelection="${['':'Tampilkan Semua']}"
                                  from='${unitTypeList}' class="form-control select2"
                                  optionKey="id"></g:select>
                    </div>
                </div>

                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-search"></i> Cari</button>
            </g:form>

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-units" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUnitUrl = '${g.createLink(controller:'unitPm',action:'detailUnit')}';

    $(document).ready(function(){
        $('#areaList').select2();
        $('#unitTypeList').select2();

        $('#dataTables-units').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'unitPm',action:'unitListDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "areaId", "value": $('#areaList').val() } );
                aoData.push( { "name": "unitTypeId", "value": $('#unitTypeList').val() } );
            },
            "columnDefs": [
                { "orderable": false, "targets": 5 }
            ],
            "aoColumns": [
                { "mData": "name", "sTitle":"Nama", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUnitUrl+"/"+full.id+"'>"+full.name+"</a>";
                        }
                },
//                { "mData": "propName", "sTitle":"Properti" },
                { "mData": "areaName", "sTitle":"Area" },
                { "mData": "subArea", "sTitle":"Sub Area" },
                { "mData": "typeName", "sTitle":"Tipe Unit" },
                { "mData": "resident", "sTitle":"Nama Pemilik" },
                { "mData": "tenant", "sTitle":"Nama Penyewa" }
            ]
        });
    });

</script>
</body>
</html>