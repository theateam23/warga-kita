<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Change Password</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Change Password" description="Update your Password" />
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Change Password</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <g:form action="updatePassword" method="POST" name="changePassForm">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Current Password:</label>
                            <div class="col-sm-8">
                                <input type="password" name="oldPass" class="form-control" placeholder="Current Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">New Password:</label>
                            <div class="col-sm-8">
                                <input type="password" name="newPass" class="form-control" placeholder="New Password" maxlength="50" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm New Password:</label>
                            <div class="col-sm-8">
                                <input type="password" name="confirmNewPass" class="form-control" placeholder="Confirm New Password" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <g:link controller="dashboard" class="btn btn-default">Batal</g:link>
                        <button type="submit" id="continueBtn" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
                    </div>
                </g:form>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $.validator.addMethod("matchPassword", function(value, element) {
                var pass = $('input[name="newPass"]').val();
                if(pass!=$(element).val()){
                    return false;
                }
                return true;
            }, "Password doesn't match");

            $("#changePassForm").validate({
                rules: {
                    newPass: {
                        required: true,
                        minlength: 8
                    },
                    confirmNewPass: {
                        required: true,
                        matchPassword: true,
                        minlength: 8
                    }
                },
                submitHandler: function(form) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    form.submit();
                }

            });

        });

    </script>
</body>
</html>