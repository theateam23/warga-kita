<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Profile</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Profile" description="Update Your Profile" />
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Profile</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-horizontal">
                    <g:form action="submitEdit" method="POST" name="profileForm" onsubmit="return false">
                        <input type="hidden" value="${profile.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${profile.imageFileName}" name="imageFileName" id="imageFileName" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">${profile.email}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${profile.name}" required>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Profile Picture:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport" accept="image/*">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${profile.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 2 MB</label>
                    </div>
                </div>
                <div class="box-footer">
                    <g:link controller="profile" action="index" class="btn btn-default">Batal</g:link>
                    <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </section>

    <script>
        var fileUploaded = ${profile.imageFileId==null || profile.imageFileId==''?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${profile.imageFileId==null || profile.imageFileId==''?"false":"true"};

        $(document).ready(function(){
            $("#profileForm").validate();
            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 2097152) {
                        vex.dialog.alert({
                            message: "Filesize is too big."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                            $('#buttonDelete').show();
                        }
                    });
                }

            });

            $('#propList').select2({
                placeholder: "Select Property",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            });
        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            if($('#profileForm').valid()){
                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                document.forms['profileForm'].submit();
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>
