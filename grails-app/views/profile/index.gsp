<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail News</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Profile" description="Update Your Profile" />
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Profile</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${profile.email}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${profile.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Profile Picture:</label>
                        <g:if test="${profile.imageFileId!=null}">
                            <div class="col-sm-8" style="padding-top: 10px">
                                <img src="${g.createLink(controller:'profile', action:'showPicture', params:[id:profile.imageFileId])}" style="max-height: 150px" />
                            </div>
                        </g:if>
                        <g:else>
                            <div class="col-sm-8">
                                <p class="form-control-static">No Picture</p>
                            </div>
                        </g:else>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <g:link controller="profile" action="edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>