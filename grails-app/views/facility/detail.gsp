<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Detail Fasilitas</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Fasilitas Umum" description="Tambah dan Edit Fasilitas Umum" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="facility" action="index"><i class="fa fa-gear"></i> Facility</g:link></li>
            <li class="active">Detail Facility</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <div class="box-body" id="testBody">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${facility.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${facility.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${facility.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Syarat dan Ketentuan:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">${facility.tnc}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8" style="padding-top: 10px">
                            <g:if test="${facility.imageFileId!=null && facility.imageFileId!=""}">
                                <img src="${createLink(controller:'facility', action: 'showImage', params:[id:facility.imageFileId])}" style="max-height:200px;" />
                            </g:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="facility" action="index" class="btn btn-default">Kembali</g:link>
                <div class="pull-right">
                    <g:link controller="facility" action="edit" params="[id:facility.id]" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</g:link>
                    <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </section>

    <g:form name="formDelete" controller="facility" action="delete" method="POST">
        <input type="hidden" name="id" value="${facility.id}" />
    </g:form>


    <script>
        function confirmDelete(){
            vex.dialog.confirm({
                message: 'Apakah anda yakin untuk menghapus data fasilitas umum ini?',
                callback: function (value) {
                    if (value) {
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
</body>
</html>