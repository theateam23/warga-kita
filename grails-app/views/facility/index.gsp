<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Facility Maintenance</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Data Fasilitas Umum" description="Tambah dan Edit Fasilitas Umum" />
    <ol class="breadcrumb">
        <li>Maintenance</li>
        <li class="active"><i class="fa fa-gear"></i> Facility</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Fasilitas Umum</h3>
        </div>
        <div class="box-body">
            <property:alert message="${flash.message}" type="success"/>
            <property:alert message="${flash.error}" type="danger"/>

            <div class="pull-right">
                <g:link controller="facility" action="create" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Fasilitas Umum</g:link>
            </div>

            <br style="clear:both"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-facilities" style="width: 100%">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<script>
    var detailUrl = '${g.createLink(controller:'facility',action:'detail')}';

    $(document).ready(function(){
        $('#dataTables-facilities').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'facility',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propId", "value": $('#propertyList').val() } );
            },
            "aoColumns": [
                { "mData": "propName", "sTitle":"Properti" },
                { "mData": "areaName", "sTitle":"Area" },
                { "mData": "name", "sTitle":"Nama", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailUrl+"/"+full.id+"'>"+full.name+"</a>";
                        }
                }
            ]
        });
    });

</script>
</body>
</html>