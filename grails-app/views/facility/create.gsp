<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<g:applyLayout name="fileUploadLayout">
<head>
    <title>Tambah Fasilitas</title>
</head>

<body>
<content tag="fileUploadContent">
    <section class="content-header">
        <property:pageHeader title="Data Fasilitas Umum" description="Tambah dan Edit Fasilitas Umum" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="facility" action="index"><i class="fa fa-gear"></i> Facility</g:link></li>
            <li class="active">Create Facility</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Fasilitas Umum</h3>
            </div>
            <div class="box-body">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <div class="form-horizontal">
                    <g:form action="confirmAdd" method="POST" name="facilityForm" onsubmit="return false">
                        <input type="hidden" value="${facility.imageFileId}" name="imageFileId" id="imageFileId" />
                        <input type="hidden" value="${facility.imageFileName}" name="imageFileName" id="imageFileName" />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Properti:</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="propName" value="${facility.propName}" />
                                <p class="form-control-static">${facility.propName}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="areaList" class="col-sm-2 control-label">Area:</label>
                            <div class="col-sm-8">
                                <g:select id="areaList" name='areaId' value="${facility.areaId}"
                                          from='${areaList}' class="form-control select2"
                                          optionKey="id" optionValue="name"></g:select>
                                <div id="area-error-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tfName" class="col-sm-2 control-label">Nama:</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control" id="tfName" maxlength="200" placeholder="Name" value="${facility.name}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Syarat dan Ketentuan:</label>
                            <div class="col-sm-8">
                                <textarea rows="4" cols="50" name="tnc" class="form-control" placeholder="Term and Condition" maxlength="500" required>${facility.tnc}</textarea>
                            </div>
                        </div>
                    </g:form>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <g:uploadForm method="post" action="upload">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-sm-12">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-file"></i>
                                            <span id="btnText"></span>
                                            <input type="file" name="fileImport" id="fileImport">
                                        </span>
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <label id="uploadResult">${facility.imageFileName}</label>
                                        <input type="button" id="buttonUpload" class="btn btn-warning" onclick="submitFile()" value="Upload File"/>
                                        <input type="button" id="buttonDelete" class="btn btn-danger" onclick="removeFile()" value="Remove File"/>
                                    </div>
                                </div>
                                <div id="progress">
                                    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success"style="width: 0%"></div></div>
                                </div>
                            </g:uploadForm>
                        </div>
                        <label class="col-sm-offset-2 upload-tooltip">Ukuran maksimum file adalah 100 KB</label>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <g:link controller="facility" action="index" class="btn btn-default">Batal</g:link>
                <button type="button" id="continueBtn" onclick="submitForm()" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Konfirmasi</button>
            </div>
        </div>
    </section>

    <script>
        var areaByPropertyUrl = '${g.createLink(controller:'facility', action:'areaByPropertyJSON')}';
        var fileUploaded = ${facility.imageFileId==null || facility.imageFileId==""?"false":"true"};
        var f = null;
        var isErr = false;
        var isChangeFile = ${facility.imageFileId==null || facility.imageFileId==""?"false":"true"};

        $(function () {
            $('#areaList').select2({
                placeholder: "Select Area",
                allowClear: true,
                escapeMarkup: function (m) { return m; }
            }).on("change", function(){
                $('#areaList').valid();
            });

            $('#facilityForm').validate({
                rules: {
                    'areaId': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("id") == "areaList" ){
                        $('#area-error-msg').html('');
                        error.appendTo('#area-error-msg');
                    } else{
                        error.insertAfter(element);
                    }
                },
                success: function(label,element) {
                    if ($(element).attr("id") == "areaList" ){
                        $('#area-error-msg').html('');
                    } else {
                        $(label).remove();
                    }
                }
            });

            if(isChangeFile){
                $('#btnText').html("Ganti File");
                $('#buttonDelete').show();
            } else {
                $('#btnText').html("Pilih File");
                $('#buttonDelete').hide();
            }
            $('#buttonUpload').hide();
            $('#progress').hide();
            $("#fileImport").on("change", function(){
                isErr = false;
                $('#uploadResult').removeClass("success");
                $('#uploadResult').removeClass("error");
                fileUploaded = false;
                var file = this.files[0],
                        content = file.name + " - " + file.size +"b";
                $('#uploadResult').html(content);
            });
            $('#fileImport').fileupload({
                singleFileUploads: true,
                context: $('#fileImport')[0],
                add: function (e, data) {
                    if(data.originalFiles[0]['size'] > 102400) {
                        vex.dialog.alert({
                            message: "Ukuran file terlalu besar."
                        });
                    } else {
                        $('#progress .progress-bar').css('width','0%');
                        $('#buttonUpload').show();
                        f = data;
                    }
                },
                progress: function(e, data){
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                done: function (e, data) {
                    if(data.result.result==false){
                        isErr = true;
                        fileUploaded = false;
                        $('#imageFileId').val('');
                        $('#imageFileName').val('');
                    } else {
                        fileUploaded = true;
                        $('#imageFileId').val(data.result.fileId);
                        $('#imageFileName').val(data.result.fileName);
                    }
                    $('#btnText').html("Ganti File");
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                        }
                    });
                },
                fail:function(e, data){
                    isErr = true;
                    $('#progress').fadeOut( "slow", function() {
                        if(isErr){
                            $('#uploadResult').append(" - Upload Error");
                            $('#uploadResult').removeClass("success");
                            $('#uploadResult').addClass("error");
                        } else {
                            $('#uploadResult').append(" - Upload Completed");
                            $('#uploadResult').removeClass("error");
                            $('#uploadResult').addClass("success");
                        }
                    });
                }

            });

        });

        function submitFile(){
            $('#buttonUpload').hide();
            $('#progress').show();
            f.submit();
        }

        function removeFile(){
            $('#buttonDelete').hide();
            $("#fileImport").val('');
            $('#btnText').html("Pilih File");
            $('#uploadResult').html("");
            $('#imageFileId').val("");
            $('#imageFileName').val("");
            fileUploaded = false;
            isChangeFile = false;
        }

        function submitForm() {
            if($('#facilityForm').valid()){
                if(fileUploaded) {
                    $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                    document.forms['facilityForm'].submit();
                } else {
                    vex.dialog.confirm({
                        message: "You haven't uploaded any image file. Continue?",
                        callback: function (value) {
                            if (value) {
                                $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
                                document.forms['facilityForm'].submit();
                            }
                        }
                    });
                }
            }
        }
    </script>
</content>
</body>
</g:applyLayout>
</html>