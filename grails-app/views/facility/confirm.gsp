<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${facility.stage} ${facility.action}</title>
</head>

<body>
    <section class="content-header">
        <property:pageHeader title="Data Fasilitas Umum" description="Tambah dan Edit Fasilitas Umum" />
        <ol class="breadcrumb">
            <li>Maintenance</li>
            <li><g:link controller="facility" action="index"><i class="fa fa-gear"></i> Facility</g:link></li>
            <li>${facility.action}</li>
            <li class="active">${facility.stage}</li>
        </ol>
    </section>

    <section class="content container-fluid">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">${facility.stage}</h3>
            </div>
            <g:form action="${facility.actionType=='add'?'submitAdd':'submitEdit'}" class="form-horizontal" method="POST">
                <div class="box-body">
                    <property:alert message="${flash.message}" type="success" title="${facility.action}"/>
                    <property:alert message="${flash.error}" type="danger" title="${facility.action}"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Properti:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="propName" value="${facility.propName}" />
                            <p class="form-control-static">${facility.propName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Area:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" value="${facility.id}" />
                            <input type="hidden" name="areaId" value="${facility.areaId}" />
                            <p class="form-control-static">${facility.areaName}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="name" value="${facility.name}" />
                            <p class="form-control-static">${facility.name}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Syarat dan Ketentuan:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="tnc" value="${facility.tnc}" />
                            <p class="form-control-static">${facility.tnc}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Upload Gambar:</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="imageFileId" value="${facility.imageFileId}" />
                            <input type="hidden" name="imageFileName" value="${facility.imageFileName}" />
                            <g:if test="${facility.stage == 'Confirm'}">
                                <p class="form-control-static">${facility.imageFileName}</p>
                            </g:if>
                            <g:else>
                                <p class="form-control-static"><a href="#" onclick="showImage('${facility.imageFileId}')">${facility.imageFileName}</a></p>
                            </g:else>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <g:if test="${facility.stage == 'Confirm'}">
                        <button type="button" onclick="window.history.back();" class="btn btn-default">Kembali</button>
                        <button type="submit" onclick="$.blockUI({ message: 'Sedang diproses, harap menunggu.' });" class="btn btn-info pull-right">Simpan</button>
                    </g:if>
                    <g:else>
                        <g:link controller="facility" action="index" class="btn btn-success pull-right">Selesai</g:link>
                    </g:else>
                </div>
            </g:form>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="imageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-12" id="imageContainer">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showImage(imageFileId){
            $('#imageContainer').html('<img src="${createLink(controller:'facility',action: 'showImage')}/'+imageFileId+'" style="max-width:500px;" />');
            $('#imageModal').modal('show');
        }
    </script>
</body>
</html>