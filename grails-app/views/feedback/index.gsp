<%--
  Created by IntelliJ IDEA.
  User: enpee_000
  Date: 04/03/2018
  Time: 12:33
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Feedback</title>
</head>

<body>
<section class="content-header">
    <property:pageHeader title="Kritik dan Saran" description="Kritik dan Saran dari Penghuni" />
    <ol class="breadcrumb">
        <li>Report</li>
        <li class="active"><i class="fa fa-comments"></i> Feedback</li>
    </ol>
</section>

<section class="content container-fluid">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Kritik dan Saran</h3>
        </div>
        <div class="box-body">
            <div class="form-horizontal col-md-7">
                <property:alert message="${flash.message}" type="success"/>
                <property:alert message="${flash.error}" type="danger"/>

                <g:form action="index" method="GET" style="margin-bottom: 20px;" name="form1">
                    %{--<div class="form-group">--}%
                        %{--<label class="col-sm-2 control-label">Properti:</label>--}%
                        %{--<div class="col-sm-8">--}%
                            %{--<p class="form-control-static">${propName}</p>--}%
                        %{--</div>--}%
                    %{--</div>--}%
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="submitDate" />
                                <input type="hidden" name="startDate" id="startDate" value="${startDate}"/>
                                <input type="hidden" name="endDate" id="endDate" value="${endDate}"/>
                            </div>
                        </div>
                    </div>
                </g:form>

                <div class="form-group">
                    <div class="pull-right" id="downloadDiv">
                        <button type="button" onclick="submitSearch()" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </div>

            <br style="clear:both;"/>

            <table class="table table-striped table-bordered table-hover" id="dataTables-feedback" style="width: 100%;">

            </table>
        </div>
        <div class="box-footer">
        </div>
    </div>
</section>

<g:form name="formDelete" controller="feedback" action="delete" method="POST">
</g:form>

<script>
    var detailFeedbackUrl = '${g.createLink(controller:'feedback',action:'detail')}';

    $(document).ready(function(){
        $('#propertyList').select2();
        $('#submitDate').daterangepicker({
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": "  to  "
            },
            startDate: moment(),
            endDate: moment()
        }, function(start, end, label) {
            $("#startDate").val(start._d.getTime());
            $("#endDate").val(end._d.getTime());
        });

        var ps = $('#startDate').val();
        var pe = $('#endDate').val();
        var drp = $("#submitDate").data('daterangepicker');

        if(ps==null || ps==''){
            $("#startDate").val(drp.startDate._d.getTime());
        } else {
            drp.startDate = moment(new Date(parseInt(ps)));
        }
        if(pe==null || pe=='') {
            $("#endDate").val(drp.endDate._d.getTime());
        } else {
            drp.endDate = moment(moment(new Date(parseInt(pe))));
        }
        drp.updateElement();
        $('#dataTables-feedback').dataTable({
            "language": {
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Berikutnya"
                },
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "emptyTable": "Data tidak ditemukan"
            },
            "bLengthChange": false,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": true,
            "sPaginationType": "simple_numbers",
            "sAjaxSource": '${g.createLink(controller:'feedback',action:'listDatatable')}',
            "fnServerParams": function ( aoData ) {
                aoData.push( { "name": "propId", "value": $('#propertyList').val() } );
                aoData.push( { "name": "startDate", "value": $('#startDate').val() } );
                aoData.push( { "name": "endDate", "value": $('#endDate').val() } );
            },
            "columnDefs": [
                { "orderable": false, "targets": 4 },
                { "orderable": false, "targets": 5 }
            ],
            "aoColumns": [
//                { "mData": "property", "sTitle":"Properti" },
                { "mData": "id", "sTitle":"Tanggal Dibuat", "mRender":
                        function(data, type, full){
                            return "<a href='"+detailFeedbackUrl+"/"+full.id+"'>"+full.date+"</a>";
                        }
                },
                { "mData": "user", "sTitle":"Nama Penghuni" },
                { "mData": "title", "sTitle":"Judul" },
                { "mData": "description", "sTitle":"Keterangan" },
                { "mData": "id", "sTitle":"File Diupload", "mRender":
                        function(data, type, full){
                            if(full.url!=null) {
                                return "<a href='" + full.url + "'><i class=\"fa fa-file\" style=\"font-size:24px\"></i></a>";
                            } else {
                                return "-";
                            }
                        }
                },
                { "mData": "id", "sTitle":"Hapus", "mRender":
                        function(data, type, full){
                            return '<button type="button" class="btn btn-danger" onclick="confirmDelete(\''+full.id+'\')"><i class="fa fa-trash"></i></button>';
                        }
                }
            ]
        });
    });

    function confirmDelete(id){
        vex.dialog.confirm({
            message: 'Apakah anda yakin untuk menghapus kritik dan saran ini?',
            callback: function (value) {
                if (value) {
                    $('#formDelete').html('<input type="hidden" value="'+id+'" name="id" />');
                    $('#formDelete').submit();
                }
            }
        });
    }

    function submitSearch(){
        $.blockUI({ message: 'Sedang diproses, harap menunggu.' });
        $('#form1').attr("action",'${g.createLink(controller:'feedback',action:'index')}');
        $('#form1').submit();
    }

</script>
</body>
</html>