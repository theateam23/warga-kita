

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.model.security.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.model.security.UserRole'
grails.plugin.springsecurity.authority.className = 'com.model.security.Role'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/dashboard'
grails.plugin.springsecurity.successHandler.alwaysUseDefault = true

grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/**',             access: ['isFullyAuthenticated()']],
	[pattern: '/homePage/**',         access: ['permitAll']],
	[pattern: '/mobileApi/**',         access: ['permitAll']],
	[pattern: '/facilityBookingApi/**',         access: ['permitAll']],
	[pattern: '/homeGuardApi/**',         access: ['permitAll']],
	[pattern: '/inboxMessageApi/**',         access: ['permitAll']],
	[pattern: '/memberApi/**',         access: ['permitAll']],
	[pattern: '/newsAnnouncementApi/**',         access: ['permitAll']],
	[pattern: '/userMobileApi/**',         access: ['permitAll']],
	[pattern: '/vehicleApi/**',         access: ['permitAll']],
	[pattern: '/visitorApi/**',         access: ['permitAll']],
	[pattern: '/news/showImage',         access: ['permitAll']],
	[pattern: '/announcement/showImage',         access: ['permitAll']],
	[pattern: '/userMobile/showImage',         access: ['permitAll']],
	[pattern: '/visitor/showImage',         access: ['permitAll']],
	[pattern: '/facility/showImage',         access: ['permitAll']],
	[pattern: '/propertyRequest/downloadFile',         access: ['permitAll']],
	[pattern: '/memberPm/showImage',         access: ['permitAll']],
	[pattern: '/vehiclePm/showImage',         access: ['permitAll']],
	[pattern: '/propertyAdmin/showImage',         access: ['permitAll']],
	[pattern: '/auth/**',        access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]

]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/webApi/**',         filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'],
	[pattern: '/**',             filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter']
]

//grails.plugin.forceSSL.enabled = { request ->
//	if(request.serverName.contains('warga-kita.com')) {
//		return true
//	}
//	return false
//}

grails.plugin.springsecurity.rest.token.storage.jwt.secret = "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo"

if (grails.util.Environment.current == grails.util.Environment.DEVELOPMENT) {
	file.upload.path="/Users/pristantyo/data/"
	grails.plugin.forceSSL.enabled = false
} else {
	file.upload.path="/home/ubuntu/data/"
	grails.plugin.forceSSL.enabled = true
}

one.to.one.property.manager=1

