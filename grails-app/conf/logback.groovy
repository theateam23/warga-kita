import grails.util.BuildSettings
import grails.util.Environment
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import ch.qos.logback.core.util.FileSize


def HOME_DIR = ""

if(Environment.current == Environment.DEVELOPMENT) {
    HOME_DIR = "/Users/pristantyo"
} else {
    HOME_DIR = "/home/ubuntu"
}

appender("ROLLING", RollingFileAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{dd-MMM-yyyy HH:mm:ss z} %level %logger - %msg%n"
    }
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${HOME_DIR}/logs/wargakita-%d{yyyy-MM-dd}.log"
        maxHistory = 14
        totalSizeCap = FileSize.valueOf("2GB")
    }
}


appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{dd-MMM-yyyy HH:mm:ss z} %level %logger - %msg%n"
    }
}


logger('com.property', DEBUG, ["STDOUT", 'ROLLING'], false)
root(ERROR, ["STDOUT", 'ROLLING'])
