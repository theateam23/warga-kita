import com.model.CustomUserDetailsService
import com.model.LoggingSecurityEventListener
import com.model.security.UserPasswordEncoderListener
// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener, ref('hibernateDatastore'))
    authenticationSuccessHandler(LoggingSecurityEventListener)
    userDetailsService(CustomUserDetailsService)
}
