package com.property.jobs

import com.model.AppConstants
import com.model.general.UserMobileUnit
import com.model.information.InboxMessage
import com.model.information.PropertyReminder
import com.model.information.PropertyReminderNotif
import com.model.property.Member
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.property.Vehicle
import com.property.utils.BaseJob
import com.property.utils.FirebaseUtil
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat

class DailyJob extends BaseJob {
    static triggers = {
      simple repeatInterval: 60000l // execute job once in 1 minutes
    }

    def execute() {
        updateJobStatus("dailyJob")
        def now = new SimpleDateFormat("HH:mm").format(new Date())
        if ("06:00".equals(now)) {
            sendBirthdayNotification()
            sendTaxExpiredNotification()
        }
    }

    def sendBirthdayNotification() {
        updateJobStatus("sendBirthdayNotification")

        //insert data when there is any member dob = today
        def notifDobList = UserMobileUnit.executeQuery("SELECT new Map(umu.userMobile as user, m as member) " +
                "FROM UserMobileUnit umu, Member m WHERE m.unit = umu.unit " +
                "AND month(m.dateOfBirth)=month(current_date()) AND day(m.dateOfBirth)=day(current_date())")

        int idx = 0
        def jobLog = getJobLog("Birthday_Notification")
        notifDobList.each { map ->
            InboxMessage.withTransaction{ status ->
                try {
                    def inboxMessage = new InboxMessage()
                    inboxMessage.title = "Selamat Ulang Tahun"
                    inboxMessage.content = "Hi ${map.member.name}, Happy Bday, semoga happy dan sehat selalu."
                    inboxMessage.userMobile = map.user
                    inboxMessage.save()

                    def member = Member.get(map.member.id)
                    member.isNotified = AppConstants.FLAG_YES
                    member.save()

                    if (idx == AppConstants.MAX_BATCH_FIREBASE) {
                        Thread.sleep(500)
                        idx = 0
                    }

                    def response = FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, inboxMessage.userMobile.id )
                    if (!response.isSuccess) {
                        jobLog.status = AppConstants.JOB_FAILED
                        jobLog.message = response.message
                    }
                    idx++

                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    jobLog.status = AppConstants.JOB_FAILED
                    jobLog.message = e.getMessage()
                    jobLog.stackTrace = ExceptionUtils.getStackTrace(e)
                } finally {
                    jobLog.save(flush: true)
                }
            }
        }
    }

    def sendTaxExpiredNotification() {
        updateJobStatus("sendTaxExpiredNotification")

        //insert data when there is any due tax date = today
        def notifTaxList = UserMobileUnit.executeQuery("SELECT new Map(umu.userMobile as user, v as vehicle) " +
                "FROM UserMobileUnit umu, Vehicle v WHERE v.unit = umu.unit " +
                "AND month(v.taxDueDate)=month(current_date()) AND day(v.taxDueDate)=day(current_date())")

        int idx = 0
        def jobLog = getJobLog("Tax_Expired_Notification")
        notifTaxList.each { map ->
            InboxMessage.withTransaction{ status ->
                try {
                    def inboxMessage = new InboxMessage()
                    inboxMessage.title = "Pajak Tahunan"
                    inboxMessage.content = "Pajak Tahunan kendaraan anda dengan plat nomor: ${map.vehicle.plateNo} jatuh tempo hari ini."
                    inboxMessage.userMobile = map.user
                    inboxMessage.save()

                    def vehicle = Vehicle.get(map.vehicle.id)
                    vehicle.isNotified = AppConstants.FLAG_YES
                    vehicle.save()

                    if (idx == AppConstants.MAX_BATCH_FIREBASE) {
                        Thread.sleep(500)
                        idx = 0
                    }

                    def response = FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, inboxMessage.userMobile.id )
                    if (!response.isSuccess) {
                        jobLog.status = AppConstants.JOB_FAILED
                        jobLog.message = response.message
                    }
                    idx++

                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    jobLog.status = AppConstants.JOB_FAILED
                    jobLog.message = e.getMessage()
                    jobLog.stackTrace = ExceptionUtils.getStackTrace(e)
                } finally {
                    jobLog.save(flush: true)
                }
            }
        }
    }

}
