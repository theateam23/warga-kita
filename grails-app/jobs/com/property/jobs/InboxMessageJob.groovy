package com.property.jobs

import com.model.AppConstants
import com.model.general.JobLog
import com.model.general.JobStatus
import com.model.general.UserMobileUnit
import com.model.information.BroadcastReminder
import com.model.information.InboxMessage
import com.model.information.PropertyReminder
import com.model.information.PropertyReminderNotif
import com.model.property.Member
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.property.Vehicle
import com.property.utils.BaseJob
import com.property.utils.FirebaseUtil
import org.apache.commons.lang.exception.ExceptionUtils

class InboxMessageJob extends BaseJob {
    static triggers = {
      simple repeatInterval: 600000l // execute job once in 10 minutes
    }

    def execute() {
        insertRecordReminderNotif()
        sendReminderNotification()

    }

    def insertRecordReminderNotif(){
        updateJobStatus("insertRecordReminderNotif")

        def currentDay = new Date().clearTime()
        Property.list().each { prop ->
            PropertyReminder.findAllByPropertyAndIsActive(prop, AppConstants.FLAG_YES).each { reminder ->
                def deadline = getDeadline(currentDay,reminder.reminderDay)
                PropertyUnit.executeQuery("FROM PropertyUnit WHERE area.property.id = :propId AND (ownerResident IS NOT NULL OR tenant IS NOT NULL)",
                        [propId:prop.id]).each{ PropertyUnit unit ->
                    def recordExists = PropertyReminderNotif.findByUnitAndDeadline(unit, deadline)!=null
                    if(!recordExists){
                        PropertyReminderNotif.withTransaction{ status ->
                            try {
                                def obj = new PropertyReminderNotif()
                                obj.reminder = reminder
                                obj.unit = unit
                                obj.deadline = deadline
                                obj.isPaid = AppConstants.FLAG_NO
                                obj.isNotif = AppConstants.FLAG_NO
                                obj.save()
                            } catch (Exception e){
                                log.error ExceptionUtils.getStackTrace(e)
                                status.setRollbackOnly()
                            }
                        }
                    }
                }
            }
        }
    }

    def sendReminderNotification(){
        updateJobStatus("sendReminderNotification")

        def currentDay = new Date()
        int idx = 0
        def jobLog = getJobLog("Property_Reminder_Notification")
        PropertyReminderNotif.findAllByIsNotifAndDeadlineLessThan(AppConstants.FLAG_NO, currentDay).each{ obj ->
            InboxMessage.withTransaction{ status ->
                try {
                    UserMobileUnit.findAllByUnit(obj.unit).each { umu ->
                        def inboxMessage = new InboxMessage()
                        inboxMessage.title = "Reminder: ${obj.reminder.name}"
                        inboxMessage.content = obj.reminder.message
                        inboxMessage.userMobile = umu.userMobile
                        inboxMessage.save()

                        if (idx == AppConstants.MAX_BATCH_FIREBASE) {
                            Thread.sleep(500)
                            idx = 0
                        }
                        def response = FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, inboxMessage.userMobile.id )
                        if (!response.isSuccess) {
                            jobLog.status = AppConstants.JOB_FAILED
                            jobLog.message = response.message
                        }
                        idx++

                    }

                    obj.isNotif = AppConstants.FLAG_YES
                    obj.save()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    jobLog.status = AppConstants.JOB_FAILED
                    jobLog.message = e.getMessage()
                    jobLog.stackTrace = ExceptionUtils.getStackTrace(e)
                } finally {
                    jobLog.save(flush: true)
                }
            }
        }
    }

    def getDeadline(Date currentDay, def reminderDay){
        def dlYear = currentDay[Calendar.YEAR]
        def dlMonth = currentDay[Calendar.MONTH]
        def dlDay = currentDay[Calendar.DAY_OF_MONTH]
        if(dlDay >= reminderDay) {
            if (dlMonth == Calendar.DECEMBER) {
                dlYear += 1
                dlMonth = Calendar.JANUARY
            } else {
                dlMonth++
            }
        }
        def calDeadline = new GregorianCalendar(dlYear, dlMonth, 1, 0, 0, 0)
        def maxDay = calDeadline.getActualMaximum(Calendar.DAY_OF_MONTH)
        if(maxDay<reminderDay){
            reminderDay = maxDay
        }
        return new GregorianCalendar(dlYear, dlMonth, reminderDay, 0, 0, 0).time
    }

}
