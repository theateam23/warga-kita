package com.property.jobs

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserRecord
import com.model.AppConstants
import com.model.general.FacilityBooking
import com.model.general.JobLog
import com.model.general.JobStatus
import com.model.general.TmpMail
import com.model.information.BroadcastReminder
import com.model.information.InboxMessage
import com.model.security.ResetPasswordToken
import com.model.security.UserMobile
import com.property.utils.BaseJob
import com.property.utils.FirebaseUtil
import org.apache.commons.lang.exception.ExceptionUtils

class MinuteJob extends BaseJob {
    static triggers = {
      simple repeatInterval: 60000l // execute job once in 60 seconds
    }

    def mailService

    def execute() {
        updateUidUserMobile()
        sendBroadcastReminder()
        updateBookingStatus()
        updateTokenExpired()
        sendMailPool()
    }

    def updateBookingStatus() {
        updateJobStatus("updateBookingStatus")

        try {
            def currentDate = new Date()
            def statusList = [AppConstants.BOOKING_STATUS_OPEN, AppConstants.BOOKING_STATUS_BOOKED]
            //update to closed
            def fbs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE startDate <= :currentDate AND startTime <= :currentTime AND status IN :statusList",
                    [currentDate:currentDate, currentTime: currentDate.getTime(), statusList: statusList])
            fbs.each{ fb ->
                fb.updatedBy = "System"
                if(fb.endTime > currentDate.getTime()){
                    fb.status = AppConstants.BOOKING_STATUS_BOOKED
                } else {
                    fb.status = AppConstants.BOOKING_STATUS_CLOSED
                }
                fb.save(flush:true)
            }
        } catch (Exception e){
            log.error ExceptionUtils.getStackTrace(e)
        }

    }

    def updateTokenExpired() {
        updateJobStatus("updateTokenExpired")

        try {
            // delete reset password token
            def currentTime = new Date().time
            ResetPasswordToken.executeQuery("FROM ResetPasswordToken WHERE (:currentTime - createdTime) >= (expiredTime * 1000)", [currentTime:currentTime]).each { obj ->
                obj.delete()
            }
        } catch (Exception e){
            log.error ExceptionUtils.getStackTrace(e)
        }
    }

    def sendMailPool() {
        updateJobStatus("sendMailPool")

        try {
            // sending tmp mail
            def tmpList = TmpMail.findAllByStatus(AppConstants.PROCESS_NEW, [max: 20])
            tmpList.each { tm ->
                tm.status = AppConstants.PROCESS_IN_PROGRESS
                tm.save(flush: true)
            }

            tmpList.each { tm ->
                mailService.sendMail {
                    to tm.emailTo
                    from tm.emailFrom
                    subject tm.subject
                    html tm.content
                }
                tm.delete(flush: true)
                Thread.sleep(100)
            }
        } catch (Exception e){
            log.error ExceptionUtils.getStackTrace(e)
        }

    }

    def sendBroadcastReminder() {
        updateJobStatus("sendBroadcastReminder")

        def currentDate = new Date()
        int idx = 0
        def jobLog = getJobLog("Broadcast_Message_Reminder")
        BroadcastReminder.findAllByReminderDateLessThanEquals(currentDate).each { obj ->
            InboxMessage.withTransaction{ status ->
                try {
                    def inboxMessage = new InboxMessage()
                    inboxMessage.title = "Reminder: ${obj.inboxMessage.title}"
                    inboxMessage.content = obj.inboxMessage.content
                    inboxMessage.userMobile = obj.inboxMessage.userMobile
                    inboxMessage.save()

                    obj.delete()

                    if (idx == AppConstants.MAX_BATCH_FIREBASE) {
                        Thread.sleep(500)
                        idx = 0
                    }
                    def response = FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, obj.topicId)
                    if (!response.isSuccess) {
                        jobLog.status = AppConstants.JOB_FAILED
                        jobLog.message = response.message
                    }
                    idx++

                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    jobLog.status = AppConstants.JOB_FAILED
                    jobLog.message = e.getMessage()
                    jobLog.stackTrace = ExceptionUtils.getStackTrace(e)
                } finally {
                    jobLog.save(flush: true)
                }
            }
        }
    }

    def updateUidUserMobile() {
        updateJobStatus("updateUidUserMobile")

        UserMobile.findAllByUidIsNull().each {um->
            try {
                UserRecord userRecord = FirebaseAuth.getInstance().getUserByEmail(um.email)
                if (userRecord != null) {
                    um.uid = userRecord.uid
                    um.save(flush:true)
                    log.debug("Updating uid firebase for user ${um.email}")
                }
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                mailService.sendMail {
                    to "pristantyo@gmail.com"
                    from "support@warga-kita.com"
                    subject "updateUidUserMobile Error Occured"
                    html um.email
                }
            }


        }
    }
}
