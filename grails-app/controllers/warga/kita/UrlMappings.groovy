package warga.kita

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
            }
        }

        "/api/property/getProperties" (controller:'mobileApi', action:'getProperties')
        "/api/property/getPropertiesByUser" (controller:'mobileApi', action:'getPropertiesByUser')
        "/api/property/pushNotification" (controller:'mobileApi', action:'pushNotification')
        "/api/property/getAreasByProperty" (controller:'mobileApi', action:'getAreasByProperty')
        "/api/property/getSubAreaByAreaId" (controller:'mobileApi', action:'getSubAreaByAreaId')
        "/api/property/getUnitByPropertyAndSubArea" (controller:'mobileApi', action:'getUnitByPropertyAndSubArea')
        "/api/property/submitFeedback" (controller:'mobileApi', action:'submitFeedback')
        "/api/property/getRequestFormList" (controller:'mobileApi', action:'getRequestFormList')

        "/api/property/getUser" (controller:'userMobileApi', action:'getUser')
        "/api/property/registerUserMobile" (controller:'userMobileApi', action:'registerUserMobile')
        "/api/property/updateUser" (controller:'userMobileApi', action:'updateUser')

        "/api/property/createBooking" (controller:'facilityBookingApi', action:'createBooking')
        "/api/property/getBookingList" (controller:'facilityBookingApi', action:'getBookingList')
        "/api/property/getFacilityByPropertyId" (controller:'facilityBookingApi', action:'getFacilityByPropertyId')

        "/api/property/getMemberByUser" (controller:'memberApi', action:'getMemberByUser')
        "/api/property/getMember" (controller:'memberApi', action:'getMember')
        "/api/property/createMember" (controller:'memberApi', action:'createMember')
        "/api/property/updateMember" (controller:'memberApi', action:'updateMember')
        "/api/property/deleteMember" (controller:'memberApi', action:'deleteMember')
        "/api/property/getMemberByName" (controller:'memberApi', action:'getMemberByName')

        "/api/property/getVehicleByUser" (controller:'vehicleApi', action:'getVehicleByUser')
        "/api/property/getVehicle" (controller:'vehicleApi', action:'getVehicle')
        "/api/property/createVehicle" (controller:'vehicleApi', action:'createVehicle')
        "/api/property/updateVehicle" (controller:'vehicleApi', action:'updateVehicle')
        "/api/property/deleteVehicle" (controller:'vehicleApi', action:'deleteVehicle')
        "/api/property/getVehicleByPlateNo" (controller:'vehicleApi', action:'getVehicleByPlateNo')

        "/api/property/getNewsByUser" (controller:'newsAnnouncementApi', action:'getNewsByUser')
        "/api/property/getNewsByUserAndCategory" (controller:'newsAnnouncementApi', action:'getNewsByUserAndCategory')
        "/api/property/getNewsCategory" (controller:'newsAnnouncementApi', action:'getNewsCategory')
        "/api/property/getAnnouncementByUser" (controller:'newsAnnouncementApi', action:'getAnnouncementByUser')

        "/api/property/getMessagesByUser" (controller:'inboxMessageApi', action:'getMessagesByUser')
        "/api/property/deleteMessageById" (controller:'inboxMessageApi', action:'deleteMessageById')
        "/api/property/deleteMessageByListId" (controller:'inboxMessageApi', action:'deleteMessageByListId')
        "/api/property/readMessageById" (controller:'inboxMessageApi', action:'readMessageById')
        "/api/property/readMessageByListId" (controller:'inboxMessageApi', action:'readMessageByListId')
        "/api/property/getMessagesSummaryByUser" (controller:'inboxMessageApi', action:'getMessagesSummaryByUser')

        "/api/property/addHomeGuard" (controller:'homeGuardApi', action:'addHomeGuard')
        "/api/property/deleteHomeGuard" (controller:'homeGuardApi', action:'deleteHomeGuard')
        "/api/property/getHomeGuardList" (controller:'homeGuardApi', action:'getHomeGuardList')
        "/api/property/checkHomeGuard" (controller:'homeGuardApi', action:'checkHomeGuard')

        "/api/property/createVisitor" (controller:'visitorApi', action:'createVisitor')
        "/api/property/getVisitorList" (controller:'visitorApi', action:'getVisitorList')
        "/api/property/updateVisitorStatus" (controller:'visitorApi', action:'updateVisitorStatus')

        "/"(controller:'homePage')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
