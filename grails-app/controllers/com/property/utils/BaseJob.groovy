package com.property.utils

import com.model.AppConstants
import com.model.general.JobLog
import com.model.general.JobStatus

class BaseJob {
    def getJobLog(def name) {
        def jobLog = new JobLog()
        jobLog.name = name
        jobLog.createdDate = new Date()
        jobLog.status = AppConstants.JOB_SUCCESS

        return jobLog
    }

    def updateJobStatus(String name) {
        def jobStatus = JobStatus.get(name)
        if (jobStatus == null) {
            jobStatus = new JobStatus()
            jobStatus.id = name.toUpperCase()
            jobStatus.name = name
        }
        jobStatus.lastUpdated = new Date()
        jobStatus.save(flush: true)
    }
}
