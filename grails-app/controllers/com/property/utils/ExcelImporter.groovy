package com.property.utils

import grails.util.Holders
import org.grails.plugins.excelimport.AbstractExcelImporter
import org.grails.plugins.excelimport.ExcelImportService

class ExcelImporter extends AbstractExcelImporter {

    static Map UNIT_BOOK_COLUMN_MAP = [
            sheet:'Sheet1',
            startRow: 1,
            columnMap:  [
                    'A':'name',
                    'B':'subArea',
                    'C':'unitType'
            ]
    ]

    static Map MEMBER_BOOK_COLUMN_MAP = [
            sheet:'Sheet1',
            startRow: 1,
            columnMap:  [
                    'A':'name',
                    'B':'dateOfBirth',
                    'C':'placeOfBirth',
                    'D':'profession',
                    'E':'idKTP',
                    'F':'email',
                    'G':'address',
                    'H':'status',
                    'I':'gender',
                    'J':'education',
                    'K':'hobby',
                    'L':'description',
                    'M':'relationship'
            ]
    ]

    static Map VEHICLE_BOOK_COLUMN_MAP = [
            sheet:'Sheet1',
            startRow: 1,
            columnMap:  [
                    'A':'plateNo',
                    'B':'vehicleType',
                    'C':'brand',
                    'D':'color',
                    'E':'taxDueDate'
            ]
    ]

    static Map UNIT_IMPORTER_ARAFAH = [
            sheet: 'Sheet3',
            startRow: 1,
            columnMap: [
                    'A':'unit',
                    'B':'statusRumah',
                    'C':'statusKepemilikan',
                    'D':'noKTP',
                    'E':'namaPenghuni',
                    'F':'statusPenghuni',
                    'G':'tempatLahir',
                    'H':'tanggalLahir',
                    'I':'pekerjaan',
                    'J':'email',
                    'K':'noPolisi',
                    'L':'jenisKendaraan'
            ]
    ]

    static Map USER_MOBILE_COLUMN_MAP = [
            sheet: 'Sheet1',
            startRow: 1,
            columnMap: [
                    'A':'name',
                    'B':'email',
                    'C':'phoneNo',
                    'D':'unit',
                    'E':'status'
            ]
    ]

    static Map SECURITY_USER_COLUMN_MAP = [
            sheet: 'Sheet1',
            startRow: 1,
            columnMap: [
                    'A':'name',
                    'B':'email',
                    'C':'idKTP',
                    'D':'dob',
                    'E':'phoneNo',
                    'F':'gender',
                    'G':'title'
            ]
    ]

    public ExcelImporter(fileName) {
        read(fileName)
    }

    public List<Map> getUnitList() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, UNIT_BOOK_COLUMN_MAP)
    }

    public List<Map> getMemberList() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, MEMBER_BOOK_COLUMN_MAP)
    }

    public List<Map> getVehicleList() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, VEHICLE_BOOK_COLUMN_MAP)
    }

    public List<Map> getUnitArafah() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, UNIT_IMPORTER_ARAFAH)
    }

    public List<Map> getUserMobileList() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, USER_MOBILE_COLUMN_MAP)
    }

    public List<Map> getSecurityUserList() {
        return Holders.grailsApplication.mainContext.getBean("excelImportService").convertColumnMapConfigManyRows(workbook, SECURITY_USER_COLUMN_MAP)
    }


}
