package com.property.utils

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserRecord
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import com.google.firebase.messaging.Notification
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse

class FirebaseUtil {
    static def errorMap = ["email-already-exists":"Email sudah terdaftar.", "invalid-email":"email tidak valid."]

    static def createUser(String email) {
        def result = ['isSuccess':false]

        String password = org.apache.commons.lang.RandomStringUtils.random(8, false, true)
        UserRecord.CreateRequest request = new UserRecord.CreateRequest()
            .setEmail(email)
            .setEmailVerified(false)
            .setPassword(password)
            .setDisabled(false)

        try {
            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created new user: " + userRecord.getUid());
            result.isSuccess = true
            result.password = password
            result.uid = userRecord.getUid()
        } catch (com.google.firebase.auth.FirebaseAuthException e) {
            if (errorMap.containsKey(e.errorCode)) {
                result.message = errorMap.get(e.errorCode)
            } else {
                result.message = e.errorCode
            }
        } catch (Exception e) {
            result.message = e.getMessage()
        }

        return result
    }

    static def deleteUser(String uid) {
        def result = ['isSuccess':false]

        try {
            FirebaseAuth.getInstance().deleteUser(uid);
            result.isSuccess = true
        } catch (com.google.firebase.auth.FirebaseAuthException e) {
            if (errorMap.containsKey(e.errorCode)) {
                result.message = errorMap.get(e.errorCode)
            } else {
                result.message = e.errorCode
            }
        } catch (Exception e) {
            result.message = e.getMessage()
        }

        return result
    }

    static def sendNotification(String title, String message, String topic) {
        def result = ['isSuccess':false]

        try {
            Message msg = Message.builder()
                    .putData("priority", "high")
                    .setNotification(new Notification(title, message))
                    .setTopic("/topics/${topic}")
                    .build()

            String response = FirebaseMessaging.getInstance().send(msg)
            result.isSuccess = true

        } catch (com.google.firebase.auth.FirebaseAuthException e) {
            if (errorMap.containsKey(e.errorCode)) {
                result.message = errorMap.get(e.errorCode)
            } else {
                result.message = e.errorCode
            }
        } catch (Exception e) {
            result.message = e.getMessage()
        }

        return result
    }

//    static def sendNotification(String title, String message, String topic) {
//        RestBuilder rest = new RestBuilder()
//        String url = "https://fcm.googleapis.com/fcm/send"
//
//        def result = [:]
//        def restParam = [
//                "priority": "high",
//                "notification": [
//                        "sound": "default",
//                        "title": title,
//                        "body": message
//                ],
//                "to": "/topics/${topic}"
//        ]
//        restParam.returnSecureToken = true
//        RestResponse restResponse = rest.post(url) {
//            header 'Authorization', 'key=AAAA1WcDiN8:APA91bH1UYFXM7mjzwW3DH15eX9ZY1wa5y0IIyoYC_1eJBhz_vLhbn8H5YU10cYcP9T8H05DyWpEWNi6BF41ghJLOZb5ryyL8jGzf952usd64y8p1PANLoMADR93e25WCUqT638wf-78'
//            contentType "application/json"
//            json restParam
//        }
//
//        if (restResponse.statusCode.value()!=200) {
//            result.isSuccess = false
//            result.message = restResponse.json.error.message
//        } else {
//            result.isSuccess = true
//            result.message = "success"
//        }
//
//        return result
//    }




}
