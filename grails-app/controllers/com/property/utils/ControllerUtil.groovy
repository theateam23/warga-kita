package com.property.utils

import com.model.AppConstants

import java.text.SimpleDateFormat

class ControllerUtil {

    public static boolean hasValue(obj){
        if(obj==null){
            return false
        }
        if(obj instanceof String){
            if(obj.trim().length()==0){
                return false
            }
        }

        return true
    }

    public static Long getTimeZoneOffset(){
        Calendar cal = Calendar.getInstance();
        return ((cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET)) / (1000))
    }

    public static String appendWc(obj){
        if(obj==null || obj.trim().length()==0){
            return '%'
        } else {
            return '%'+obj.trim()+'%'
        }
    }

    public static String getFlagYesNo(obj){
        if(obj!=null && AppConstants.FLAG_YES.equals(obj)){
            return "Ya"
        } else {
            return "Tidak"
        }
    }

    public static String padLeft(int cnt, def pad, def content){
        def result = content
        for(int i=0; i<cnt; i++){
            result = "${pad}${result}"
        }
        return result
    }

    public static String parseDate(def dt, String format){
        if(dt!=null){
            def sdf = new SimpleDateFormat(format)
            return sdf.format(dt)
        }
        return ""
    }

    public static Date convertToDate(String dt, String format){
        if(hasValue(dt)){
            def sdf = new SimpleDateFormat(format)
            return sdf.parse(dt)
        }
        return null
    }

    public static String convertToStrDate(def t, SimpleDateFormat sdf){
        if(t!=null && t instanceof Long){
            return sdf.format(new Date(t))
        } else if(t!=null && t instanceof Date){
            return sdf.format(t)
        }
        return null
    }

    public static List convertToList(def obj){
        def result = []
        if(obj!=null) {
            if (obj instanceof String) {
                result.add(obj)
            } else {
                result = obj
            }
        }
        return result
    }

    public static def shorten(String longString, int cnt){
        if(longString==null){
            return ""
        }
        if(longString.length()>cnt){
            return longString.substring(0, cnt-4).concat("...")
        } else {
            return longString
        }
    }

    public static def parseDatatables(Class<?> c, dtParams, queryParams, columns){
        def pmax = dtParams.iDisplayLength ?: 10
        def poffset = dtParams.iDisplayStart ?: 0
        def sortIdx = dtParams.iSortCol_0 ? dtParams.int('iSortCol_0') : 0
        def porder = dtParams.sSortDir_0 ?: 'asc'
        def psort = columns.get(sortIdx)

        def criteria = c.createCriteria()
        def objs = criteria.list(max:pmax, offset:poffset){
            queryParams.each{ prop, det ->
                def val = det.value
                def query = det.query
                "${query}"(prop, val)
            }
            order(psort,porder)
        }
        def result = [:]
        result.sEcho = dtParams.sEcho
        result.iTotalRecords = objs.totalCount
        result.iTotalDisplayRecords = objs.totalCount
        result.objs = objs
        return result
    }

    public static def parseDatatablesQuery(Class<?> c, dtParams, query, queryCount, queryParams, columns){
        def pmax = dtParams.iDisplayLength ?: 10
        def poffset = dtParams.iDisplayStart ?: 0
        def sortIdx = dtParams.iSortCol_0 ? dtParams.int('iSortCol_0') : 0
        def porder = dtParams.sSortDir_0 ?: 'asc'
        def psort = columns.get(sortIdx)

        query += " order by ${psort} ${porder} "
        def objs = c.executeQuery(query, queryParams, [max:pmax, offset:poffset])
        def cnt = c.executeQuery(queryCount, queryParams)

        def result = [:]
        result.sEcho = dtParams.sEcho
        result.iTotalRecords = cnt
        result.iTotalDisplayRecords = cnt
        result.objs = objs
        return result
    }

    public static String getLoginType(def providerId){
        if(providerId == 'facebook.com'){
            return AppConstants.LOGIN_FACEBOOK
        } else if(providerId == 'google.com'){
            return AppConstants.LOGIN_GOOGLE
        } else if(providerId == 'twitter.com'){
            return AppConstants.LOGIN_TWITTER
        } else {
            return AppConstants.LOGIN_NORMAL
        }
    }

    public static String getActive(def isActive){
        if(AppConstants.FLAG_YES.equals(isActive)){
            return "Aktif"
        } else {
            return "Tidak aktif"
        }
    }
}
