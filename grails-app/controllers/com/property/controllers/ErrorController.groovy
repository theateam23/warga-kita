package com.property.controllers


import com.bertramlabs.plugins.SSLRequired

@SSLRequired
class ErrorController {

    def index() {
        log.debug "IN index... $params"
        if(params.status==null){
            redirect(controller:'dashboard')
        } else if (params.status == '404'){
            response.status = 404
            render view: '/notFound'
        } else {
            response.status = 500
            render view:'/error'
        }
    }

}
