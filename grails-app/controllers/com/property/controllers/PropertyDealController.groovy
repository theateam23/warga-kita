package com.property.controllers

import com.model.AppConstants
import com.model.maintenance.PropertyType
import com.model.maintenance.UnitType
import com.model.property.Property
import com.model.property.PropertyDeal
import com.model.property.PropertyRequest
import com.model.property.PropertyUnit
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserRole
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat


import com.bertramlabs.plugins.SSLRequired

@SSLRequired
@Secured(['ROLE_ADMIN','ROLE_SALES'])
class PropertyDealController {

    def springSecurityService

    def index() {
        log.debug "index..."
        [statusList:AppConstants.getDealStatusList(), status:params.status, name:params.name]
    }

    def listDatatable(){
        def columns = ['name', 'developer', 'address', 'propertyType.name', 'startDate', 'status']
        def baseQuery = "FROM PropertyDeal WHERE lower(name) like :name AND status like :status "

        def qName = ControllerUtil.hasValue(params.name)?"%${params.name.toLowerCase()}%".toString():"%"
        def qStatus = ControllerUtil.hasValue(params.status)?params.status:'%'

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [name: qName, status: qStatus]
        def result = ControllerUtil.parseDatatablesQuery(PropertyDeal.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def currentDate = new Date()
        objs.each{ obj ->
            def det = [id:obj.id, name:obj.name, developer:obj.developer, address:obj.address, propertyType:obj?.propertyType?.name]
            if(obj.status.equals(AppConstants.DEAL_STATUS_IN_PROGRESS) && obj.startDate!=null){
                det.startDate = sdf.format(obj.startDate)
                use(groovy.time.TimeCategory) {
                    def duration = currentDate - obj.startDate
                    det.dayCount = duration.days
                }
            } else {
                det.startDate = ""
                det.dayCount = ""
            }
            det.status = AppConstants.getDealStatusName(obj.status)
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def obj = PropertyDeal.get(params.id)
        if(obj!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def currentDate = new Date()
            def det = [id:obj.id, name:obj.name, developer:obj.developer, address:obj.address, propertyType:obj?.propertyType?.name,
                        attachment: obj.attachment, attachmentName: obj.attachmentName, description: obj.description, status:AppConstants.getDealStatusName(obj.status)]
            if(obj.status.equals(AppConstants.DEAL_STATUS_IN_PROGRESS) && obj.startDate!=null){
                det.startDate = sdf.format(obj.startDate)
                use(groovy.time.TimeCategory) {
                    def duration = currentDate - obj.startDate
                    det.dayCount = duration.days
                }
            }

            if(obj.createdDate!=null) {
                det.createdDate = sdf.format(obj.createdDate)
                def createdBy = User.get(obj.createdBy)
                if(createdBy!=null){
                    det.createdBy = UserInfo.findByUser(createdBy)?.name
                }
            }

            def currentUser = User.get(springSecurityService.principal.id)
            def userRole = UserRole.findByUser(currentUser)
            def isSales = userRole.role.authority == AppConstants.ROLE_SALES
            def isAdmin = userRole.role.authority == AppConstants.ROLE_ADMIN

            det.canEdit = AppConstants.DEAL_STATUS_NEW.equals(obj.status)
            det.canTake = isSales && AppConstants.DEAL_STATUS_NEW.equals(obj.status)
            if(AppConstants.DEAL_STATUS_IN_PROGRESS.equals(obj.status)){
                if(isAdmin || obj.sales?.user?.id == currentUser.id){
                    det.canRelease = true
                }
            }
            if(isSales && obj.sales?.user?.id == currentUser.id && AppConstants.DEAL_STATUS_IN_PROGRESS.equals(obj.status)){
                det.canSubmit = true
            }
            if(isAdmin && AppConstants.DEAL_STATUS_SUBMITTED.equals(obj.status)){
                det.canApprove = true
            }

            [detail: det]
        } else {
            flash.error = "Deal tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def propertyTypeList = PropertyType.list()
        if(params._action_create == 'Back'){
            [detail: [name:params.name, developer:params.developer, address:params.address, propertyTypeId: params.propertyTypeId,
                    attachment:params.attachment, attachmentName: params.attachmentName, description: params.description],propertyTypeList:propertyTypeList]
        } else {
            [detail:[:],propertyTypeList:propertyTypeList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"

        def name = params.name
        def developer = params.developer
        def address = params.address
        def propertyTypeId = params.propertyTypeId
        def description = params.description
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [name:name, developer:developer, address:address, propertyTypeId:propertyTypeId, description: description,
                      attachment:attachment, attachmentName:attachmentName, stage:'Confirm', actionType: 'add', action: 'Propose New Deal']
        if(ControllerUtil.hasValue(propertyTypeId)){
            def propertyType = PropertyType.get(propertyTypeId)
            result.propertyTypeName = propertyType?.name
        }
        render(view:'confirm', model: [detail: result])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"

        def name = params.name
        def developer = params.developer
        def address = params.address
        def propertyTypeId = params.propertyTypeId
        def description = params.description
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [name:name, developer:developer, address:address, propertyTypeId:propertyTypeId, description: description,
                      attachment:attachment, attachmentName:attachmentName, actionType: 'add', action: 'Propose New Deal']
        def isError = false
        def message = ""

        def currentUser = User.get(springSecurityService.principal.id)
        def userRole = UserRole.findByUser(currentUser)
        def isAdmin = userRole.role.authority == AppConstants.ROLE_ADMIN

        PropertyDeal.withTransaction { status ->
            try {
                def obj = new PropertyDeal()
                obj.name = name
                obj.developer = developer
                obj.address = address
                if(ControllerUtil.hasValue(propertyTypeId)){
                    obj.propertyType = PropertyType.get(propertyTypeId)
                }
                obj.description = description
                if(!isAdmin){
                    obj.startDate = new Date()
                    obj.status = AppConstants.DEAL_STATUS_IN_PROGRESS
                    obj.sales = UserInfo.findByUser(currentUser)
                } else {
                    obj.status = AppConstants.DEAL_STATUS_NEW
                }
                obj.createdBy = currentUser.id

                if(ControllerUtil.hasValue(attachment)) {
                    //copy file from tmp
                    def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(attachment)
                    def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").concat(attachment)
                    def src = new File(srcFilePath)
                    def dst = new File(dstFilePath)
                    dst << src.bytes
                    if (src.exists()) src.delete()
                }
                obj.attachment = attachment
                obj.attachmentName = attachmentName

                result.propertyTypeName = obj.propertyType?.name

                obj.save(flush: true)
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "System error"
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Deal baru telah diajukan."
        }
        render view: 'confirm', model: [detail: result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def obj = PropertyDeal.get(params.id)
        def propertyTypeList = PropertyType.list()
        if(obj==null){
            flash.message = "Deal tidak ditemukan"
            redirect(action: 'index')
        } else if(AppConstants.DEAL_STATUS_SUBMITTED.equals(obj.status)){
            flash.message = "Tidak bisa ubah data deal yang sudah diajukan."
            redirect(action: 'index')
        } else {
            if (params._action_edit == 'Back') {
                [propertyTypeList:propertyTypeList, detail: [id: params.id, name: params.name, developer: params.developer, address: params.address, description: params.description,
                                                             propertyTypeId: params.propertyTypeId, attachment: params.attachment, attachmentName: params.attachmentName]]
            } else {
                [propertyTypeList:propertyTypeList, detail: [id: obj.id, name: obj.name, developer: obj.developer, address: obj.address, description: obj.description,
                                                             propertyTypeId: obj.propertyType?.id, attachment: obj.attachment, attachmentName: obj.attachmentName]]
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"

        def id = params.id
        def name = params.name
        def developer = params.developer
        def address = params.address
        def propertyTypeId = params.propertyTypeId
        def description = params.description
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [id:id, name:name, developer:developer, address:address, propertyTypeId:propertyTypeId, description: description,
                      attachment:attachment, attachmentName:attachmentName, stage:'Confirm', actionType: 'edit', action: 'Edit Deal']
        if(ControllerUtil.hasValue(propertyTypeId)){
            def propertyType = PropertyType.get(propertyTypeId)
            result.propertyTypeName = propertyType?.name
        }
        render(view:'confirm', model: [detail: result])
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"

        def id = params.id
        def name = params.name
        def developer = params.developer
        def address = params.address
        def propertyTypeId = params.propertyTypeId
        def description = params.description
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def result = [id:id, name:name, developer:developer, address:address, propertyTypeId:propertyTypeId, description: description,
                      attachment:attachment, attachmentName:attachmentName, actionType: 'edit', action: 'Edit Deal']
        def isError = false
        def message = ""

        def obj = PropertyDeal.get(id)
        if(obj==null){
            flash.message = "Deal tidak ditemukan"
            redirect(action: 'index')
            return
        } else if(AppConstants.DEAL_STATUS_SUBMITTED.equals(obj.status)){
            flash.message = "Tidak bisa ubah data deal yang sudah diajukan."
            redirect(action: 'index')
            return
        } else {
            def currentUser = User.get(springSecurityService.principal.id)
            PropertyDeal.withTransaction { status ->
                try {
                    def oldFile = obj.attachment
                    obj.name = name
                    obj.developer = developer
                    obj.address = address
                    if (ControllerUtil.hasValue(propertyTypeId)) {
                        obj.propertyType = PropertyType.get(propertyTypeId)
                    }
                    obj.description = description
                    obj.updatedBy = currentUser.id

                    if(oldFile!=attachment && ControllerUtil.hasValue(attachment)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(attachment)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").concat(attachment)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }
                    obj.attachment = attachment
                    obj.attachmentName = attachmentName

                    result.propertyTypeName = obj.propertyType?.name

                    obj.save(flush: true)
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "System error"
                }
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Deal berhasil diperbaharui"
        }
        render view: 'confirm', model: [detail: result]
    }

    def takeDeal(){
        log.debug "IN takeDeal... $params"
        def id = params.id
        def obj = PropertyDeal.get(id)
        if(obj==null){
            flash.error = "Deal tidak ditemukan"
        } else if(!AppConstants.DEAL_STATUS_NEW.equals(obj.status)){
            flash.error = "Deal ini tidak bisa diambil"
        } else {
            def currentUser = User.get(springSecurityService.principal.id)
            def userRole = UserRole.findByUser(currentUser)
            def isSales = userRole.role.authority == AppConstants.ROLE_SALES

            if(!isSales){
                flash.error = "Anda bukan tim sales"
            } else {
                PropertyDeal.withTransaction { status ->
                    try {
                        obj.sales = UserInfo.findByUser(currentUser)
                        obj.status = AppConstants.DEAL_STATUS_IN_PROGRESS
                        obj.startDate = new Date()
                        obj.updatedBy = currentUser.id
                        obj.save(flush: true)
                        flash.message = "Deal sedang dalam proses."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        flash.error = "Sistem bermasalah, silahkan coba lagi beberapa saat."
                    }
                }
            }
        }
        redirect(action: 'index')
    }

    def submitDeal(){
        log.debug "IN submitDeal... $params"
        def id = params.id
        def obj = PropertyDeal.get(id)
        if(obj==null){
            flash.error = "Deal tidak ditemukan"
        } else if(!AppConstants.DEAL_STATUS_IN_PROGRESS.equals(obj.status)){
            flash.error = "Deal ini tidak bisa disubmit"
        } else {
            def currentUser = User.get(springSecurityService.principal.id)
            def userRole = UserRole.findByUser(currentUser)
            def isSales = userRole.role.authority == AppConstants.ROLE_SALES

            if(!isSales){
                flash.error = "Anda bukan tim sales"
            } else {
                PropertyDeal.withTransaction { status ->
                    try {
                        obj.status = AppConstants.DEAL_STATUS_SUBMITTED
                        obj.updatedBy = currentUser.id
                        obj.save(flush: true)
                        flash.message = "Deal sudah diajukan untuk menunggu dinilai."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        flash.error = "Sistem bermasalah, silahkan coba lagi beberapa saat."
                    }
                }
            }
        }
        redirect(action: 'index')
    }

    def releaseDeal(){
        log.debug "IN releaseDeal... $params"
        def id = params.id
        def obj = PropertyDeal.get(id)
        if(obj==null){
            flash.error = "Deal tidak ditemukan"
        } else if(!AppConstants.DEAL_STATUS_IN_PROGRESS.equals(obj.status)){
            flash.error = "Deal tidak bisa dilepas."
        } else {
            def currentUser = User.get(springSecurityService.principal.id)
            def userRole = UserRole.findByUser(currentUser)
            def isSales = userRole.role.authority == AppConstants.ROLE_SALES

            if(!isSales){
                flash.error = "Anda bukan tim sales"
            } else {
                PropertyDeal.withTransaction { status ->
                    try {
                        obj.status = AppConstants.DEAL_STATUS_NEW
                        obj.startDate = null
                        obj.updatedBy = currentUser.id
                        obj.save(flush: true)
                        flash.message = "Anda sudah tidak memegang deal ini."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        flash.error = "Sistem bermasalah, silahkan coba lagi beberapa saat."
                    }
                }
            }
        }
        redirect(action: 'index')
    }

    def approveDeal(){
        log.debug "IN approveDeal... $params"
        def obj = PropertyDeal.get(params.id)
        if(obj!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def currentDate = new Date()
            def det = [id:obj.id, name:obj.name, developer:obj.developer, address:obj.address, propertyType:obj?.propertyType?.name,
                       attachment: obj.attachment, attachmentName: obj.attachmentName, description: obj.description]
            if(obj.startDate!=null){
                det.startDate = sdf.format(obj.startDate)
                use(groovy.time.TimeCategory) {
                    def duration = currentDate - obj.startDate
                    det.dayCount = duration.days
                }
            }
            det.status = AppConstants.getDealStatusName(obj.status)
            if(obj.createdDate!=null) {
                det.createdDate = sdf.format(obj.createdDate)
                def createdBy = User.get(obj.createdBy)
                if(createdBy!=null){
                    det.createdBy = UserInfo.findByUser(createdBy)?.name
                }
            }
            if (params._action_approveDeal == 'Back') {
                det.code = params.code
                det.fee = params.fee
                det.totalUnit = params.totalUnit
                det.expiryDate = params.expiryDate
            }
            [detail: det]
        } else {
            flash.error = "Deal tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmApproveDeal(){
        log.debug "IN confirmApproveDeal... $params"

        def id = params.id
        def code = params.code
        def fee = params.fee
        def totalUnit = params.totalUnit
        def expiryDate = params.expiryDate

        if(!ControllerUtil.hasValue(code) || !ControllerUtil.hasValue(fee) || !ControllerUtil.hasValue(totalUnit) || !ControllerUtil.hasValue(expiryDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_approveDeal = 'Back'
            redirect(action: 'approveDeal', params:params)
            return
        }

        def obj = PropertyDeal.get(params.id)
        if(obj!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def currentDate = new Date()
            def det = [id:obj.id, name:obj.name, developer:obj.developer, address:obj.address, propertyType:obj?.propertyType?.name,
                       attachment: obj.attachment, attachmentName: obj.attachmentName, description: obj.description]
            if(obj.startDate!=null){
                det.startDate = sdf.format(obj.startDate)
                use(groovy.time.TimeCategory) {
                    def duration = currentDate - obj.startDate
                    det.dayCount = duration.days
                }
            }
            det.status = AppConstants.getDealStatusName(obj.status)
            if(obj.createdDate!=null) {
                det.createdDate = sdf.format(obj.createdDate)
                def createdBy = User.get(obj.createdBy)
                if(createdBy!=null){
                    det.createdBy = UserInfo.findByUser(createdBy)?.name
                }
            }

            det.code = code
            det.fee = fee
            det.totalUnit = totalUnit
            det.expiryDate = expiryDate
            det.stage = "Confirm"

            [detail: det]
        } else {
            flash.error = "Deal tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitApproveDeal(){
        log.debug "IN submitApproveDeal... $params"
        def id = params.id
        def code = params.code
        def fee = params.fee
        def totalUnit = params.totalUnit
        def expiryDate = params.expiryDate

        if(!ControllerUtil.hasValue(code) || !ControllerUtil.hasValue(fee) || !ControllerUtil.hasValue(totalUnit) || !ControllerUtil.hasValue(expiryDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            redirect(action: 'approveDeal', params:params)
            return
        }

        def obj = PropertyDeal.get(id)
        if(obj==null){
            flash.error = "Deal tidak ditemukan"
            redirect(action: 'index')
            return
        } else if(!AppConstants.DEAL_STATUS_SUBMITTED.equals(obj.status)){
            flash.error = "Deal tidak bisa dilepas"
            redirect(action: 'index')
            return
        } else {
            def currentUser = User.get(springSecurityService.principal.id)
            def userRole = UserRole.findByUser(currentUser)
            def isAdmin = userRole.role.authority == AppConstants.ROLE_ADMIN

            if(!isAdmin){
                flash.error = "Anda bukan admin"
                redirect(action: 'index')
                return
            } else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def currentDate = new Date()
                def det = [id:obj.id, name:obj.name, developer:obj.developer, address:obj.address, propertyType:obj?.propertyType?.name,
                           attachment: obj.attachment, attachmentName: obj.attachmentName, description: obj.description]
                if(obj.startDate!=null){
                    det.startDate = sdf.format(obj.startDate)
                    use(groovy.time.TimeCategory) {
                        def duration = currentDate - obj.startDate
                        det.dayCount = duration.days
                    }
                }
                det.status = AppConstants.getDealStatusName(obj.status)
                if(obj.createdDate!=null) {
                    det.createdDate = sdf.format(obj.createdDate)
                    def createdBy = User.get(obj.createdBy)
                    if(createdBy!=null){
                        det.createdBy = UserInfo.findByUser(createdBy)?.name
                    }
                }

                det.code = code
                det.fee = fee
                det.totalUnit = totalUnit
                det.expiryDate = expiryDate

                PropertyDeal.withTransaction { status ->
                    try {
                        obj.status = AppConstants.DEAL_STATUS_APPROVED
                        obj.updatedBy = currentUser.id
                        obj.save()

                        def prop = new Property()
                        prop.code = code
                        prop.name = obj.name
                        prop.address = obj.address
                        prop.developer = obj.developer
                        prop.sales = obj.sales
                        prop.propertyType = obj.propertyType
                        if(ControllerUtil.hasValue(totalUnit)){
                            prop.totalUnit = Integer.valueOf(totalUnit)
                        }
                        if(ControllerUtil.hasValue(fee)){
                            prop.fee = new BigDecimal(fee)
                        }
                        if (ControllerUtil.hasValue(expiryDate)) {
                            prop.expiryDate = sdf.parse(expiryDate)
                        }
                        prop.save()

                        det.stage = "Result"
                        flash.message = "Deal sudah disetujui, data properti sudah ditambahkan."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        det.stage = 'Confirm'
                        flash.error = "Sistem bermasalah, silahkan coba lagi beberapa saat."
                    }
                }

                render view:'confirmApproveDeal', model:[detail:det]
            }
        }
    }


    def delete(){
        log.debug "IN delete... $params"
        def obj = PropertyDeal.get(params.id)

        def currentUser = User.get(springSecurityService.principal.id)
        def userRole = UserRole.findByUser(currentUser)
        def isAdmin = userRole.role.authority == AppConstants.ROLE_ADMIN

        def isError = false, message = ""
        if(!isAdmin){
            if(obj.status!=AppConstants.DEAL_STATUS_NEW || obj.createdBy!=currentUser.id){
                message = "You can't delete this deal"
                isError = true
            }
        }

        if(!isError){
            Member.withTransaction { status ->
                try {
                    if(ControllerUtil.hasValue(obj.attachment)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").concat(obj.attachment))
                        if (image.exists()) image.delete()
                    }
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "System error"
                }
            }
        }

        if(isError) {
            flash.error = message
        } else {
            flash.message = "Deal berhasil dihapus"
        }
        redirect(action: 'index')
    }

    def downloadFile() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").concat(params.id)
            def fileName = PropertyDeal.findByAttachment(params.id)?.attachmentName?:"attachment"
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] fileBytes = is.bytes
                response.setHeader('Content-length', fileBytes.length.toString())
                response.setHeader('Content-disposition', "attachment;filename=\"${fileName}\"")
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << fileBytes
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}deal${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }
}
