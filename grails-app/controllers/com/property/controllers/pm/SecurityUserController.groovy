package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.*
import com.model.information.BroadcastReminder
import com.model.information.InboxMessage
import com.model.maintenance.Gender
import com.model.maintenance.RoleMobile
import com.model.property.Property
import com.model.property.PropertyArea
import com.model.property.PropertyUnit
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import com.property.utils.ExcelImporter
import com.property.utils.FirebaseUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.plugins.mail.MailMessageBuilder
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat



class SecurityUserController implements ResourceLoaderAware {

    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    GrailsApplication grailsApplication
    def mailService
    def mailMessageContentRenderer

    def index() {
        log.debug "IN index... $params"
        [:]
    }

    def listDatatable(){
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)

        def columns = ['security.email', 'security.name', 'security.phoneNo', 'isDefault']
        def baseQuery = "FROM SecurityProperty WHERE property.id = :propId "
        def query = baseQuery
        def queryCount = "SELECT count(security) " + baseQuery

        def queryParams = [propId:prop.id]
        def result = ControllerUtil.parseDatatablesQuery(SecurityProperty.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ obj ->
            aaData.add([id:obj.security.id, email: obj.security.email, name: obj.security.name, isDefaultDisplay: ControllerUtil.getFlagYesNo(obj.isDefault),
                        isDefault: obj.isDefault, phoneNo: obj.security.phoneNo, createdDate:sdf.format(obj.security.createdDate)])
        }
        result.aaData = aaData
        result.objs = null

        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(params.id)
        def security = SecurityProperty.findBySecurityAndProperty(userMobile,prop)
        if(security!=null){
            def result = [:]
            result.id = params.id
            result.email = userMobile.email
            result.name = userMobile.name
            result.phoneNo = userMobile.phoneNo
            result.idKTP = userMobile.idKTP
            result.dob = ControllerUtil.parseDate(userMobile.dob, "yyyy-MM-dd")
            result.genderName = userMobile.gender?.name
            result.title = userMobile.title
            result.propName = prop.name
            [user:result]
        } else {
            flash.error = "User keamanan tidak ditemukan"
            redirect(action:'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def result = [:]
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def genderList = Gender.list()
        if(params._action_create == 'Back'){
            result = [propName: prop.name, id:params.id, email:params.email, name:params.name, phoneNo:params.phoneNo,
                      idKTP:params.idKTP, dob:params.dob, genderId:params.genderId, title:params.title]
            [user: result, areaList:areaList, genderList:genderList]
        } else {
            result = [propName: prop.name]
            [user: result, areaList:areaList, genderList:genderList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def idKTP = params.idKTP
        def dob = params.dob
        def propName = params.propName
        def genderId = params.genderId
        def title = params.title

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'editUser', params:params)
            return
        }

        def result = [propName: propName, email:email, name:name, phoneNo:phoneNo,
                      idKTP:idKTP, dob:dob, genderId:genderId, title:title,
                      actionType: 'add', action:'Create User', stage:'Confirm']
        if(ControllerUtil.hasValue(genderId)){
            result.genderName = Gender.get(genderId)?.name
        }
        render view:'confirm', model: [user: result]
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def idKTP = params.idKTP
        def dob = params.dob
        def propName = params.propName
        def genderId = params.genderId
        def title = params.title

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def isError = false, message = ''

        def result = [propName: propName, email:email, name:name, phoneNo:phoneNo,
                      idKTP:idKTP, dob:dob, genderId:genderId, title:title,
                      actionType: 'add', action:'Create User', stage:'Result']

        UserMobile.withTransaction { status ->
            try {

                def response = FirebaseUtil.createUser(email)
                if (response.isSuccess) {
                    def userMobile = new UserMobile()
                    userMobile.email = email
                    userMobile.name = name
                    userMobile.phoneNo = phoneNo
                    userMobile.idKTP = idKTP
                    userMobile.title = title
                    userMobile.uid = response.uid
                    userMobile.dob = ControllerUtil.convertToDate(dob, "yyyy-MM-dd")
                    if (ControllerUtil.hasValue(genderId)) {
                        userMobile.gender = Gender.get(genderId)
                        result.genderName = userMobile.gender?.name
                    }
                    userMobile.createdBy = currentUser
                    userMobile.save()

                    def ump = new UserMobileProperty()
                    ump.userMobile = userMobile
                    ump.property = prop
                    ump.save()

                    def sp = new SecurityProperty()
                    sp.security = userMobile
                    sp.property = prop
                    sp.save()

                    log.debug("sending email userWelcome ${email} : ${response.password}")
                    mailService.sendMail {
                        to email
                        from "support@warga-kita.com"
                        subject "Warga Kita Reset Password"
                        html view: "userWelcome", model: [userName: name, email: email, password: response.password]
                    }
                } else {
                    isError = true
                    message = response.message
                }

            } catch (Exception e) {
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "Server bermasalah, coba lagi beberapa saat."
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "User berhasil ditambahkan"
        }
        render view: 'confirm', model: [user:result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def result = [:]
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(params.id)
        def sp = SecurityProperty.findBySecurityAndProperty(userMobile,prop)
        if(sp!=null){
            def genderList = Gender.list()
            if(params._action_edit == 'Back'){
                result = [propName: prop.name, id:params.id, email:params.email, name:params.name, phoneNo:params.phoneNo,
                          idKTP:params.idKTP, dob:params.dob, title:params.title]
                [user: result, genderList:genderList]
            } else {
                result = [propName: prop.name, id:userMobile.id, email:userMobile.email, name:userMobile.name, phoneNo:userMobile.phoneNo,
                          idKTP:userMobile.idKTP, dob:userMobile.dob, genderId:userMobile.gender?.id, title:userMobile.title]
                [user: result, genderList:genderList]
            }
        } else {
            flash.error = "User keamanan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def id = params.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def idKTP = params.idKTP
        def dob = params.dob
        def propName = params.propName
        def genderId = params.genderId
        def title = params.title

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'editUser', params:params)
            return
        }

        def result = [propName: propName, id:id, email:email, name:name, phoneNo:phoneNo,
                      idKTP:idKTP, dob:dob, genderId:genderId, title:title,
                      actionType: 'edit', action:'Edit User', stage:'Confirm']
        if(ControllerUtil.hasValue(genderId)){
            result.genderName = Gender.get(genderId)?.name
        }
        render view:'confirm', model: [user: result]
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id

        def id = params.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def idKTP = params.idKTP
        def dob = params.dob
        def propName = params.propName
        def genderId = params.genderId
        def title = params.title

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(id)
        def sp = SecurityProperty.findBySecurityAndProperty(userMobile,prop)
        def isError = false, message = ''

        def result = [propName: propName, id:id, email:email, name:name, phoneNo:phoneNo,
                      idKTP:idKTP, dob:dob, genderId:genderId, title:title,
                      actionType: 'edit', action:'Edit User', stage:'Result']
        if(sp!=null){
            UserMobile.withTransaction { status ->
                try {
                    userMobile.email = email
                    userMobile.name = name
                    userMobile.phoneNo = phoneNo
                    userMobile.idKTP = idKTP
                    userMobile.title = title
                    userMobile.dob = ControllerUtil.convertToDate(dob, "yyyy-MM-dd")
                    if (ControllerUtil.hasValue(genderId)) {
                        userMobile.gender = Gender.get(genderId)
                        result.genderName = userMobile.gender?.name
                    }
                    userMobile.updatedBy = currentUser
                    userMobile.save()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "User berhasil diperbaharui"
            }
            render view: 'confirm', model: [user:result]
        } else {
            flash.error = "User keamanan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def currentUser = springSecurityService.principal.id
        def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: currentUser]).collect { it.property }
        def prop = propList.get(0)
        def umps = UserMobileProperty.executeQuery("SELECT ump FROM UserMobileProperty ump " +
                "WHERE ump.userMobile.id = :userId AND ump.property.id = :propId",
                [userId: params.id, propId: prop.id])
        if(umps.size()>0){
            def obj = umps.get(0)
            UserMobileProperty.withTransaction { status ->
                try{
                    def deleteUser = false
                    def userMobile = obj.userMobile
                    if(UserMobileProperty.findAllByUserMobile(obj.userMobile).size()==1){
                        deleteUser = true
                    }
                    obj.delete()
                    if(deleteUser){
                        def firebaseUid = userMobile.uid
                        UserMobileUnit.findAllByUserMobile(userMobile).each{ it.delete() }
                        SecurityProperty.findAllBySecurity(userMobile).each{ it.delete() }
                        InboxMessage.findAllByUserMobile(userMobile).each{ im ->
                            BroadcastReminder.findAllByInboxMessage(im).each { br ->
                                br.delete()
                            }
                            im.delete()
                        }
                        userMobile.delete()

                        FirebaseUtil.deleteUser(firebaseUid)
                    }

                    flash.message = "User berhasil dihapus untuk properti ini."
                }  catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    flash.error = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "User keamanan tidak ditemukan"
        }
        redirect(action: 'index')
    }

    def setAsDefault(){
        log.debug "IN setAsDefault... $params"
        def currentUser = springSecurityService.principal.id
        def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: currentUser]).collect { it.property }
        def prop = propList.get(0)
        def umps = UserMobileProperty.executeQuery("SELECT ump FROM UserMobileProperty ump " +
                "WHERE ump.userMobile.id = :userId AND ump.property.id = :propId",
                [userId: params.id, propId: prop.id])
        if(SecurityProperty.findAllByPropertyAndIsDefault(prop,AppConstants.FLAG_YES).size()==3){
            flash.error = "Petugas dengan status petugas utama sudah penuh"
        } else {
            if (umps.size() > 0) {
                def obj = umps.get(0)
                UserMobileProperty.withTransaction { status ->
                    try {
                        def sec = SecurityProperty.findBySecurity(obj.userMobile)
                        sec.isDefault = AppConstants.FLAG_YES
                        sec.save()

                        flash.message = "Petugas ini sekarang adalah petugas utama untuk properti ini."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        flash.error = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            } else {
                flash.error = "Petugas keamanan tidak ditemukan"
            }
        }
        redirect(action: 'index')
    }

    def resetFromDefault(){
        log.debug "IN resetFromDefault... $params"
        def currentUser = springSecurityService.principal.id
        def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: currentUser]).collect { it.property }
        def prop = propList.get(0)
        def umps = UserMobileProperty.executeQuery("SELECT ump FROM UserMobileProperty ump " +
                "WHERE ump.userMobile.id = :userId AND ump.property.id = :propId",
                [userId: params.id, propId: prop.id])
        if(umps.size()>0){
            def obj = umps.get(0)
            UserMobileProperty.withTransaction { status ->
                try{
                    def sec = SecurityProperty.findBySecurity(obj.userMobile)
                    sec.isDefault = AppConstants.FLAG_NO
                    sec.save()

                    flash.message = "Petugas ini sekarang bukan lagi petugas utama untuk properti ini."
                }  catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    flash.error = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Petugas keamanan tidak ditemukan"
        }
        redirect(action: 'index')
    }

    def uploadFile(){
        log.debug "IN uploadFile... $params"
        [:]
    }

    def confirmUpload(){
        log.debug "IN confirmUpload... $params"
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("securityUser${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            try {
                def importer = new ExcelImporter(filePath)
                def userList = importer.getSecurityUserList()
                def cnt = 1

                if (userList.size() > 100) {
                    flash.error = "Jumlah data tidak boleh lebih dari 100 user"
                    redirect(action:'uploadFile')
                } else {
                    userList.each{ map ->
                        map.put("cnt", cnt)
                        cnt++
                    }

                    [fileImportId: params.fileImportId, userList:userList]
                }

            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                flash.error = "Server bermasalah, coba lagi beberapa saat."
                redirect(action:'uploadFile')
            }
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def submitImport(){
        log.debug "IN submitImport... $params"
        def currentUser = springSecurityService.principal.id
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("securityUser${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        int totalRecord =0, totalSuccess = 0
        if(file.exists()){
            def importer = new ExcelImporter(filePath)
            def userList = importer.getSecurityUserList()
            totalRecord = userList.size()
            def cnt = 1
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                    [currentUser:currentUser]).collect{ it.property }
            if(propList.size()==0){
                flash.error = "Properti tidak ditemukan"
                redirect(action:"index")
                return
            }
            def prop = propList[0]
            userList.each{ map ->
                map.put("cnt", cnt)
                cnt++

                def actionStartTime = System.currentTimeMillis()

                if( !ControllerUtil.hasValue(map.get("name")) ||
                    !ControllerUtil.hasValue(map.get("email")) ||
                    !ControllerUtil.hasValue(map.get("idKTP")) ||
                    !ControllerUtil.hasValue(map.get("dob")) ||
                    !ControllerUtil.hasValue(map.get("phoneNo")) ||
                    !ControllerUtil.hasValue(map.get("gender"))){
                    map.put("message", "Lengkapi data yang belum diisi.")
                } else {
                    def gender = Gender.findByCode(map.get("gender"))
                    if(UserMobile.findByEmail(map.get("email"))!=null){
                        map.put("message", "User sudah terdaftar")
                    } else if(gender==null){
                        map.put("message", "Jenis Kelamin Salah")
                    } else {
                        UserMobile.withTransaction { statusTrx ->

                            log.debug("creating user email: ${map.get("email")}")
                            def response = FirebaseUtil.createUser(map.get("email"))
                            log.debug "creating user firebase time : ${System.currentTimeMillis() - actionStartTime} ms, status : ${response.message}"
                            if (response.isSuccess) {
                                try {
                                    def obj = new UserMobile()
                                    obj.name = map.get("name")
                                    obj.email = map.get("email")
                                    obj.idKTP = map.get("idKTP")
                                    if(ControllerUtil.hasValue(map.get("dob"))){
                                        obj.dob = sdf.parse(map.get("dob"))
                                    }
                                    obj.phoneNo = map.get("phoneNo")
                                    obj.gender = gender
                                    obj.title = map.get("title")
                                    obj.createdBy = currentUser
                                    obj.uid = response.uid
                                    obj.save(flash: true)

                                    def ump = new UserMobileProperty()
                                    ump.userMobile = obj
                                    ump.property = prop
                                    ump.save(flash: true)

                                    def sp = new SecurityProperty()
                                    sp.property = prop
                                    sp.security = obj
                                    sp.save(flash: true)
                                    map.put("message", "User berhasil ditambahkan.")

                                    def tm = new TmpMail()
                                    tm.emailTo = obj.email
                                    tm.emailFrom = "support@warga-kita.com"
                                    tm.subject = "Pendaftaran User Baru Warga Kita"
                                    tm.content = mailMessageContentRenderer.render(new StringWriter(),"userWelcome", [userName: obj.name, email: obj.email, password: response.password], Locale.ENGLISH, "").out.toString()
                                    tm.status = AppConstants.PROCESS_NEW
                                    tm.save(flash: true)

                                    totalSuccess++

                                } catch (Exception e) {
                                    statusTrx.setRollbackOnly()
                                    log.error(ExceptionUtils.getStackTrace(e))
                                    map.put("message", "Sistem error - Data tidak diproses")
                                }
                            } else {
                                map.put("message", response.message)
                            }

                        }
                    }
                }
                log.debug "processing time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms, status : ${map.get("message")}"
            }
            file.delete()
            flash.message="File berhasil diproses, ${totalSuccess} user ditambahkan dari ${totalRecord} data."
            render view:'resultUpload', model:[userList:userList]
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").toString().concat("securityUser${File.separator}"))
                    uploadDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def downloadTemplate(){
        log.debug "IN downloadTemplate... $params"
        byte[] imageInByte = assetResourceLocator.findAssetForURI('security-user-template.xls').getInputStream()?.bytes
        response.setHeader('Content-length', imageInByte.length.toString())
        response.setHeader("Content-disposition", "attachment; filename=\"security-user-template.xls\"")
        response.contentType = "application/vnd.ms-excel" // or the appropriate image content type
        response.outputStream << imageInByte
        response.outputStream.flush()
    }
}
