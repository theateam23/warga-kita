package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.information.PropertyAnnouncement
import com.model.information.PropertyAnnouncementTarget
import com.model.maintenance.RoleMobile
import com.model.property.Property
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat


@Deprecated
class AnnouncementController {

    def springSecurityService
    GrailsApplication grailsApplication
    
    def index() {
        log.debug "IN index... $params"
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId
        def columns = ['periodStart', 'periodEnd', 'property.name', 'title', '', '', 'createdDate']
        def baseQuery = "FROM PropertyAnnouncement WHERE property.propertyManager.user.id = :pmId"
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [pmId: pmId]
        def result = ControllerUtil.parseDatatablesQuery(PropertyAnnouncement.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        def sdf2 = new SimpleDateFormat("yyyy-MM-dd")
        objs.each{ obj ->
            def targetNameList = []
            PropertyAnnouncementTarget.findAllByPropertyAnnouncement(obj).each{
                targetNameList.add(it.roleMobile.name)
            }
            aaData.add([id:obj.id, title: obj.title, propName: obj.property.name, periodStart: sdf2.format(obj.periodStart), periodEnd: sdf2.format(obj.periodEnd),
                        createdDate:sdf.format(obj.createdDate), target: targetNameList.join(", "), imageFileId: obj.imageFileId, imageFileName:obj.imageFileName])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def pmId = springSecurityService.principal?.pmId
        def announcement = PropertyAnnouncement.executeQuery("FROM PropertyAnnouncement where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(announcement!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def periodDate = "${sdf.format(announcement.periodStart)} to ${sdf.format(announcement.periodEnd)}"
            def targetNameList = []
            PropertyAnnouncementTarget.findAllByPropertyAnnouncement(announcement).each { t ->
                targetNameList.add(t.roleMobile.name)
            }
            def result = [id: params.id, title: announcement.title, propId: announcement.property.id, periodDate:periodDate, propName: announcement.property.name,
                          imageFileId: announcement.imageFileId, imageFileName: announcement.imageFileName]
            [announcement: result, targetNameList: targetNameList]
        } else {
            flash.error = "Pengumuman tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def pmId = springSecurityService.principal?.pmId
        def isError = false, message = ''
        def announcement = PropertyAnnouncement.executeQuery("FROM PropertyAnnouncement where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()

        if(announcement!=null){
            PropertyAnnouncement.withTransaction { status ->
                try {
                    PropertyAnnouncementTarget.findAllByPropertyAnnouncement(announcement).each { t ->
                        t.delete()
                    }
                    announcement.delete()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Announcement tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Pengumuman berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def roleList = RoleMobile.list()
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        if(params._action_create == 'Back'){
            def target = []
            if(params.target!=null){
                if(params.target instanceof String){
                    target.add(params.target)
                } else {
                    target = params.target
                }
            }
            [announcement: [propName:prop.name, title:params.title, propId: params.propId, target: target,
                            periodStart: params.periodStart, periodEnd: params.periodEnd,
                            imageFileId: params.imageFileId, imageFileName: params.imageFileName], roleList:roleList]
        } else {
            [announcement:[propName:prop.name], roleList:roleList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def pmId = springSecurityService.principal?.pmId
        def title = params.title
        def periodStart = params.periodStart
        def periodEnd = params.periodEnd
        def propName = params.propName
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        if(!ControllerUtil.hasValue(periodStart) || !ControllerUtil.hasValue(periodEnd) || target.size()==0 || !ControllerUtil.hasValue(params.imageFileId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def lPeriodStart = Long.valueOf(periodStart)
        def lPeriodEnd = Long.valueOf(periodEnd)
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def periodDate = "${ControllerUtil.convertToStrDate(lPeriodStart,sdf)} to ${ControllerUtil.convertToStrDate(lPeriodEnd,sdf)}"
        def result = [title:title, periodStart: periodStart, periodEnd: periodEnd, periodDate:periodDate, target:target]
        def targetNameList = []
        target.each{ roleId ->
            targetNameList.add(RoleMobile.get(roleId).name)
        }
        result.imageFileId = params.imageFileId
        result.imageFileName = params.imageFileName
        result.propName = propName
        result.action = 'Create Announcement'
        result.stage = 'Confirm'
        result.actionType = 'add'
        render(view:'confirm', model: [announcement: result, targetNameList:targetNameList])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def title = params.title
        def periodStart = params.periodStart
        def periodEnd = params.periodEnd
        def propName = params.propName
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        def lPeriodStart = Long.valueOf(periodStart)
        def lPeriodEnd = Long.valueOf(periodEnd)
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def periodDate = "${ControllerUtil.convertToStrDate(lPeriodStart,sdf)} to ${ControllerUtil.convertToStrDate(lPeriodEnd,sdf)}"
        def result = [title:title, periodStart: periodStart, periodEnd: periodEnd, periodDate:periodDate, propName:propName, imageFileId: params.imageFileId,
                      imageFileName: params.imageFileName, target:params.target, actionType: 'add', action:'Create Announcement']
        def isError = false
        def message = ""

        def targetNameList = []
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)
            PropertyAnnouncement.withTransaction { status ->
                try {
                    def announcement = new PropertyAnnouncement()
                    announcement.title = title
                    announcement.periodStart = new Date(lPeriodStart)
                    announcement.periodEnd = new Date(lPeriodEnd)
                    announcement.property = prop
                    announcement.createdBy = currentUser
                    announcement.imageFileId = params.imageFileId
                    announcement.imageFileName = params.imageFileName
                    announcement.save(flush: true)

                    if(ControllerUtil.hasValue(params.imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                    }

                    target.each { roleId ->
                        def roleMobile = RoleMobile.get(roleId)
                        def pnt = new PropertyAnnouncementTarget()
                        pnt.roleMobile = roleMobile
                        pnt.propertyAnnouncement = announcement
                        pnt.save()
                        targetNameList.add(roleMobile.name)
                    }

                    result.propName = prop.name
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Properti tidak ditemukan"
            redirect(action:"index")
            return
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Pengumuman berhasil ditambahkan"
        }
        render view: 'confirm', model: [announcement: result, targetNameList:targetNameList]
    }

    def edit(){
        log.debug "IN edit... $params"
        def roleList = RoleMobile.list()
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        if(params._action_edit == 'Back'){
            def target = []
            if(params.target!=null){
                if(params.target instanceof String){
                    target.add(params.target)
                } else {
                    target = params.target
                }
            }
            [announcement: [id:params.id, title: params.title, propName:prop.name, periodStart: params.periodStart, periodEnd:params.periodEnd, target:target,
                            imageFileId: params.imageFileId, imageFileName: params.imageFileName], roleList:roleList]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def announcement = PropertyAnnouncement.executeQuery("FROM PropertyAnnouncement where id = :id AND property.propertyManager.user.id = :pmId",
                    [id:params.id, pmId:pmId]).first()
            if (announcement != null) {
                def target = []
                PropertyAnnouncementTarget.findAllByPropertyAnnouncement(announcement).each { t ->
                    target.add(t.roleMobile.id)
                }
                def result = [propName:prop.name, id:params.id, title: announcement.title, periodStart: announcement.periodStart?.time?:'',
                              periodEnd: announcement.periodEnd?.time?:'', target:target, imageFileId: announcement.imageFileId, imageFileName: announcement.imageFileName,
                              actionType: 'edit', action: 'Edit Announcement', stage: 'Confirm']
                [announcement: result, roleList:roleList]
            } else {
                flash.error = "Pengumuman tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def title = params.title
        def periodStart = params.periodStart
        def periodEnd = params.periodEnd
        def propName = params.propName
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        if(!ControllerUtil.hasValue(periodStart) || !ControllerUtil.hasValue(periodEnd) || target.size()==0 || !ControllerUtil.hasValue(params.imageFileId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def announcement = PropertyAnnouncement.executeQuery("FROM PropertyAnnouncement where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(announcement!=null){
            def lPeriodStart = Long.valueOf(periodStart)
            def lPeriodEnd = Long.valueOf(periodEnd)
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def periodDate = "${ControllerUtil.convertToStrDate(lPeriodStart,sdf)} to ${ControllerUtil.convertToStrDate(lPeriodEnd,sdf)}"
            def targetNameList = []
            target.each{ roleId ->
                targetNameList.add(RoleMobile.get(roleId).name)
            }
            def result = [id:params.id, title:title, propName:propName, periodDate:periodDate, periodStart:periodStart, periodEnd:periodEnd, target:target,
                          imageFileId: params.imageFileId, imageFileName: params.imageFileName, actionType: 'edit', action:'Edit Announcement', stage:'Confirm']
            render view:'confirm', model: [announcement:result, targetNameList: targetNameList]
        } else {
            flash.error = "Pengumuman tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def title = params.title
        def periodStart = params.periodStart
        def periodEnd = params.periodEnd
        def propName = params.propName
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        def isError = false
        def message = ""
        def lPeriodStart = Long.valueOf(periodStart)
        def lPeriodEnd = Long.valueOf(periodEnd)
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def periodDate = "${ControllerUtil.convertToStrDate(lPeriodStart,sdf)} to ${ControllerUtil.convertToStrDate(lPeriodEnd,sdf)}"
        def result = [title:title, periodStart: periodStart, periodEnd: periodEnd, periodDate:periodDate, propName:propName,
                      imageFileId: params.imageFileId, imageFileName: params.imageFileName, actionType: 'edit', action:'Edit Announcement']

        def targetNameList = []
        def announcement = PropertyAnnouncement.executeQuery("FROM PropertyAnnouncement where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(announcement!=null){
            PropertyAnnouncement.withTransaction { status ->
                try {
                    def oldFile = announcement.imageFileId
                    announcement.title = title
                    announcement.periodStart = new Date(lPeriodStart)
                    announcement.periodEnd = new Date(lPeriodEnd)
                    announcement.updatedBy = currentUser
                    announcement.imageFileId = params.imageFileId
                    announcement.imageFileName = params.imageFileName
                    announcement.save(flush: true)

                    if(oldFile!=params.imageFileId && ControllerUtil.hasValue(params.imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }

                    //delete old target
                    PropertyAnnouncementTarget.findAllByPropertyAnnouncement(announcement).each { it.delete(flush:true) }
                    //recreate
                    target.each { roleId ->
                        def roleMobile = RoleMobile.get(roleId)
                        def pnt = new PropertyAnnouncementTarget()
                        pnt.roleMobile = roleMobile
                        pnt.propertyAnnouncement = announcement
                        pnt.save()
                        targetNameList.add(roleMobile.name)
                    }
                } catch (Exception e){
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Pengumuman tidak ditemukan"
            redirect(action: 'index')
            return
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Pengumuman berhasil diperbaharui"
        }

        render view:'confirm', model: [announcement: result, targetNameList:targetNameList]
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }
}
