package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.general.SecurityProperty
import com.model.general.UserMobileUnit
import com.model.information.BroadcastMessageLog
import com.model.information.BroadcastReminder
import com.model.information.InboxMessage
import com.model.property.Property
import com.model.property.PropertyArea
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import com.property.utils.FirebaseUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat

class BroadcastMessageController {

    def springSecurityService
    def userService
    GrailsApplication grailsApplication

    def index() {
        log.debug "index..."
        [areaName:params.areaName]
    }

    def listDatatable(){
        log.debug "$params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def propId = PropertyAdminProperty.findByPropertyAdmin(currentPm)?.property?.id

        def columns = ['createdDate', 'title', 'reminderDate', 'target', 'createdBy']
        def baseQuery = "FROM BroadcastMessageLog WHERE (target is null OR lower(target) like :areaName) AND property.id = :propId "

        def areaName = ControllerUtil.hasValue(params.areaName)?"%${params.areaName.toLowerCase()}%".toString():"%"

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [areaName: areaName, propId:propId]
        def result = ControllerUtil.parseDatatablesQuery(BroadcastMessageLog.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ obj ->
            def det = [id:obj.id, title:obj.title, target:obj.target]
            if(obj.createdDate!=null) {
                det.createdDate = sdf.format(obj.createdDate)
            } else {
                det.createdDate = ""
            }
            if(obj.reminderDate!=null) {
                det.reminderDate = sdf.format(obj.reminderDate)
            } else {
                det.reminderDate = ""
            }
            if(!ControllerUtil.hasValue(obj.target)){
                det.target = AppConstants.ALL
            }
            det.createdBy = userService.getUserName(obj.createdBy)
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList[0]

        def obj = BroadcastMessageLog.findByIdAndProperty(params.id, prop)
        if(obj==null){
            flash.error = "Pengumuman tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        def det = [id:obj.id, title:obj.title, target:obj.target, content:obj.content]
        if(obj.createdDate!=null) {
            det.createdDate = sdf.format(obj.createdDate)
        } else {
            det.createdDate = ""
        }
        if(obj.reminderDate!=null) {
            det.reminderDate = sdf.format(obj.reminderDate)
            det.hasReminder = true
        } else {
            det.reminderDate = ""
            det.hasReminder = false
        }
        if(!ControllerUtil.hasValue(obj.target)){
            det.target = AppConstants.ALL
        }
        det.createdBy = userService.getUserName(obj.createdBy)
        [obj:det]
    }

    def create() {
        log.debug "IN create... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        [propName: prop.name, broadcastTitle: params.broadcastTitle, broadcastMessage:params.broadcastMessage,
         hasReminder: params.hasReminder, reminderDate: params.reminderDate, areaId:params.areaId, areaList:areaList]
    }

    def confirm(){
        log.debug "IN confirm... $params"
        def propName = params.propName
        def broadcastTitle = params.broadcastTitle
        def broadcastMessage = params.broadcastMessage
        def hasReminder = params.hasReminder
        def reminderDate = params.reminderDate
        def areaId = params.areaId

        if(!ControllerUtil.hasValue(broadcastTitle) || !ControllerUtil.hasValue(broadcastMessage)){
            flash.error = "Silakan isi semua data yang diperlukan"
            redirect(action: 'create', params:params)
            return
        }

        if(AppConstants.FLAG_NO.equals(hasReminder) && !ControllerUtil.hasValue(reminderDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            redirect(action: 'create', params:params)
            return
        }

        def result = [areaId:areaId, propName:propName, broadcastTitle: broadcastTitle, broadcastMessage: broadcastMessage, hasReminder:hasReminder, reminderDate:reminderDate]

        if(ControllerUtil.hasValue(areaId)){
            result.target = PropertyArea.get(areaId)?.name
        } else {
            result.target = AppConstants.ALL
        }
        result.action = 'Create Message'
        result.actionType = 'add'
        result.stage = 'Confirm'
        render(view:'confirm', model: [obj: result])
    }

    def submit(){
        log.debug "IN submit... $params"
        def currentUser = springSecurityService.principal?.id
        def propName = params.propName
        def broadcastTitle = params.broadcastTitle
        def broadcastMessage = params.broadcastMessage
        def hasReminder = params.hasReminder
        def reminderDate = params.reminderDate
        def areaId = params.areaId

        def result = [propName:propName, broadcastTitle:broadcastTitle, broadcastMessage:broadcastMessage, hasReminder:hasReminder, reminderDate:reminderDate]

        def isError = false
        def message = ""

        def propList = Property.executeQuery("SELECT property FROM PropertyAdminProperty pap WHERE pap.propertyAdmin.id = :currentUser",
                [currentUser:currentUser])
        if(propList.size()>0){
            def prop = propList.get(0)
            def topic = ""
            def notifList = UserMobileUnit.executeQuery("FROM UserMobileUnit WHERE unit.area.property.id = :propId", [propId:prop.id]).collect{ it.userMobile }
            notifList.addAll(SecurityProperty.findAllByProperty(prop).collect{ it.security })

            if(ControllerUtil.hasValue(areaId)){
                result.target = PropertyArea.get(areaId)?.name
                topic = areaId
            } else {
                result.target = prop.name
                topic = prop.id
            }

            def sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa")
            InboxMessage.withTransaction{ status ->
                notifList.each { userMobile ->
                    try {
                        def inboxMessage = new InboxMessage()
                        inboxMessage.title = broadcastTitle
                        inboxMessage.content = broadcastMessage
                        inboxMessage.userMobile = userMobile
                        inboxMessage.save()

//                        if(ControllerUtil.hasValue(reminderDate)){
//                            BroadcastReminder reminder = new BroadcastReminder()
//                            reminder.inboxMessage = inboxMessage
//                            reminder.reminderDate = sdf.parse(reminderDate)
//                            reminder.save()
//                        }

                    } catch (Exception e){
                        log.error ExceptionUtils.getStackTrace(e)
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                        return false
                    }
                }
                if(isError){
                    status.setRollbackOnly()
                } else {
                    def response = FirebaseUtil.sendNotification(broadcastTitle, broadcastMessage, topic)

                    if (!response.isSuccess) {
                        status.setRollbackOnly()
                        isError = true
                        message = response.message
                    }

                    try {
                        def inboxMessage = new InboxMessage()
                        inboxMessage.title = broadcastTitle
                        inboxMessage.content = broadcastMessage
                        inboxMessage.save()

                        def broadcastMsgLog = new BroadcastMessageLog()
                        broadcastMsgLog.title = broadcastTitle
                        broadcastMsgLog.content = broadcastMessage
                        broadcastMsgLog.target = result.target
                        broadcastMsgLog.createdDate = new Date()
                        broadcastMsgLog.createdBy = currentUser
                        broadcastMsgLog.property = prop


                        if(ControllerUtil.hasValue(reminderDate)){
                            BroadcastReminder reminder = new BroadcastReminder()
                            reminder.inboxMessage = inboxMessage
                            reminder.reminderDate = sdf.parse(reminderDate)
                            reminder.topicId = prop.id
                            reminder.save()

                            broadcastMsgLog.reminderDate = reminder.reminderDate
                        }

                        broadcastMsgLog.save()

                    } catch (Exception e){
                        log.error ExceptionUtils.getStackTrace(e)
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                        return false
                    }

                }
            }
        } else {
            message = "Data properti tidak ditemukan"
        }
        if(isError) {
            flash.error = message
            redirect action: 'create', params:params
        } else {
            flash.message = "Pengumuman berhasil dikirim."
            redirect action: 'index'
        }
//
//        BroadcastMessageLog.list().each { it ->
//            log.debug(it.content)
//        }
    }

}
