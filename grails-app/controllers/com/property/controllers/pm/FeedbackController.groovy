package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.property.UserFeedback
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class FeedbackController {
    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        [propName:prop.name, startDate:params.startDate, endDate:params.endDate]
    }

    def listDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def columns = ['userMobileProperty.property.name', 'createdDate', 'userMobileProperty.userMobile.name', 'title', 'description']
        def baseQuery = "FROM UserFeedback WHERE userMobileProperty.property.id like :propId AND userMobileProperty.property.propertyManager.id = :pmInfoId " +
                "AND createdDate BETWEEN :startDate AND :endDate "

        def prop = PropertyAdminProperty.findByPropertyAdmin(currentPm)
        def propId = prop.property.id
        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date() + 1).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }

        def query = "" + baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [startDate:startDate, propId:propId, endDate:endDate, pmInfoId:pmInfo.id]
        def result = ControllerUtil.parseDatatablesQuery(UserFeedback.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ obj ->
            def det = [id:obj.id, property: obj.userMobileProperty.property.name, user:obj.userMobileProperty.userMobile.name, description:obj.description, title: obj.title]
            if(obj.createdDate!=null) {
                det.date = sdf.format(obj.createdDate)
            }
            if(ControllerUtil.hasValue(obj.fileId)){
                det.url = g.createLink(controller: 'feedback', action: 'downloadFile', params: [id: obj.fileId])
                det.fileName = obj.fileName
            }
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        def objs = UserFeedback.executeQuery("FROM UserFeedback WHERE id = :objId AND userMobileProperty.property.id in :propIdList",
                [propIdList:propIdList, objId:params.id])
        if(objs.size()>0) {
            def obj = objs.get(0)
            def result = [id: params.id, propName: obj.userMobileProperty.property.name, title: obj.title, description: obj.description,
                          user: obj.userMobileProperty.userMobile.name, fileId: obj.fileId, fileName: obj.fileName]
            if(obj.createdDate!=null) {
                def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                result.date = sdf.format(obj.createdDate)
            }
            [detail: result]
        } else {
            flash.error = "Kritik dan saran tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def downloadFile() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("feedback${File.separator}attachment${File.separator}").concat(params.id)
            def fileName = UserFeedback.findByFileId(params.id)?.fileName?:"attachment"
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] fileBytes = is.bytes
                response.setHeader('Content-length', fileBytes.length.toString())
                response.setHeader('Content-disposition', "attachment;filename=\"${fileName}\"")
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << fileBytes
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = UserFeedback.get(params.id)
        if(obj!=null && obj.userMobileProperty.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.userMobileProperty.property.id)){
            UserFeedback.withTransaction { status ->
                try {
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Feedback tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
        } else {
            flash.message = "Kritik dan saran berhasil dihapus"
        }
        redirect(action: 'index')
    }
}
