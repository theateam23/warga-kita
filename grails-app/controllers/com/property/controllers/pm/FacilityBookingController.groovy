package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.CodeSequence
import com.model.general.FacilityBooking
import com.model.general.PropertyAdminProperty
import com.model.property.Facility
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class FacilityBookingController {

    def bookingService
    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(propIdList.size()==0){
            propIdList.add("")
        }
        def facilityList = Facility.executeQuery("FROM Facility WHERE area.property.id IN :propIdList",[propIdList:propIdList])
        def tzoffset = ControllerUtil.getTimeZoneOffset()
        [facilityList:facilityList, facilityId:params.facilityId, tzoffset:tzoffset]
    }

    def getEvents(){
        log.debug "IN getEvents... $params"
        def result = []
        def id = params.id
        def start = params.start?Long.valueOf(params.start):null
        def end = params.end?Long.valueOf(params.end):null

        if(start!=null && end!=null){
            def startDate = new Date(start)
            def endDate = new Date(end)
            def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            def statusList = [AppConstants.BOOKING_STATUS_OPEN, AppConstants.BOOKING_STATUS_BOOKED, AppConstants.BOOKING_STATUS_CLOSED]
            FacilityBooking.executeQuery("FROM FacilityBooking WHERE facility.id = :facilityId AND status in :statusList AND startDate BETWEEN :startDate AND :endDate",
                [facilityId:id, startDate:startDate, endDate:endDate, statusList:statusList]).each{ fb ->
                result.add([
                        title: "${fb.code} - ${fb.description}",
                        start: sdf.format(new Date(fb.startTime)),
                        end: sdf.format(new Date(fb.endTime)),
                        color: getColorCode(fb.status)
                ])
            }
        }
        render result as JSON
    }

    private String getColorCode(def status){
        if(AppConstants.BOOKING_STATUS_OPEN.equals(status)){
            return "#0040ff"
        } else if(AppConstants.BOOKING_STATUS_BOOKED.equals(status)){
            return "#ff8000"
        } else if(AppConstants.BOOKING_STATUS_CLOSED.equals(status)){
            return "#009900"
        }
    }

    def booking(){
        log.debug "IN booking... $params"
        def id = params.id
        def start = params.start?Long.valueOf(params.start):null
        def end = params.end?Long.valueOf(params.end):null
        def tzoffset = ControllerUtil.getTimeZoneOffset()
        if(start!=null && end!=null){
            def facility = Facility.get(id)
            if(facility==null){
                flash.error = "Data fasilitas umum tidak ditemukan"
                redirect action: 'index'
                return
            }
            def detail = [id:facility.id, name:facility.name, startDate: start, endDate: end]

            [facility:detail, tzoffset:tzoffset]
        } else {
            redirect action: 'index'
        }
    }

    def confirmBooking(){
        log.debug "IN confirmBooking... $params"
        def id = params.id
        def startDate = params.startDate?Long.valueOf(params.startDate):null
        def endDate = params.endDate?Long.valueOf(params.endDate):null
        def start = params.start?Long.valueOf(params.start):null
        def end = params.end?Long.valueOf(params.end):null
        def description = params.description

        if(!ControllerUtil.hasValue(id) || !ControllerUtil.hasValue(start) || !ControllerUtil.hasValue(end)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params.start = startDate
            params.end = endDate
            redirect(action: 'booking', params:params)
            return
        }

        def facility = Facility.get(id)
        if(facility==null){
            flash.error = "Data fasilitas umum tidak ditemukan"
            redirect action:'index'
            return
        }

        def isAvailable = bookingService.isBookingAvailable(id, new Date(startDate), start, end)

        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdfTime = new SimpleDateFormat("HH:mm")
        def strStartTime = sdfTime.format(new Date(start))
        def strEndTime = sdfTime.format(new Date(end))
        if(!isAvailable){
            flash.error = "Periode waktu sudah dipakai, dari ${strStartTime} sampai ${strEndTime}"
            params.start = startDate
            params.end = endDate
            redirect(action: 'booking', params:params)
            return
        }

        def strStartDate = sdf.format(new Date(startDate))
        def detail = [id:id, name:facility.name, startDate:startDate, endDate:endDate, start:start, end:end, description:description,
                    strStartDate:strStartDate, strStartTime:strStartTime, strEndTime:strEndTime, action: 'Booking',
                    stage: 'Confirm', actionType: 'add']

        render view:'confirm',model:[facilityBooking:detail]
    }

    def submitBooking(){
        log.debug "IN submitBooking... $params"
        def id = params.id
        def startDate = params.startDate?Long.valueOf(params.startDate):null
        def endDate = params.endDate?Long.valueOf(params.endDate):null
        def start = params.start?Long.valueOf(params.start):null
        def end = params.end?Long.valueOf(params.end):null
        def description = params.description

        def facility = Facility.get(id)
        if(facility==null){
            flash.error = "Data fasilitas umum tidak ditemukan"
            redirect action:'index'
            return
        }

        def isAvailable = bookingService.isBookingAvailable(id, new Date(startDate), start, end)

        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdfTime = new SimpleDateFormat("HH:mm")
        def strStartTime = sdfTime.format(new Date(start))
        def strEndTime = sdfTime.format(new Date(end))
        if(!isAvailable){
            flash.error = "Periode waktu sudah dipakai, dari ${strStartTime} sampai ${strEndTime}"
            params.start = startDate
            params.end = endDate
            redirect(action: 'booking', params:params)
            return
        }

        def dtStartDate = new Date(startDate)
        def strStartDate = sdf.format(dtStartDate)
        def result = [id:id, name:facility.name, startDate:startDate, endDate:endDate, start:start, end:end, description:description,
                      strStartDate:strStartDate, strStartTime:strStartTime, strEndTime:strEndTime, action: 'Booking',
                      stage: 'Result', actionType: 'add']
        def isError = false, message = ""

        FacilityBooking.withTransaction { status ->
            try {
                def seq = CodeSequence.get(AppConstants.CODE_SEQUENCE_FACILITY_BOOKING)
                if(seq == null){
                    seq = new CodeSequence()
                    seq.id = AppConstants.CODE_SEQUENCE_FACILITY_BOOKING
                    seq.seq = 1
                    seq.save(flush:true)
                }
                def code = AppConstants.CODE_SEQUENCE_FACILITY_BOOKING + "-" + ControllerUtil.padLeft(5,"0",seq.seq)
                def obj = new FacilityBooking()
                obj.code = code
                obj.facility = facility
                obj.startDate = dtStartDate
                obj.startTime = start
                obj.endTime = end
                obj.description = description
                obj.createdBy = springSecurityService.principal.id
                obj.status = AppConstants.BOOKING_STATUS_OPEN
                obj.save(flush:true)

                seq.seq = seq.seq+1
                seq.save(flush:true)

                result.code = code
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "Server bermasalah, coba lagi beberapa saat."
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Pemesanan berhasil, kode pemesanan anda adalah ${result.code}"
        }
        render view: 'confirm', model: [facilityBooking:result]
    }

}
