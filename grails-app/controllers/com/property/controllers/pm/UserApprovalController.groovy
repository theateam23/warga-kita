package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.maintenance.RoleMobile
import com.model.property.PropertyUnit
import com.model.property.TmpUserMobile
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.converters.JSON

import java.text.SimpleDateFormat



class UserApprovalController {

    def springSecurityService
    def mailService

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        [propName:prop.name]
    }

    def listDatatable(){
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)

        def pmId = springSecurityService.principal?.pmId
        def columns = ['tum.id', 'tum.email', 'tum.name', 'u.area.name', 'u.subArea','u.name', 'tum.role','tum.createdDate']
        def baseQuery = "FROM TmpUserMobile tum, PropertyUnit u " +
                "WHERE u.id = tum.unitId AND u.area.property.propertyManager.user.id = :pmId " +
                "AND u.area.property.id = :propId "
        def query = "SELECT tum " + baseQuery
        def queryCount = "SELECT count(tum.id) " + baseQuery

        def propId = prop.id
        def queryParams = [pmId: pmId, propId:propId]
        def result = ControllerUtil.parseDatatablesQuery(TmpUserMobile.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ tum ->
            def unit = PropertyUnit.get(tum.unitId)
            aaData.add([id:tum.id, email: tum.email, fullName: tum.name, unitName: unit?.name,
                        area: unit?.area?.name, subArea: unit?.subArea, role: tum.role,
                        createdDate:sdf.format(tum.createdDate)])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def approveUser(){
        log.debug "IN approveUser... $params"

        if(params.userId==null){
            flash.error = "Tidak ada user yang terpilih"
            redirect(action:'index')
            return
        }

        def currentUser = springSecurityService.principal.id
        def userIds = []
        if(params.userId instanceof String){
            userIds.add(params.userId)
        } else {
            userIds = params.userId
        }
        def mapProperty = [:] //map to contain properties to reduce query
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def result = []
        userIds.each { userId ->
            def detail = [:]
            def tum = TmpUserMobile.get(userId)
            def unit = PropertyUnit.get(tum.unitId)
            def um = UserMobile.findByEmail(tum.email)
            def role = RoleMobile.findByName(tum.role)

            detail.name = tum.name
            detail.email = tum.email
            detail.unitName = unit?.name
            detail.area = unit?.area?.name
            detail.subArea = unit?.subArea
            detail.role = tum.role

            if(prop.id != tum.propertyId || prop.id != unit.area.property.id){
                detail.status = "Failed"
                detail.message = "Data properti tidak ditemukan."
            } else if(um!=null) {
                detail.status = "Failed"
                detail.message = "Email user sudah terdaftar."
            } else if((unit.ownerResident != null || unit.tenant != null) && tum.role!=AppConstants.ROLE_SUB_USER) {
                detail.status = "Failed"
                detail.message = "Status Pemilik/Penyewa untuk unit ini sudah terisi"
            } else {
                TmpUserMobile.withTransaction { status ->
                    try{

                        if(um==null){
                            um = new UserMobile()
                            um.createdBy = currentUser
                            um.email = tum.email
                            um.name = tum.name
                            um.phoneNo = tum.phoneNo
                            um.address = tum.address
                            um.title = tum.title
                            um.idKTP = tum.idKTP
                            um.save()

                            def ump = new UserMobileProperty()
                            ump.property = unit.area.property
                            ump.userMobile = um
                            ump.save()

                            def umu = new UserMobileUnit()
                            umu.roleMobile = role
                            umu.unit = unit
                            umu.userMobile = um
                            umu.save()

                            if(AppConstants.ROLE_TENANT.equals(tum.role)){
                                unit.tenant = um
                                unit.save()
                            } else if(AppConstants.ROLE_OWNER_RESIDENT.equals(tum.role)){
                                unit.ownerResident = um
                                unit.save()
                            }

                            tum.delete()
                            detail.status = "Success"

                            mailService.sendMail {
                                to um.email
                                from "support@warga-kita.com"
                                subject "Persetujuan User Mobile Warga Kita"
                                html view: "/emails/emailUserMobileResult", model: [userMobile: [email:um.email, phoneNo:um.phoneNo, name: um.name, unitName: unit.name, areaName: unit.area.name]]
                            }
                        } else {
                            detail.status = "Failed"
                            detail.message = "User sudah terdaftar."
                        }
                    } catch(Exception e){
                        log.debug "ERROR processing $userId: ${e.getMessage()}"
                        status.setRollbackOnly()
                    }
                }
            }
            result.add(detail)
        }

        flash.message="User mobile sudah ditambahkan."
        [result:result]
    }

    def rejectUser(){
        log.debug "IN rejectUser... $params"

        if(params.userId==null){
            flash.error = "Tidak ada user yang terpilih"
            redirect(action:'index')
            return
        }

        def userIds = []
        if(params.userId instanceof String){
            userIds.add(params.userId)
        } else {
            userIds = params.userId
        }
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        userIds.each { userId ->
            def tum = TmpUserMobile.get(userId)
            if(!propIdList.contains(tum.propertyId)){
                log.debug "Skipped, belongs to ungranted property... ${tum.id}"
                return
            }
            if(tum!=null){
                try{
                    tum.delete(flush:true)
                } catch(Exception e){
                    log.debug "ERROR processing $userId: ${e.getMessage()}"
                    status.setRollbackOnly()
                }
            } else {
                log.debug "User (TUM) not found: $userId"
            }
        }

        flash.message="User mobile sudah dihapus."
        redirect(action:'index')
    }
}
