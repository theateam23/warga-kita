package com.property.controllers.pm

import com.model.maintenance.UnitType
import com.model.property.PropertyUnit
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils



class UnitTypeController {

    def springSecurityService

    def index() {
        log.debug "IN index... $params"
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId
        def columns = ['code', 'name']
        def baseQuery = "FROM UnitType WHERE pmId = :pmId"
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [pmId: pmId]
        def result = ControllerUtil.parseDatatablesQuery(UnitType.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.id, code: obj.code, name: obj.name, description: obj.description])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def pmId = springSecurityService.principal?.pmId
        def unitType = UnitType.findByIdAndPmId(params.id, pmId)
        if(unitType!=null) {
            def result = [id: params.id, code: unitType.code, name: unitType.name, description: unitType.description]
            [unitType: result]
        } else {
            flash.error = "Tipe unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def pmId = springSecurityService.principal?.pmId
        def isError = false, message = ''
        def unitType = UnitType.findByIdAndPmId(params.id,pmId)

        if(unitType!=null && unitType.pmId==pmId){
            def units = PropertyUnit.findAllByUnitType(unitType)
            if(units.size()>0){
                isError = true
                message = "This Unit Type is still being used"
            } else {
                UnitType.withTransaction { status ->
                    try {
                        unitType.delete()
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        } else {
            message = "Tipe unit tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Data tipe unit berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        if(params._action_create == 'Back'){
            [unitType: [code: params.code, name:params.name, description:params.description]]
        } else {
            [unitType:[:]]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def pmId = springSecurityService.principal?.pmId
        def name = params.name
        def code = params.code
        def description = params.description

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(code)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [code:code, name:name, description: description]
        def unitType = UnitType.findByCodeAndPmId(code, pmId)
        if(unitType==null){
            result.action = 'Create Unit Type'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render(view:'confirm', model: [unitType: result])
        } else {
            flash.error = "Tipe unit dengan kode ${code} sudah ada"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def code = params.code
        def description = params.description

        def result = [id:id, code:code, name:name, description: description, actionType: 'add', action:'Create Unit Type']
        def isError = false
        def message = ""

        def unitType = UnitType.findByCodeAndPmId(code, pmId)
        if(unitType==null){
            UnitType.withTransaction { status ->
                try {
                    unitType = new UnitType()
                    unitType.code = code
                    unitType.name = name
                    unitType.description = description
                    unitType.pmId = pmId
                    unitType.createdBy = currentUser
                    unitType.save(flush: true)
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Tipe unit dengan kode ${code} sudah ada"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data tipe unit berhasil ditambahkan"
        }
        render view: 'confirm', model: [unitType: result]
    }

    def edit(){
        log.debug "IN edit... $params"
        if(params._action_edit == 'Back'){
            [unitType: [id:params.id, code: params.code, name:params.name, description: params.description]]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def unitType = UnitType.findByIdAndPmId(params.id, pmId)
            if (unitType != null) {
                def result = [id:params.id, code: unitType.code, name: unitType.name, description: unitType.description,
                              actionType: 'edit', action: 'Edit Unit Type', stage: 'Confirm']
                [unitType: result]
            } else {
                flash.error = "Tipe unit tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def code = params.code
        def description = params.description

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(code)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def unitType = UnitType.findByIdAndPmId(params.id, pmId)
        if(unitType!=null){
            def checkCode = UnitType.findByCodeAndPmId(code, pmId)
            if(checkCode!=null && checkCode.id != id) {
                flash.error = "Tipe unit dengan kode ${code} sudah ada"
                redirect(action: 'edit')
                return
            }
            def result = [id:params.id, code:code, name:name, description: description,
                          actionType: 'edit', action:'Edit Unit Type', stage:'Confirm']
            render view:'confirm', model: [unitType:result]
        } else {
            flash.error = "Tipe unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def code = params.code
        def description = params.description

        def unitType = UnitType.findByIdAndPmId(params.id,pmId)
        if(unitType!=null){
            def checkCode = UnitType.findByCode(code)
            if(checkCode!=null && checkCode.id != id){
                flash.error = "Tipe unit dengan kode ${code} sudah ada"
                redirect(action: 'edit')
                return
            }
            unitType.id = id
            unitType.code = code
            unitType.name = name
            unitType.description = description
            unitType.updatedBy = currentUser
            unitType.save(flush:true)
            def result = [id:id, code:code, name:name,
                          actionType: 'edit', action:'Edit Unit Type', stage:'Result']
            flash.message = "Data tipe unit berhasil diperbaharui"
            render view:'confirm', model: [unitType: result]
        } else {
            flash.error = "Tipe unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

}
