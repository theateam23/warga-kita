package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.information.PropertyReminder
import com.model.information.PropertyReminderNotif
import com.model.property.PropertyUnit
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat

class ReminderListController {

    def springSecurityService
    def userService
    GrailsApplication grailsApplication
    
    def index() {
        log.debug "IN index... $params"
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId
        def columns = ['pr.name', 'pr.description', 'pr.createdDate', 'ui.name', 'pr.isActive']
        def baseQuery = "FROM PropertyReminder pr, UserInfo ui WHERE pr.property.propertyManager.user.id = :pmId AND ui.user.id = pr.createdBy"
        def query = "SELECT pr, ui " + baseQuery
        def queryCount = "SELECT count(pr.id) " + baseQuery
        def queryParams = [pmId: pmId]
        def result = ControllerUtil.parseDatatablesQuery(PropertyReminder.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ arrObj ->
            def obj = arrObj[0]
            def userObj = arrObj[1]
            aaData.add([id:obj.id, name: obj.name, description: obj.description, createdBy: userObj.name,
                        createdDate:sdf.format(obj.createdDate), status: ControllerUtil.getActive(obj.isActive)])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def pmId = springSecurityService.principal?.pmId
        def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(obj!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            def result = [id: params.id, name:obj.name, description:obj.description, createdBy: userService.getUserName(obj.createdBy), createdDate: sdf.format(obj.createdDate),
                          updatedBy: userService.getUserName(obj.updatedBy), reminderDay: obj.reminderDay, message: obj.message,
                          updatedDate: obj.updatedDate?sdf.format(obj.updatedDate):"", status: ControllerUtil.getActive(obj.isActive), isActive:obj.isActive]
            [detail: result]
        } else {
            flash.error = "Pengingat tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def activate(){
        log.debug "IN activate... $params"
        def pmId = springSecurityService.principal?.pmId
        def isError = false, message = ''
        def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()

        if(obj!=null){
            PropertyReminder.withTransaction { status ->
                try {
                    obj.isActive = AppConstants.FLAG_YES
                    obj.save()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Pengingat tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Pengingat sudah diaktifkan"
            redirect(action: 'index')
        }
    }

    def deactivate(){
        log.debug "IN deactivate... $params"
        def pmId = springSecurityService.principal?.pmId
        def isError = false, message = ''
        def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()

        if(obj!=null){
            PropertyReminder.withTransaction { status ->
                try {
                    obj.isActive = AppConstants.FLAG_NO
                    obj.save()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Pengingat tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Pengingat sudah dinonaktifkan"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def reminderPeriodeList = AppConstants.getReminderPeriodeList()
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        if(params._action_create == 'Back'){
            [detail:[name:params.name, description:params.description, message:params.message, reminderDay:params.reminderDay], reminderPeriodeList:reminderPeriodeList]
        } else {
            [detail:[:], reminderPeriodeList:reminderPeriodeList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def name = params.name
        def description = params.description
        def message = params.message
        def reminderDay = params.reminderDay

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(message) || !ControllerUtil.hasValue(reminderDay)){
            flash.error = "Please fill all required fields"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [name:name, description: description, message: message, reminderDay:reminderDay]
        result.action = 'Create Reminder'
        result.stage = 'Confirm'
        result.actionType = 'add'
        render(view:'confirm', model: [detail: result])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def name = params.name
        def description = params.description
        def message = params.message
        def reminderDay = params.reminderDay

        def result = [name:name, description: description, message: message, reminderDay:reminderDay,
                      actionType: 'add', action:'Create Reminder']
        def isError = false
        def msg = ""

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:currentUser]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)
            PropertyReminder.withTransaction { status ->
                try {
                    def obj = new PropertyReminder()
                    obj.name = name
                    obj.description = description
                    obj.message = message
                    obj.reminderDay = Integer.parseInt(reminderDay)
                    obj.isActive = AppConstants.FLAG_YES
                    obj.createdBy = currentUser
                    obj.property = prop
                    obj.save(flush: true)
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    msg = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Properti tidak ditemukan"
            redirect(action:"index")
            return
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = msg
        } else {
            result.stage = 'Result'
            flash.message = "Pengingat berhasil ditambahkan"
        }
        render view: 'confirm', model: [detail: result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def reminderPeriodeList = AppConstants.getReminderPeriodeList()
        if(params._action_edit == 'Back'){
            [detail: [id:params.id, name: params.name, description:params.description, message:params.message, reminderDay:params.reminderDay],
             reminderPeriodeList:reminderPeriodeList]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                    [id:params.id, pmId:pmId]).first()
            if (obj != null) {
                def result = [id:params.id, name:obj.name, description: obj.description, message:obj.message, reminderDay:obj.reminderDay,
                              actionType: 'edit', action: 'Edit Reminder', stage: 'Confirm']
                [detail: result]
            } else {
                flash.error = "Pengingat tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def description = params.description
        def message = params.message
        def reminderDay = params.reminderDay

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(reminderDay) || !ControllerUtil.hasValue(params.message)){
            flash.error = "Data harap dilengkapi"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(obj!=null){
            def result = [id:params.id, name:name, description:description, message:message, reminderDay:reminderDay,
                          actionType: 'edit', action:'Edit Reminder', stage:'Confirm']
            render view:'confirm', model: [detail:result]
        } else {
            flash.error = "Pengingat tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def description = params.description
        def message = params.message
        def reminderDay = params.reminderDay

        def isError = false
        def msg = ""
        def result = [name:name, description:description, message:message, reminderDay:reminderDay,
                      actionType: 'edit', action:'Edit Reminder']

        def obj = PropertyReminder.executeQuery("FROM PropertyReminder where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(obj!=null){
            PropertyReminder.withTransaction { status ->
                try {
                    obj.name = name
                    obj.description = description
                    obj.message = message
                    obj.reminderDay = Integer.parseInt(reminderDay)
                    obj.updatedBy = currentUser
                    obj.save(flush: true)
                } catch (Exception e){
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Pengingat tidak ditemukan."
            redirect(action: 'index')
            return
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Pengingat berhasil diperbarui."
        }

        render view:'confirm', model: [detail: result]
    }

}
