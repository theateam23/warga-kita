package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.property.PropertyArea
import com.model.property.Facility
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils



class AreaPmController {

    def springSecurityService

    def index() {
        log.debug "IN index... $params"
    }

    def areaListDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
            [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def columns = ['name', 'address', 'property.name']
        def baseQuery = "FROM PropertyArea WHERE property.propertyManager.id = :pmInfoId AND property.id in :propertyList"
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [pmInfoId:pmInfo.id, propertyList:propertyList]
        def result = ControllerUtil.parseDatatablesQuery(PropertyArea.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.id, name: obj.name, address: obj.address, propId: obj.property.id, propName: obj.property.name])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def createArea(){
        log.debug "IN createArea... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()==0){
            flash.error = "Properti tidak ditemukan"
            redirect(action: 'index')
        } else {
            def prop = propertyList.get(0)
            if(params._action_createArea == 'Back'){
                [area: [propName:prop.name, name:params.name, address:params.address, propId: params.propId]]
            } else {
                [area: [propName:prop.name]]
            }
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def name = params.name
        def address = params.address
        def propName = params.propName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createArea = 'Back'
            redirect(action: 'createArea', params:params)
            return
        }

        def result = [name:name, address:address, propName:propName]
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)

            result.action = 'Create Area'
            result.stage = 'Confirm'
            result.actionType = 'add'
            result.propName = prop.name
            render view: 'confirm', model: [area: result]

        } else {
            flash.error = "Properti tidak ditemukan"
            params._action_createArea = 'Back'
            redirect(action: 'createArea', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def name = params.name
        def address = params.address
        def propName = params.propName

        def result = [name:name, address:address, propName:propName, actionType: 'add', action:'Create Area']
        def isError = false
        def message = ""

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)
                PropertyArea.withTransaction { status ->
                    try {
                        def area = new PropertyArea()
                        area.name = name
                        area.address = address
                        area.property = prop
                        area.createdBy = springSecurityService.principal.id
                        area.save(flush: true)
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
        } else {
            message = "Data properti tidak ditemukan"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Area berhasil ditambahkan"
        }
        render view: 'confirm', model: [area:result]
    }

    def detailArea(){
        log.debug "IN detailArea... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def obj = PropertyArea.get(params.id)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        if(obj==null || obj.property.propertyManager.id!=pmInfo.id || !propertyList.contains(obj.property.id)){
            flash.error = "Area tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def result = [id:params.id, name:obj.name, address: obj.address, propName: obj.property.name]
        [area:result]
    }

    def deleteArea(){
        log.debug "IN deleteArea... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = PropertyArea.get(params.id)
        if(obj!=null && obj.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.property.id)){
            PropertyArea.withTransaction { status ->
                try {
                    Facility.findAllByArea(obj).each { f ->
                        f.delete()
                    }
                    PropertyUnit.findAllByArea(obj).each { u ->
                        u.delete()
                    }
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Area is still used"
                }
            }
        } else {
            message = "Area tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailArea', params:params)
        } else {
            flash.message = "Area berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def editArea(){
        log.debug "IN editArea... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(params._action_editArea == 'Back'){
            [propertyList: propertyList, area: [id:params.id, name:params.name,
                        address:params.address, propName:params.propName]]
        } else {
            def obj = PropertyArea.get(params.id)
            def propId = propertyList.get(0).id
            if (obj != null && obj.property.propertyManager.id == pmInfo.id && propId==obj.property.id) {
                def result = [id:params.id, name: obj.name, address: obj.address, propName: obj.property.name,
                              actionType: 'edit', action: 'Edit Area', stage: 'Confirm']
                [area: result]
            } else {
                flash.error = "Area tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def name = params.name
        def address = params.address
        def propName = params.propName

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editArea = 'Back'
            redirect(action: 'editArea', params:params)
            return
        }

        def obj = PropertyArea.get(params.id)
        if(obj!=null && obj.property.propertyManager.id == pmInfo.id){
            def result = [id: params.id, name: name, address: address, propName: propName, action : 'Edit Area', stage: 'Confirm']
            render view: 'confirm', model: [area: result]
        } else {
            flash.error = "Area tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def name = params.name
        def address = params.address
        def propName = params.propName

        def obj = PropertyArea.get(params.id)
        if(obj!=null && obj.property.propertyManager.id == pmInfo.id){
            obj.name = name
            obj.address = address
            obj.updatedBy = currentUser
            obj.save(flush:true)
            def result = [name:name, address:address, propName:propName, actionType: 'edit', action:'Edit Area', stage:'Result']
            flash.message = "Area berhasil diperbaharui"
            render view:'confirm', model: [area: result]
        } else {
            flash.error = "Area tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def detailPropertyJSON(){
        log.debug "IN detailPropertyJSON... $params"
        def output = [:]
        def prop = Property.get(params.id)
        if(prop!=null){
            output.result = true
            output.address = prop.address
            output.name = prop.name
            output.pmName = prop.propertyManager.name
            output.developer = prop.developer
        } else {
            output.result = false
            output.message = "Data properti tidak ditemukan"
        }
        render output as JSON
    }
}
