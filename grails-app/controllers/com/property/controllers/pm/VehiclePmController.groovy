package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.maintenance.VehicleType
import com.model.property.PropertyArea
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.property.Vehicle
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import com.property.utils.ExcelImporter
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import pl.touk.excel.export.WebXlsxExporter

import java.text.SimpleDateFormat



class VehiclePmController implements ResourceLoaderAware {

    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        [propName: prop.name, areaId: params.areaId, unitId: params.unitId, areaList:areaList]
    }

    def listDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def columns = ['plateNo', 'unit.area.name', 'unit.name', 'vehicleType.name', 'taxDueDate']
        def baseQuery = "FROM Vehicle WHERE unit.id like :unitId AND unit.area.id like :areaId " +
                "AND unit.area.property.propertyManager.id = :pmInfoId"

        def areaId = params.areaId? params.areaId:'%'
        def unitId = params.unitId? params.unitId:'%'

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [areaId:areaId, pmInfoId:pmInfo.id, unitId:unitId]
        def result = ControllerUtil.parseDatatablesQuery(Vehicle.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        objs.each{ obj ->
            def det = [id:obj.id, plateNo:obj.plateNo, unitId: obj.unit.id, unitName: obj.unit.name,
                       areaId: obj.unit.area.id, areaName: obj.unit.area.name, vehicleType: obj.vehicleType.name]
            if(obj.taxDueDate!=null){
                det.taxDueDate = sdf.format(obj.taxDueDate)
            } else {
                det.taxDueDate = ""
            }
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailVehicle(){
        log.debug "IN detailVehicle... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def obj = Vehicle.get(params.id)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(obj==null || obj.unit.area.property.propertyManager.id!=pmInfo.id || !propertyList.contains(obj.unit.area.property.id)){
            flash.error = "Data kendaraan tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def result = [id:params.id, plateNo:obj.plateNo, vehicleType: obj.vehicleType.name, brand: obj.brand, color: obj.color,
                      propName: obj.unit.area.property.name,areaName: obj.unit.area.name, unitName: obj.unit.name,
                      imageFileId: obj.imageFileId, imageFileName: obj.imageFileName]
        if(obj.taxDueDate!=null){
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            result.taxDueDate = sdf.format(obj.taxDueDate)
        }
        [vehicle:result]
    }

    def createVehicle(){
        log.debug "IN createVehicle... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def vehicleTypeList = VehicleType.list()
        if(params._action_createVehicle == 'Back'){
            [vehicle: [propName: prop.name, areaId:params.areaId, unitId:params.unitId, plateNo:params.plateNo, vehicleType: params.vehicleType,
                    brand: params.brand, color: params.color, taxDueDate: params.taxDueDate, imageFileId: params.imageFileId, imageFileName: params.imageFileName],
                    areaList: areaList, vehicleTypeList:vehicleTypeList]
        } else {
            [vehicle: [propName: prop.name], areaList: areaList, vehicleTypeList:vehicleTypeList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... " + params

        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def plateNo = params.plateNo
        def vehicleType = params.vehicleType
        def brand = params.brand
        def color = params.color
        def taxDueDate = params.taxDueDate
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        if(!ControllerUtil.hasValue(unitId) || !ControllerUtil.hasValue(plateNo) || !ControllerUtil.hasValue(vehicleType) || !ControllerUtil.hasValue(taxDueDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createVehicle = 'Back'
            redirect(action: 'createVehicle', params:params)
            return
        }

        def area = PropertyArea.get(areaId)
        def unit = PropertyUnit.get(unitId)
        def result = [propName: propName, areaId:areaId, areaName: area.name, unitId:unitId, unitName:unit.name,
                      plateNo: plateNo, vehicleType: vehicleType, brand: brand, color: color, taxDueDate: taxDueDate, imageFileId: imageFileId, imageFileName: imageFileName]

        result.action = 'Create Vehicle'
        result.stage = 'Confirm'
        result.actionType = 'add'
        render view:'confirm', model: [vehicle:result]
    }

    def submitAdd(){
        log.debug "IN submitAdd... " + params

        def currentUser = springSecurityService.principal.id
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def plateNo = params.plateNo
        def vehicleType = params.vehicleType
        def brand = params.brand
        def color = params.color
        def taxDueDate = params.taxDueDate
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        def area = PropertyArea.get(areaId)
        def unit = PropertyUnit.get(unitId)
        def result = [propName: propName, areaId:areaId, areaName: area.name, unitId:unitId, unitName:unit.name,
                      plateNo: plateNo, vehicleType: vehicleType, brand: brand, color: color, taxDueDate: taxDueDate,
                      imageFileId: imageFileId, imageFileName: imageFileName, actionType: 'add', action:'Create Vehicle']
        def isError = false
        def message = ""

        Vehicle.withTransaction { status ->
            try {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def obj = new Vehicle()
                obj.plateNo = plateNo
                obj.unit = unit
                if (ControllerUtil.hasValue(taxDueDate)) {
                    obj.taxDueDate = sdf.parse(taxDueDate)
                }
                obj.color = color
                obj.brand = brand
                obj.vehicleType = VehicleType.findByCode(vehicleType)
                obj.createdBy = currentUser
                if(ControllerUtil.hasValue(imageFileId)) {
                    //copy file from tmp
                    def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}tmp${File.separator}").concat(imageFileId)
                    def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(imageFileId)
                    def src = new File(srcFilePath)
                    def dst = new File(dstFilePath)
                    dst << src.bytes
                    if (src.exists()) src.delete()
                }
                obj.imageFileId = imageFileId
                obj.imageFileName = imageFileName

                obj.save(flush: true)
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "Server bermasalah, coba lagi beberapa saat."
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data kendaraan berhasil ditambahkan"
        }
        render view: 'confirm', model: [vehicle:result]
    }

    def deleteVehicle(){
        log.debug "IN deleteVehicle... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = Vehicle.get(params.id)
        if(obj!=null && obj.unit.area.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.unit.area.property.id)){
            Vehicle.withTransaction { status ->
                try {
                    if(ControllerUtil.hasValue(obj.imageFileId)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(obj.imageFileId))
                        if (image.exists()) image.delete()
                    }
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Data kendaraan tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailVehicle', params:params)
        } else {
            flash.message = "Data kendaraan berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def editVehicle(){
        log.debug "IN editVehicle... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def vehicleTypeList = VehicleType.list()
        def propIdList = propertyList.collect { it.id }
        def obj = Vehicle.get(params.id)
        if (obj != null && obj.unit.area.property.propertyManager.id == pmInfo.id && propIdList.contains(obj.unit.area.property.id)) {
            if(params._action_editVehicle == 'Back'){
                [vehicle: [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name, plateNo:params.plateNo,
                          vehicleType: params.vehicleType, brand: params.brand, color: params.color, taxDueDate: params.taxDueDate,
                          imageFileId: params.imageFileId, imageFileName: params.imageFileName],
                          propertyList: propertyList, vehicleTypeList:vehicleTypeList]
            } else {
                def result = [id: obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name, plateNo:obj.plateNo,
                              vehicleType: obj.vehicleType.code, brand: obj.brand, color: obj.color, imageFileId: obj.imageFileId, imageFileName: obj.imageFileName,
                              actionType: 'edit', action: 'Edit Area', stage: 'Confirm']
                if(obj.taxDueDate!=null){
                    def sdf = new SimpleDateFormat("yyyy-MM-dd")
                    result.dateOfBirth = sdf.format(obj.taxDueDate)
                }
                [propertyList:propertyList, vehicleTypeList:vehicleTypeList, vehicle: result]
            }
        } else {
            flash.error = "Data kendaraan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def plateNo = params.plateNo
        def vehicleType = params.vehicleType
        def brand = params.brand
        def color = params.color
        def taxDueDate = params.taxDueDate
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        if(!ControllerUtil.hasValue(plateNo) || !ControllerUtil.hasValue(vehicleType) || !ControllerUtil.hasValue(taxDueDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editVehicle = 'Back'
            redirect(action: 'editVehicle', params:params)
            return
        }

        def obj = Vehicle.get(params.id)
        if(obj!=null && obj.unit.area.property.propertyManager.id == pmInfo.id){
            def result = [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                          plateNo: plateNo, vehicleType: vehicleType, brand: brand, color: color, taxDueDate: taxDueDate,
                          imageFileId: imageFileId, imageFileName: imageFileName, action:'Edit Vehicle', stage:'Confirm']
            render view:'confirm', model: [vehicle: result]
        } else {
            flash.error = "Data kendaraan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def plateNo = params.plateNo
        def vehicleType = params.vehicleType
        def brand = params.brand
        def color = params.color
        def taxDueDate = params.taxDueDate
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        def obj = Vehicle.get(params.id)
        if(obj!=null && obj.unit.area.property.propertyManager.id == pmInfo.id){
            def result = [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                          plateNo: plateNo, vehicleType: vehicleType, brand: brand, color: color, taxDueDate: taxDueDate,
                          imageFileId: imageFileId, imageFileName: imageFileName, actionType: 'edit', action:'Edit Vehicle']
            def isError = false
            def message = ""

            Vehicle.withTransaction { status ->
                try {
                    def sdf = new SimpleDateFormat("yyyy-MM-dd")
                    def oldFile = obj.imageFileId
                    
                    obj.plateNo = plateNo
                    if (ControllerUtil.hasValue(taxDueDate)) {
                        obj.taxDueDate = sdf.parse(taxDueDate)
                    }
                    obj.color = color
                    obj.brand = brand
                    obj.vehicleType = VehicleType.findByCode(vehicleType)
                    obj.updatedBy = currentUser

                    if(oldFile!=imageFileId && ControllerUtil.hasValue(imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}tmp${File.separator}").concat(imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }
                    obj.imageFileId = imageFileId
                    obj.imageFileName = imageFileName

                    obj.save(flush: true)
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }

            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "Data kendaraan berhasil diperbaharui"
            }
            render view:'confirm', model: [vehicle: result]
        } else {
            flash.error = "Data kendaraan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def uploadFile(){
        log.debug "IN uploadFile... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        [propertyList: propertyList]
    }

    def confirmUpload(){
        log.debug "IN confirmUpload... $params"
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            try {
                def importer = new ExcelImporter(filePath)
                def vehicleList = importer.getVehicleList()
                def cnt = 1
                vehicleList.each{ map ->
                    map.put("cnt", cnt)
                    cnt++
                }
                def prop = Property.get(params.propId)
                def area = PropertyArea.get(params.areaId)
                def unit = PropertyUnit.get(params.unitId)
                [fileImportId: params.fileImportId, vehicleList:vehicleList, propName:prop.name,
                 areaId:params.areaId, areaName:area.name, unitId:params.unitId, unitName:unit.name]
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                flash.error = "Server bermasalah, coba lagi beberapa saat."
                redirect(action:'uploadFile')
            }
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def submitImport(){
        log.debug "IN submitImport... $params"
        def currentUser = springSecurityService.principal.id
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            def importer = new ExcelImporter(filePath)
            def vehicleList = importer.getVehicleList()
            def cnt = 1
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            vehicleList.each{ map ->
                map.put("cnt", cnt)
                cnt++

                if(!ControllerUtil.hasValue(map.get("plateNo")) || !ControllerUtil.hasValue(map.get("vehicleType")) || !ControllerUtil.hasValue(map.get("taxDueDate"))){
                    map.put("message", "Missing mandatory fields")
                } else {
                    def vehicleType = VehicleType.findByCode(map.get("vehicleType"))
                    if(vehicleType==null){
                        map.put("message", "Invalid relationship code")
                    } else {
                        try {
                            def obj = new Vehicle()
                            obj.unit = PropertyUnit.get(params.unitId)
                            obj.plateNo = map.get("plateNo")
                            if (ControllerUtil.hasValue(map.get("taxDueDate"))) {
                                obj.taxDueDate = sdf.parse(map.get("taxDueDate"))
                            }
                            obj.color = map.get("color")
                            obj.brand = map.get("brand")
                            obj.vehicleType = VehicleType.findByCode(map.get("vehicleType"))
                            obj.createdBy = currentUser
                            obj.save(flush: true)
                            map.put("message", "Added")
                        } catch (Exception e) {
                            log.error(ExceptionUtils.getStackTrace(e))
                            map.put("message", "System error - Skipped")
                        }
                    }
                }
            }
            file.delete()
            flash.message="File has been imported"
            render view:'resultUpload', model:[vehicleList:vehicleList]
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").toString())
                    uploadDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def uploadImage(){
        log.debug "IN uploadImage... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def downloadTemplate(){
        log.debug "IN downloadTemplate... $params"
        byte[] imageInByte = assetResourceLocator.findAssetForURI('vehicle-template.xls').getInputStream()?.bytes
        response.setHeader('Content-length', imageInByte.length.toString())
        response.setHeader("Content-disposition", "attachment; filename=\"vehicle-template.xls\"")
        response.contentType = "application/vnd.ms-excel" // or the appropriate image content type
        response.outputStream << imageInByte
        response.outputStream.flush()
    }

    def downloadReport(){
        log.debug "IN downloadReport... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def areaId = params.areaId? params.areaId:'%'
        def unitId = params.unitId? params.unitId:'%'

        def prop = PropertyAdminProperty.findByPropertyAdmin(currentPm)
        def baseQuery = "FROM Vehicle WHERE unit.id like :unitId AND unit.area.id like :areaId AND unit.area.property.id like :propId " +
                "AND unit.area.property.propertyManager.id = :pmInfoId "
        def queryParams = [propId:prop?.property?.id, areaId:areaId, unitId:unitId, pmInfoId:pmInfo.id]
        def list = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdf2 = new SimpleDateFormat("yyyyMMddHHmmss")
        Vehicle.executeQuery(baseQuery, queryParams).each { obj ->
            def det = [id:obj.id, unitName:obj.unit?.name, plateNo:obj.plateNo, vehicleType: obj.vehicleType?.name, brand: obj.brand, color:obj.color]
            if(obj.taxDueDate!=null) {
                det.taxDueDate = sdf.format(obj.taxDueDate)
            }
            list.add(det)
        }

        def headers = ['Unit', 'Plat Nomor', 'Tipe Kendaraan', 'Merk', 'Warna', 'Masa Berlaku STNK']
        def withProperties = ['unitName', 'plateNo', 'vehicleType', 'brand', 'color', 'taxDueDate']

        new WebXlsxExporter().with {
            setResponseHeaders(response, "laporan_kendaraan_${sdf2.format(new Date())}.xlsx".toString())
            fillHeader(headers)
            add(list, withProperties)
            save(response.outputStream)
        }
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def areaByPropertyJSON(){
        log.debug "IN areaByPropertyJSON... $params"
        def output = [:]
        def prop = Property.get(params.propId)
        if(prop!=null){
            output.result = true
            def data = []
            PropertyArea.findAllByProperty(prop).each{
                data.add([id:it.id, name:it.name])
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Data properti tidak ditemukan"
        }
        render output as JSON
    }

    def unitByAreaJSON(){
        log.debug "IN unitByAreaJSON... $params"
        def output = [:]
        def area = PropertyArea.get(params.areaId)
        if(area!=null){
            output.result = true
            def data = []
            PropertyUnit.findAllByArea(area).each{
                data.add([id:it.id, name:it.name])
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Area tidak ditemukan"
        }
        render output as JSON
    }
}
