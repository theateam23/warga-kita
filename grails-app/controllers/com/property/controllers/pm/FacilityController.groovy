package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.FacilityBooking
import com.model.general.PropertyAdminProperty
import com.model.property.PropertyArea
import com.model.property.Facility
import com.model.property.Property
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest



class FacilityController {

    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId

        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(propIdList.size()==0){
            propIdList.add("")
        }
        def columns = ['area.property.name', 'area.name', 'name']
        def baseQuery = "FROM Facility WHERE area.property.propertyManager.pmId = :pmId AND area.property.id in :propIdList "
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [pmId: pmId, propIdList:propIdList]
        def result = ControllerUtil.parseDatatablesQuery(Facility.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.id, areaId: obj.area.id, areaName: obj.area.name, propName:obj.area.property.name, name: obj.name])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        def facilities = Facility.executeQuery("FROM Facility WHERE id = :objId AND area.property.id in :propIdList",
            [propIdList:propIdList, objId:params.id])
        if(facilities.size()>0) {
            def facility = facilities.get(0)
            def result = [id: params.id, propName: facility.area.property.name, areaId: facility.area.id, areaName: facility.area.name,
                          name: facility.name, imageFileId: facility.imageFileId, imageFileName: facility.imageFileName, tnc: facility.tnc]
            [facility: result]
        } else {
            flash.error = "Data fasilitas umum tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def isError = false, message = ''
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        def facility = Facility.executeQuery("FROM Facility WHERE id = :objId AND area.property.id in :propIdList",
                [propIdList:propIdList, objId:params.id]).first()

        if(facility!=null){
            def bookings = FacilityBooking.findAllByFacilityAndStatus(facility ,AppConstants.BOOKING_STATUS_OPEN)
            Facility.withTransaction { status ->
                try {
                    bookings.each{ booking ->
                        booking.delete()
                    }
                    if(ControllerUtil.hasValue(facility.imageFileId)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(facility.imageFileId))
                        if (image.exists()) image.delete()
                    }
                    facility.delete()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Facility tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Data fasilitas berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        if(params._action_create == 'Back'){
            [areaList:areaList, facility:[propName:prop.name, areaId: params.areaId, name:params.name,
                        imageFileId:params.imageFileId, imageFileName: params.imageFileName, tnc: params.tnc]]
        } else {
            [areaList:areaList, facility:[propName:prop.name]]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def name = params.name
        def propName = params.propName
        def areaId = params.areaId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName
        def tnc = params.tnc

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(areaId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def area = PropertyArea.get(areaId)
        def result = [areaId:areaId, areaName:area.name, propName:propName,
                      name:name, imageFileId:imageFileId, imageFileName:imageFileName, tnc:tnc]
        result.action = 'Create Facility'
        result.actionType = 'add'
        result.stage = 'Confirm'
        render(view:'confirm', model: [facility: result])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def propName = params.propName
        def name = params.name
        def areaId = params.areaId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName
        def tnc = params.tnc

        def result = [propName:propName, areaId:areaId, name:name, imageFileId:imageFileId, imageFileName:imageFileName,
                      tnc:tnc, actionType: 'add', action:'Create Facility']
        def isError = false
        def message = ""

        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:currentUser]).collect{ it.property.id }
        def areas = PropertyArea.executeQuery("FROM PropertyArea WHERE id = :objId AND property.id in :propIdList",
                        [objId:areaId, propIdList:propIdList])
        if(areas.size()>0){
            def area = areas.get(0)
            Facility.withTransaction { status ->
                try {
                    def facility = new Facility()
                    facility.area = area
                    facility.name = name
                    facility.imageFileId = imageFileId
                    facility.imageFileName = imageFileName
                    facility.tnc = tnc
                    facility.createdBy = currentUser
                    facility.save(flush: true)

                    if(ControllerUtil.hasValue(imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                    }

                    result.propName = area.property.name
                    result.areaName = area.name
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Area tidak ditemukan"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data fasilitas umum berhasil ditambahkan"
        }
        render view: 'confirm', model: [facility: result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        if(params._action_edit == 'Back'){
            [areaList:areaList, facility: [id:params.id, propName: prop.name, areaId: params.areaId, name:params.name,
                        imageFileId:params.imageFileId, imageFileName:params.imageFileName, tnc:params.tnc]]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def facilities = Facility.executeQuery("FROM Facility WHERE id = :objId AND area.property.propertyManager.pmId = :pmId",
                    [pmId:pmId, objId:params.id])
            if (facilities.size()>0) {
                def facility = facilities.get(0)
                def result = [id:params.id, propName: prop.name, areaId: facility.area.id, name: facility.name,
                              imageFileId: facility.imageFileId, imageFileName:facility.imageFileName,
                              tnc: facility.tnc, actionType: 'edit', action: 'Edit Facility', stage: 'Confirm']
                [areaList:areaList, facility: result]
            } else {
                flash.error = "Data fasilitas umum tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def name = params.name
        def propName = params.propName
        def areaId = params.areaId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName
        def tnc = params.tnc

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(areaId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def facilities = Facility.executeQuery("FROM Facility WHERE id = :objId AND area.property.propertyManager.pmId = :pmId",
                [pmId:pmId, objId:params.id])
        def area = PropertyArea.get(params.areaId)
        if(facilities.size()>0 && area!=null){
            def result = [id:params.id, propName: propName, areaId:areaId,
                          areaName: area.name, name:name, imageFileId: imageFileId, imageFileName:imageFileName, 
                          tnc:tnc, actionType: 'edit', action:'Edit Facility', stage:'Confirm']
            render view:'confirm', model: [facility:result]
        } else {
            flash.error = "Data fasilitas umum tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def id = params.id
        def name = params.name
        def propName = params.propName
        def areaId = params.areaId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName
        def tnc = params.tnc

        def isError = false
        def message = ""
        def result = [id:id, propName:propName, areaId: areaId, name:name, imageFileId:imageFileId, imageFileName:imageFileName, tnc:tnc,
                      actionType: 'edit', action:'Edit Facility', stage:'Result']

        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:currentUser]).collect{ it.property.id }
        def facilities = Facility.executeQuery("FROM Facility WHERE id = :objId AND area.property.id in :propIdList",
                [propIdList:propIdList, objId:params.id])
        if(facilities.size()>0){
            def facility = facilities.get(0)
            Facility.withTransaction { status ->
                try {
                    def areas = PropertyArea.executeQuery("FROM PropertyArea WHERE id = :objId AND property.id in :propIdList",
                            [objId:areaId, propIdList:propIdList])
                    if(areas.size()==0){
                        throw new Exception("Area tidak ditemukan")
                    }
                    def area = areas.get(0)
                    def oldFile = facility.imageFileId
                    facility.area = area
                    facility.name = name
                    facility.imageFileId = imageFileId
                    facility.imageFileName = imageFileName
                    facility.tnc = tnc
                    facility.updatedBy = currentUser
                    facility.save(flush: true)

                    if(oldFile!=imageFileId && ControllerUtil.hasValue(imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }

                    result.propName = area.property.name
                    result.areaName = area.name
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Data fasilitas umum tidak ditemukan"
            redirect(action: 'index')
            return
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data fasilitas berhasil diperbaharui"
        }

        render view:'confirm', model: [facility: result]
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def areaByPropertyJSON(){
        log.debug "IN areaByPropertyJSON... $params"
        def output = [:]
        def prop = Property.get(params.propId)
        if(prop!=null){
            output.result = true
            def data = []
            PropertyArea.findAllByProperty(prop).each{
                data.add([id:it.id, name:it.name])
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Data properti tidak ditemukan"
        }
        render output as JSON
    }
}
