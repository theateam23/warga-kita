package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.CodeSequence
import com.model.general.FacilityBooking
import com.model.general.PropertyAdminProperty
import com.model.general.UserMobileUnit
import com.model.property.Facility
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class ManageFacilityBookingController {
    def bookingService
    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        def pmId = springSecurityService.principal?.pmId
        def facilityList = Facility.executeQuery("FROM Facility WHERE area.property.propertyManager.pmId = :pmId",[pmId:pmId])
        def statusList = AppConstants.getFacilityBookingStatusList()
        [facilityList:facilityList, facilityId:params.facilityId, status:params.status, code:params.code, statusList:statusList]
    }

    def listDatatable(){
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(propIdList.size()==0){
            propIdList.add("")
        }
        def columns = ['facility.name', 'code', 'startDate, startTime', 'startDate, startTime', 'description', 'status', '']
        def baseQuery = "FROM FacilityBooking " +
                "WHERE lower(code) like lower(:code) " +
                "AND facility.id like :facilityId " +
                "AND status like :status " +
                "AND facility.area.property.id in :propIdList "
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery

        def facilityId = params.facilityId?:'%'
        def code = params.code?ControllerUtil.appendWc(params.code):'%'
        def status = params.status?:'%'
        def queryParams = [facilityId:facilityId, code:code, propIdList:propIdList, status:status]
        def result = ControllerUtil.parseDatatablesQuery(FacilityBooking.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdfDate = new SimpleDateFormat("yyyy-MM-dd")
        def sdfTime = new SimpleDateFormat("HH:mm")
        objs.each{ obj ->
            def userName = UserMobile.findByEmail(obj.createdBy)?.name?:UserInfo.findByUser(User.get(obj.createdBy))?.name
            aaData.add([id:obj.id, facilityName: obj.facility.name, userName: userName,
                        bookDate: sdfDate.format(obj.startDate), bookTime: "${sdfTime.format(obj.startTime)}-${sdfTime.format(obj.endTime)}",
                        code:obj.code, startDate:sdfDate.format(obj.startDate), status:obj.status])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propList.get(0)
        def objs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE facility.area.property.id = :propId AND id = :id", [propId:prop.id, id:params.id])
        if(objs.size()>0) {
            def sdfDate = new SimpleDateFormat("yyyy-MM-dd")
            def sdfTime = new SimpleDateFormat("HH:mm")
            def obj = objs.get(0)
            def result = [id: params.id, code: obj.code, facilityName:obj.facility.name, status: obj.status, description:obj.description,
                          bookDate: sdfDate.format(obj.startDate), bookTime: "${sdfTime.format(obj.startTime)}-${sdfTime.format(obj.endTime)}"]
            def userMobile = UserMobile.findByEmail(obj.createdBy)
            if(userMobile!=null){
                result.fromMobile = true
                result.unit = UserMobileUnit.findByUserMobile(userMobile)?.unit?.name
                result.name = userMobile.name
                result.email = userMobile.email
                result.phoneNo = userMobile.phoneNo
            } else {
                result.fromMobile = false
                result.name = UserInfo.findByUser(User.get(obj.createdBy))?.name
            }
            result.isOpen = AppConstants.BOOKING_STATUS_OPEN.equals(obj.status)
            result.isNew = AppConstants.BOOKING_STATUS_NEW.equals(obj.status)
            [detail: result]
        } else {
            flash.error = "Booking tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def approve(){
        log.debug "IN approve... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def fbs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE id = :id AND facility.area.property.id IN :propIdList",
                [id:params.id, propIdList:propIdList])
        if(fbs.size()==0){
            flash.error = "Data tidak ditemukan"
        } else {
            def fb = fbs.get(0)
            def isAvailable = bookingService.isBookingAvailable(fb.facility.id, fb.startDate, fb.startTime, fb.endTime)
            if(!isAvailable){
                flash.error = "Periode waktu sudah dipakai."
            } else {
                FacilityBooking.withTransaction { status ->
                    try {
                        fb.updatedBy = springSecurityService.principal.id
                        fb.status = AppConstants.BOOKING_STATUS_OPEN
                        fb.save(flush: true)
                        flash.message = "Permintaan ${fb.code} berhasil disetujui."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        flash.error = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }
        redirect(action:'index', params:[facilityId:params.facilityId, status:params.status, code:params.code])
    }

    def cancel(){
        log.debug "IN cancel... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def fbs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE id = :id AND facility.area.property.id IN :propIdList",
                [id:params.id, propIdList:propIdList])
        if(fbs.size()==0){
            flash.error = "Data tidak ditemukan"
        } else {
            def fb = fbs.get(0)
            FacilityBooking.withTransaction { status ->
                try{
                    fb.updatedBy = springSecurityService.principal.id
                    fb.status = AppConstants.BOOKING_STATUS_CANCELED
                    fb.save(flush:true)
                    flash.message = "Permintaan ${fb.code} berhasil dibatalkan"
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    flash.error = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        }
        redirect(action:'index', params:[facilityId:params.facilityId, status:params.status, code:params.code])
    }

    def reject(){
        log.debug "IN reject... $params"
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def fbs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE id = :id AND facility.area.property.id IN :propIdList",
                [id:params.id, propIdList:propIdList])
        if(fbs.size()==0){
            flash.error = "Data tidak ditemukan"
        } else {
            def fb = fbs.get(0)
            FacilityBooking.withTransaction { status ->
                try{
                    fb.updatedBy = springSecurityService.principal.id
                    fb.status = AppConstants.BOOKING_STATUS_REJECTED
                    fb.save(flush:true)
                    flash.message = "Permintaan ${fb.code} berhasil ditolak"
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    flash.error = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        }
        redirect(action:'index', params:[facilityId:params.facilityId, status:params.status, code:params.code])
    }
}
