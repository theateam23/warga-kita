package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.property.HomeGuard
import com.model.property.HomeGuardLog
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import pl.touk.excel.export.WebXlsxExporter

import java.text.SimpleDateFormat



class HomeGuardController {
    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        [propName:prop.name, startDate:params.startDate, endDate:params.endDate]
    }

    def listDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def pap = PropertyAdminProperty.findByPropertyAdmin(currentPm)

        def columns = ['unit','guardDate','updatedBy','updatedDate','imageFileName','imageFileId']
        def baseQuery = "FROM HomeGuardLog WHERE propertyId = :propertyId " +
                "AND (guardDate BETWEEN :startDate AND :endDate) "

        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date()).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [startDate:startDate, endDate:endDate, propertyId:pap.property.id]
        def result = ControllerUtil.parseDatatablesQuery(HomeGuard.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        objs.each{ obj ->
            def det = [id:obj.id, unitName:obj.unit, checkBy:obj.updatedBy?:"-"]
            if(obj.guardDate!=null) {
                det.date = "${sdf.format(obj.guardDate)}"
            } else {
                det.date = "-"
            }

            if(ControllerUtil.hasValue(obj.imageFileId)){
                det.imageFileId = obj.imageFileId
                det.imageFileName = obj.imageFileName
            }

            if(obj.updatedDate!=null){
                det.checkDate = "${sdf.format(obj.updatedDate)}"
            } else {
                det.checkDate = "-"
            }

            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("homeGuard${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def downloadReport(){
        log.debug "IN downloadReport... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def prop = PropertyAdminProperty.findByPropertyAdmin(currentPm)
        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date() + 1).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }

        def baseQuery = "FROM HomeGuardLog WHERE propertyId = :propertyId " +
                "AND (guardDate BETWEEN :startDate AND :endDate) "
        def queryParams = [startDate:startDate, endDate:endDate, propertyId:prop.property.id]
        def list = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        def sdf3 = new SimpleDateFormat("yyyyMMdd")
        HomeGuardLog.executeQuery(baseQuery, queryParams).each { obj ->
            def det = [id:obj.id, unitName:obj.unit, checkBy:obj.updatedBy?:"-"]
            if(obj.guardDate!=null) {
                det.date = "${sdf.format(obj.guardDate)}"
            } else {
                det.date = "-"
            }

            if(ControllerUtil.hasValue(obj.imageFileId)){
                det.url = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'homeGuard', action: 'showImage', params: [id: obj.imageFileId]))
                det.fileName = obj.imageFileName
            }

            if(obj.updatedDate!=null){
                det.checkDate = "${sdf.format(obj.updatedDate)}"
            } else {
                det.checkDate = "-"
            }
            list.add(det)
        }

        def headers = ['Date', 'Unit', 'Check Date', 'Check By']
        def withProperties = ['date', 'unitName', 'checkDate', 'checkBy']

        new WebXlsxExporter().with {
            setResponseHeaders(response, "laporan_jagarumah_${sdf3.format(startDate)}_${sdf3.format(endDate)}.xlsx".toString())
            fillHeader(headers)
            add(list, withProperties)
            save(response.outputStream)
        }
    }

}
