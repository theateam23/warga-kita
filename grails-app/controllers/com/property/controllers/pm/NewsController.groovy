package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.information.PropertyNews
import com.model.information.PropertyNewsTarget
import com.model.maintenance.NewsCategory
import com.model.maintenance.RoleMobile
import com.model.property.Property
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat



class NewsController {

    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId
        def columns = ['title', 'property.name', 'summary', '', '', 'createdDate']
        def baseQuery = "FROM PropertyNews WHERE property.propertyManager.user.id = :pmId"
        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [pmId: pmId]
        def result = ControllerUtil.parseDatatablesQuery(PropertyNews.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        objs.each{ obj ->
            def targetNameList = []
            PropertyNewsTarget.findAllByPropertyNews(obj).each{
                targetNameList.add(it.roleMobile.name)
            }
            aaData.add([id:obj.id, title: obj.title, subtitle:obj.subtitle, propName: obj.property.name, summary: ControllerUtil.shorten(obj.summary,100),
                        createdDate:sdf.format(obj.createdDate), target: targetNameList.join(", "), imageFileId: obj.imageFileId, imageFileName:obj.imageFileName])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def pmId = springSecurityService.principal?.pmId
        def propertyNews = PropertyNews.executeQuery("FROM PropertyNews where id = :id AND property.propertyManager.user.id = :pmId", 
                [id:params.id, pmId:pmId]).first()
        if(propertyNews!=null) {
            def targetNameList = []
            PropertyNewsTarget.findAllByPropertyNews(propertyNews).each { t ->
                targetNameList.add(t.roleMobile.name)
            }
            def result = [id: params.id, title: propertyNews.title, subtitle: propertyNews.subtitle, summary: propertyNews.summary, description: propertyNews.description,
                          propId: propertyNews.property.id, propName: propertyNews.property.name, imageFileId: propertyNews.imageFileId,
                          imageFileName: propertyNews.imageFileName, category: propertyNews.category?.name, categoryIcon: propertyNews.category?.icon,
                          email:propertyNews.email, phoneNo:propertyNews.phoneNo, sms:propertyNews.sms, address:propertyNews.address]
            [propertyNews: result, targetNameList: targetNameList]
        } else {
            flash.error = "Data informasi tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def delete(){
        log.debug "IN delete... $params"
        def pmId = springSecurityService.principal?.pmId
        def isError = false, message = ''
        def propertyNews = PropertyNews.executeQuery("FROM PropertyNews where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()

        if(propertyNews!=null){
            PropertyNews.withTransaction { status ->
                try {
                    PropertyNewsTarget.findAllByPropertyNews(propertyNews).each { t ->
                        t.delete()
                    }
                    if(ControllerUtil.hasValue(propertyNews.imageFileId)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(propertyNews.imageFileId))
                        if (image.exists()) image.delete()
                    }
                    propertyNews.delete()
                } catch (Exception e) {
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "News tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detail', params:params)
        } else {
            flash.message = "Data informasi berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def roleList = RoleMobile.list()
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def categoryList = NewsCategory.list()

        if(params._action_create == 'Back'){
            def target = []
            if(params.target!=null){
                if(params.target instanceof String){
                    target.add(params.target)
                } else {
                    target = params.target
                }
            }
            [propertyNews: [propName:prop.name, title:params.title, subtitle:params.subtitle, description:params.description, summary:params.summary, propId: params.propId, target: target,
                            imageFileId: params.imageFileId, imageFileName: params.imageFileName, category:params.category,
                            email:params.email, phoneNo:params.phoneNo, sms:params.sms, address:params.address],
             roleList:roleList, categoryList:categoryList]
        } else {
            [propertyNews:[propName:prop.name], roleList:roleList, categoryList:categoryList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def pmId = springSecurityService.principal?.pmId
        def title = params.title
        def subtitle = params.subtitle
        def description = params.description
        def summary = params.summary
        def propName = params.propName
        def category = params.category
        def email = params.email
        def phoneNo = params.phoneNo
        def sms = params.sms
        def address = params.address
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        if(!ControllerUtil.hasValue(title) || !ControllerUtil.hasValue(subtitle) || !ControllerUtil.hasValue(description) || target.size()==0){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [propName:propName, title:title, subtitle:subtitle, description:description, summary:summary, target:target, category:category,
                      email:email, phoneNo:phoneNo, sms:sms, address:address]
        def targetNameList = []
        target.each{ roleId ->
            targetNameList.add(RoleMobile.get(roleId).name)
        }
        def cat = NewsCategory.get(category)
        result.imageFileId = params.imageFileId
        result.imageFileName = params.imageFileName
        result.categoryName = cat?.name
        result.categoryIcon = cat?.icon
        result.action = 'Create News'
        result.stage = 'Confirm'
        result.actionType = 'add'
        render(view:'confirm', model: [propertyNews: result, targetNameList:targetNameList])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def title = params.title
        def subtitle = params.subtitle
        def description = params.description
        def summary = params.summary
        def propName = params.propName
        def category = params.category
        def email = params.email
        def phoneNo = params.phoneNo
        def sms = params.sms
        def address = params.address
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        def result = [propName:propName, title:title, subtitle:subtitle, description:description, summary:summary, imageFileId: params.imageFileId, category:category,
                      email:email, phoneNo:phoneNo, sms:sms, address:address, imageFileName: params.imageFileName, target:params.target, actionType: 'add', action:'Create News']
        def isError = false
        def message = ""

        def targetNameList = []
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)
            def cat = NewsCategory.get(category)
            PropertyNews.withTransaction { status ->
                try {
                    def propertyNews = new PropertyNews()
                    propertyNews.title = title
                    propertyNews.subtitle = subtitle
                    propertyNews.description = description.trim()
                    propertyNews.summary = summary.trim()
                    propertyNews.property = prop
                    propertyNews.createdBy = currentUser
                    propertyNews.imageFileId = params.imageFileId
                    propertyNews.imageFileName = params.imageFileName
                    propertyNews.email = email
                    propertyNews.phoneNo = phoneNo
                    propertyNews.sms = sms
                    propertyNews.address = address
                    propertyNews.category = cat
                    propertyNews.save(flush: true)

                    if(ControllerUtil.hasValue(params.imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                    }

                    target.each { roleId ->
                        def roleMobile = RoleMobile.get(roleId)
                        def pnt = new PropertyNewsTarget()
                        pnt.roleMobile = roleMobile
                        pnt.propertyNews = propertyNews
                        pnt.save()
                        targetNameList.add(roleMobile.name)
                    }

                    result.propName = prop.name
                    result.categoryName = cat.name
                    result.categoryIcon = cat.icon
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Properti tidak ditemukan"
            redirect(action:"index")
            return
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data informasi berhasil ditambahkan"
        }
        render view: 'confirm', model: [propertyNews: result, targetNameList:targetNameList]
    }

    def edit(){
        log.debug "IN edit... $params"
        def roleList = RoleMobile.list()
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def categoryList = NewsCategory.list()

        if(params._action_edit == 'Back'){
            def target = []
            if(params.target!=null){
                if(params.target instanceof String){
                    target.add(params.target)
                } else {
                    target = params.target
                }
            }
            [propertyNews: [propName:prop.name, id:params.id, title: params.title, subtitle: params.subtitle, description: params.description, summary:params.summary, target:target,
                imageFileId: params.imageFileId, imageFileName: params.imageFileName, category:params.category,
                email:params.email, phoneNo:params.phoneNo, sms:params.sms, address:params.address], roleList:roleList, categoryList:categoryList]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def propertyNews = PropertyNews.executeQuery("FROM PropertyNews where id = :id AND property.propertyManager.user.id = :pmId",
                    [id:params.id, pmId:pmId]).first()
            if (propertyNews != null) {
                def target = []
                PropertyNewsTarget.findAllByPropertyNews(propertyNews).each { t ->
                    target.add(t.roleMobile.id)
                }
                def result = [propName:prop.name, id:params.id, title: propertyNews.title, subtitle: propertyNews.subtitle, description: propertyNews.description,
                              summary:propertyNews.summary, target:target, imageFileId: propertyNews.imageFileId, imageFileName: propertyNews.imageFileName,
                              email:propertyNews.email, phoneNo:propertyNews.phoneNo, sms:propertyNews.sms, address:propertyNews.address,
                              category:propertyNews.category?.id, actionType: 'edit', action: 'Edit News', stage: 'Confirm']
                [propertyNews: result, roleList:roleList, categoryList:categoryList]
            } else {
                flash.error = "Data informasi tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def title = params.title
        def subtitle = params.subtitle
        def description = params.description
        def summary = params.summary
        def propName = params.propName
        def category = params.category
        def email = params.email
        def phoneNo = params.phoneNo
        def sms = params.sms
        def address = params.address
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        if(!ControllerUtil.hasValue(title) || !ControllerUtil.hasValue(subtitle) || !ControllerUtil.hasValue(description) || target.size()==0){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def propertyNews = PropertyNews.executeQuery("FROM PropertyNews where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(propertyNews!=null){
            def targetNameList = []
            target.each{ roleId ->
                targetNameList.add(RoleMobile.get(roleId).name)
            }
            def result = [id:params.id, title:title, subtitle:subtitle, propName:propName, description: description, summary:summary, target:target,
                          email:email, phoneNo:phoneNo, sms:sms, address:address,
                          imageFileId: params.imageFileId, imageFileName: params.imageFileName, actionType: 'edit', action:'Edit News', stage:'Confirm']
            def cat = NewsCategory.get(category)
            result.category = category
            result.categoryName = cat.name
            result.categoryIcon = cat.icon
            render view:'confirm', model: [propertyNews:result, targetNameList: targetNameList]
        } else {
            flash.error = "Data informasi tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def pmId = springSecurityService.principal?.pmId
        def id = params.id
        def title = params.title
        def subtitle = params.subtitle
        def description = params.description
        def summary = params.summary
        def propName = params.propName
        def category = params.category
        def email = params.email
        def phoneNo = params.phoneNo
        def sms = params.sms
        def address = params.address
        def target = []
        if(params.target!=null){
            if(params.target instanceof String){
                target.add(params.target)
            } else {
                target = params.target
            }
        }

        def isError = false
        def message = ""
        def result = [title:title, subtitle:subtitle, description:description, summary:summary, propName:propName, imageFileId: params.imageFileId,
                      email:email, phoneNo:phoneNo, sms:sms, address:address, category: category, imageFileName: params.imageFileName, actionType: 'edit', action:'Edit News']

        def targetNameList = []
        def propertyNews = PropertyNews.executeQuery("FROM PropertyNews where id = :id AND property.propertyManager.user.id = :pmId",
                [id:params.id, pmId:pmId]).first()
        if(propertyNews!=null){
            PropertyNews.withTransaction { status ->
                try {
                    def cat = NewsCategory.get(category)
                    def oldFile = propertyNews.imageFileId
                    propertyNews.title = title
                    propertyNews.subtitle = subtitle
                    propertyNews.description = description.trim()
                    propertyNews.summary = summary.trim()
                    propertyNews.updatedBy = currentUser
                    propertyNews.imageFileId = params.imageFileId
                    propertyNews.imageFileName = params.imageFileName
                    propertyNews.email = email
                    propertyNews.phoneNo = phoneNo
                    propertyNews.sms = sms
                    propertyNews.address = address
                    propertyNews.category = cat
                    propertyNews.save(flush: true)

                    if(oldFile!=params.imageFileId && ControllerUtil.hasValue(params.imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}tmp${File.separator}").concat(params.imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(params.imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }

                    //delete old target
                    PropertyNewsTarget.findAllByPropertyNews(propertyNews).each { it.delete(flush:true) }
                    //recreate
                    target.each { roleId ->
                        def roleMobile = RoleMobile.get(roleId)
                        def pnt = new PropertyNewsTarget()
                        pnt.roleMobile = roleMobile
                        pnt.propertyNews = propertyNews
                        pnt.save()
                        targetNameList.add(roleMobile.name)
                    }

                    result.categoryName = cat.name
                    result.categoryIcon = cat.icon
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Data informasi tidak ditemukan"
            redirect(action: 'index')
            return
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data informasi berhasil diperbaharui"
        }

        render view:'confirm', model: [propertyNews: result, targetNameList:targetNameList]
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }
}
