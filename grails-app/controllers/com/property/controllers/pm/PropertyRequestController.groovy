package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.property.Property
import com.model.property.PropertyRequest
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat



class PropertyRequestController {

    GrailsApplication grailsApplication
    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        [propName:prop.name, startDate:params.startDate, endDate:params.endDate]
    }

    def listDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def columns = ['property.name', 'createdDate', 'title', 'description', 'fileName']
        def baseQuery = "FROM PropertyRequest WHERE property.id like :propId AND property.propertyManager.id = :pmInfoId "
//                "AND createdDate BETWEEN :startDate AND :endDate "

        def prop = PropertyAdminProperty.findByPropertyAdmin(currentPm)
        def propId = prop.property.id
//        def startDate = null, endDate = null
//        try{
//            if(ControllerUtil.hasValue(params.startDate)){
//                startDate = new Date(Long.valueOf(params.startDate))
//            } else {
//                startDate = new Date().clearTime()
//            }
//            if(ControllerUtil.hasValue(params.endDate)){
//                endDate = new Date(Long.valueOf(params.endDate))
//            } else {
//                endDate = (new Date() + 1).clearTime()
//            }
//        } catch (Exception e){
//            log.debug "Error: ${e.getMessage()}"
//        }

        def query = "" + baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
//        def queryParams = [propId:propId, startDate:startDate, endDate:endDate, pmInfoId:pmInfo.id]
        def queryParams = [propId:propId, pmInfoId:pmInfo.id]
        log.debug "$queryParams"
        def result = ControllerUtil.parseDatatablesQuery(PropertyRequest.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ obj ->
            def det = [id:obj.id, property: obj.property.name, description:ControllerUtil.shorten(obj.description,100), title: obj.title, fileName: obj.fileName]
            if(obj.createdDate!=null) {
                det.date = sdf.format(obj.createdDate)
            }
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def obj = PropertyRequest.findByPropertyAndId(prop, params.id)
        if(obj!=null) {
            def result = [id: params.id, propName: obj.property.name, title: obj.title, description: obj.description,
                          fileId: obj.fileId, fileName: obj.fileName]
            [detail: result]
        } else {
            flash.error = "Surat keterangan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        if(params._action_create == 'Back'){
            [detail:[propName:prop.name, title:params.title, description: params.description,
                     fileId:params.fileId, fileName: params.fileName]]
        } else {
            [detail:[propName:prop.name]]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"

        def propName = params.propName
        def title = params.title
        def description = params.description
        def fileId = params.fileId
        def fileName = params.fileName

        if(!ControllerUtil.hasValue(fileId) || !ControllerUtil.hasValue(title)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [propName: propName, description: description,
                      title:title, fileId: fileId, fileName: fileName]
        result.action = 'Submit Request Form'
        result.actionType = 'add'
        result.stage = 'Confirm'
        render(view:'confirm', model: [detail: result])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal?.id
        def propName = params.propName
        def title = params.title
        def description = params.description
        def fileId = params.fileId
        def fileName = params.fileName

        def result = [propName:propName, title:title, description: description, fileId:fileId, fileName:fileName,
                      actionType: 'add', action:'Submit Request Form']
        def isError = false
        def message = ""

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propertyList.size()>0){
            def prop = propertyList.get(0)
            PropertyRequest.withTransaction { status ->
                try {
                    def obj = new PropertyRequest()
                    obj.property = prop
                    obj.title = title
                    obj.description = description
                    obj.fileId = fileId
                    obj.fileName = fileName
                    obj.createdBy = currentUser
                    obj.save(flush: true)

                    if(ControllerUtil.hasValue(fileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(fileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(fileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                    }
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Data properti tidak ditemukan"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Surat keterangan berhasil ditambahkan"
        }
        render view: 'confirm', model: [detail: result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        if(params._action_edit == 'Back'){
            [detail: [id:params.id, propName:prop.name, title:params.title, description: params.description,
                      fileId:params.fileId, fileName:params.fileName]]
        } else {
            def pmId = springSecurityService.principal?.pmId
            def requestList = PropertyRequest.executeQuery("FROM PropertyRequest WHERE id = :objId AND property.propertyManager.pmId = :pmId",
                    [pmId:pmId, objId:params.id])
            if (requestList.size()>0) {
                def obj = requestList.get(0)
                def result = [id:params.id, propName:prop.name, title: obj.title, description: obj.description,
                              fileId: obj.fileId, fileName:obj.fileName, actionType: 'edit', action: 'Edit Request Form', stage: 'Confirm']
                [detail: result]
            } else {
                flash.error = "Surat keterangan tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal?.pmId

        def id = params.id
        def propName = params.propName
        def title = params.title
        def description = params.description
        def fileId = params.fileId
        def fileName = params.fileName

        if(!ControllerUtil.hasValue(title) || !ControllerUtil.hasValue(fileId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def objs = PropertyRequest.executeQuery("FROM PropertyRequest WHERE id = :objId AND property.propertyManager.pmId = :pmId",
                [pmId:pmId, objId:id])
        if(objs.size()>0){
            def result = [id:params.id, propName: propName, title:title, description: description,
                          fileId: fileId, fileName:fileName, actionType: 'edit', action:'Edit Request Form', stage:'Confirm']
            render view:'confirm', model: [detail:result]
        } else {
            flash.error = "Surat keterangan tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal?.id
        def id = params.id
        def propName = params.propName
        def title = params.title
        def description = params.description
        def fileId = params.fileId
        def fileName = params.fileName

        def isError = false
        def message = ""
        def result = [id:id, propName:propName, title:title, description: description, fileId:fileId, fileName:fileName,
                      actionType: 'edit', action:'Edit Request Form', stage:'Result']

        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:currentUser]).collect{ it.property.id }
        def objs = PropertyRequest.executeQuery("FROM PropertyRequest WHERE id = :objId AND property.id in :propIdList",
                [propIdList:propIdList, objId:params.id])
        if(objs.size()>0){
            def obj = objs.get(0)
            PropertyRequest.withTransaction { status ->
                try {
                    def oldFile = obj.fileId
                    obj.title = title
                    obj.description = description
                    obj.fileId = fileId
                    obj.fileName = fileName
                    obj.createdBy = currentUser
                    obj.save(flush: true)

                    if(oldFile!=fileId && ControllerUtil.hasValue(fileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(fileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(fileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }

                    result.propName = obj.property?.name
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "Surat keterangan tidak ditemukan"
            redirect(action: 'index')
            return
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Surat keterangan berhasil diperbaharui"
        }

        render view:'confirm', model: [detail: result]
    }

    def downloadFile() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(params.id)
            def fileName = PropertyRequest.findByFileId(params.id)?.fileName?:"attachment"
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] fileBytes = is.bytes
                response.setHeader('Content-length', fileBytes.length.toString())
                response.setHeader('Content-disposition', "attachment;filename=\"${fileName}\"")
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << fileBytes
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def delete(){
        log.debug "IN delete... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = PropertyRequest.get(params.id)
        if(obj!=null && obj.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.property.id)){
            PropertyRequest.withTransaction { status ->
                try {
                    if(ControllerUtil.hasValue(obj.fileId)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(obj.fileId))
                        if (image.exists()) image.delete()
                    }
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Request form tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
        } else {
            flash.message = "Surat keterangan berhasil dihapus"
        }
        redirect(action: 'index')
    }
}
