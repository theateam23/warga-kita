package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.general.TmpMail
import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.information.BroadcastReminder
import com.model.information.InboxMessage
import com.model.maintenance.Gender
import com.model.maintenance.RoleMobile
import com.model.property.PropertyArea
import com.model.property.PropertyUnit
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import com.property.utils.ExcelImporter
import com.property.utils.FirebaseUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import java.text.SimpleDateFormat



class UserMobileController implements ResourceLoaderAware {

    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    def mailService
    def mailMessageContentRenderer
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
        [:]
    }

    def listDatatable(){
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)

        def columns = ['umu.userMobile.email', 'umu.userMobile.name', 'umu.unit.area.name', 'umu.unit.subArea',
                       'umu.unit.name', 'umu.roleMobile.name', 'umu.userMobile.createdDate']
        def baseQuery = "FROM UserMobileUnit umu WHERE umu.unit.area.property.id = :propId "
        def query = "SELECT umu " + baseQuery
        def queryCount = "SELECT count(umu.userMobile.id) " + baseQuery

        def queryParams = [propId:prop.id]
        def result = ControllerUtil.parseDatatablesQuery(UserMobile.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        objs.each{ obj ->
            aaData.add([id:obj.userMobile.id, email: obj.userMobile.email, name: obj.userMobile.name,
                        area: obj.unit.area.name, subArea: obj.unit.subArea, unit: obj.unit.name,
                        role: obj.roleMobile.name, createdDate:sdf.format(obj.userMobile.createdDate)])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailUser(){
        log.debug "IN detailUser... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(params.id)
        def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile,prop)
        if(ump!=null){
            def result = [:]
            result.id = params.id
            result.email = userMobile.email
            result.name = userMobile.name
            result.phoneNo = userMobile.phoneNo
            result.propName = prop.name
            result.title = userMobile.title

            def umu = UserMobileUnit.findByUserMobile(userMobile)
            result.area = umu.unit.area.name
            result.subArea = umu.unit.subArea
            result.unit = umu.unit.name
            result.role = umu.roleMobile.name
            [user:result]
        } else {
            flash.error = "User Mobile tidak ditemukan"
            redirect(action:'index')
            return
        }
    }

    def createUser(){
        log.debug "IN createUser... $params"
        def result = [:]
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def roleList = RoleMobile.findAllByRoleType(AppConstants.ROLE_TYPE_UNIT)
        def genderList = Gender.list()
        if(params._action_createUser == 'Back'){
            result = [propName: prop.name, id:params.id, email:params.email, name:params.name, phoneNo:params.phoneNo,
                      roleId:params.roleId, itle:params.title, areaId:params.areaId, unitId:params.unitId, subArea:params.subArea]
            [user: result, areaList:areaList, genderList:genderList, roleList:roleList]
        } else {
            result = [propName: prop.name]
            [user: result, areaList:areaList, genderList:genderList, roleList:roleList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def roleId = params.roleId
        def subArea = params.subArea
        def title = params.title

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(unitId)){
            flash.error = "Silakan isi semua data yang diperlukan."
            params._action_createUser = 'Back'
            redirect(action: 'createUser', params:params)
            return
        }

        if(UserMobile.findByEmail(email)!=null){
            flash.error = "User dengan email ini, sudah ada."
            params._action_createUser = 'Back'
            redirect(action: 'createUser', params:params)
            return
        }

        def result = [propName: propName, email:email, name:name, phoneNo:phoneNo,
                      roleId:roleId, title:title, areaId:areaId, unitId:unitId, subArea:subArea,
                      actionType: 'add', action:'Create User', stage:'Confirm']
        if(ControllerUtil.hasValue(roleId)){
            result.roleName = RoleMobile.get(roleId)?.name
        }
        if(ControllerUtil.hasValue(unitId)){
            result.unitName = PropertyUnit.get(unitId)?.name
        }
        if(ControllerUtil.hasValue(areaId)){
            result.areaName = PropertyArea.get(areaId)?.name
        }
        render view:'confirm', model: [user: result]
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def roleId = params.roleId
        def subArea = params.subArea
        def title = params.title

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def unit = PropertyUnit.get(unitId)
        def roleMobile = RoleMobile.get(roleId)
        def isError = false, message = ''

        def result = [propName: propName, email:email, name:name, phoneNo:phoneNo,
                      roleId:roleId, title:title, areaId:areaId, unitId:unitId, subArea:subArea,
                      actionType: 'add', action:'Create User', stage:'Result']
        if(unit!=null && unit.area.property.id == prop.id){
            if(UserMobile.findByEmail(email)!=null){
                message = "User with this email already exists"
                isError = true
            } else if((unit.tenant!=null && roleMobile.name != AppConstants.ROLE_SUB_USER) ||
                    (unit.ownerResident!=null && roleMobile.name != AppConstants.ROLE_SUB_USER)){
                isError = true
                message = "Unit has already been assigned"
            } else {
                result.areaName = unit.area.name
                result.subArea = unit.subArea
                result.roleName = roleMobile.name
                result.unitName = unit.name
                UserMobile.withTransaction { status ->
                    try {

                        def response = FirebaseUtil.createUser(email)
                        if (response.isSuccess) {
                            def userMobile = new UserMobile()
                            userMobile.email = email
                            userMobile.name = name
                            userMobile.phoneNo = phoneNo
                            userMobile.createdBy = currentUser
                            userMobile.uid = response.uid
                            userMobile.save()

                            def ump = new UserMobileProperty()
                            ump.userMobile = userMobile
                            ump.property = prop
                            ump.save()

                            def umu = new UserMobileUnit()
                            umu.unit = unit
                            umu.userMobile = userMobile
                            umu.roleMobile = roleMobile
                            umu.save()

                            if (AppConstants.ROLE_TENANT.equals(roleMobile.name)) {
                                unit.tenant = userMobile
                                unit.save()
                            } else if (AppConstants.ROLE_OWNER_RESIDENT.equals(roleMobile.name)) {
                                unit.ownerResident = userMobile
                                unit.save()
                            }

                            log.debug("sending email userWelcome ${email} : ${response.password}")
                            mailService.sendMail {
                                to userMobile.email
                                from "support@warga-kita.com"
                                subject "Pendaftaran User Baru Warga Kita"
                                html view: "userWelcome", model: [userName: name, email: email, password: response.password]
                            }
                        } else {
                            isError = true
                            message = response.message
                        }

                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "User berhasil ditambahkan"
            }
            render view: 'confirm', model: [user:result]
        } else {
            flash.error = "Unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def editUser(){
        log.debug "IN editUser... $params"
        def result = [:]
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(params.id)
        def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile,prop)
        def umu = UserMobileUnit.findByUserMobile(userMobile)
        if(userMobile!=null && umu!=null && ump!=null){
            def areaList = PropertyArea.findAllByProperty(ump.property)
            def roleList = RoleMobile.findAllByRoleType(AppConstants.ROLE_TYPE_UNIT)
            if(params._action_editUser == 'Back'){
                result = [propName: prop.name, id:params.id, email:params.email, name:params.name, phoneNo:params.phoneNo,
                          roleId:params.roleId, areaId:params.areaId, unitId:params.unitId, subArea:params.subArea]
                [user: result, areaList:areaList, roleList:roleList]
            } else {
                result = [propName: prop.name, id:userMobile.id, email:userMobile.email, name:userMobile.name, phoneNo:userMobile.phoneNo,
                          roleId:umu.roleMobile?.id, areaId:umu.unit?.area?.id, unitId:umu.unit?.id, subArea: umu.unit?.subArea]
                [user: result, areaList:areaList, roleList:roleList]
            }
        } else {
            flash.error = "User Mobile tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def id = params.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def roleId = params.roleId
        def subArea = params.subArea

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(unitId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editUser = 'Back'
            redirect(action: 'editUser', params:params)
            return
        }

        if(UserMobile.findByEmailAndIdNotEqual(email,id)!=null){
            flash.error = "User dengan email ini, sudah ada."
            params._action_createUser = 'Back'
            redirect(action: 'editUser', params:params)
            return
        }

        def result = [propName: propName, id:id, email:email, name:name, phoneNo:phoneNo,
                      roleId:roleId, areaId:areaId, unitId:unitId, subArea:subArea,
                      actionType: 'edit', action:'Edit User', stage:'Confirm']
        if(ControllerUtil.hasValue(roleId)){
            result.roleName = RoleMobile.get(roleId)?.name
        }
        if(ControllerUtil.hasValue(unitId)){
            result.unitName = PropertyUnit.get(unitId)?.name
        }
        if(ControllerUtil.hasValue(areaId)){
            result.areaName = PropertyArea.get(areaId)?.name
        }
        render view:'confirm', model: [user: result]
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id

        def id = params.id
        def email = params.email
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def roleId = params.roleId
        def subArea = params.subArea

        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: springSecurityService.principal.id]).collect { it.property }
        def prop = propertyList.get(0)
        def userMobile = UserMobile.get(id)
        def unit = PropertyUnit.get(unitId)
        def roleMobile = RoleMobile.get(roleId)
        def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile,prop)
        def isError = false, message = ''

        def result = [propName: propName, id:id, email:email, name:name, phoneNo:phoneNo,
                      roleId:roleId, areaId:areaId, unitId:unitId, subArea:subArea,
                      actionType: 'edit', action:'Edit User', stage:'Result']
        if(ump!=null && unit!=null && unit.area.property.id == prop.id){
            if((unit.tenant!=null && unit.tenant.id != userMobile.id && roleMobile.name != AppConstants.ROLE_SUB_USER) ||
                (unit.ownerResident!=null && unit.ownerResident.id != userMobile.id && roleMobile.name != AppConstants.ROLE_SUB_USER)){
                isError = true
                message = "Unit has already been assigned"
            } else if(UserMobile.findByEmailAndIdNotEqual(email,id)!=null){
                message = "User with this email already exists"
                isError = true
            } else {
                result.areaName = unit.area.name
                result.subArea = unit.subArea
                result.roleName = roleMobile.name
                result.unitName = unit.name
                UserMobile.withTransaction { status ->
                    try {
                        userMobile.email = email
                        userMobile.name = name
                        userMobile.phoneNo = phoneNo
                        userMobile.updatedBy = currentUser
                        userMobile.save()

                        def umu = UserMobileUnit.findByUserMobile(userMobile)
                        if(umu!=null && umu.unit.id!=unitId){
                            def oldUnit = umu.unit
                            oldUnit.tenant = null
                            oldUnit.ownerResident = null
                            oldUnit.save()
                            umu.delete()

                            umu = new UserMobileUnit()
                            umu.unit = unit
                            umu.userMobile = userMobile
                            umu.roleMobile = roleMobile
                            umu.save()
                        } else if(umu == null){
                            umu = new UserMobileUnit()
                            umu.unit = unit
                            umu.userMobile = userMobile
                            umu.roleMobile = roleMobile
                            umu.save()
                        }

                        if (AppConstants.ROLE_TENANT.equals(roleMobile.name)) {
                            unit.tenant = userMobile
                            unit.ownerResident = null
                            unit.save()
                        } else if (AppConstants.ROLE_OWNER_RESIDENT.equals(roleMobile.name)) {
                            unit.ownerResident = userMobile
                            unit.tenant = null
                            unit.save()
                        }
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "User berhasil diperbaharui"
            }
            render view: 'confirm', model: [user:result]
        } else {
            flash.error = "User Mobile tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def deleteUser(){
        log.debug "IN deleteUser... $params"
        def currentUser = springSecurityService.principal.id
        def propIdList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser: currentUser]).collect { it.property.id }
        def umps = UserMobileProperty.executeQuery("SELECT ump FROM UserMobileProperty ump " +
                "WHERE ump.userMobile.id = :userId AND ump.property.id in :propIdList",
                [userId: params.id, propIdList: propIdList])
        if(umps.size()>0){
            def obj = umps.get(0)
            UserMobileProperty.withTransaction { status ->
                try{
                    def deleteUser = false
                    def userMobile = obj.userMobile
                    if(UserMobileProperty.findAllByUserMobile(obj.userMobile).size()==1){
                        deleteUser = true
                    }
                    obj.delete()
                    if(deleteUser){
                        def firebaseUid = userMobile.uid
                        UserMobileUnit.findAllByUserMobile(userMobile).each{ it.delete() }
                        InboxMessage.findAllByUserMobile(userMobile).each{ im ->
                            BroadcastReminder.findAllByInboxMessage(im).each { br ->
                                br.delete()
                            }
                            im.delete()
                        }

                        def unitOwner = PropertyUnit.findByOwnerResident(userMobile)
                        if (unitOwner) {
                            unitOwner.ownerResident = null
                            unitOwner.save()
                        }

                        def unitTenant = PropertyUnit.findByTenant(userMobile)
                        if (unitTenant) {
                            unitTenant.tenant = null
                            unitTenant.save()
                        }

                        userMobile.delete()

                        FirebaseUtil.deleteUser(firebaseUid)
                    }

                    flash.message = "User berhasil dihapus dari properti ini"
                }  catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    flash.error = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            flash.error = "User Mobile tidak ditemukan"
        }
        redirect(action: 'index')
    }

    def uploadFile(){
        log.debug "IN uploadFile... $params"
        [:]
    }

    def confirmUpload(){
        log.debug "IN confirmUpload... $params"
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            try {
                def importer = new ExcelImporter(filePath)
                def userList = importer.getUserMobileList()

                if (userList.size() > 100) {
                    flash.error = "Jumlah data tidak boleh lebih dari 100 user"
                    redirect(action:'uploadFile')
                } else {
                    def cnt = 1
                    userList.each{ map ->
                        map.put("cnt", cnt)
                        cnt++
                    }

                    [fileImportId: params.fileImportId, userList:userList]
                }
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                flash.error = "Server bermasalah, coba lagi beberapa saat."
                redirect(action:'uploadFile')
            }
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def submitImport(){
        log.debug "IN submitImport... $params"
        def currentUser = springSecurityService.principal.id
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        int totalRecord =0, totalSuccess = 0
        if(file.exists()){
            def importer = new ExcelImporter(filePath)
            def userList = importer.getUserMobileList()
            totalRecord = userList.size()
            def cnt = 1
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                    [currentUser:currentUser]).collect{ it.property }
            if(propList.size()==0){
                flash.error = "Properti tidak ditemukan"
                redirect(action:"index")
                return
            }
            def prop = propList[0]
            userList.each{ map ->
                map.put("cnt", cnt)
                cnt++

                def actionStartTime = System.currentTimeMillis()

                if( !ControllerUtil.hasValue(map.get("name")) ||
                    !ControllerUtil.hasValue(map.get("email")) ||
                    !ControllerUtil.hasValue(map.get("phoneNo")) ||
                    !ControllerUtil.hasValue(map.get("status")) ||
                    !ControllerUtil.hasValue(map.get("unit"))){
                    map.put("message", "Lengkapi data yang belum diisi.")
                } else {
                    def roleMobile = RoleMobile.findByNameAndRoleType(map.get("status"), AppConstants.ROLE_TYPE_UNIT)
                    def units = PropertyUnit.executeQuery("FROM PropertyUnit WHERE area.property.id = :propId AND name = :name",
                                [propId:prop.id, name:map.get("unit")])
                    if(UserMobile.findByEmail(map.get("email"))!=null){
                        map.put("message", "User sudah terdaftar.")
                    } else if(roleMobile==null){
                        map.put("message", "Nama status salah.")
                    } else if(units.size()==0){
                        map.put("message", "Nama unit salah.")
                    } else {
                        def isError = false
                        def unit = units[0]
                        if((unit.ownerResident != null || unit.tenant != null) && roleMobile.name!=AppConstants.ROLE_SUB_USER){
                            map.put("message","Status Pemilik/Penyewa unit sudah terisi.")
                        } else {
                            UserMobile.withTransaction { statusTrx ->
                                try {

                                    def response = FirebaseUtil.createUser(map.get("email"))
                                    log.debug "creating user firebase time : ${System.currentTimeMillis() - actionStartTime} ms, status : ${response.message}"
                                    if (response.isSuccess) {
                                        def obj = new UserMobile()
                                        obj.name = map.get("name")
                                        obj.email = map.get("email")
                                        obj.phoneNo = map.get("phoneNo")
                                        obj.createdBy = currentUser
                                        obj.uid = response.uid
                                        obj.save(flush: true)

                                        def ump = new UserMobileProperty()
                                        ump.userMobile = obj
                                        ump.property = prop
                                        ump.save(flush: true)

                                        def umu = new UserMobileUnit()
                                        umu.roleMobile = roleMobile
                                        umu.unit = unit
                                        umu.userMobile = obj
                                        umu.save(flush: true)

                                        def tm = new TmpMail()
                                        tm.emailTo = obj.email
                                        tm.emailFrom = "support@warga-kita.com"
                                        tm.subject = "Pendaftaran User Baru Warga Kita"
                                        tm.content = mailMessageContentRenderer.render(new StringWriter(),"userWelcome", [userName: obj.name, email: obj.email, password: response.password], Locale.ENGLISH, "").out.toString()
                                        tm.status = AppConstants.PROCESS_NEW
                                        tm.save(flash: true)

                                        map.put("message", "User berhasil ditambahkan.")

                                        totalSuccess++;
                                    } else {
                                        map.put("message", response.message)
                                    }

                                } catch (Exception e) {
                                    statusTrx.setRollbackOnly()
                                    log.error(ExceptionUtils.getStackTrace(e))
                                    map.put("message", "Sistem error - Data tidak diproses")
                                }
                            }
                        }
                    }
                }
                log.debug "processing time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms, status : ${map.get("message")}"
            }
            file.delete()
            flash.message="File berhasil diproses, ${totalSuccess} user ditambahkan dari ${totalRecord} data."
            render view:'resultUpload', model:[userList:userList]
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").toString().concat("userMobile${File.separator}"))
                    uploadDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def downloadTemplate(){
        log.debug "IN downloadTemplate... $params"
        byte[] imageInByte = assetResourceLocator.findAssetForURI('user-mobile-template.xls').getInputStream()?.bytes
        response.setHeader('Content-length', imageInByte.length.toString())
        response.setHeader("Content-disposition", "attachment; filename=\"user-mobile-template.xls\"")
        response.contentType = "application/vnd.ms-excel" // or the appropriate image content type
        response.outputStream << imageInByte
        response.outputStream.flush()
    }

    def unitByAreaJSON(){
        log.debug "IN unitByAreaJSON... $params"
        def output = [:]
        def area = PropertyArea.get(params.areaId)
        def ump = null
        if(ControllerUtil.hasValue(params.umpId)) {
            ump = UserMobileProperty.get(params.umpId)
        }
        if(area!=null){
            output.result = true
            def data = []
            PropertyUnit.findAllByArea(area).each{
                if(ump!=null){
                    def detail = [id: it.id, name: "${it.name} - ${it.unitType.name}", hasOwner: it.ownerResident != null, hasTenant: it.tenant != null]
                    def umu = UserMobileUnit.findByUserMobileAndUnit(ump.userMobile, it)
                    if(umu!=null){
                        if(umu.roleMobile.name == AppConstants.ROLE_OWNER_RESIDENT){
                            detail.hasOwner = false
                        } else if(umu.roleMobile.name == AppConstants.ROLE_TENANT){
                            detail.hasTenant = false
                        }
                    }
                    data.add(detail)
                } else {
                    data.add([id: it.id, subArea: it.subArea, name: "${it.name} - ${it.unitType.name}", hasOwner: it.ownerResident != null, hasTenant: it.tenant != null])
                }
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Area tidak ditemukan"
        }
        render output as JSON
    }

    def areaByPropertyJSON(){
        log.debug "IN areaByPropertyJSON... $params"
        def output = [:]
        def propIds = []
        if(params."propIds[]" instanceof String){
            propIds.add(params."propIds[]")
        } else {
            propIds = params."propIds[]"
        }
        if(propIds.size()>0){
            output.result = true
            def data = []
            PropertyArea.executeQuery("FROM PropertyArea WHERE property.id IN :propIds", [propIds:propIds]).each{
                data.add([id:it.id, name:"${it.name}"])
            }
            output.data = data
        }
        render output as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }
}
