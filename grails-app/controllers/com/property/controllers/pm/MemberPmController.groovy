package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.maintenance.Gender
import com.model.maintenance.RoleMobile
import com.model.property.PropertyArea
import com.model.property.Member
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import com.property.utils.ExcelImporter
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import pl.touk.excel.export.WebXlsxExporter

import java.text.SimpleDateFormat



class MemberPmController implements ResourceLoaderAware {

    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        [propName: prop.name, areaId: params.areaId, unitId: params.unitId, areaList:areaList]
    }

    def listDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def columns = ['name', 'unit.area.name', 'unit.name', 'status', 'gender.name']
        def baseQuery = "FROM Member WHERE unit.id like :unitId AND unit.area.id like :areaId " +
                "AND unit.area.property.propertyManager.id = :pmInfoId"

        def areaId = params.areaId? params.areaId:'%'
        def unitId = params.unitId? params.unitId:'%'

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [areaId:areaId, pmInfoId:pmInfo.id, unitId:unitId]
        def result = ControllerUtil.parseDatatablesQuery(Member.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            def det = [id:obj.id, name:obj.name, unitId: obj.unit.id, unitName: obj.unit.name, gender: obj.gender?.name,
                       areaId: obj.unit.area.id, areaName: obj.unit.area.name, status: obj.status]
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailMember(){
        log.debug "IN detailMember... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def obj = Member.get(params.id)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(obj==null || obj.unit.area.property.propertyManager.id!=pmInfo.id || !propertyList.contains(obj.unit.area.property.id)){
            flash.error = "Penghuni tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def result = [id:params.id, name:obj.name, gender: obj.gender?.name, placeOfBirth: obj.placeOfBirth, address: obj.address, profession: obj.profession,
                      idKTP: obj.idKTP, email: obj.email, status: obj.status, description: obj.description, education: obj.education,
                      hobby: obj.hobby, propName: obj.unit.area.property.name,
                      areaName: obj.unit.area.name, unitName: obj.unit.name, imageFileId: obj.imageFileId, imageFileName: obj.imageFileName]
        if(obj.dateOfBirth!=null){
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            result.dateOfBirth = sdf.format(obj.dateOfBirth)
        }
        [member:result]
    }

    def createMember(){
        log.debug "IN createMember... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def genderList = Gender.list()
        def statusList = RoleMobile.findAllByRoleType(AppConstants.ROLE_TYPE_UNIT).collect{ it.name }
        if(params._action_createMember == 'Back'){
            [member: [propName: prop.name, gender:params.gender, areaId:params.areaId, unitId:params.unitId, name:params.name, placeOfBirth: params.placeOfBirth,
                    address: params.address, profession: params.profession, idKTP: params.idKTP, email: params.email, status: params.status, description: params.description, 
                    education: params.education, hobby: params.hobby, dateOfBirth: params.dateOfBirth,
                    imageFileId: params.imageFileId, imageFileName: params.imageFileName],
                areaList: areaList, statusMemberList: statusList, genderList:genderList]
        } else {
            [member: [propName: prop.name], areaList: areaList, statusMemberList: statusList, genderList:genderList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... " + params

        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def name = params.name
        def dateOfBirth = params.dateOfBirth
        def placeOfBirth = params.placeOfBirth
        def address = params.address
        def profession = params.profession
        def idKTP = params.idKTP
        def email = params.email
        def statusMember = params.status
        def description = params.description
        def education = params.education
        def hobby = params.hobby
        def genderId = params.genderId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        if(!ControllerUtil.hasValue(unitId) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(statusMember)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createMember = 'Back'
            redirect(action: 'createMember', params:params)
            return
        }

        def area = PropertyArea.get(areaId)
        def gender = ControllerUtil.hasValue(genderId)?Gender.findByCode(genderId):null
        def unit = PropertyUnit.get(unitId)
        def result = [propName: propName, genderId: genderId, genderName: gender?.name, areaId:areaId, areaName: area.name,
                      unitId:unitId, unitName:unit.name, name: name, dateOfBirth: dateOfBirth, placeOfBirth: placeOfBirth, address: address, idKTP: idKTP, email: email,
                      status: statusMember, description: description, profession: profession, education: education, hobby: hobby,
                      imageFileId: imageFileId, imageFileName: imageFileName]

        result.action = 'Create Member'
        result.stage = 'Confirm'
        result.actionType = 'add'
        render view:'confirm', model: [member:result]
    }

    def submitAdd(){
        log.debug "IN submitAdd... " + params

        def currentUser = springSecurityService.principal.id
        def propName = params.propName
        def areaId = params.areaId
        def unitId = params.unitId
        def name = params.name
        def dateOfBirth = params.dateOfBirth
        def placeOfBirth = params.placeOfBirth
        def address = params.address
        def profession = params.profession
        def idKTP = params.idKTP
        def email = params.email
        def statusMember = params.status
        def description = params.description
        def education = params.education
        def hobby = params.hobby
        def genderId = params.genderId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        def area = PropertyArea.get(areaId)
        def gender = ControllerUtil.hasValue(genderId)?Gender.findByCode(genderId):null
        def unit = PropertyUnit.get(unitId)
        def result = [genderId: genderId, genderName: gender?.name, propName: propName, areaId:areaId, areaName: area.name,
                      unitId:unitId, unitName:unit.name, name: name, dateOfBirth: dateOfBirth, placeOfBirth: placeOfBirth, address: address, idKTP: idKTP, email: email,
                      status: statusMember, description: description, profession: profession, education: education, hobby: hobby,
                      imageFileId: imageFileId, imageFileName: imageFileName, actionType: 'add', action:'Create Member']
        def isError = false
        def message = ""

        Member.withTransaction { status ->
            try {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def obj = new Member()
                obj.name = name
                obj.unit = unit
                if (ControllerUtil.hasValue(dateOfBirth)) {
                    obj.dateOfBirth = sdf.parse(dateOfBirth)
                }
                obj.placeOfBirth = placeOfBirth
                obj.profession = profession
                obj.idKTP = idKTP
                obj.email = email
                obj.address = address
                obj.status = statusMember
                obj.education = education
                obj.hobby = hobby
                obj.description = description
                obj.gender = gender
                obj.createdBy = currentUser

                if(ControllerUtil.hasValue(imageFileId)) {
                    //copy file from tmp
                    def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}tmp${File.separator}").concat(imageFileId)
                    def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(imageFileId)
                    def src = new File(srcFilePath)
                    def dst = new File(dstFilePath)
                    dst << src.bytes
                    if (src.exists()) src.delete()
                }
                obj.imageFileId = imageFileId
                obj.imageFileName = imageFileName

                obj.save(flush: true)
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "Server bermasalah, coba lagi beberapa saat."
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data penghuni berhasil ditambahkan"
        }
        render view: 'confirm', model: [member:result]
    }

    def deleteMember(){
        log.debug "IN deleteMember... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = Member.get(params.id)
        if(obj!=null && obj.unit.area.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.unit.area.property.id)){
            Member.withTransaction { status ->
                try {
                    if(ControllerUtil.hasValue(obj.imageFileId)) {
                        def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(obj.imageFileId))
                        if (image.exists()) image.delete()
                    }
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Member tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailMember', params:params)
        } else {
            flash.message = "Data penghuni berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def editMember(){
        log.debug "IN editMember... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def genderList = Gender.list()
        def statusList = RoleMobile.findAllByRoleType(AppConstants.ROLE_TYPE_UNIT).collect{ it.name }
        def propIdList = propertyList.collect { it.id }
        def obj = Member.get(params.id)
        if (obj != null && obj.unit.area.property.propertyManager.id == pmInfo.id && propIdList.contains(obj.unit.area.property.id)) {
            if(params._action_editMember == 'Back'){
                [member: [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                          genderId: params.genderId, genderName: params.genderName, name:params.name,address: params.address, profession: params.profession,
                          idKTP: params.idKTP, email: params.email, status: params.status, description: params.description, education: params.education,
                          hobby: params.hobby, dateOfBirth: params.dateOfBirth, placeOfBirth: params.placeOfBirth,
                          imageFileId: params.imageFileId, imageFileName: params.imageFileName],
                 propertyList: propertyList, statusMemberList:statusList, genderList:genderList]
            } else {
                def result = [id: obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                              genderId: obj.gender?.id, genderName: obj.gender?.name, name:obj.name,
                              address: obj.address, profession: obj.profession, idKTP: obj.idKTP, email: obj.email, status: obj.status, description: obj.description,
                              education: obj.education, hobby: obj.hobby, placeOfBirth: obj.placeOfBirth,
                              imageFileId: obj.imageFileId, imageFileName: obj.imageFileName, actionType: 'edit', action: 'Edit Area', stage: 'Confirm']
                if(obj.dateOfBirth!=null){
                    def sdf = new SimpleDateFormat("yyyy-MM-dd")
                    result.dateOfBirth = sdf.format(obj.dateOfBirth)
                }
                [propertyList:propertyList, statusMemberList:statusList, member: result, genderList:genderList]
            }
        } else {
            flash.error = "Penghuni tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def name = params.name
        def dateOfBirth = params.dateOfBirth
        def placeOfBirth = params.placeOfBirth
        def address = params.address
        def profession = params.profession
        def idKTP = params.idKTP
        def email = params.email
        def statusMember = params.status
        def description = params.description
        def education = params.education
        def hobby = params.hobby
        def genderId = params.genderId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(statusMember)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editMember = 'Back'
            redirect(action: 'editMember', params:params)
            return
        }

        def obj = Member.get(params.id)
        def gender = ControllerUtil.hasValue(genderId)?Gender.findByCode(genderId):null
        if(obj!=null && obj.unit.area.property.propertyManager.id == pmInfo.id){
            def result = [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                          genderId: genderId, genderName: gender?.name, name: name, dateOfBirth: dateOfBirth, placeOfBirth: placeOfBirth, address: address,
                          idKTP: idKTP, email: email, status: statusMember, description: description, profession: profession, education: education, hobby: hobby,
                          imageFileId: imageFileId, imageFileName: imageFileName, action:'Edit Member', stage:'Confirm']
            render view:'confirm', model: [member: result]
        } else {
            flash.error = "Penghuni tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def name = params.name
        def dateOfBirth = params.dateOfBirth
        def placeOfBirth = params.placeOfBirth
        def address = params.address
        def profession = params.profession
        def idKTP = params.idKTP
        def email = params.email
        def statusMember = params.status
        def description = params.description
        def education = params.education
        def hobby = params.hobby
        def genderId = params.genderId
        def imageFileId = params.imageFileId
        def imageFileName = params.imageFileName

        def obj = Member.get(params.id)
        def gender = ControllerUtil.hasValue(genderId)?Gender.findByCode(genderId):null
        if(obj!=null && obj.unit.area.property.propertyManager.id == pmInfo.id){
            def result = [id:obj.id, propName: obj.unit.area.property.name, areaName:obj.unit.area.name, unitName:obj.unit.name,
                          genderId: genderId, genderName: gender?.name, name: name, dateOfBirth: dateOfBirth, placeOfBirth: placeOfBirth, address: address,
                          idKTP: idKTP, email: email, status: statusMember,description: description, profession: profession, education: education, hobby: hobby,
                          imageFileId: imageFileId, imageFileName: imageFileName, actionType: 'edit', action:'Edit Member']
            def isError = false
            def message = ""

            Member.withTransaction { status ->
                try {
                    def sdf = new SimpleDateFormat("yyyy-MM-dd")
                    def oldFile = obj.imageFileId
                    obj.name = name
                    if (ControllerUtil.hasValue(dateOfBirth)) {
                        obj.dateOfBirth = sdf.parse(dateOfBirth)
                    }
                    obj.placeOfBirth = placeOfBirth
                    obj.profession = profession
                    obj.idKTP = idKTP
                    obj.email = email
                    obj.address = address
                    obj.status = statusMember
                    obj.education = education
                    obj.hobby = hobby
                    obj.description = description
                    obj.gender = gender
                    obj.updatedBy = currentUser

                    if(oldFile!=imageFileId && ControllerUtil.hasValue(imageFileId)) {
                        //copy file from tmp
                        def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}tmp${File.separator}").concat(imageFileId)
                        def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(imageFileId)
                        def src = new File(srcFilePath)
                        def dst = new File(dstFilePath)
                        dst << src.bytes
                        if (src.exists()) src.delete()
                        //delete old file
                        if(oldFile!=null) {
                            def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(oldFile)
                            def old = new File(oldFilePath)
                            if (old.exists()) old.delete()
                        }
                    }
                    obj.imageFileId = imageFileId
                    obj.imageFileName = imageFileName

                    obj.save(flush: true)
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }

            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "Data penghuni berhasil diperbaharui"
            }
            render view:'confirm', model: [member: result]
        } else {
            flash.error = "Penghuni tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def uploadFile(){
        log.debug "IN uploadFile... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        [areaList: areaList, propName:prop.name]
    }

    def confirmUpload(){
        log.debug "IN confirmUpload... $params"
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            try {
                def importer = new ExcelImporter(filePath)
                def memberList = importer.getMemberList()
                def cnt = 1
                memberList.each{ map ->
                    map.put("cnt", cnt)
                    cnt++
                }
                def area = PropertyArea.get(params.areaId)
                def prop = area.property
                def unit = PropertyUnit.get(params.unitId)
                [fileImportId: params.fileImportId, memberList:memberList, propName:prop.name,
                 areaId:params.areaId, areaName:area.name, unitId:params.unitId, unitName:unit.name]
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                flash.error = "Server bermasalah, coba lagi beberapa saat."
                redirect(action:'uploadFile')
            }
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def submitImport(){
        log.debug "IN submitImport... $params"
        def currentUser = springSecurityService.principal.id
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            def importer = new ExcelImporter(filePath)
            def memberList = importer.getMemberList()
            def cnt = 1
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def validStatus = RoleMobile.findAllByRoleType(AppConstants.ROLE_TYPE_UNIT).collect{ it.name }
            memberList.each{ map ->
                map.put("cnt", cnt)
                cnt++

                if(!ControllerUtil.hasValue(map.get("name")) || !ControllerUtil.hasValue(map.get("status"))){
                    map.put("message", "Missing mandatory fields")
                } else {
                    if(!validStatus.contains(map.get("status"))){
                        map.put("message", "Invalid status")
                    } else {
                        def gender = null
                        if(ControllerUtil.hasValue(map.get("gender"))){
                            gender = Gender.findByCode(map.get("gender"))
                            if(gender==null) {
                                map.put("message", "Invalid gender")
                            } else {
                                try {
                                    def obj = new Member()
                                    obj.name = map.get("name")
                                    obj.unit = PropertyUnit.get(params.unitId)
                                    if (ControllerUtil.hasValue(map.get("dateOfBirth"))) {
                                        obj.dateOfBirth = sdf.parse(map.get("dateOfBirth"))
                                    }
                                    obj.placeOfBirth = map.get("placeOfBirth")
                                    obj.profession = map.get("profession")
                                    obj.idKTP = map.get("idKTP")
                                    obj.email = map.get("email")
                                    obj.address = map.get("address")
                                    obj.status = map.get("status")
                                    obj.education = map.get("education")
                                    obj.hobby = map.get("hobby")
                                    obj.description = map.get("description")
                                    obj.gender = gender
                                    obj.createdBy = currentUser
                                    obj.save(flush: true)
                                    map.put("message", "Added")
                                } catch (Exception e) {
                                    log.error(ExceptionUtils.getStackTrace(e))
                                    map.put("message", "System error - Skipped")
                                }
                            }
                        }
                    }
                }
            }
            file.delete()
            flash.message="File has been imported"
            render view:'resultUpload', model:[memberList:memberList]
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").toString())
                    uploadDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def uploadImage(){
        log.debug "IN uploadImage... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def downloadTemplate(){
        log.debug "IN downloadTemplate... $params"
        byte[] imageInByte = assetResourceLocator.findAssetForURI('member-template.xls').getInputStream()?.bytes
        response.setHeader('Content-length', imageInByte.length.toString())
        response.setHeader("Content-disposition", "attachment; filename=\"member-template.xls\"")
        response.contentType = "application/vnd.ms-excel" // or the appropriate image content type
        response.outputStream << imageInByte
        response.outputStream.flush()
    }

    def downloadReport(){
        log.debug "IN downloadReport... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def areaId = params.areaId? params.areaId:'%'
        def unitId = params.unitId? params.unitId:'%'

        def prop = PropertyAdminProperty.findByPropertyAdmin(currentPm)
        def baseQuery = "FROM Member WHERE unit.id like :unitId AND unit.area.id like :areaId AND unit.area.property.id = :propId " +
                "AND unit.area.property.propertyManager.id = :pmInfoId "
        def queryParams = [propId:prop?.property?.id, areaId:areaId, unitId:unitId, pmInfoId:pmInfo.id]
        def list = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdf2 = new SimpleDateFormat("yyyyMMddHHmmss")
        Member.executeQuery(baseQuery, queryParams).each { obj ->
            def det = [id:obj.id, unitName:obj.unit?.name, name:obj.name, placeOfBirth:obj.placeOfBirth, profession:obj.profession,
                       idKTP:obj.idKTP, email:obj.email, address:obj.address, status:obj.status, education:obj.education,
                       hobby:obj.hobby, gender:obj.gender?.name]
            if(obj.dateOfBirth!=null) {
                det.dateOfBirth = sdf.format(obj.dateOfBirth)
            }
            list.add(det)
        }

        def headers = ['Unit', 'Nama', 'Tanggal Lahir', 'Tempat Lahir', 'Pekerjaan', 'Email', 'Jenis Kelamin']
        def withProperties = ['unitName', 'name', 'dateOfBirth', 'placeOfBirth', 'profession', 'email', 'gender']

        new WebXlsxExporter().with {
            setResponseHeaders(response, "laporan_penghuni_${sdf2.format(new Date())}.xlsx".toString())
            fillHeader(headers)
            add(list, withProperties)
            save(response.outputStream)
        }
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def areaByPropertyJSON(){
        log.debug "IN areaByPropertyJSON... $params"
        def output = [:]
        def prop = Property.get(params.propId)
        if(prop!=null){
            output.result = true
            def data = []
            PropertyArea.findAllByProperty(prop).each{
                data.add([id:it.id, name:it.name])
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Data properti tidak ditemukan"
        }
        render output as JSON
    }

    def unitByAreaJSON(){
        log.debug "IN unitByAreaJSON... $params"
        def output = [:]
        def area = PropertyArea.get(params.areaId)
        if(area!=null){
            output.result = true
            def data = []
            PropertyUnit.findAllByArea(area).each{
                data.add([id:it.id, name:it.name])
            }
            output.data = data
        } else {
            output.result = false
            output.message = "Area tidak ditemukan"
        }
        render output as JSON
    }
}
