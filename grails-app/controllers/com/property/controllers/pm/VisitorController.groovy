package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.maintenance.UnitType
import com.model.property.Member
import com.model.property.VisitorLog
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import pl.touk.excel.export.WebXlsxExporter
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat



class VisitorController {

    def springSecurityService

    def index() {
        log.debug "IN index... $params"
        [startDate:params.startDate, endDate:params.endDate, name:params.name]
    }

    def listDatatable(){
        def pmId = springSecurityService.principal?.pmId
        def propId = PropertyAdminProperty.findByPropertyAdmin(User.get(pmId)).property.id
        def columns = ['visitDate', 'name', 'phoneNo', 'vehicleNo', 'status']
        def baseQuery = "FROM VisitorLog WHERE propertyId = :propId AND (visitDate BETWEEN :startDate AND :endDate) "

        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        def name = ControllerUtil.appendWc(params.name)
        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date()).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [propId: propId, startDate:startDate, endDate:endDate]
        if(endDate!=null){
            queryParams.endDate = endDate
        }
        def result = ControllerUtil.parseDatatablesQuery(VisitorLog.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.id, name:obj.name, phoneNo:obj.phoneNo, visitDate:obj.visitDate?sdf.format(obj.visitDate):"",
                vehicleNo:obj.vehicleNo, status: AppConstants.getStatusVisitor(obj.status)])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def pmId = springSecurityService.principal?.pmId
        def propId = PropertyAdminProperty.findByPropertyAdmin(User.get(pmId)).property.id
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        def visitorLog = VisitorLog.executeQuery("FROM VisitorLog WHERE id = :id AND propertyId = :propId", [id:params.id, propId:propId])
        if(visitorLog.size() > 0) {
            def obj = visitorLog.get(0)
            def result = [id:obj.id, name:obj.name, phoneNo:obj.phoneNo, visitDate:obj.visitDate?sdf.format(obj.visitDate):"",
                          vehicleNo:obj.vehicleNo, status: AppConstants.getStatusVisitor(obj.status)]
            [visitor: result]
        } else {
            flash.error = "Data tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def showImage(){
        log.debug "IN showImage... $params"
        def visitor = VisitorLog.get(params.id)
        if(visitor!=null && ControllerUtil.hasValue(visitor.image)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("visitor${File.separator}images${File.separator}").concat(visitor.image)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def downloadReport(){
        log.debug "IN downloadReport... $params"
        def pmId = springSecurityService.principal?.pmId
        def propId = PropertyAdminProperty.findByPropertyAdmin(User.get(pmId)).property.id

        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        def sdf2 = new SimpleDateFormat("yyyyMMdd")
        def baseQuery = "FROM VisitorLog WHERE propertyId = :propId AND (visitDate BETWEEN :startDate AND :endDate) order by visitDate, name "

        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date()).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }


        def queryParams = [propId: propId, startDate:startDate, endDate:endDate]
        def list = []
        VisitorLog.executeQuery(baseQuery, queryParams).each { obj ->
            def det = [name:obj.name, phoneNo:obj.phoneNo, visitDate:obj.visitDate?sdf.format(obj.visitDate):"",
                       vehicleNo:obj.vehicleNo, status: AppConstants.getStatusVisitor(obj.status)]

            list.add(det)
        }

        def headers = ['Tanggal Kunjung', 'Name', 'No Telepon', 'Plat Nomor', 'Status']
        def withProperties = ['visitDate', 'name', 'phoneNo', 'vehicleNo', 'status']

        new WebXlsxExporter().with {
            setResponseHeaders(response, "laporan_pengunjung_${sdf2.format(startDate)}_${sdf2.format(endDate)}.xlsx".toString())
            fillHeader(headers)
            add(list, withProperties)
            save(response.outputStream)
        }
    }
}
