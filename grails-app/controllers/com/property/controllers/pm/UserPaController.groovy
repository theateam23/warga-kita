package com.property.controllers.pm

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.property.Property
import com.model.security.*
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils



class UserPaController {

    LinkGenerator grailsLinkGenerator
    def springSecurityService
    def mailService

    def index() {
        log.debug "IN index... $params"
    }

    def resetPassword(){
        log.debug "IN resetPassword... $params"
        def user = User.get(params.id)
        def userName = UserInfo.findByUser(user)?.name?:user?.username
        //generate token
        def token = UUID.randomUUID().toString().replaceAll("-","")
        ResetPasswordToken.withTransaction { status ->
            try{
                def obj = ResetPasswordToken.findByEmail(user.username)
                if(obj!=null){
                    obj.delete()
                }
                obj = new ResetPasswordToken()
                obj.email = user.username
                obj.token = token
                obj.createdTime = new Date().time
                obj.expiredTime = 3600
                obj.save()

                def tokenLink = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'auth', action: 'changePassword', params: [id: token]))

                log.debug("sending email emailPassword")
                mailService.sendMail {
                    to user.username
                    from "support@warga-kita.com"
                    subject "Warga Kita Reset Password"
                    html view: "emailPassword", model: [userName: userName?:"", tokenLink: tokenLink]
                }
                flash.message = "Email telah dikirim"
            } catch (Exception e) {
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                flash.error = "Server bermasalah, coba lagi beberapa saat."
            }
        }
        redirect(action: 'detailUser', params:params)
    }

    def paListDatatable(){
        def pmId = springSecurityService.principal.pmId
        def columns = ['i.user.username', 'i.name', 'i.phoneNo']
        def baseQuery = "FROM UserRole ur, UserInfo i WHERE ur.role.authority = :role AND i.user.username = ur.user.username AND i.pmId = :pmId"
        def query = "SELECT i " + baseQuery
        def queryCount = "SELECT count(i) " + baseQuery
        def queryParams = [role:AppConstants.ROLE_PA, pmId:pmId]
        def result = ControllerUtil.parseDatatablesQuery(UserInfo.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.user.id, username: obj.user.username, name: obj.name, phoneNo: obj.phoneNo])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailUser(){
        log.debug "IN detailUser... $params"
        def pmId = springSecurityService.principal.pmId
        def user = User.get(params.id)
        def userInfo = UserInfo.findByUserAndPmId(user, pmId)
        if(userInfo==null){
            flash.error = "User tidak ditemukan"
            redirect(action:'index')
            return
        }
        def userPropertyList = PropertyAdminProperty.findAllByPropertyAdmin(user).collect{it.property.name}
        def result = [id:params.id, username:userInfo.user.username, name:userInfo.name, phoneNo: userInfo.phoneNo]
        [user:result, userPropertyList:userPropertyList]
    }

    def deletePa(){
        log.debug "IN deletePa... $params"
        def isError = false, message = ''
        def user = User.get(params.id)
        if(user!=null){
            User.withTransaction { status ->
                try {
                    def userInfo = UserInfo.findByUser(user)
                    def pap = PropertyAdminProperty.findByPropertyAdmin(user)

                    UserRole.findAllByUser(user).each{
                        it.delete()
                    }
                    pap.delete()
                    userInfo.delete()
                    user.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "User tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailUser', params:params)
        } else {
            flash.message = "User berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def createPa(){
        log.debug "IN createPa... $params"
        def pmId = springSecurityService.principal.pmId
        def propertyList = Property.executeQuery("FROM Property WHERE propertyManager.user.id = :pmId", [pmId:pmId])
        if(propertyList.size()==0){
            flash.error = "Properti tidak ditemukan"
            redirect(action: 'index')
        } else {
            def prop = propertyList.get(0)
            if (params._action_createPa == 'Back') {
                [user: [propName: prop.name, username: params.username, name: params.name, phoneNo : params.phoneNo]]
            } else {
                [user: [propName: prop.name]]
            }
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName

        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createPa = 'Back'
            redirect(action: 'createPa', params:params)
            return
        }

        def result = [username:username, name:name, phoneNo:phoneNo, propName:propName]
        def user = User.findByUsername(username)
        if(user==null){
            result.action = 'Create Property Admin'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render view:'confirm', model: [user:result]
        } else {
            flash.error = "User dengan nama ${username}, sudah ada"
            params._action_createPa = 'Back'
            redirect(action: 'createPa', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def currentUser = springSecurityService.principal.id
        def pmId = springSecurityService.principal.pmId
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def propName = params.propName

        def result = [id:params.id, username:username, name:name, phoneNo: phoneNo, propName:propName, actionType: 'add', action:'Create Property Admin']
        def isError = false
        def message = ""

        def user = User.findByUsername(username)
        def userPropertyList = []
        if(user==null){
            User.withTransaction { status ->
                try {
                    String randomString = org.apache.commons.lang.RandomStringUtils.random(9, true, true)

                    user = new User()
                    user.username = username
                    user.password = randomString
                    user.save()

                    def role = Role.findByAuthority(AppConstants.ROLE_PA)
                    new UserRole(user: user, role: role).save(flush: true)

                    def propertyList = Property.executeQuery("FROM Property WHERE propertyManager.user.id = :pmId", [pmId:pmId])
                    def prop = propertyList.get(0)
                    def paProp = new PropertyAdminProperty()
                    paProp.property = prop
                    paProp.propertyAdmin = user
                    paProp.save()

                    def userInfo = new UserInfo()
                    userInfo.user = user
                    userInfo.name = name
                    userInfo.phoneNo = phoneNo
                    userInfo.pmId = pmId
                    userInfo.createdBy = currentUser
                    userInfo.save()

                    log.debug("sending email emailNewUser")
                    mailService.sendMail {
                        to user.username
                        from "support@warga-kita.com"
                        subject "Warga Kita User Password"
                        html view: "emailNewUser", model: [userName: name, password: randomString]
                    }
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "User with username ${username} already exists"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "User berhasil ditambahkan"
        }
        render view: 'confirm', model: [user:result]
    }

    def editPa(){
        log.debug "IN editPa... $params"
        def pmId = springSecurityService.principal.pmId
        def propertyList = Property.executeQuery("FROM Property WHERE propertyManager.user.id = :pmId", [pmId:pmId])

        if(params._action_editPa == 'Back'){
            def propIds = []
            if(params.propIds!=null){
                if(params.propIds instanceof String){
                    propIds.add(params.propIds)
                } else {
                    propIds = params.propIds
                }
            }
            [propertyList:propertyList, user: [id:params.id, username:params.username, name:params.name,
                                               phoneNo:params.phoneNo, propIds:propIds as JSON]]
        } else {
            def user = User.get(params.id)
            if (user != null) {
                def userInfo = UserInfo.findByUserAndPmId(user,pmId)
                if(userInfo==null){
                    flash.error = "User tidak ditemukan"
                    redirect(action: 'index')
                } else {
                    def propIds = PropertyAdminProperty.findAllByPropertyAdmin(user).collect{it.property.id}
                    def result = [id: params.id, username: userInfo.user.username, name: userInfo.name, propIds:propIds as JSON,
                                  phoneNo: userInfo.phoneNo, actionType: 'edit', action: 'Edit Property Admin', stage: 'Confirm']
                    [propertyList:propertyList, user: result]
                }
            } else {
                flash.error = "User tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def pmId = springSecurityService.principal.pmId
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def propIds = []
        if(params.propIds!=null){
            if(params.propIds instanceof String){
                propIds.add(params.propIds)
            } else {
                propIds = params.propIds
            }
        }

//        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name) || propIds.size()==0){
        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editPa = 'Back'
            redirect(action: 'createPa', params:params)
            return
        }

        def user = User.get(params.id)
        if(user!=null){
            def userInfo = UserInfo.findByUserAndPmId(user,pmId)
            def userPropertyList = []
            propIds.each{ propId ->
                userPropertyList.add(Property.get(propId).name)
            }
            if(userInfo==null){
                flash.error = "User tidak ditemukan"
                redirect(action: 'index')
            } else {
                def result = [id: params.id, username: user.username, name: name, phoneNo: phoneNo, propIds:propIds,
                              actionType: 'edit', action: 'Edit Property Admin', stage: 'Confirm']
                render view: 'confirm', model: [user: result, userPropertyList:userPropertyList]
            }
        } else {
            flash.error = "User tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id
        def pmId = springSecurityService.principal.pmId
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def propIds = []
        if(params.propIds!=null){
            if(params.propIds instanceof String){
                propIds.add(params.propIds)
            } else {
                propIds = params.propIds
            }
        }

        def isError = false, message = ''
        def user = User.get(params.id)
        def userPropertyList = []
        if(user!=null){
            def userInfo = UserInfo.findByUserAndPmId(user,pmId)
            if(userInfo==null){
                flash.error = "User tidak ditemukan"
                redirect(action: 'index')
                return
            }
            User.withTransaction { status ->
                try {
                    //delete existing relation
                    PropertyAdminProperty.findAllByPropertyAdmin(user).each{
                        it.delete(flush:true)
                    }
                    //create
                    /*propIds.each { propId ->
                        def paProp = new PropertyAdminProperty()
                        paProp.property = Property.get(propId)
                        paProp.propertyAdmin = user
                        paProp.save()
                        userPropertyList.add(paProp.property.name)
                    }*/
                    def propertyList = Property.executeQuery("FROM Property WHERE propertyManager.user.id = :pmId", [pmId:pmId])
                    def prop = propertyList.get(0)
                    def paProp = new PropertyAdminProperty()
                    paProp.property = prop
                    paProp.propertyAdmin = user
                    paProp.save()
                    userPropertyList.add(paProp.property.name)

                    userInfo.name = name
                    userInfo.phoneNo = phoneNo
                    userInfo.updatedBy = currentUser
                    userInfo.save()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
            def result = [id:user.id, username:user.username, name:name, phoneNo: phoneNo, propIds:propIds,
                          actionType: 'edit', action:'Edit Property Admin']
            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "User berhasil diperbaharui"
            }
            render view: 'confirm', model: [user: result, userPropertyList:userPropertyList]
        } else {
            flash.error = "User tidak ditemukan"
            redirect(action: 'index')
        }
    }
}
