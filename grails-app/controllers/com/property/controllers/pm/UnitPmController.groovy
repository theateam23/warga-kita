package com.property.controllers.pm

import com.model.general.PropertyAdminProperty
import com.model.maintenance.UnitType
import com.model.property.PropertyArea
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import com.property.utils.ExcelImporter
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest



class UnitPmController implements ResourceLoaderAware {

    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def unitTypeList = UnitType.findAllByPmId(springSecurityService.principal.pmId)
        [propName: prop.name, areaId: params.areaId, unitTypeId: params.unitTypeId, areaList:areaList, unitTypeList:unitTypeList]
    }

    def unitListDatatable(){
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)

        def columns = ['name', 'subArea', 'unitType.name', 'area.name', 'area.property.name', '', '']
        def baseQuery = "FROM PropertyUnit WHERE area.id like :areaId " +
                "AND unitType.id like :unitTypeId " +
                "AND area.property.propertyManager.id = :pmInfoId"

        def areaId = params.areaId? params.areaId:'%'
        def unitTypeId = params.unitTypeId? params.unitTypeId:'%'

        def query = baseQuery
        def queryCount = "SELECT count(id) " + baseQuery
        def queryParams = [areaId:areaId, pmInfoId:pmInfo.id, unitTypeId:unitTypeId]
        def result = ControllerUtil.parseDatatablesQuery(PropertyUnit.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            def det = [id:obj.id, name:obj.name, subArea:obj.subArea, typeId: obj.unitType.id, typeName:obj.unitType.name,
                       areaId: obj.area.id, areaName: obj.area.name, propId: obj.area.property.id,
                       propName:obj.area.property.name, resident: obj.ownerResident?.name, tenant: obj.tenant?.name]
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailUnit(){
        log.debug "IN detailUnit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def obj = PropertyUnit.get(params.id)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }
        if(obj==null || obj.area.property.propertyManager.id!=pmInfo.id || !propertyList.contains(obj.area.property.id)){
            flash.error = "Unit tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def result = [id:params.id, name:obj.name, subArea: obj.subArea, propName: obj.area.property.name,
                        areaName: obj.area.name, unitType: obj.unitType.toString()]
        [unit:result]
    }

    def createUnit(){
        log.debug "IN createUnit... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def unitTypeList = UnitType.findAllByPmId(springSecurityService.principal.pmId)
        if(params._action_createUnit == 'Back'){
            [unit: [propName: prop.name, areaId:params.areaId, name:params.name, subArea:params.subArea,
                    unitTypeId:params.unitTypeId], areaList: areaList, unitTypeList: unitTypeList]
        } else {
            [unit: [propName: prop.name], areaList:areaList, unitTypeList: unitTypeList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... " + params
        def propName = params.propName
        def areaId = params.areaId
        def name = params.name
        def subArea = params.subArea
        def unitTypeId = params.unitTypeId

        if(!ControllerUtil.hasValue(areaId) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(unitTypeId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createUnit = 'Back'
            redirect(action: 'createUnit', params:params)
            return
        }


        def area = PropertyArea.get(areaId)
        def prop = area.property

        //validate total unit
        def sum = PropertyUnit.executeQuery("SELECT count(u) FROM PropertyUnit u WHERE area.property.id = :propId", [propId: prop.id]).first()?:0
        if(sum >= prop.totalUnit){
            flash.error = "Tidak bisa menambah unit lagi, silahkan hubungi admin untuk menambah kuota."
            params._action_createUnit = 'Back'
            redirect(action: 'editUnit', params:params)
        } else {
            def unitType = UnitType.get(unitTypeId)
            def result = [propName: propName, areaId: areaId, areaName: area.name,
                          name: name, subArea: subArea, unitTypeId: unitTypeId, unitTypeName: unitType.toString()]

            result.action = 'Create Unit'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render view: 'confirm', model: [unit: result]
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... " + params
        def currentUser = springSecurityService.principal.id
        def propName = params.propName
        def areaId = params.areaId
        def name = params.name
        def subArea = params.subArea
        def unitTypeId = params.unitTypeId

        def area = PropertyArea.get(areaId)
        def unitType = UnitType.get(unitTypeId)
        def result = [propName: propName, areaId:areaId, areaName: area.name,
                      name:name, subArea:subArea, unitTypeId:unitTypeId, unitTypeName: unitType.toString(),
                      actionType: 'add', action:'Create Unit']
        def isError = false
        def message = ""

        Property.withTransaction { status ->
            try {
                def unit = new PropertyUnit()
                unit.area = area
                unit.name = name
                unit.subArea = subArea
                unit.unitType = unitType
                unit.createdBy = currentUser
                unit.save(flush: true)
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "Server bermasalah, coba lagi beberapa saat."
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data unit berhasil ditambahkan"
        }
        render view: 'confirm', model: [unit:result]
    }

    def deleteUnit(){
        log.debug "IN deleteUnit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property.id }

        def isError = false, message = ''
        def obj = PropertyUnit.get(params.id)
        if(obj!=null && obj.area.property.propertyManager.id==pmInfo.id && propertyList.contains(obj.area.property.id)){
            PropertyUnit.withTransaction { status ->
                try {
                    obj.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Unit is still used"
                }
            }
        } else {
            message = "Unit tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailUnit', params:params)
        } else {
            flash.message = "Data unit berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def editUnit(){
        log.debug "IN editUnit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        def unitTypeList = UnitType.findAllByPmId(springSecurityService.principal.pmId)
        if(params._action_editUnit == 'Back'){
            [unit: [id:params.id, name:params.name, subArea: params.subArea, propName: prop.name,
                    areaId:params.areaId, unitTypeId: params.unitTypeId],
                    areaList: areaList, unitTypeList: unitTypeList]
        } else {
            def obj = PropertyUnit.get(params.id)
            if (obj != null && obj.area.property.id == prop.id) {
                def result = [id:params.id, name: obj.name, subArea: obj.subArea, areaId: obj.area.id,
                              propName: obj.area.property.name, unitTypeId: obj.unitType.id,
                              actionType: 'edit', action: 'Edit Area', stage: 'Confirm']
                [areaList:areaList, unitTypeList: unitTypeList, unit: result]
            } else {
                flash.error = "Unit tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propName = params.propName
        def areaId = params.areaId
        def name = params.name
        def subArea = params.subArea
        def unitTypeId = params.unitTypeId

        if(!ControllerUtil.hasValue(areaId) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(unitTypeId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editUnit = 'Back'
            redirect(action: 'editUnit', params:params)
            return
        }

        def obj = PropertyUnit.get(params.id)
        if(obj!=null && obj.area.property.propertyManager.id == pmInfo.id){
            def area = PropertyArea.get(areaId)
            //validate total unit
            def sum = PropertyUnit.findAllByAreaAndIdNotEqual(area, obj.id).size()
            if(sum >= area.totalUnit){
                flash.error = "Tidak bisa menambah unit lagi, silahkan hubungi admin untuk menambah kuota."
                params._action_editUnit = 'Back'
                redirect(action: 'editUnit', params:params)
            } else {
                def unitType = UnitType.get(unitTypeId)
                def result = [id      : params.id, name: name, subArea: subArea, areaId: areaId, areaName: area.name,
                              propName: propName, unitTypeId: unitTypeId, unitTypeName: unitType.toString(),
                              action  : 'Edit Unit', stage: 'Confirm']
                render view: 'confirm', model: [unit: result]
            }
        } else {
            flash.error = "Unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def currentUser = springSecurityService.principal.id
        def currentPm = User.get(springSecurityService.principal.pmId)
        def pmInfo = UserInfo.findByUser(currentPm)
        def propName = params.propName
        def areaId = params.areaId
        def name = params.name
        def subArea = params.subArea
        def unitTypeId = params.unitTypeId

        def obj = PropertyUnit.get(params.id)
        if(obj!=null && obj.area.property.propertyManager.id == pmInfo.id){
            def area = PropertyArea.get(areaId)
            def unitType = UnitType.get(unitTypeId)

            obj.name = name
            obj.subArea = subArea
            obj.area = area
            obj.unitType = unitType
            obj.updatedBy = currentUser
            obj.save(flush:true)
            def result = [id:params.id, name:name, subArea: subArea, areaId: areaId, areaName: area.name,
                          propName:propName, unitTypeId:unitTypeId, unitTypeName:unitType.toString(),
                          actionType: 'edit', action:'Edit Unit', stage:'Result']
            flash.message = "Data unit berhasil diperbaharui"
            render view:'confirm', model: [unit: result]
        } else {
            flash.error = "Unit tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def uploadFile(){
        log.debug "IN uploadFile... $params"
        def propertyList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        def prop = propertyList.get(0)
        def areaList = PropertyArea.findAllByProperty(prop)
        [propName:prop.name, areaList:areaList]
    }

    def confirmUpload(){
        log.debug "IN confirmUpload... $params"
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("unit${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            try {
                def importer = new ExcelImporter(filePath)
                def unitList = importer.getUnitList()
                def cnt = 1

                def area = PropertyArea.get(params.areaId)
                def prop = area.property

                //validate total unit
                def sum = PropertyUnit.executeQuery("SELECT count(u) FROM PropertyUnit u WHERE area.property.id = :propId", [propId: prop.id]).first()?:0
                if(sum + unitList.size() >= prop.totalUnit){
                    flash.error = "Tidak bisa menambah unit lagi, silahkan hubungi admin untuk menambah kuota."
                    redirect(action:'uploadFile')
                } else {
                    unitList.each{ map ->
                        if(map.get("subArea") instanceof Double){
                            map.put("subArea", String.format("%.0f",map.get("subArea")))
                        }
                        map.put("cnt", cnt)
                        cnt++
                    }

                    [fileImportId: params.fileImportId, unitList:unitList, propName:params.propName,
                     areaId:params.areaId, areaName:area.name]
                }

            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                flash.error = "Server bermasalah, coba lagi beberapa saat."
                redirect(action:'uploadFile')
            }
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def submitImport(){
        log.debug "IN submitImport... $params"
        def currentUser = springSecurityService.principal.id
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("unit${File.separator}").concat(params.fileImportId)
        def file = new File(filePath)
        if(file.exists()){
            def importer = new ExcelImporter(filePath)
            def unitList = importer.getUnitList()
            def cnt = 1

            def area = PropertyArea.get(params.areaId)

            unitList.each{ map ->
                if(map.get("subArea") instanceof Double){
                    map.put("subArea", String.format("%.0f",map.get("subArea")))
                }
                map.put("cnt", cnt)
                cnt++

                //validate unit name and unit type
                def unitName = map.get("name")
                def subArea = map.get("subArea")
                def unitType = map.get("unitType")
                if(!ControllerUtil.hasValue(unitName) || !ControllerUtil.hasValue(unitType) || !ControllerUtil.hasValue(subArea)){
                    map.put("message", "Ada kolom yang kosong")
                } else if(PropertyUnit.findByAreaAndNameIlike(area, unitName)!=null){
                    map.put("message", "Nama unit sudah terpakai")
                } else if(UnitType.findByCode(unitType)==null){
                    map.put("message", "Tipe unit tidak ditemukan")
                } else {
                    PropertyUnit.withTransaction { status ->
                        try {
                            def unit = new PropertyUnit()
                            unit.area = area
                            unit.name = map.get("name")
                            unit.subArea = map.get("subArea")
                            unit.unitType = UnitType.findByCode(map.get("unitType"))
                            unit.createdBy = currentUser
                            unit.save()
                            map.put("message", "Unit Ditambahkan")
                        } catch (Exception e) {
                            map.put("message", "Sistem error - Data tidak diproses")
                        }
                    }
                }
            }
            file.delete()
            flash.message="File berhasil diproses."
            render view:'resultUpload', model:[unitList:unitList]
        } else {
            flash.error = "File tidak ditemukan"
            redirect(action:'uploadFile')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").toString().concat("unit${File.separator}"))
                    uploadDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def downloadTemplate(){
        log.debug "IN downloadTemplate... $params"
        byte[] imageInByte = assetResourceLocator.findAssetForURI('unit-template.xls').getInputStream()?.bytes
        response.setHeader('Content-length', imageInByte.length.toString())
        response.setHeader("Content-disposition", "attachment; filename=\"unit-template.xls\"")
        response.contentType = "application/vnd.ms-excel" // or the appropriate image content type
        response.outputStream << imageInByte
        response.outputStream.flush()
    }
}
