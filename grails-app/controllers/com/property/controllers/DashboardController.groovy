package com.property.controllers

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.general.SecurityProperty
import com.model.general.UserMobileUnit
import com.model.property.Member
import com.model.property.PropertyArea
import com.model.property.Facility
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.security.User
import com.model.security.UserMobile
import com.model.security.UserRole
import com.bertramlabs.plugins.SSLRequired

@SSLRequired
class DashboardController {

    def springSecurityService
    def salesService

    def index() {
        log.debug "dashboard..."
        def roles = session.getAttribute(AppConstants.SESSION_ROLE)
        if(roles==null || roles.size()==0){
            roles = springSecurityService.principal.getAuthorities().collect{it.authority.toString()}
            session.setAttribute(AppConstants.SESSION_ROLE, roles)
        }
        def bulletList = []

        if(roles.contains(AppConstants.ROLE_ADMIN)){
            //total property
            def detail = [:]
            detail.count = Property.executeQuery("SELECT count(p) FROM Property p").first()
            detail.description = "Jumlah Property"
            detail.controller = "propertyAdmin"
            detail.action = "index"
            detail.linkText = "Detail"
            detail.color = "primary"
            detail.icon = "fa fa-location-arrow fa-5x"
            bulletList.add(detail)

            //total area
            detail = [:]
            detail.count = PropertyArea.executeQuery("SELECT count(b) FROM PropertyArea b").first()
            detail.description = "Jumlah Area"
            detail.color = "green"
            detail.icon = "fa fa-building-o fa-5x"
            bulletList.add(detail)

            //total unit
            detail = [:]
            def sumUnit = PropertyUnit.executeQuery("SELECT count(u) FROM PropertyUnit u").first()
            detail.count = "${sumUnit}"
            detail.description = "Jumlah Unit"
            detail.color = "teal"
            detail.icon = "fa fa-home fa-5x"
            bulletList.add(detail)

            //total user PM
            detail = [:]
            detail.count = UserRole.executeQuery("SELECT count(ur) FROM UserRole ur WHERE role.authority = :auth", [auth:AppConstants.ROLE_PM]).first()
            detail.description = "Jumlah Manager"
            detail.controller = "userAdmin"
            detail.action = "index"
            detail.linkText = "Detail"
            detail.color = "yellow"
            detail.icon = "fa fa-user-plus fa-5x"
            bulletList.add(detail)

            detail = [:]
            detail.count = UserRole.executeQuery("SELECT count(ur) FROM UserRole ur WHERE role.authority = :auth", [auth:AppConstants.ROLE_PA]).first()
            detail.description = "Jumlah Property Admin"
            detail.color = "green"
            detail.icon = "fa fa-users fa-5x"
            bulletList.add(detail)

            //total user mobile
            detail = [:]
            detail.count = UserMobile.executeQuery("SELECT count(um) FROM UserMobile um").first()
            detail.description = "Jumlah User Mobile"
            detail.color = "teal"
            detail.icon = "fa fa-mobile fa-5x"
            bulletList.add(detail)
            
        }

        if(roles.contains(AppConstants.ROLE_PA) || roles.contains(AppConstants.ROLE_PM)){
            //total property
            def currentUser = User.get(springSecurityService.principal.id)
            def prop = PropertyAdminProperty.findAllByPropertyAdmin(currentUser).get(0).property
            def propId = prop.id
            def detail = [:]

            //total tenant
            detail = [:]
            detail.count = UserMobileUnit.executeQuery("SELECT count(umu) FROM UserMobileUnit umu WHERE umu.unit.area.property.id = :propId",
                    [propId: propId]).first()
            detail.description = "Jumlah User Mobile"
            detail.controller = "userMobile"
            detail.action = "index"
            detail.linkText = "Lihat User Mobile"
            detail.color = "yellow"
            detail.icon = "fa fa-mobile-phone fa-5x"
            bulletList.add(detail)

            //total penghuni
            detail = [:]
            detail.count = Member.executeQuery("SELECT count(m) FROM Member m WHERE m.unit.area.property.id = :propId",
                    [propId: propId]).first()
            detail.description = "Jumlah Penghuni"
            detail.controller = "memberPm"
            detail.action = "index"
            detail.linkText = "Lihat Daftar Penghuni"
            detail.color = "teal"
            detail.icon = "fa fa-user fa-5x"
            bulletList.add(detail)

            //total security
            detail = [:]
            detail.count = SecurityProperty.executeQuery("SELECT count(rjp) FROM SecurityProperty rjp WHERE rjp.property.id = :propId", [propId: propId]).first()
            detail.description = "Jumlah Keamanan"
            detail.controller = "securityUser"
            detail.action = "index"
            detail.linkText = "Lihat User Keamanan"
            detail.color = "teal"
            detail.icon = "fa fa-shield fa-5x"
            bulletList.add(detail)

            //total area
            detail = [:]
            detail.count = PropertyArea.executeQuery("SELECT count(b) FROM PropertyArea b WHERE b.property.id = :propId", [propId: propId]).first()
            detail.description = "Jumlah Area"
            detail.controller = "areaPm"
            detail.action = "index"
            detail.linkText = "Lihat Daftar Area"
            detail.color = "green"
            detail.icon = "fa fa-building-o fa-5x"
            bulletList.add(detail)

            //total unit
            detail = [:]
            def totalUnit = PropertyUnit.executeQuery("SELECT count(u) FROM PropertyUnit u WHERE u.area.property.id = :propId", [propId: propId]).first()
//                def resident = UserMobileUnit.executeQuery("SELECT count(distinct umu.unit) FROM UserMobileUnit umu WHERE umu.roleMobile.name=:role AND umu.unit.area.property.id = :propId",
//                        [role: AppConstants.ROLE_OWNER_RESIDENT, propId: propId]).first()
            detail.count = totalUnit
            detail.description = "Jumlah Unit Terdaftar"
            detail.controller = "unitPm"
            detail.action = "index"
            detail.linkText = "Lihat Daftar Unit"
            detail.color = "teal"
            detail.icon = "fa fa-home fa-5x"
            bulletList.add(detail)

            //total facility
            if(!AppConstants.PERUMAHAN.equals(prop.propertyType.name)){
                detail = [:]
                detail.count = Facility.executeQuery("SELECT count(f) FROM Facility f WHERE f.area.property.id = :propId",
                        [propId: propId]).first()
                detail.description = "Jumlah Fasilitas"
                detail.controller = "facility"
                detail.action = "index"
                detail.linkText = "Lihat Daftar Fasilitas"
                detail.color = "green"
                detail.icon = "ion-ios-basketball fa-4x"
                bulletList.add(detail)
            }


        }

        if(roles.contains(AppConstants.ROLE_SALES)){
            def currentUser = springSecurityService.principal.id
            def detail = [:]
            // todo: add total property active from current sales that convert to Silver, Gold, Platinum
            def salesRanking = salesService.getSalesRank(currentUser)
            detail.count = salesRanking
            detail.description = "Sales Level"
            detail.color = "primary"
            detail.icon = "fa fa-rocket fa-5x"
            detail.fontSize = "22px"
            bulletList.add(detail)

            // todo: add total property active from current sales
            detail = [:]
            detail.count = Property.executeQuery("SELECT count(p) FROM Property p WHERE p.expiryDate IS NOT NULL AND p.expiryDate > :currentDate AND sales.user.id = :currentUser",
                    [currentDate:new Date(), currentUser:currentUser]).first()
            detail.description = "Jumlah Property Aktif"
            detail.color = "teal"
            detail.icon = "fa fa-building fa-5x"
            detail.fontSize = "22px"
            bulletList.add(detail)

            // todo: add total total komisi bulan ini
            detail = [:]
            def fee = Property.executeQuery("SELECT sum(p.salesCommission) FROM PropertyPayment p WHERE p.expiryDate IS NOT NULL AND p.expiryDate > :currentDate AND salesId = :currentUser",
                    [currentDate:new Date(), currentUser:currentUser])?.first()?:0.0
            detail.count = String.format("%.2f",fee)
            detail.description = "Komisi Bulan Ini"
            detail.color = "green"
            detail.icon = "fa fa-money fa-5x"
            detail.fontSize = "22px"
            bulletList.add(detail)
        }

        [bulletList:bulletList]
    }
}
