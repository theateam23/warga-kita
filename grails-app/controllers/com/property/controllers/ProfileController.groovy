package com.property.controllers

import com.model.AppConstants
import com.model.general.FacilityBooking
import com.model.general.PropertyAdminProperty
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest


import com.bertramlabs.plugins.SSLRequired

@SSLRequired
class ProfileController implements ResourceLoaderAware {
    ResourceLoader resourceLoader
    def assetResourceLocator
    def springSecurityService
    def passwordEncoder

    def index() {
        log.debug "profile.index..."
        def userId = springSecurityService.principal.id
        def user = User.get(userId)
        def userInfo = UserInfo.findByUser(user)
        [profile:[email:user.username, name:userInfo.name, imageFileId:userInfo.imageFileId]]
    }

    def edit(){
        log.debug "IN edit... $params"
        def userId = springSecurityService.principal.id
        def user = User.get(userId)
        def userInfo = UserInfo.findByUser(user)
        if(params._action_edit == 'Back'){
            [profile: [email: user.username, name: params.name, imageFileId: params.imageFileId, imageFileName: params.imageFileName]]
        } else {
            [profile: [email: user.username, name: userInfo.name, imageFileId: userInfo.imageFileId, imageFileName:userInfo.imageFileName]]
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def name = params.name

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_edit = 'Back'
            redirect(action: 'edit', params:params)
            return
        }

        def userId = springSecurityService.principal.id
        def user = User.get(userId)
        def userInfo = UserInfo.findByUser(user)
        def oldFile = userInfo.imageFileId

        userInfo.name = name
        userInfo.imageFileId = params.imageFileId
        userInfo.imageFileName = params.imageFileName
        userInfo.save(flush:true)

        if(oldFile!=params.imageFileId && ControllerUtil.hasValue(params.imageFileId)) {
            //copy file from tmp
            def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}tmp${File.separator}").concat(params.imageFileId)
            def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}images${File.separator}").concat(params.imageFileId)
            def src = new File(srcFilePath)
            def dst = new File(dstFilePath)
            dst << src.bytes
            if (src.exists()) src.delete()
        }

        flash.message="Profile berhasil diperbaharui"
        redirect(action: 'index')
    }

    def profilePic() {
        def userId = springSecurityService.principal.id
        def user = User.get(userId)
        def userInfo = UserInfo.findByUser(user)
        def file = null
        if(ControllerUtil.hasValue(userInfo.imageFileId)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}images${File.separator}").concat(userInfo.imageFileId)
            file = new File(filePath)
        }

        if(file!=null && file.exists()){
            InputStream is = new BufferedInputStream(new FileInputStream(file))
            def mimeType = URLConnection.guessContentTypeFromStream(is)
            byte[] imageInByte = is.bytes
            response.setHeader('Content-length', imageInByte.length.toString())
            response.contentType = mimeType // or the appropriate image content type
            response.outputStream << imageInByte
            response.outputStream.flush()
        } else {
            byte[] imageInByte = assetResourceLocator.findAssetForURI('no-pic.png').getInputStream()?.bytes
            response.setHeader('Content-length', imageInByte.length.toString())
            response.contentType = "image/png" // or the appropriate image content type
            response.outputStream << imageInByte
            response.outputStream.flush()
        }
    }

    def getTaskCount(){
        log.debug "IN getTaskCount..."
        def result = [:]
        def propList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE propertyAdmin.id = :currentUser",
                [currentUser:springSecurityService.principal.id]).collect{ it.property }
        if(propList.size()==0){
            result.result = true
            result.totalCount = 0
        } else {
            def prop = propList.get(0)
            def pmId = springSecurityService.principal?.pmId
            def query = "SELECT count(tum.id) FROM TmpUserMobile tum, Property p WHERE p.id = tum.propertyId AND p.propertyManager.user.id = :pmId AND p.id = :propId"
            def queryParams = [pmId: pmId, propId: prop.id]
            result.userCount = PropertyAdminProperty.executeQuery(query, queryParams).first()

            def propType = prop.propertyType?.name
            if(AppConstants.PAGUYUBAN.equals(propType) || AppConstants.PERUMAHAN.equals(propType)){
                result.hasFacility = false
                result.totalCount = result.userCount
            } else {
                result.hasFacility = true
                query = "SELECT count(id) FROM FacilityBooking WHERE facility.area.property.id = :propId AND facility.area.property.propertyManager.user.id = :pmId AND status = :status"
                queryParams = [pmId: pmId, propId: prop.id, status:AppConstants.BOOKING_STATUS_NEW]
                result.bookingCount = FacilityBooking.executeQuery(query, queryParams).first()
                result.totalCount = result.userCount + result.bookingCount
            }
            result.result = true
        }
        render result as JSON
    }

    def changePassword(){
        log.debug "IN changePassword... $params"
        [:]
    }

    def updatePassword(){
        log.debug "IN updatePassword... $params"
        def user = User.get(springSecurityService.principal.id)
        def valid = passwordEncoder.isPasswordValid(user.password, params.oldPass, null)
        if(!valid){
            flash.error = "Password Salah"
            redirect(action: 'changePassword')
        } else {
            def newPass = params.newPass
            if(!ControllerUtil.hasValue(newPass)){
                flash.error = "Silakan isi semua data yang diperlukan"
                redirect(action: 'changePassword')
            } else {
                user.password = newPass
                user.save(flush:true)
                flash.message = "Password berhasil diperbaharui"
                redirect(action: 'changePassword')
            }
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        //log.debug result.result
        render result as JSON
    }

    def showPicture(){
        def filePath = grailsApplication.config.getProperty("file.upload.path").concat("profile${File.separator}images${File.separator}").concat(params.id)
        def file = new File(filePath)
        if(file.exists()){
            InputStream is = new BufferedInputStream(new FileInputStream(file))
            def mimeType = URLConnection.guessContentTypeFromStream(is)
            byte[] imageInByte = is.bytes
            response.setHeader('Content-length', imageInByte.length.toString())
            response.contentType = mimeType // or the appropriate image content type
            response.outputStream << imageInByte
            response.outputStream.flush()
        } else {
            response.outputStream.flush()
        }
    }
}
