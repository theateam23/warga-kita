package com.property.controllers



import com.bertramlabs.plugins.SSLRequired

@SSLRequired
class HomePageController {
    def mailService
    def index() {

    }

    def subscription(){

    }

    def reseller(){}

    def submitReseller(){
        println "in submitReseller....$params"

        log.debug("sending email emailSubscribeReseller")
        mailService.sendMail {
            to "support@warga-kita.com"
            from "support@warga-kita.com"
            subject "Request Reseller"
            html view: "emailSubmitReseller", model: [name: params.name, email: params.email, phoneNo: params.phoneNo, address: params.address, reason: params.reason]
        }

        flash.message="Data sudah dikirim, selanjutnya admin kami akan segera menghubungi anda"
        render view:'reseller'
    }

    def submitSubscription(){
        println "in submitSubscription....$params"

        log.debug("sending email emailSubscribeReseller")
        mailService.sendMail {
            to "support@warga-kita.com"
            from "support@warga-kita.com"
            subject "Subscription"
            html view: "emailSubmitCustomer", model: [name: params.name, email: params.email, phoneNo: params.phoneNo, property: params.property, reason: params.reason]
        }

        flash.message="Data sudah dikirim, selanjutnya admin kami akan segera menghubungi anda"
        render view:'subscription'
    }
}
