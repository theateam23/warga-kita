package com.property.controllers

import com.model.property.Property
import com.model.property.PropertyPayment
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import pl.touk.excel.export.WebXlsxExporter

import java.text.SimpleDateFormat

//import com.bertramlabs.plugins.SSLRequired

//@SSLRequired
@Secured(['ROLE_ADMIN','ROLE_SALES'])
class PropertyPaymentController {

    def springSecurityService
    def salesService

    def index() {
        log.debug "index..."
        [propName:params.propName, salesName:params.salesName, startDate:params.startDate, endDate:params.endDate]
    }

    def listDatatable(){
        def columns = ['pp.createdDate', 'p.name', 'p.sales.name', 'pp.paymentAmount', 'pp.salesCommission', 'pp.expiryDate']
        def baseQuery = "FROM PropertyPayment pp, Property p WHERE lower(p.name) like :propName " +
                "AND lower(p.sales.name) like :salesName AND pp.createdDate BETWEEN :startDate AND :endDate "

        def propName = ControllerUtil.hasValue(params.propName)?"%${params.propName.toLowerCase()}%".toString():"%"
        def salesName = ControllerUtil.hasValue(params.salesName)?"%${params.salesName.toLowerCase()}%".toString():"%"
        def startDate = null, endDate = null
        try{
            if(ControllerUtil.hasValue(params.startDate)){
                startDate = new Date(Long.valueOf(params.startDate))
            } else {
                startDate = new Date().clearTime()
            }
            if(ControllerUtil.hasValue(params.endDate)){
                endDate = new Date(Long.valueOf(params.endDate))
            } else {
                endDate = (new Date()).clearTime()
            }
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }

        def query = "SELECT pp " + baseQuery
        def queryCount = "SELECT count(pp.id) " + baseQuery
        def queryParams = [propName: propName, salesName: salesName, startDate:startDate, endDate:endDate]
        def result = ControllerUtil.parseDatatablesQuery(PropertyPayment.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        def sdf2 = new SimpleDateFormat("yyyy-MM-dd")

        objs.each{ obj ->
            def det = [id:obj.id, propertyName:obj.propertyName, salesName:obj.salesName, createdDate: sdf.format(obj.createdDate),
                        paymentAmount: obj.paymentAmount, salesCommission: obj.salesCommission]
            if(obj.expiryDate!=null){
                det.expiryDate = sdf2.format(obj.expiryDate)
            } else {
                det.expiryDate = ""
            }
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def obj = PropertyPayment.get(params.id)
        if(obj!=null) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            def sdf2 = new SimpleDateFormat("yyyy-MM-dd")

            def det = [id:obj.id, propName:obj.propertyName, salesName:obj.salesName, createdDate: sdf.format(obj.createdDate), description: obj.description,
                       paymentAmount: obj.paymentAmount, salesCommission: obj.salesCommission, attachment: obj.attachment, attachmentName: obj.attachmentName]
            if(obj.expiryDate!=null){
                det.expiryDate = sdf2.format(obj.expiryDate)
            } else {
                det.expiryDate = ""
            }
            def user = User.get(obj.createdBy)
            if(user!=null) {
                det.createdOn = "${UserInfo.findByUser(user)?.name?:user.username} pada ${sdf.format(obj.createdDate)}"
            } else {
                det.createdOn = sdf.format(obj.createdDate)
            }
            def prop = Property.get(obj.propertyId)
            if(prop!=null){
                det.address = prop.address
                det.totalUnit = prop.totalUnit
            }

            [detail: det]
        } else {
            flash.error = "Data tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        def propList = Property.list()
        if(params._action_create == 'Back'){
            [detail:[propId:params.propId, description: params.description, paymentAmount: params.paymentAmount, expiryDate:params.expiryDate,
                     attachment: params.attachment, attachmentName: params.attachmentName], propList:propList]
        } else {
            [detail:[:],propList:propList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"

        def propId = params.propId
        def description = params.description
        def paymentAmount = params.paymentAmount
        def expiryDate = params.expiryDate
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(paymentAmount) || !ControllerUtil.hasValue(expiryDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def prop = Property.get(propId)
        def result = [propId:propId, propName:prop?.name, paymentAmount: paymentAmount, expiryDate: expiryDate, description: description,
                      attachment:attachment, attachmentName:attachmentName, stage:'Confirm', actionType: 'add', action: 'Buat Pembayaran']
        result.address = prop.address
        result.totalUnit = prop.totalUnit
        result.salesName = prop.sales?.name
        result.oldExpiryDate = ControllerUtil.convertToStrDate(prop.expiryDate, new SimpleDateFormat("yyyy-MM-dd"))
        def salesRanking = salesService.getSalesRank(prop.sales?.id)
        result.salesCommission = salesService.calculateCommission(new BigDecimal(paymentAmount),salesRanking)

        render(view:'confirm', model: [detail: result])
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"

        def propId = params.propId
        def description = params.description
        def paymentAmount = params.paymentAmount
        def expiryDate = params.expiryDate
        def attachment = params.attachment
        def attachmentName = params.attachmentName

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(paymentAmount) || !ControllerUtil.hasValue(expiryDate)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def prop = Property.get(propId)
        def result = [propId:propId, propName:prop?.name, paymentAmount: paymentAmount, expiryDate: expiryDate, description: description,
                      attachment:attachment, attachmentName:attachmentName, actionType: 'add', action: 'Buat Pembayaran']
        result.address = prop.address
        result.totalUnit = prop.totalUnit
        result.salesName = prop.sales?.name
        result.oldExpiryDate = ControllerUtil.convertToStrDate(prop.expiryDate, new SimpleDateFormat("yyyy-MM-dd"))
        def salesRanking = salesService.getSalesRank(prop.sales?.id)
        result.salesCommission = salesService.calculateCommission(new BigDecimal(paymentAmount),salesRanking)

        def isError = false
        def message = ""

        PropertyPayment.withTransaction { status ->
            try {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def obj = new PropertyPayment()
                obj.propertyId = propId
                obj.propertyName = prop?.name
                obj.salesId = prop.sales?.user?.id
                obj.salesName = prop.sales?.name
                obj.description = description
                obj.paymentAmount = new BigDecimal(paymentAmount)
                obj.expiryDate = sdf.parse(expiryDate)
                obj.createdBy = springSecurityService.principal.id
                obj.salesCommission = result.salesCommission

                if(ControllerUtil.hasValue(attachment)) {
                    //copy file from tmp
                    def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(attachment)
                    def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}payment${File.separator}").concat(attachment)
                    def src = new File(srcFilePath)
                    def dst = new File(dstFilePath)
                    dst << src.bytes
                    if (src.exists()) src.delete()
                }
                obj.attachment = attachment
                obj.attachmentName = attachmentName
                obj.save(flush: true)

                prop.expiryDate = obj.expiryDate
                prop.updatedBy = obj.createdBy
                prop.save(flush: true)
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                isError = true
                message = "System error"
            }
        }

        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Pembayaran telah didaftarkan"
        }
        render view: 'confirm', model: [detail: result]
    }

    def downloadFile() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}payment${File.separator}").concat(params.id)
            def fileName = PropertyPayment.findByAttachment(params.id)?.attachmentName?:"attachment"
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] fileBytes = is.bytes
                response.setHeader('Content-length', fileBytes.length.toString())
                response.setHeader('Content-disposition', "attachment;filename=\"${fileName}\"")
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << fileBytes
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}payment${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def report(){
        log.debug "IN report... $params"
        [reportMonth:params.reportMonth]
    }

    def downloadReport(){
        log.debug "IN downloadReport... $params"

        def month = params.reportMonth

        if(!ControllerUtil.hasValue(month)){
            flash.error = "Silakan isi semua data yang diperlukan"
            redirect(action: report, params:params)
            return
        }

        def startDate = null, endDate = null
        try{
            def sdf = new SimpleDateFormat("MMM yyyy")
            def reportMonth = sdf.parse(month)
            def dlYear = reportMonth[Calendar.YEAR]
            def dlMonth = reportMonth[Calendar.MONTH]
            startDate = new GregorianCalendar(dlYear, dlMonth, 1, 0, 0, 0)
            def maxDay = startDate.getActualMaximum(Calendar.DAY_OF_MONTH)
            endDate =  new GregorianCalendar(dlYear, dlMonth, maxDay, 23, 59, 59)
        } catch (Exception e){
            log.debug "Error: ${e.getMessage()}"
        }
        println startDate.time
        println endDate.time
        def list = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def sdf2 = new SimpleDateFormat("yyyyMMddHHmmss")
        PropertyPayment.executeQuery("SELECT pp, p.sales FROM PropertyPayment pp, Property p WHERE pp.createdDate BETWEEN :startDate AND :endDate ORDER BY pp.createdDate ",
                [startDate:startDate.time, endDate:endDate.time]).each { objs ->
            PropertyPayment pp = objs[0]
            UserInfo sales = objs[1]
            def det = [propertyName:pp.propertyName, salesName:sales.name, paymentAmount: pp.paymentAmount, salesCommission: pp.salesCommission,
                       salesBankAccount:sales.bankAccount, salesBankName:sales.bankName]
            if(pp.createdDate!=null) {
                det.createdDate = sdf.format(pp.createdDate)
            }
            list.add(det)
        }


        def headers = ['Tanggal', 'Properti', 'Sales', 'Komisi', 'Bank Sales', 'No Rekening Sales']
        def withProperties = ['createdDate', 'propertyName', 'salesName', 'salesCommission', 'salesBankName', 'salesBankAccount']

        new WebXlsxExporter().with {
            setResponseHeaders(response, "laporan_pembayaran_${sdf2.format(new Date())}.xlsx".toString())
            fillHeader(headers)
            add(list, withProperties)
            save(response.outputStream)
        }
    }

    def detailPropertyJSON(){
        log.debug "IN detailPropertyJSON... $params"
        def output = [:]
        def prop = Property.get(params.propId)
        if(prop!=null){
            output.result = true
            output.address = prop.address
            output.totalUnit = prop.totalUnit
            output.salesName = prop.sales?.name
            output.expiryDate = ControllerUtil.convertToStrDate(prop.expiryDate, new SimpleDateFormat("yyyy-MM-dd"))
        } else {
            output.result = false
            output.message = "Properti tidak ditemukan"
        }
        render output as JSON
    }
}
