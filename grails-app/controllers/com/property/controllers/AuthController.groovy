package com.property.controllers

import com.model.security.ResetPasswordToken
import com.model.security.User
import com.model.security.UserInfo
import com.property.utils.ControllerUtil
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils


import com.bertramlabs.plugins.SSLRequired

@SSLRequired
class AuthController {

    def mailService
    LinkGenerator grailsLinkGenerator

    def index(){
        redirect(controller: 'dashboard')
    }

    def register(){

    }

    def forgotPassword(){

    }

    def resetPassword(){
        log.debug "IN resetPassword... $params"
        def email = params.email
        if(!ControllerUtil.hasValue(email)){
            flash.message = "Silakan masukan alamat email anda"
            redirect(action: 'forgotPassword')
            return
        }
        def user = User.findByUsername(email)
        if(user==null){
            flash.message = "Email belum terdaftar"
            redirect(action: 'forgotPassword')
            return
        }
        def userName = UserInfo.findByUser(user)?.name
        //generate token
        def token = UUID.randomUUID().toString().replaceAll("-","")
        ResetPasswordToken.withTransaction { status ->
            try{
                def obj = ResetPasswordToken.findByEmail(email)
                if(obj!=null){
                    obj.delete()
                }
                obj = new ResetPasswordToken()
                obj.email = email
                obj.token = token
                obj.createdTime = new Date().time
                obj.expiredTime = 3600
                obj.save()

                def tokenLink = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'auth', action: 'changePassword', params: [id: token]))
                println tokenLink

                mailService.sendMail {
                    to email
                    from "support@warga-kita.com"
                    subject "Warga Kita Reset Password"
                    html view: "emailPassword", model: [userName: userName?:email, tokenLink: tokenLink]
                }
                flash.message = "Email telah dikirim"
            } catch (Exception e) {
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                flash.message = "Server bermasalah, coba lagi beberapa saat."
            }
        }
        redirect(action: 'forgotPassword')
    }

    def changePassword(){
        log.debug "IN changePassword... $params"
        def token = params.id
        if(!ControllerUtil.hasValue(token)){
            flash.message = "Invalid Link"
            redirect(controller:'login', action: 'auth')
            return
        }
        def obj = ResetPasswordToken.findByToken(token)
        if(obj==null){
            flash.message = "Invalid Link"
            redirect(controller:'login', action: 'auth')
            return
        }
        [id:params.id]
    }

    def updatePassword(){
        log.debug "IN updatePassword... $params"
        def token = params.id
        def pass = params.password
        if(!ControllerUtil.hasValue(token) || !ControllerUtil.hasValue(pass)){
            flash.message = "Invalid Link"
            redirect(controller:'login', action: 'auth')
            return
        }
        def obj = ResetPasswordToken.findByToken(token)
        if(obj==null){
            flash.message = "Invalid Link"
            redirect(controller:'login', action: 'auth')
            return
        }
        def user = User.findByUsername(obj.email)
        if(user==null){
            flash.message = "Invalid Link"
            redirect(controller:'login', action: 'auth')
            return
        }
        User.withTransaction { status ->
            try {
                user.password = pass
                user.save()
                obj.delete()
                flash.message = "Password berhasil diperbaharui, silakan login dengan password yang baru"
            } catch (Exception e){
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                flash.message = "System Error"
            }
        }
        redirect(controller:'login', action: 'auth')
    }
}
