package com.property.controllers.admin

import com.model.AppConstants
import com.model.property.Property
import com.model.security.ResetPasswordToken
import com.model.security.Role
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserRole
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class UserAdminController {

    LinkGenerator grailsLinkGenerator
    def mailService

    def index() {
        log.debug "IN index... $params"
    }

    def resetPassword(){
        log.debug "IN resetPassword... $params"
        def user = User.get(params.id)
        def userName = UserInfo.findByUser(user)?.name?:user?.username
        //generate token
        def token = UUID.randomUUID().toString().replaceAll("-","")
        ResetPasswordToken.withTransaction { status ->
            try{
                def obj = ResetPasswordToken.findByEmail(user.username)
                if(obj!=null){
                    obj.delete()
                }
                obj = new ResetPasswordToken()
                obj.email = user.username
                obj.token = token
                obj.createdTime = new Date().time
                obj.expiredTime = 3600
                obj.save()

                def tokenLink = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'auth', action: 'changePassword', params: [id: token]))

                log.debug("sending email emailPassword")
                mailService.sendMail {
                    to user.username
                    from "support@warga-kita.com"
                    subject "Warga Kita Reset Password"
                    html view: "emailPassword", model: [userName: userName?:"", tokenLink: tokenLink]
                }
                flash.message = "Email telah dikirim"
            } catch (Exception e) {
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                flash.error = "Server bermasalah, coba lagi beberapa saat."
            }
        }
        redirect(action: 'detailUser', params:params)
    }

    def pmListDatatable(){
        def columns = ['i.user.username', 'i.name', 'i.phoneNo']
        def query = "SELECT i FROM UserRole ur, UserInfo i WHERE ur.role.authority = :role and i.user.username = ur.user.username"
        def queryCount = "SELECT count(i) FROM UserRole ur, UserInfo i WHERE ur.role.authority = :role and i.user.username = ur.user.username"
        def queryParams = [role:AppConstants.ROLE_PM]
        def result = ControllerUtil.parseDatatablesQuery(UserInfo.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.user.id, username: obj.user.username, name: obj.name, phoneNo: obj.phoneNo])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detailUser(){
        log.debug "IN detailUser... $params"
        def user = User.get(params.id)
        def userInfo = UserInfo.findByUser(user)
        def result = [id:params.id, username:userInfo.user.username, name:userInfo.name, phoneNo: userInfo.phoneNo]
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        def propertyList = Property.findAllByPropertyManager(userInfo)
        [user:result, propertyList:propertyList]
    }

    def deletePm(){
        log.debug "IN deletePM... $params"
        def isError = false, message = ''
        def user = User.get(params.id)
        if(user!=null){
            User.withTransaction { status ->
                try {
                    def userInfo = UserInfo.findByUser(user)
                    UserRole.findAllByUser(user).each{
                        it.delete()
                    }
                    Property.findAllByPropertyManager(userInfo).each{
                        it.delete()
                    }
                    userInfo.delete()
                    user.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "User tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailUser', params:params)
        } else {
            flash.message = "User berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def createPm(){
        log.debug "IN createPm... $params"
        if(params._action_createPm == 'Back'){
            [user: [username:params.username, name:params.name, phoneNo:params.phoneNo]]
        } else {
            [user:[:]]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo

        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createPm = 'Back'
            redirect(action: 'createPm', params:params)
            return
        }

        def result = [username:username, name:name, phoneNo:phoneNo]
        def user = User.findByUsername(username)
        if(user==null){
            result.action = 'Create Property Manager'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render view:'confirm', model: [user:result]
        } else {
            flash.error = "User dengan nama ${username} sudah ada"
            params._action_createPm = 'Back'
            redirect(action: 'createPm', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def result = [username:username, name:name, phoneNo: phoneNo,
                      actionType: 'add', action:'Create Property Manager']
        def isError = false
        def message = ""

        def user = User.findByUsername(username)
        if(user==null){
            User.withTransaction { status ->
                try {
                    String randomString = org.apache.commons.lang.RandomStringUtils.random(9, true, true)

                    user = new User()
                    user.username = username
                    user.password = randomString
                    user.save(flush: true)

                    def role = Role.findByAuthority(AppConstants.ROLE_PM)
                    new UserRole(user: user, role: role).save(flush: true)

                    def userInfo = new UserInfo()
                    userInfo.user = user
                    userInfo.name = name
                    userInfo.phoneNo = phoneNo
                    userInfo.pmId = user.id //PM id is the same as user id since admin only create PM
                    userInfo.save(flush: true)

                    log.debug("sending email emailNewUser")
                    mailService.sendMail {
                        to user.username
                        from "support@warga-kita.com"
                        subject "Warga Kita User Password"
                        html view: "emailNewUser", model: [userName: name, password: randomString]
                    }
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "User with username ${username} already exists"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "User berhasil ditambahkan"
        }
        render view: 'confirm', model: [user:result]
    }

    def editPm(){
        log.debug "IN editPm... $params"
        if(params._action_editPm == 'Back'){
            [user: [id:params.id, username:params.username, name:params.name, phoneNo:params.phoneNo]]
        } else {
            def user = User.get(params.id)
            if (user != null) {
                def userInfo = UserInfo.findByUser(user)
                def result = [id:params.id, username: userInfo.user.username, name: userInfo.name, phoneNo: userInfo.phoneNo,
                              actionType: 'edit', action: 'Edit Property Manager', stage: 'Confirm']
                [user: result]
            } else {
                flash.error = "User tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo

        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editPm = 'Back'
            redirect(action: 'editPm', params:params)
            return
        }

        def result = [id:params.id, username:username, name:name, phoneNo: phoneNo,
                      actionType: 'edit', action:'Edit Property Manager', stage:'Confirm']

        def user = User.get(params.id)
        if(user!=null){
            render view:'confirm', model: [user:result]
        } else {
            flash.error = "User tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo

        def user = User.get(params.id)
        if(user!=null){
            def result = [name:name, phoneNo: phoneNo,
                          actionType: 'edit', action:'Edit Property Manager', stage:'Result']

            def userInfo = UserInfo.findByUser(user)
            userInfo.name = name
            userInfo.phoneNo = phoneNo
            userInfo.save(flush:true)
            flash.message = "User berhasil diperbaharui"
            result.username = user.username

            render view:'confirm', model: [user:result]
        } else {
            flash.error = "User tidak ditemukan"
            redirect(action: 'index')
        }
    }
}
