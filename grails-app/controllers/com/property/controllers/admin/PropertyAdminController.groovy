package com.property.controllers.admin

import com.model.AppConstants
import com.model.general.*
import com.model.information.*
import com.model.maintenance.PropertyType
import com.model.property.*
import com.model.security.*
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.core.GrailsApplication
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest



class PropertyAdminController {

    def springSecurityService
    GrailsApplication grailsApplication

    def index() {
        log.debug "IN index... " + params
    }

    def propertyListDatatable(){
        def columns = ['name', 'developer', 'address', 'propertyManager.name']
        def query = "FROM Property"
        def queryCount = "SELECT count(id) FROM Property"
        def queryParams = [:]
        def result = ControllerUtil.parseDatatablesQuery(Property.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            aaData.add([id:obj.id, name: obj.name, developer: obj.developer, address: obj.address,
                        pmId: obj.propertyManager?.id?:"", pmName: obj.propertyManager?.name?:""])
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def createProperty(){
        log.debug "IN createProperty... " + params
        def propertyTypeList = PropertyType.list()
        if(params._action_createProperty == 'Back'){
            [property: [name:params.name, code:params.code, developer:params.developer, address:params.address,
                        pmId: params.pmId, pmText: params.pmText, propertyTypeId: params.propertyTypeId,
                        totalUnit: params.totalUnit, securityPhoneNo: params.securityPhoneNo, securityEmails: params.securityEmails,
                        mobileBackground: params.mobileBackground, mobileBackgroundName: params.mobileBackgroundName,
                        webLoginBackground: params.webLoginBackground, webLoginBackgroundName: params.webLoginBackgroundName,
                        logo: params.logo, logoName: params.logoName], propertyTypeList:propertyTypeList]
        } else {
            [property:[:], propertyTypeList:propertyTypeList]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... " + params
        def name = params.name
        def code = params.code
        def developer = params.developer
        def address = params.address
        def pmId = params.pmId
        def pmText = params.pmText
        def propertyTypeId = params.propertyTypeId
        def totalUnit = params.totalUnit
        def securityPhoneNo = params.securityPhoneNo
        def securityEmails = params.securityEmails
        def mobileBackground = params.mobileBackground
        def mobileBackgroundName = params.mobileBackgroundName
        def webLoginBackground = params.webLoginBackground
        def webLoginBackgroundName = params.webLoginBackgroundName
        def logo = params.logo
        def logoName = params.logoName

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(pmId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_createProperty = 'Back'
            redirect(action: 'editPm', params:params)
            return
        }

        def result = [name:name, code:code, developer:developer, address:address, pmId:pmId, pmText:pmText, propertyTypeId:propertyTypeId,totalUnit:totalUnit,
                      mobileBackground: mobileBackground, mobileBackgroundName: mobileBackgroundName,
                      webLoginBackground: webLoginBackground, webLoginBackgroundName: webLoginBackgroundName,
                      logo: logo, logoName: logoName, securityPhoneNo: securityPhoneNo, securityEmails: securityEmails]
        def pm = User.get(pmId)
        if(pm!=null){
            if(ControllerUtil.hasValue(propertyTypeId)){
                result.propertyTypeName = PropertyType.get(propertyTypeId)?.name
            }
            result.action = 'Create Property'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render view:'confirm', model: [property:result]
        } else {
            flash.error = "Properti Manager tidak ditemukan"
            params._action_createProperty = 'Back'
            redirect(action: 'createProperty', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... " + params
        def currentUser = springSecurityService.principal.id
        def name = params.name
        def code = params.code
        def developer = params.developer
        def address = params.address
        def pmId = params.pmId
        def pmText = params.pmText
        def propertyTypeId = params.propertyTypeId
        def totalUnit = params.totalUnit
        def securityPhoneNo = params.securityPhoneNo
        def securityEmails = params.securityEmails
        def mobileBackground = params.mobileBackground
        def mobileBackgroundName = params.mobileBackgroundName
        def webLoginBackground = params.webLoginBackground
        def webLoginBackgroundName = params.webLoginBackgroundName
        def logo = params.logo
        def logoName = params.logoName

        def result = [name:name, code:code, developer:developer, address:address, pmId:pmId, pmText:pmText, propertyTypeId:propertyTypeId,totalUnit:totalUnit,
                      mobileBackground: mobileBackground, mobileBackgroundName: mobileBackgroundName,
                      webLoginBackground: webLoginBackground, webLoginBackgroundName: webLoginBackgroundName,
                      logo: logo, logoName: logoName, securityPhoneNo: securityPhoneNo, securityEmails: securityEmails, actionType: 'add', action:'Create Property']
        def isError = false
        def message = ""

        def pm = User.get(pmId)
        if(pm!=null){
            Property.withTransaction { status ->
                try {
                    def property = new Property()
                    property.name = name
                    property.code = code
                    property.developer = developer
                    property.address = address
                    property.propertyManager = UserInfo.findByUser(pm)

                    if(ControllerUtil.hasValue(propertyTypeId)){
                        property.propertyType = PropertyType.get(propertyTypeId)
                        result.propertyTypeName = property.propertyType?.name
                    }
                    if(ControllerUtil.hasValue(totalUnit)){
                        property.totalUnit = Integer.valueOf(totalUnit)
                    }

                    property.createdBy = currentUser
                    property.securityPhoneNo = securityPhoneNo
                    property.securityEmails = securityEmails

                    def files = [mobileBackground, webLoginBackground, logo]
                    files.each { fileId ->
                        if(ControllerUtil.hasValue(fileId)) {
                            //copy file from tmp
                            def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(fileId)
                            def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}images${File.separator}").concat(fileId)
                            def src = new File(srcFilePath)
                            def dst = new File(dstFilePath)
                            dst << src.bytes
                            if (src.exists()) src.delete()
                        }
                    }

                    property.mobileBackground = mobileBackground
                    property.mobileBackgroundName = mobileBackgroundName
                    property.webLoginBackground = webLoginBackground
                    property.webLoginBackgroundName = webLoginBackgroundName
                    property.logo = logo
                    property.logoName = logoName

                    property.save(flush: true)

                    def pap = new PropertyAdminProperty()
                    pap.property = property
                    pap.propertyAdmin = property.propertyManager.user
                    pap.save(flush:true)
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Data Properti Manager tidak ditemukan"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Data properti berhasil ditambahkan"
        }
        render view: 'confirm', model: [property:result]
    }

    def detailProperty(){
        log.debug "IN detailProperty... " + params
        def prop = Property.get(params.id)
        if(prop==null){
            flash.error = "Properti tidak ditemukan"
            redirect(action: 'index')
            return
        }
        def result = [id:params.id, name:prop.name, code:prop.code, developer: prop.developer, address: prop.address,
                      propertyTypeName: prop.propertyType?.name,
                      mobileBackground: prop.mobileBackground, mobileBackgroundName: prop.mobileBackgroundName,
                      webLoginBackground: prop.webLoginBackground, webLoginBackgroundName: prop.webLoginBackgroundName,
                      logo: prop.logo, logoName: prop.logoName, totalUnit: prop.totalUnit, securityEmails: prop.securityEmails, securityPhoneNo: prop.securityPhoneNo]
        if(prop.propertyManager!=null){
            result.pmText = "${prop.propertyManager.user.username} - ${prop.propertyManager.name}"
        }
        [property:result]
    }

    def deleteProperty(){
        log.debug "IN deleteProperty... " + params
        def isError = false, message = ''
        def prop = Property.get(params.id)
        if(prop!=null){
            Property.withTransaction { status ->
                try {
                    PropertyAdminProperty.findAllByProperty(prop).each { it.delete() }
                    RoleJobProperty.findAllByProperty(prop).each { it.delete() }
                    SecurityProperty.findAllByProperty(prop).each { it.delete() }
                    UserMobileProperty.findAllByProperty(prop).each { ump ->
                        UserFeedback.findAllByUserMobileProperty(ump).each { it.delete() }
                        ump.delete()
                    }

                    BroadcastMessageLog.findAllByProperty(prop).each { it.delete() }
                    PropertyAnnouncement.findAllByProperty(prop).each { pa ->
                        if(ControllerUtil.hasValue(pa.imageFileId)) {
                            def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("announcement${File.separator}images${File.separator}").concat(pa.imageFileId))
                            if (image.exists()) image.delete()
                        }
                        PropertyAnnouncementTarget.findAllByPropertyAnnouncement(pa).each { it.delete() }
                        pa.delete()
                    }
                    PropertyNews.findAllByProperty(prop).each { pn ->
                        if(ControllerUtil.hasValue(pn.imageFileId)) {
                            def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("news${File.separator}images${File.separator}").concat(pn.imageFileId))
                            if (image.exists()) image.delete()
                        }
                        PropertyNewsTarget.findAllByPropertyNews(pn).each { it.delete() }
                        pn.delete()
                    }
                    PropertyReminder.findAllByProperty(prop).each { pr ->
                        PropertyReminderNotif.findAllByReminder(pr).each { it.delete() }
                        pr.delete()
                    }

                    PropertyArea.findAllByProperty(prop).each { a ->
                        Facility.findAllByArea(a).each { f ->
                            if(ControllerUtil.hasValue(f.imageFileId)) {
                                def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("facility${File.separator}images${File.separator}").concat(f.imageFileId))
                                if (image.exists()) image.delete()
                            }
                            FacilityBooking.findAllByFacility(f).each { it.delete() }
                            f.delete()
                        }
                        PropertyUnit.findAllByArea(a).each { u ->
                            SubUserUnit.findAllByUnit(u).each { it.delete() }
                            UserMobileUnit.findAllByUnit(u).each { it.delete() }
                            HomeGuard.findAllByUnit(u).each { it.delete() }
                            Member.findAllByUnit(u).each {
                                if(ControllerUtil.hasValue(it.imageFileId)) {
                                    def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(it.imageFileId))
                                    if (image.exists()) image.delete()
                                }
                                it.delete()
                            }
                            Vehicle.findAllByUnit(u).each {
                                if(ControllerUtil.hasValue(it.imageFileId)) {
                                    def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(it.imageFileId))
                                    if (image.exists()) image.delete()
                                }
                                it.delete()
                            }
                            u.delete()
                        }
                        a.delete()
                    }
                    PropertyRequest.findAllByProperty(prop).each {
                        if(ControllerUtil.hasValue(it.fileId)) {
                            def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}requestForm${File.separator}").concat(it.fileId))
                            if (image.exists()) image.delete()
                        }
                        it.delete()
                    }

                    prop.delete()
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "Server bermasalah, coba lagi beberapa saat."
                }
            }
        } else {
            message = "Data properti tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailProperty', params:params)
        } else {
            flash.message = "Data properti berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def editProperty(){
        log.debug "IN editProperty... " + params
        def propertyTypeList = PropertyType.list()
        if(params._action_editProperty == 'Back'){
            [property: [id:params.id, name:params.name, code:params.code, developer: params.developer,
                        address:params.address, pmId:params.pmId, pmText:params.pmText,
                        propertyTypeId: params.propertyTypeId,totalUnit: params.totalUnit, securityEmails: params.securityEmails,
                        mobileBackground: params.mobileBackground, mobileBackgroundName: params.mobileBackgroundName,
                        webLoginBackground: params.webLoginBackground, webLoginBackgroundName: params.webLoginBackgroundName,
                        logo: params.logo, logoName: params.logoName, securityPhoneNo: params.securityPhoneNo], propertyTypeList:propertyTypeList]
        } else {
            def prop = Property.get(params.id)
            if (prop != null) {
                def result = [id:params.id, name: prop.name, code:prop.code, developer: prop.developer, address: prop.address,
                              pmId: prop.propertyManager?.user?.id,
                              propertyTypeId: prop.propertyType?.id, totalUnit: prop.totalUnit, securityEmails: prop.securityEmails,
                              mobileBackground: prop.mobileBackground, mobileBackgroundName: prop.mobileBackgroundName,
                              webLoginBackground: prop.webLoginBackground, webLoginBackgroundName: prop.webLoginBackgroundName,
                              logo: prop.logo, logoName: prop.logoName,securityPhoneNo: prop.securityPhoneNo, actionType: 'edit', action: 'Edit Property', stage: 'Confirm']
                if(prop.propertyManager!=null){
                    result.pmText = "${prop.propertyManager.user.username} - ${prop.propertyManager.name}"
                }
                [property: result, propertyTypeList:propertyTypeList]
            } else {
                flash.error = "Properti tidak ditemukan"
                redirect(action: 'index')
            }
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... " + params
        def name = params.name
        def code = params.code
        def developer = params.developer
        def address = params.address
        def pmId = params.pmId
        def pmText = params.pmText
        def propertyTypeId = params.propertyTypeId
        def totalUnit = params.totalUnit
        def securityPhoneNo = params.securityPhoneNo
        def securityEmails = params.securityEmails
        def mobileBackground = params.mobileBackground
        def mobileBackgroundName = params.mobileBackgroundName
        def webLoginBackground = params.webLoginBackground
        def webLoginBackgroundName = params.webLoginBackgroundName
        def logo = params.logo
        def logoName = params.logoName

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(pmId)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editProperty = 'Back'
            redirect(action: 'editPm', params:params)
            return
        }

        def prop = Property.get(params.id)
        if(prop!=null){
            def result = [id:params.id, name:name, code:code, developer:developer, address:address, pmId:pmId, pmText:pmText,propertyTypeId:propertyTypeId,totalUnit:totalUnit,
                          mobileBackground: mobileBackground, mobileBackgroundName: mobileBackgroundName,
                          webLoginBackground: webLoginBackground, webLoginBackgroundName: webLoginBackgroundName,
                          logo: logo, logoName: logoName, securityPhoneNo: securityPhoneNo, securityEmails: securityEmails, action:'Edit Property', stage:'Confirm']
            if(ControllerUtil.hasValue(propertyTypeId)){
                result.propertyTypeName = PropertyType.get(propertyTypeId)?.name
            }
            render view:'confirm', model: [property: result]
        } else {
            flash.error = "Properti tidak ditemukan"
            redirect(action: 'index')
        }

        def isOneToOne = grailsApplication.config.getProperty("one.to.one.property.manager")=="1"
        if(isOneToOne){
            def pm = User.get(pmId)
            def pap = PropertyAdminProperty.findByPropertyAdmin(pm)
            if(pap!=null && pap.property.id != params.id){
                flash.error = "Properti Manager sudah memegang properti lain."
                params._action_editProperty = 'Back'
                redirect(action: 'editPm', params:params)
                return
            }
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... " + params
        def currentUser = springSecurityService.principal.id
        def name = params.name
        def code = params.code
        def developer = params.developer
        def address = params.address
        def pmId = params.pmId
        def pmText = params.pmText
        def propertyTypeId = params.propertyTypeId
        def totalUnit = params.totalUnit
        def securityPhoneNo = params.securityPhoneNo
        def securityEmails = params.securityEmails
        def mobileBackground = params.mobileBackground
        def mobileBackgroundName = params.mobileBackgroundName
        def webLoginBackground = params.webLoginBackground
        def webLoginBackgroundName = params.webLoginBackgroundName
        def logo = params.logo
        def logoName = params.logoName

        def isError = false, message = ''
        def prop = Property.get(params.id)
        def result = [name:name, code:code, developer:developer, address:address, id:params.id,propertyTypeId:propertyTypeId,totalUnit:totalUnit,
                      mobileBackground: mobileBackground, mobileBackgroundName: mobileBackgroundName,
                      webLoginBackground: webLoginBackground, webLoginBackgroundName: webLoginBackgroundName,
                      logo: logo, logoName: logoName, pmId:pmId, pmText:pmText, securityEmails: securityEmails,
                      securityPhoneNo: securityPhoneNo, actionType: 'edit', action:'Edit Property', stage:'Result']
        if(prop!=null){
            def isOneToOne = grailsApplication.config.getProperty("one.to.one.property.manager")=="1"
            if(isOneToOne){
                def pm = User.get(pmId)
                def pap = PropertyAdminProperty.findByPropertyAdmin(pm)
                if(pap!=null && pap.property.id != params.id){
                    isError = true
                    message = "Data properti Manager has already been assigned to other property"
                }
            }

            if(!isError) {
                def pm = User.get(params.pmId)
                Property.withTransaction { status ->
                    try {
                        def newPm = UserInfo.findByUser(pm)
                        if (prop.propertyManager==null || prop.propertyManager.id != newPm.id) {
                            //delete relation
                            PropertyAdminProperty.findAllByProperty(prop).each {
                                it.delete(flush: true)
                            }
                            //create new one
                            def pap = new PropertyAdminProperty()
                            pap.property = prop
                            pap.propertyAdmin = pm
                            pap.save(flush: true)
                        }

                        prop.name = name
                        prop.code = code
                        prop.developer = developer
                        prop.address = address
                        prop.propertyManager = newPm

                        if(ControllerUtil.hasValue(propertyTypeId)){
                            prop.propertyType = PropertyType.get(propertyTypeId)
                            result.propertyTypeName = prop.propertyType?.name
                        }
                        if(ControllerUtil.hasValue(totalUnit)){
                            prop.totalUnit = Integer.valueOf(totalUnit)
                        }
                        prop.securityPhoneNo = securityPhoneNo
                        prop.securityEmails = securityEmails
                        prop.updatedBy = currentUser

                        def oldFiles = [prop.mobileBackground, prop.webLoginBackground, prop.logo]
                        def files = [
                                [old:prop.mobileBackground, new:mobileBackground],
                                [old:prop.webLoginBackground, new:webLoginBackground],
                                [old:prop.logo, new: logo]
                        ]
                        files.each { d ->
                            if(d.old!=d.new && ControllerUtil.hasValue(d.new)) {
                                //copy file from tmp
                                def srcFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").concat(d.new)
                                def dstFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}images${File.separator}").concat(d.new)
                                def src = new File(srcFilePath)
                                def dst = new File(dstFilePath)
                                dst << src.bytes
                                if (src.exists()) src.delete()
                                //delete old file
                                if(d.old!=null) {
                                    def oldFilePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}images${File.separator}").concat(d.old)
                                    def old = new File(oldFilePath)
                                    if (old.exists()) old.delete()
                                }
                            }
                        }

                        prop.mobileBackground = mobileBackground
                        prop.mobileBackgroundName = mobileBackgroundName
                        prop.webLoginBackground = webLoginBackground
                        prop.webLoginBackgroundName = webLoginBackgroundName
                        prop.logo = logo
                        prop.logoName = logoName

                        prop.save(flush: true)
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isError = true
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }

            if(isError) {
                result.stage = 'Confirm'
                flash.error = message
            } else {
                result.stage = 'Result'
                flash.message = "Data properti berhasil diperbaharui"
            }
            render view:'confirm', model: [property: result]
        } else {
            flash.error = "Properti tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def upload(){
        log.debug "IN upload... ${params}"
        def result = [:]
        try {
            if (request instanceof MultipartHttpServletRequest) {
                for(filename in request.getFileNames()){
                    MultipartFile uploadedFile = request.getFile(filename)
                    log.debug ">>>> Name: ${uploadedFile.name} | OriginalFileName: ${uploadedFile.originalFilename} | Size: ${uploadedFile.size} | ContentType: ${uploadedFile.contentType}"
                    def fileId = UUID.randomUUID().toString().replace("-", "")
                    def uploadDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}tmp${File.separator}").toString())
                    def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}images${File.separator}").toString())
                    uploadDir.mkdirs()
                    destDir.mkdirs()
                    uploadedFile.transferTo(new File(uploadDir, fileId))

                    result = [result:true, fileId:fileId, message:'Success', fileName:uploadedFile.originalFilename]
                    break
                }
            }
        } catch(Exception e) {
            log.error "ERROR >>>>>>>>>>" + e.getMessage()
            result = [result:false,message:e.getMessage()]
        }
        render result as JSON
    }

    def showImage() {
        if(ControllerUtil.hasValue(params.id)) {
            def filePath = grailsApplication.config.getProperty("file.upload.path").concat("property${File.separator}images${File.separator}").concat(params.id)
            def file = new File(filePath)
            if (file.exists()) {
                InputStream is = new BufferedInputStream(new FileInputStream(file))
                def mimeType = URLConnection.guessContentTypeFromStream(is)
                byte[] imageInByte = is.bytes
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = mimeType // or the appropriate image content type
                response.outputStream << imageInByte
                response.outputStream.flush()
            } else {
                response.outputStream.flush()
            }
        } else {
            response.outputStream.flush()
        }
    }

    def detailPmJSON(){
        log.debug "IN detailPmJSON... " + params
        def output = [:]
        def info = UserInfo.get(params.id)
        if(info!=null){
            output.result = true
            output.username = info.user.username
            output.name = info.name
            output.phoneNo = info.phoneNo
        } else {
            output.result = false
            output.message = "Data properti Manager tidak ditemukan"
        }
        render output as JSON
    }

    def pmListJSON(){
        log.debug "IN pmListJSON... " + params
        def term = params.q? "%${params.q}%" : '%'
        def result = []
        def isOneToOne = grailsApplication.config.getProperty("one.to.one.property.manager")=="1"
        def baseQuery = ""
        if(isOneToOne) {
            def pmId = params.pmId
            if(ControllerUtil.hasValue(pmId)) {
                baseQuery = "FROM UserRole ur, UserInfo i " +
                        "WHERE ur.role.authority = :role AND i.user.username = ur.user.username " +
                        "AND (ur.user.id = '${pmId}' OR ur.user.id NOT IN (SELECT propertyAdmin.id FROM PropertyAdminProperty)) " +
                        "AND (lower(i.name) like lower(:name) OR lower(i.user.username) like lower(:name))" +
                        "ORDER BY i.user.username"
            } else {
                baseQuery = "FROM UserRole ur, UserInfo i " +
                        "WHERE ur.role.authority = :role AND i.user.username = ur.user.username " +
                        "AND (ur.user.id NOT IN (SELECT propertyAdmin.id FROM PropertyAdminProperty)) " +
                        "AND (lower(i.name) like lower(:name) OR lower(i.user.username) like lower(:name))" +
                        "ORDER BY i.user.username"
            }
        } else {
            baseQuery = "FROM UserRole ur, UserInfo i " +
                    "WHERE ur.role.authority = :role AND i.user.username = ur.user.username " +
                    "AND (lower(i.name) like lower(:name) OR lower(i.user.username) like lower(:name))" +
                    "ORDER BY i.user.username"
        }
        def query = "SELECT i " + baseQuery
        def queryCount = "SELECT count(i) " + baseQuery
        def queryParams = [role:AppConstants.ROLE_PM, name:term]

        def pmax = params.int("page_limit")
        def page = params.page? params.int("page") : 1
        def poffset = (page-1) * pmax
        UserInfo.executeQuery(query, queryParams, [max:pmax, offset:poffset]).each{ info ->
            result.add([id:info.user.id, text:"${info.user.username} - ${info.name}"])
        }
        def cnt = UserInfo.executeQuery(queryCount,queryParams).get(0)
        def output = [items:result, totalCount:cnt]
        render output as JSON
    }
}
