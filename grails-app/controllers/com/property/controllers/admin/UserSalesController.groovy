package com.property.controllers.admin

import com.model.AppConstants
import com.model.property.Property
import com.model.property.PropertyDeal
import com.model.property.PropertyPayment
import com.model.security.ResetPasswordToken
import com.model.security.Role
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserRole
import com.property.utils.ControllerUtil
import grails.converters.JSON
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class UserSalesController {

    LinkGenerator grailsLinkGenerator
    def mailService

    def index() {
        log.debug "IN index... $params"
    }

    def resetPassword(){
        log.debug "IN resetPassword... $params"
        def user = User.get(params.id)
        def userName = UserInfo.findByUser(user)?.name?:user?.username
        //generate token
        def token = UUID.randomUUID().toString().replaceAll("-","")
        ResetPasswordToken.withTransaction { status ->
            try{
                def obj = ResetPasswordToken.findByEmail(user.username)
                if(obj!=null){
                    obj.delete()
                }
                obj = new ResetPasswordToken()
                obj.email = user.username
                obj.token = token
                obj.createdTime = new Date().time
                obj.expiredTime = 3600
                obj.save()

                def tokenLink = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'auth', action: 'changePassword', params: [id: token]))

                log.debug("sending email emailPassword")
                mailService.sendMail {
                    to user.username
                    from "support@warga-kita.com"
                    subject "Warga Kita Reset Password"
                    html view: "emailPassword", model: [userName: userName?:"", tokenLink: tokenLink]
                }
                flash.message = "Email telah dikirim"
            } catch (Exception e) {
                log.error ExceptionUtils.getStackTrace(e)
                status.setRollbackOnly()
                flash.error = "Server bermasalah, coba lagi beberapa saat."
            }
        }
        redirect(action: 'detailUser', params:params)
    }

    def listDatatable(){
        def columns = ['i.user.username', 'i.name', 'i.phoneNo']
        def query = "SELECT i FROM UserRole ur, UserInfo i WHERE ur.role.authority = :role and i.user.username = ur.user.username"
        def queryCount = "SELECT count(i) FROM UserRole ur, UserInfo i WHERE ur.role.authority = :role and i.user.username = ur.user.username"
        def queryParams = [role:AppConstants.ROLE_SALES]
        def result = ControllerUtil.parseDatatablesQuery(UserInfo.class, params, query, queryCount, queryParams, columns)

        def objs = result.objs
        def aaData = []
        objs.each{ obj ->
            def det = [id:obj.user.id, username: obj.user.username, name: obj.name, phoneNo: obj.phoneNo]
            det.totalDealProposed = PropertyDeal.executeQuery("SELECT count(id) FROM PropertyDeal WHERE createdBy = :userId",[userId:obj.id])[0]
            def propIdList = Property.executeQuery("FROM Property WHERE sales.user.id = :userId",[userId:obj.user.id]).collect{it.id}
            if(propIdList.size()>0) {
                det.totalDealSecured = propIdList.size()
                det.totalRevenue = PropertyPayment.executeQuery("SELECT sum(salesCommission) FROM PropertyPayment WHERE propertyId IN :propIdList", [propIdList: propIdList])[0]?:"0"
            } else {
                det.totalDealSecured = 0
                det.totalRevenue = 0
            }
            aaData.add(det)
        }
        result.aaData = aaData
        result.objs = null
        render result as JSON
    }

    def detail(){
        log.debug "IN detail... $params"
        def user = User.get(params.id)
        def userInfo = UserInfo.findByUser(user)
        def result = [id:params.id, username:userInfo.user.username, name:userInfo.name, phoneNo: userInfo.phoneNo,
                      bankAccount: userInfo.bankAccount, bankName: userInfo.bankName]

        result.totalDealProposed = PropertyDeal.executeQuery("SELECT count(id) FROM PropertyDeal WHERE createdBy = :userId",[userId:user.id])[0]
        def propIdList = Property.executeQuery("FROM Property WHERE sales.user.id = :userId",[userId:user.id]).collect{it.id}
        if(propIdList.size()>0){
            result.totalDealSecured = propIdList.size()
        } else {
            result.totalDealSecured = 0
            result.totalRevenue = 0
        }

        def propList = []
        def sdf = new SimpleDateFormat("yyyy-MM-dd")
        Property.executeQuery("FROM Property WHERE sales.user.id = :userId",[userId:user.id]).each { prop ->
            def det = [name:prop.name, address:prop.address, developer: prop.developer, totalUnit: prop.totalUnit, fee:prop.fee]
            if(prop.expiryDate!=null){
                det.expiryDate = sdf.format(prop.expiryDate)
            }
            propList.add(det)
        }

        [user:result, propList:propList]
    }

    def delete(){
        log.debug "IN delete... $params"
        def isError = false, message = ''
        def user = User.get(params.id)
        if(user!=null){
            User.withTransaction { status ->
                try {
                    UserRole.findAllByUser(user).each{
                        it.delete()
                    }
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "System error"
                }
            }
        } else {
            message = "Sales tidak ditemukan"
            redirect(action: 'index')
        }
        if(isError) {
            flash.error = message
            redirect(action: 'detailUser', params:params)
        } else {
            flash.message = "Sales berhasil dihapus"
            redirect(action: 'index')
        }
    }

    def create(){
        log.debug "IN create... $params"
        if(params._action_create == 'Back'){
            [user: [username:params.username, name:params.name, phoneNo:params.phoneNo, bankAccount: params.bankAccount, bankName: params.bankName]]
        } else {
            [user:[:]]
        }
    }

    def confirmAdd(){
        log.debug "IN confirmAdd... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def bankName = params.bankName
        def bankAccount = params.bankAccount

        if(!ControllerUtil.hasValue(username) || !ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
            return
        }

        def result = [username:username, name:name, phoneNo:phoneNo, bankName:bankName, bankAccount:bankAccount]
        def user = User.findByUsername(username)
        if(user==null){
            result.action = 'Create Sales'
            result.stage = 'Confirm'
            result.actionType = 'add'
            render view:'confirm', model: [user:result]
        } else {
            flash.error = "User dengan nama ${username} sudah ada"
            params._action_create = 'Back'
            redirect(action: 'create', params:params)
        }
    }

    def submitAdd(){
        log.debug "IN submitAdd... $params"
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def bankName = params.bankName
        def bankAccount = params.bankAccount

        def result = [username:username, name:name, phoneNo: phoneNo, bankName:bankName, bankAccount:bankAccount,
                      actionType: 'add', action:'Create Sales']
        def isError = false
        def message = ""

        def user = User.findByUsername(username)
        if(user==null){
            User.withTransaction { status ->
                try {
                    String randomString = org.apache.commons.lang.RandomStringUtils.random(9, true, true)

                    user = new User()
                    user.username = username
                    user.password = randomString
                    user.save(flush: true)

                    def role = Role.findByAuthority(AppConstants.ROLE_SALES)
                    new UserRole(user: user, role: role).save(flush: true)

                    def userInfo = new UserInfo()
                    userInfo.user = user
                    userInfo.name = name
                    userInfo.phoneNo = phoneNo
                    userInfo.bankAccount = bankAccount
                    userInfo.bankName = bankName
                    userInfo.save(flush: true)

                    log.debug("sending email emailNewUser")
                    mailService.sendMail {
                        to user.username
                        from "support@warga-kita.com"
                        subject "Warga Kita User Password"
                        html view: "emailNewUser", model: [userName: name, password: randomString]
                    }
                } catch (Exception e){
                    log.error ExceptionUtils.getStackTrace(e)
                    status.setRollbackOnly()
                    isError = true
                    message = "System error"
                }
            }
        } else {
            message = "User with username ${username} already exists"
        }
        if(isError) {
            result.stage = 'Confirm'
            flash.error = message
        } else {
            result.stage = 'Result'
            flash.message = "Sales berhasil ditambahkan"
        }
        render view: 'confirm', model: [user:result]
    }

    def edit(){
        log.debug "IN edit... $params"
        def user = User.get(params.id)
        if (user != null) {
            def result = [:]
            result.totalDealProposed = PropertyDeal.executeQuery("SELECT count(id) FROM PropertyDeal WHERE createdBy = :userId",[userId:user.id])[0]
            def propIdList = Property.executeQuery("FROM Property WHERE sales.user.id = :userId",[userId:user.id]).collect{it.id}
            if(propIdList.size()>0){
                result.totalDealSecured = propIdList.size()
            } else {
                result.totalDealSecured = 0
                result.totalRevenue = 0
            }
            result.username = user.username
            result.id = params.id

            if(params._action_edit == 'Back'){
                result.name = params.name
                result.phoneNo = params.phoneNo
                result.bankAccount = params.bankAccount
                result.bankName = params.bankName
                [user: result]
            } else {
                def userInfo = UserInfo.findByUser(user)
                result.name = userInfo.name
                result.phoneNo = userInfo.phoneNo
                result.bankAccount = userInfo.bankAccount
                result.bankName = userInfo.bankName
                result.actionType = 'edit'
                result.action = 'Edit Property Manager'
                result.stage = 'Confirm'
                [user: result]
            }
        } else {
            flash.error = "Sales tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def confirmEdit(){
        log.debug "IN confirmEdit... $params"
        def id = params.id
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def bankName = params.bankName
        def bankAccount = params.bankAccount

        if(!ControllerUtil.hasValue(name)){
            flash.error = "Silakan isi semua data yang diperlukan"
            params._action_editPm = 'Back'
            redirect(action: 'editPm', params:params)
            return
        }

        def result = [id:id, username:username, name:name, phoneNo: phoneNo, bankName:bankName, bankAccount:bankAccount,
                      actionType: 'edit', action:'Edit Sales', stage:'Confirm']

        def user = User.get(id)
        if(user!=null){
            render view:'confirm', model: [user:result]
        } else {
            flash.error = "Sales tidak ditemukan"
            redirect(action: 'index')
        }
    }

    def submitEdit(){
        log.debug "IN submitEdit... $params"
        def id = params.id
        def username = params.username
        def name = params.name
        def phoneNo = params.phoneNo
        def bankName = params.bankName
        def bankAccount = params.bankAccount

        def user = User.get(params.id)
        if(user!=null){
            def result = [username:username, name:name, phoneNo: phoneNo, bankName:bankName, bankAccount:bankAccount,
                          actionType: 'edit', action:'Edit Sales', stage:'Result']

            def userInfo = UserInfo.findByUser(user)
            userInfo.name = name
            userInfo.phoneNo = phoneNo
            userInfo.bankAccount = bankAccount
            userInfo.bankName = bankName
            userInfo.save(flush:true)
            flash.message = "Sales berhasil diperbaharui"
            result.username = user.username

            render view:'confirm', model: [user:result]
        } else {
            flash.error = "Sales tidak ditemukan"
            redirect(action: 'index')
        }
    }
}
