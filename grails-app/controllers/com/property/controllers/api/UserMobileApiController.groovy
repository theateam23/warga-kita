package com.property.controllers.api

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserRecord
import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.general.SecurityProperty
import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.maintenance.RoleMobile
import com.model.property.Member
import com.model.property.TmpUserMobile
import com.model.property.PropertyUnit
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

class UserMobileApiController extends RestfulController{

    def mailService
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [getUser:'POST',
                             registerUserMobile:'POST',
                             updateUser:'POST']

    UserMobileApiController(){
        super(UserMobile)
    }

    def getUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getUser... $jsonData"
        def message='', isSuccess = "Y", needRegistration = 'N', result = [:]

        def email = jsonData.email
        def appName = jsonData.appName

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def user = UserMobile.findByEmail(email)
            if(user==null){
                def tmpUser = TmpUserMobile.findByEmail(email)
                if (tmpUser==null) {
                    message = "User tidak ditemukan"
                    needRegistration = "Y"
                    isSuccess = "N"
                } else {
                    message = "User menunggu persetujuan dari properti admin"
                    isSuccess = "N"
                }

            } else {
                def detail = [
                        id:user.id,
                        name: user.name,
                        mobileNo: user.phoneNo
                ]

                def ump = UserMobileProperty.findByUserMobile(user)
                if (ump != null) { // Check Expiry Date for licensing
                    if (ump.property.expiryDate.before(new Date())) {
                        message = "Masa berlaku berlangganan anda sudah habis, silakan hubungi admin."
                        isSuccess = "N"
                    }
//                    else if(!ump.property.appName.equals(appName)) {
//                        message = "User email anda masih terhubung dengan aplikasi ${ump.property.appName}, hubungi admin."
//                        isSuccess = "N"
//                    }
                    else {
                        if (ControllerUtil.hasValue(user.imageFileId)) {
                            detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'userMobile', action: 'showImage', params: [id: user.imageFileId]))
                        }
                        def security = SecurityProperty.findBySecurity(user)
                        if(security!=null){
                            detail.isSecurity = "Y"
                            detail.propertyId = security.property.id
                        } else {
                            detail.isSecurity = "N"
                            def unit = UserMobileUnit.findByUserMobile(user)?.unit
                            if (unit != null) {
                                detail.unitId = unit.id
                                detail.unitName = unit.name
                            }
                        }
                        result.user = detail
                        message = "User found"
                        isSuccess = "Y"
                    }
                }
            }
        }

        result.message = message
        result.needRegistration = needRegistration
        result.isSuccess = isSuccess

        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def registerUserMobile(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/registerUserMobile... $jsonData"
        def result = [:]
        def email = jsonData.email
        def name = jsonData.name
        def phoneNo = jsonData.mobileNo
        def unitId = jsonData.unitId
        def role = jsonData.status

        def isSuccess = "Y", message, status = 0 //0 = not registered, 1 = approval process, 2 = registered, 99 : Failed Register to Firebase

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(role) || !ControllerUtil.hasValue(unitId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def unit = PropertyUnit.get(unitId)
            def user = TmpUserMobile.findByEmail(email)
            def userMobile = UserMobile.findByEmail(email)
            def roleMobile = RoleMobile.findByName(role)
            if(user!=null){
                message = "Registrasi user berhasil, menunggu persetujuan"
                isSuccess = "N"
                status = 1
            } else if(userMobile!=null){
                message = "User sudah terdaftar"
                isSuccess = "N"
                status = 2
            } else {
                if(roleMobile==null){
                    message = "Invalid status, Silahkan hubungi admin properti anda."
                    isSuccess = "N"
                } else {
                    if (unit == null) {
                        message = "Unit tidak ditemukan, Silahkan hubungi admin properti anda."
                        isSuccess = "N"
                    } else if (user != null) {
                        message = "User sudah terdaftar, Silahkan hubungi admin properti anda."
                        isSuccess = "N"
                    } else if((unit.ownerResident != null || unit.tenant != null) && !AppConstants.ROLE_SUB_USER.equals(roleMobile.name)) {
                        message = "Status Pemilik/Penyewa untuk unit ini sudah terisi, Silahkan hubungi admin properti anda."
                        isSuccess = "N"
                    } else if(AppConstants.ROLE_SUB_USER.equals(roleMobile.name)) { // Penghuni no need approval
                        def emailValid = false
                        Member.findAllByUnit(unit).each { m ->
                            if (m.email.equals(email)) {
                                emailValid = true
                            }
                        }
                        if (!emailValid) {
                            message = "Email penghuni yang anda masukkan belum terdaftar di unit ini, tolong tambahkan data terlebih dahulu."
                            isSuccess = "N"
                        } else {
                            if (isUserExistInFirebase(email)) {
                                UserMobile.withTransaction { tx ->
                                    try{
                                        def um = new UserMobile()
                                        um.createdBy = AppConstants.BY_SYSTEM
                                        um.email = email
                                        um.name = name
                                        um.phoneNo = phoneNo
                                        //um.address = address
                                        //um.title = title
                                        //um.idKTP = idKTP
                                        um.save()

                                        def ump = new UserMobileProperty()
                                        ump.property = unit.area.property
                                        ump.userMobile = um
                                        ump.save()

                                        def umu = new UserMobileUnit()
                                        umu.roleMobile = roleMobile
                                        umu.unit = unit
                                        umu.userMobile = um
                                        umu.save()

                                        if(AppConstants.ROLE_TENANT.equals(role)){
                                            unit.tenant = um
                                            unit.save()
                                        } else if(AppConstants.ROLE_OWNER_RESIDENT.equals(role)){
                                            unit.ownerResident = um
                                            unit.save()
                                        }

                                        message = "User berhasil register"
                                        isSuccess = "Y"

                                        mailService.sendMail {
                                            to um.email
                                            from "support@warga-kita.com"
                                            subject "Registrasi Mobile Warga Kita"
                                            html view: "/emails/emailUserMobileResult", model: [userMobile: [email:um.email, phoneNo:um.phoneNo, name: um.name, unitName: unit.name, areaName: unit.area.name]]
                                        }

                                    } catch(Exception e){
                                        log.debug "ERROR processing $email : ${e.getMessage()}"
                                        log.error(ExceptionUtils.getStackTrace(e))
                                        tx.setRollbackOnly()
                                        isSuccess = "N"
                                        message = "Server bermasalah, coba lagi beberapa saat."
                                    }
                                }
                            } else {
                                message = "Registrasi Bermasalah, Coba lagi"
                                isSuccess = "N"
                                status = 99
                            }
                        }
                    } else {
                        if (isUserExistInFirebase(email)) {
                            TmpUserMobile.withTransaction { statusTrx ->
                                try {
                                    def obj = new TmpUserMobile()
                                    obj.phoneNo = phoneNo
                                    obj.email = email
                                    obj.name = name
                                    obj.role = role
                                    obj.propertyId = unit.area.property.id
                                    obj.unitId = unitId
                                    obj.save()

                                    //sent email to pa/pm
                                    def pmList = PropertyAdminProperty.executeQuery("FROM PropertyAdminProperty WHERE property.id = :propId", [propId: unit.area.property.id])
                                    if(pmList.size()>0) {

                                        log.debug("sending email emailUserMobile")
                                        mailService.sendMail {
                                            to pmList.collect{it.propertyAdmin.username}
                                            from "support@warga-kita.com"
                                            subject "Warga Kita User Mobile Registration Approval"
                                            html view: "/emails/emailUserMobile", model: [userMobile: [email:email, phoneNo:phoneNo, name: name, unitName: unit.name, areaName: unit.area.name]]
                                        }
                                    }

                                    message = "User berhasil register"
                                    isSuccess = "Y"
                                } catch (Exception e){
                                    log.error ExceptionUtils.getStackTrace(e)
                                    statusTrx.setRollbackOnly()
                                    isSuccess = "N"
                                    message = "Server bermasalah, coba lagi beberapa saat."
                                }
                            }
                        }  else {
                            message = "Registrasi Bermasalah, Coba lagi"
                            isSuccess = "N"
                            status = 99
                        }
                    }
                }
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        result.status = status
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    boolean isUserExistInFirebase(String email) {
        boolean isExist = false
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUserByEmail(email)
            if (userRecord != null) {
                isExist = true
            }
        } catch (Exception e){ }

        return isExist
    }


    def updateUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/updateUser... $jsonData"
        def result = [:]
        def email = jsonData.email
        def name = jsonData.name
        def phoneNo = jsonData.mobileNo
        def picture = jsonData.picture

        def isSuccess = "Y", message, status = 0 //0 = not registered, 1 = approval process, 2 = registered

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def userMobile = UserMobile.findByEmail(email)
            if(userMobile==null){
                message = "User tidak ditemukan"
                isSuccess = "N"
            } else {
                UserMobile.withTransaction { statusTrx ->
                    try {
                        userMobile.phoneNo = phoneNo
                        userMobile.email = email
                        userMobile.name = name

                        def fileId = null
                        if (ControllerUtil.hasValue(picture)) {
                            //delete old image if exists
                            if(ControllerUtil.hasValue(userMobile.imageFileId)) {
                                def oldImage = new File(grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}images${File.separator}").concat(userMobile.imageFileId))
                                if (oldImage.exists()) oldImage.delete()
                            }
                            //create image file
                            fileId = UUID.randomUUID().toString().replace("-", "")
                            def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}images${File.separator}").toString())
                            if(!destDir.exists()){
                                destDir.mkdirs()
                            }
                            def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("userMobile${File.separator}images${File.separator}").concat(fileId)

                            BASE64Decoder decoder = new BASE64Decoder()
                            byte[] decoded = decoder.decodeBuffer(picture)
                            OutputStream stream = new FileOutputStream(destFilePath)
                            stream.write(decoded)
                            stream.flush()
                            stream.close()

                            userMobile.imageFileId = fileId
                        }

                        userMobile.save(flush:true)

                        message = "User berhasil diperbaharui"
                    } catch (Exception e){
                        log.error ExceptionUtils.getStackTrace(e)
                        statusTrx.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        result.status = status
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }
}
