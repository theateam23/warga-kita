package com.property.controllers.api

import com.model.AppConstants
import com.model.general.SecurityProperty
import com.model.general.UserMobileUnit
import com.model.property.HomeGuard
import com.model.property.HomeGuardLog
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat

class HomeGuardApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [addHomeGuard:'POST',
                             deleteHomeGuard: 'POST',
                             getHomeGuardList: 'POST',
                             checkHomeGuard: 'POST',
    ]

    // todo: add update status

    HomeGuardApiController(){
        super(HomeGuard)
    }

    def addHomeGuard(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/addHomeGuard... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def email = jsonData.email
        def description = jsonData.description
        def fromDate = jsonData.fromDate
        def toDate = jsonData.toDate

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(fromDate) || !ControllerUtil.hasValue(toDate)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            if (userMobile == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def unit = UserMobileUnit.findByUserMobile(userMobile)?.unit
                if (unit == null) {
                    isSuccess = "N"
                    message = "Unit tidak ditemukan."
                } else {
                    HomeGuard.withTransaction { status ->
                        try {
                            def sdf = new SimpleDateFormat("yyyy-MM-dd")
                            def obj = new HomeGuard()
                            obj.unit = unit
                            obj.description = description
                            obj.fromDate = sdf.parse(fromDate)
                            obj.toDate = sdf.parse(toDate)
                            obj.createdBy = email
                            obj.save(flush: true)

                            obj.fromDate.upto(obj.toDate) { t ->
                                def log = HomeGuardLog.findByUnitAndGuardDate(unit.name, t)
                                if (log == null) {
                                    log = new HomeGuardLog()
                                    log.unit = unit.name
                                    log.area = unit.area.name
                                    log.subArea = unit.subArea
                                    log.guardDate = t
                                    log.homeGuardId = obj.id
                                    log.propertyId = unit.area.property.id
                                    log.status = AppConstants.HOMEGUARD_STATUS_NEW

                                    log.save(flush: true)
                                }
                            }

                            isSuccess = "Y"
                            message = "Data berhasil ditambahkan."
                        } catch (Exception e) {
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def deleteHomeGuard() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/deleteHomeGuard... $jsonData"
        def result = [:], isSuccess = "N", message = ""

        def homeGuardId = jsonData.homeGuardId

        if(!ControllerUtil.hasValue(homeGuardId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def homeGuard = HomeGuard.get(homeGuardId)
            HomeGuardLog.findAllByHomeGuardId(homeGuardId).each { log ->
                if(ControllerUtil.hasValue(log.imageFileId)) {
                    def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("homeGuard${File.separator}images${File.separator}").concat(log.imageFileId))
                    if (image.exists()) image.delete()
                }
                log.delete(flush: true)
            }
            homeGuard.delete(flush: true)

            message = "Data berhasil dihapus."
            isSuccess = "Y"
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getHomeGuardList(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getHomeGuardList... $jsonData"

        def sdf = new SimpleDateFormat("dd MMM yyy")

        def result = [:], isSuccess = "Y", message = ""

        def propId = jsonData.propertyId
        def email = jsonData.email

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(email)) {
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def userMobile = UserMobile.findByEmail(email)
            def isSecurity = isSecurity(userMobile)
            def list = []

            if (!isSecurity) {
                def userMobileUnit = UserMobileUnit.findAllByUserMobile(userMobile).get(0)
                HomeGuard.findAllByUnitAndToDateGreaterThanEquals(userMobileUnit.unit, new Date().clearTime()).each { it ->
                    def detail = [homeGuardId: it.id, fromDate: sdf.format(it.fromDate),
                                  toDate: sdf.format(it.toDate), description: it.description]

                    list.add(detail)
                }
            } else {
                HomeGuardLog.findAllByPropertyIdAndGuardDateAndStatus(propId, new Date().clearTime(), AppConstants.HOMEGUARD_STATUS_NEW).each { it ->
                    def detail = [homeGuardId: it.id, unit: it.unit, area: it.area, subArea: it.subArea,
                                  description: it.description]
                    list.add(detail)

                }
            }

            result.homeGuardList = list
        }

        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"

        respond(result)
    }

    def checkHomeGuard() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/checkHomeGuard... homeGuardId: ${jsonData.homeGuardId} email: ${jsonData.email}"

        def result = [:], isSuccess = "N", message = ""

        def homeGuardId = jsonData.homeGuardId
        def email = jsonData.email
        def imageFile = jsonData.imageFile
        def imageFileName = UUID.randomUUID().toString().concat(".jpg")

        if(!ControllerUtil.hasValue(homeGuardId) || !ControllerUtil.hasValue(email)) {
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def obj = HomeGuardLog.get(homeGuardId)

            def fileId = null
            if (ControllerUtil.hasValue(imageFile)) {
                //create image file
                fileId = UUID.randomUUID().toString().replace("-", "")
                def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("homeGuard${File.separator}images${File.separator}").toString())
                if(!destDir.exists()){
                    destDir.mkdirs()
                }
                def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("homeGuard${File.separator}images${File.separator}").concat(fileId)

                BASE64Decoder decoder = new BASE64Decoder()
                byte[] decoded = decoder.decodeBuffer(imageFile)
                OutputStream stream = new FileOutputStream(destFilePath)
                stream.write(decoded)
                stream.flush()
                stream.close()
            }
            obj.updatedBy = email
            obj.updatedDate = new Date()
            obj.imageFileId = fileId
            obj.imageFileName = imageFileName
            obj.status = AppConstants.HOMEGUARD_STATUS_CHECK

            obj.save(flush: true, failOnError : true)

            isSuccess = "Y"
            message = "Data berhasil diperbaharui."
        }

        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"

        respond(result)
    }

    boolean isSecurity(UserMobile userMobile) {
        def isSecurity = false

        if(SecurityProperty.findAllBySecurity(userMobile).size()>0){
            isSecurity = true
        }

        return isSecurity;
    }
}
