package com.property.controllers.api

import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.maintenance.Gender
import com.model.maintenance.MemberRelation
import com.model.property.Member
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat

class MemberApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [createMember: 'POST',
                             getMemberByUser: 'POST',
                             getMember: 'POST',
                             updateMember: 'POST',
                             deleteMember: 'POST',
                             getMemberByName: 'POST' ]

    MemberApiController(){
        super(Member)
    }

    def createMember(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/createMember... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def userEmail = jsonData.userEmail
        def name = jsonData.name
        def dateOfBirth = jsonData.dateOfBirth
        def placeOfBirth = jsonData.placeOfBirth
        def profession = jsonData.profession
        def idKTP = jsonData.idKTP
        def email = jsonData.email
        def address = jsonData.address
        def statusMember = jsonData.status
        def education = jsonData.education
        def hobby = jsonData.hobby
        def description = jsonData.description
        def relationship = jsonData.relationship
        def gender = jsonData.gender
        def imageFile = jsonData.imageFile
        def imageFileName = jsonData.imageFileName

        if(!ControllerUtil.hasValue(userEmail) || !ControllerUtil.hasValue(name)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(userEmail)
            if (userMobile == null) {
                isSuccess = "N"
                message = "User tidak ditemukan"
            } else {
                def unit = UserMobileUnit.findByUserMobile(userMobile)?.unit
                if (unit == null) {
                    isSuccess = "N"
                    message = "Unit tidak ditemukan."
                } else {
                    Member.withTransaction { status ->
                        try {
                            def sdf = new SimpleDateFormat("yyyy-MM-dd")
                            def obj = new Member()
                            obj.unit = unit
                            obj.name = name
                            if (ControllerUtil.hasValue(dateOfBirth)) {
                                obj.dateOfBirth = sdf.parse(dateOfBirth)
                            }
                            obj.placeOfBirth = placeOfBirth
                            obj.profession = profession
                            obj.idKTP = idKTP
                            obj.email = email
                            obj.address = address
                            obj.status = statusMember
                            obj.education = education
                            obj.hobby = hobby
                            obj.description = description
                            if (ControllerUtil.hasValue(relationship)) {
                                obj.relationship = MemberRelation.findByCode(relationship)
                            }
                            obj.gender = Gender.findByCode(gender)

                            def fileId = null
                            if (ControllerUtil.hasValue(imageFile)) {
                                //create image file
                                fileId = UUID.randomUUID().toString().replace("-", "")
                                def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").toString())
                                if(!destDir.exists()){
                                    destDir.mkdirs()
                                }
                                def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(fileId)

                                BASE64Decoder decoder = new BASE64Decoder()
                                byte[] decoded = decoder.decodeBuffer(imageFile)
                                OutputStream stream = new FileOutputStream(destFilePath)
                                stream.write(decoded)
                                stream.flush()
                                stream.close()
                            }
                            obj.imageFileId = fileId
                            obj.imageFileName = imageFileName
                            obj.save(flush: true, failOnError : true)

                            isSuccess = "Y"
                            message = "Data berhasil ditambahkan"
                        } catch (Exception e) {
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getMemberByUser() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getMemberByUser... $jsonData"
        def result = [:], message = "", isSuccess = "Y"

        def email = jsonData.email

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            if(userMobile==null){
                message = "User tidak ditemukan"
                isSuccess = "N"
            } else {
                def memberList = []
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                Member.executeQuery("SELECT distinct m FROM Member m, UserMobileUnit umu WHERE m.unit.id = umu.unit.id AND umu.userMobile.email = :email ",[email:email]).each{ obj ->
                    def detail = [:]
                    detail.uid = obj.id
                    detail.unitId = obj.unit.id
                    detail.unitName = obj.unit.name
                    detail.name = obj.name
                    if (obj.dateOfBirth != null) {
                        detail.dateOfBirth = sdf.format(obj.dateOfBirth)
                    }
                    detail.placeOfBirth = obj.placeOfBirth
                    detail.profession = obj.profession
                    detail.idKTP = obj.idKTP
                    detail.email = obj.email
                    detail.address = obj.address
                    detail.gender = obj.gender?.code
                    detail.status = obj.status
                    detail.education = obj.education
                    detail.hobby = obj.hobby
                    detail.description = obj.description
                    detail.relationship = obj.relationship?.code
                    if (ControllerUtil.hasValue(obj.imageFileId)) {
                        detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'memberPm', action: 'showImage', params: [id: obj.imageFileId]))
                    }
                    detail.imageFileName = obj.imageFileName
                    memberList.add(detail)
                }
                result.memberList = memberList
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getMember() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getMember... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def memberId = jsonData.memberId

        if (!ControllerUtil.hasValue(memberId)) {
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def obj = Member.get(memberId)
            if (obj == null) {
                isSuccess = "N"
                message = "Member tidak ditemukan"
            } else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def detail = [:]
                detail.unitId = obj.unit.id
                detail.unitName = obj.unit.name
                detail.name = obj.name
                if (obj.dateOfBirth != null) {
                    detail.dateOfBirth = sdf.format(obj.dateOfBirth)
                }
                detail.placeOfBirth = obj.placeOfBirth
                detail.profession = obj.profession
                detail.idKTP = obj.idKTP
                detail.email = obj.email
                detail.address = obj.address
                detail.gender = obj.gender?.code
                detail.status = obj.status
                detail.education = obj.education
                detail.hobby = obj.hobby
                detail.description = obj.description
                detail.relationship = obj.relationship?.code
                if (ControllerUtil.hasValue(obj.imageFileId)) {
                    detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'memberPm', action: 'showImage', params: [id: obj.imageFileId]))
                }
                detail.imageFileName = obj.imageFileName
                result.member = detail
                isSuccess = "Y"
                message = "Penghuni ditemukan"
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def updateMember(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/updateMember... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def memberId = jsonData.memberId
        def name = jsonData.name
        def dateOfBirth = jsonData.dateOfBirth
        def placeOfBirth = jsonData.placeOfBirth
        def profession = jsonData.profession
        def idKTP = jsonData.idKTP
        def email = jsonData.email
        def address = jsonData.address
        def statusMember = jsonData.status
        def education = jsonData.education
        def hobby = jsonData.hobby
        def description = jsonData.description
        def relationship = jsonData.relationship
        def gender = jsonData.gender
        def imageFile = jsonData.imageFile
        def imageFileName = jsonData.imageFileName

        if(!ControllerUtil.hasValue(memberId) || !ControllerUtil.hasValue(name)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def obj = Member.get(memberId)
            if (obj == null) {
                isSuccess = "N"
                message = "Member tidak ditemukan"
            } else {
                Member.withTransaction { status ->
                    try {
                        def sdf = new SimpleDateFormat("yyyy-MM-dd")
                        obj.name = name
                        if (ControllerUtil.hasValue(dateOfBirth)) {
                            obj.dateOfBirth = sdf.parse(dateOfBirth)
                        }
                        obj.placeOfBirth = placeOfBirth
                        obj.profession = profession
                        obj.idKTP = idKTP
                        obj.email = email
                        obj.address = address
                        obj.status = statusMember
                        obj.education = education
                        obj.hobby = hobby
                        obj.description = description
                        if (ControllerUtil.hasValue(relationship)) {
                            obj.relationship = MemberRelation.findByCode(relationship)
                        }
                        obj.gender = Gender.findByCode(gender)

                        def fileId = null
                        if (ControllerUtil.hasValue(imageFile)) {
                            //delete old image if exists
                            if(ControllerUtil.hasValue(obj.imageFileId)) {
                                def oldImage = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(obj.imageFileId))
                                if (oldImage.exists()) oldImage.delete()
                            }
                            //create image file
                            fileId = UUID.randomUUID().toString().replace("-", "")
                            def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").toString())
                            if(!destDir.exists()){
                                destDir.mkdirs()
                            }
                            def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(fileId)

                            BASE64Decoder decoder = new BASE64Decoder()
                            byte[] decoded = decoder.decodeBuffer(imageFile)
                            OutputStream stream = new FileOutputStream(destFilePath)
                            stream.write(decoded)
                            stream.flush()
                            stream.close()

                            obj.imageFileId = fileId
                            obj.imageFileName = imageFileName
                        }

                        obj.save(flush: true)

                        isSuccess = "Y"
                        message = "Data berhasil diperbaharui."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def deleteMember(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/deleteMember... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def memberId = jsonData.memberId

        if(!ControllerUtil.hasValue(memberId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def obj = Member.get(memberId)
            if (obj == null) {
                isSuccess = "N"
                message = "Data tidak ditemukan."
            } else {
                //delete image if exists
                if(ControllerUtil.hasValue(obj.imageFileId)) {
                    def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("member${File.separator}images${File.separator}").concat(obj.imageFileId))
                    if (image.exists()) image.delete()
                }
                obj.delete(flush:true)
                isSuccess = "Y"
                message = "Data berhasil dihapus."
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getMemberByName(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getMemberByName... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def name = jsonData.name
        def email = jsonData.email

        if(!ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            if (userMobile == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def propId = UserMobileProperty.findByUserMobile(userMobile).property.id
                def obj = Member.executeQuery("FROM Member WHERE unit.area.property.id = :propId AND UPPER(name) = UPPER(:name)",
                        [propId: propId, name: name])

                if (obj == null || obj.size() == 0) {
                    isSuccess = "N"
                    message = "Data tidak ditemukan"
                } else {
                    isSuccess = "Y"
                    message = "Data ditemukan"
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }
}
