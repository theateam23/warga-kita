package com.property.controllers.api

import com.model.AppConstants
import com.model.general.SecurityProperty
import com.model.general.UserMobileProperty
import com.model.information.PropertyAnnouncement
import com.model.information.PropertyNews
import com.model.maintenance.NewsCategory
import com.model.maintenance.RoleMobile
import com.model.property.Property
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator

import java.text.SimpleDateFormat

class NewsAnnouncementApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [getNewsByUser: 'POST',
                             getNewsByUserAndCategory: 'POST',
                             getNewsCategory: 'POST',
                             getAnnouncementByUser: 'POST']

    NewsAnnouncementApiController(){
        super(PropertyNews)
    }


    def getNewsByUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getNewsByUser... $jsonData"
        def email = jsonData.email
        def propId = jsonData.propertyId
        def poffset = jsonData.offset?:0
        def pmax = jsonData.max?:50
        def result = [:], message = '', isSuccess = "Y"

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(propId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            def prop = Property.get(propId)
            def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile, prop)
            if (userMobile == null || prop == null || ump == null) {
                message = "User tidak ditemukan"
                isSuccess = "N"
            } else {
                def roles = []
                //get all roles from user
                RoleMobile.executeQuery("SELECT distinct umu.roleMobile FROM UserMobileUnit umu WHERE umu.unit.area.property.id = :propId AND umu.userMobile.id = :userId",
                        [propId: propId, userId: userMobile.id]).each { rm ->
                    if (!roles.contains(rm.id)) {
                        roles.add(rm.id)
                    }
                }
                RoleMobile.executeQuery("SELECT distinct rjp.security FROM SecurityProperty rjp WHERE rjp.property.id = :propId AND rjp.security.id = :userId",
                        [propId: propId, userId: userMobile.id]).each { rm ->
                    if (!roles.contains(rm.id)) {
                        roles.add(rm.id)
                    }
                }

                if(roles.size()==0){
                    message = "User hasn't been assigned any role yet"
                    isSuccess = "N"
                } else {
                    def sdf = new SimpleDateFormat("dd MMM yyyy")
                    def sdfTime = new SimpleDateFormat("hh:mm a")
                    def news = []
                    def baseQuery = "FROM PropertyNewsTarget pnt WHERE pnt.propertyNews.property.id = :propId AND pnt.roleMobile.id IN :roleList "
                    def query = "SELECT distinct pnt.propertyNews " + baseQuery + " ORDER BY pnt.propertyNews.createdDate DESC"
                    def queryCount = "SELECT count(distinct pnt.propertyNews) " + baseQuery
                    def queryParams = [propId: propId, roleList: roles]

                    PropertyNews.executeQuery(query, queryParams, [max: pmax, offset: poffset]).each { n ->
                        def detail = [title: n.title, subtitle: n.subtitle, summary: n.summary, description: n.description,
                                      createdDate: sdf.format(n.createdDate), createdTime: sdfTime.format(n.createdDate),
                                      email:n.email, phoneNo:n.phoneNo, sms:n.sms, address:n.address]
                        if (ControllerUtil.hasValue(n.imageFileId)) {
                            detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'news', action: 'showImage', params: [id: n.imageFileId]))
                        }
                        detail.category = n.category?.code
                        news.add(detail)
                    }
                    def totalRecord = PropertyNews.executeQuery(queryCount, queryParams)[0]
                    result.news = news
                    result.totalRecord = totalRecord
                    result.max = pmax
                    result.offset = poffset
                }
            }
        }
        result.message = message
        result.isSuccess = isSuccess

        log.debug "IN api/property/getNewsByUser... resultSize: ${result.totalRecord}"
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"

        respond(result)
    }

    def getNewsByUserAndCategory(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getNewsByUserAndCategory... $jsonData"
        def email = jsonData.email
        def propId = jsonData.propertyId
        def categoryCode = jsonData.categoryCode
        def poffset = jsonData.offset?:0
        def pmax = jsonData.max?:50
        def result = [:], message = '', isSuccess = "Y"

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(categoryCode)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            def prop = Property.get(propId)
            def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile, prop)
            if (userMobile == null || prop == null || ump == null) {
                message = "User tidak ditemukan"
                isSuccess = "N"
            } else {
                def roles = []
                //check if security
                def isSecurity = SecurityProperty.findByPropertyAndSecurity(prop,userMobile)!=null
                if(isSecurity){
                    roles.add(AppConstants.ROLE_SECURITY)
                } else {
                    RoleMobile.executeQuery("SELECT distinct umu.roleMobile FROM UserMobileUnit umu WHERE umu.unit.area.property.id = :propId AND umu.userMobile.id = :userId",
                            [propId: propId, userId: userMobile.id]).each { rm ->
                        if (!roles.contains(rm.name)) {
                            roles.add(rm.name)
                        }
                    }
                }

                if(roles.size()==0){
                    message = "User hasn't been assigned any role yet"
                    isSuccess = "N"
                } else {
                    def sdf = new SimpleDateFormat("dd MMM yyyy")
                    def sdfTime = new SimpleDateFormat("hh:mm a")
                    def news = []
                    def baseQuery = "FROM PropertyNewsTarget pnt WHERE pnt.propertyNews.property.id = :propId AND pnt.roleMobile.name IN :roleList AND pnt.propertyNews.category.code = :categoryCode"
                    def query = "SELECT distinct pnt.propertyNews " + baseQuery + " ORDER BY pnt.propertyNews.createdDate DESC"
                    def queryCount = "SELECT count(distinct pnt.propertyNews) " + baseQuery
                    def queryParams = [propId: propId, roleList: roles, categoryCode:categoryCode]

                    PropertyNews.executeQuery(query, queryParams, [max: pmax, offset: poffset]).each { n ->
                        def detail = [title: n.title, subtitle: n.subtitle, summary: n.summary, description: n.description,
                                      createdDate: sdf.format(n.createdDate), createdTime: sdfTime.format(n.createdDate),
                                      email:n.email, phoneNo:n.phoneNo, sms:n.sms, address:n.address]
                        if (ControllerUtil.hasValue(n.imageFileId)) {
                            detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'news', action: 'showImage', params: [id: n.imageFileId]))
                        }
                        detail.category = n.category?.code
                        news.add(detail)
                    }
                    def totalRecord = PropertyNews.executeQuery(queryCount, queryParams)[0]
                    result.news = news
                    result.totalRecord = totalRecord
                    result.max = pmax
                    result.offset = poffset
                }
            }
        }
        result.message = message
        result.isSuccess = isSuccess

        log.debug "IN api/property/getNewsByUserAndCategory... resultSize: ${result.totalRecord}"
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"

        respond(result)
    }

    def getNewsCategory(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getNewsCategory... $jsonData"
        def result = [:], categoryList = []
        NewsCategory.list([sort:'idx', order:'asc']).each { c ->
            categoryList.add([code:c.code, name: c.name, description: c.description, icon: c.icon, idx: c.idx])
        }
        result.isSuccess="Y"
        result.categoryList = categoryList
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getAnnouncementByUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getAnnouncementByUser... $jsonData"
        def email = jsonData.email
        def propId = jsonData.propertyId
        def result = [:], message = '', isSuccess = "Y"

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(propId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            def prop = Property.get(propId)
            def ump = UserMobileProperty.findByUserMobileAndProperty(userMobile, prop)
            if (userMobile == null || prop == null || ump == null) {
                message = "User tidak ditemukan"
                isSuccess = "N"
            } else {
                def roles = []
                //get all roles from user
                RoleMobile.executeQuery("SELECT distinct umu.roleMobile FROM UserMobileUnit umu WHERE umu.unit.area.property.id = :propId AND umu.userMobile.id = :userId",
                        [propId: propId, userId: userMobile.id]).each { rm ->
                    if (!roles.contains(rm.id)) {
                        roles.add(rm.id)
                    }
                }
                RoleMobile.executeQuery("SELECT distinct rjp.security FROM SecurityProperty rjp WHERE rjp.property.id = :propId AND rjp.security.id = :userId",
                        [propId: propId, userId: userMobile.id]).each { rm ->
                    if (!roles.contains(rm.id)) {
                        roles.add(rm.id)
                    }
                }

                if(roles.size()==0){
                    message = "User hasn't been assigned any role yet"
                    isSuccess = "N"
                } else {
                    def sdf = new SimpleDateFormat("yyyy-MM-dd")
                    def currentDate = new Date().clearTime()
                    def announcement = []
                    def baseQuery = "FROM PropertyAnnouncementTarget pat " +
                            "WHERE pat.propertyAnnouncement.property.id = :propId AND pat.roleMobile.id in :roleList " +
                            "AND pat.propertyAnnouncement.periodStart <= :currentDate " +
                            "AND pat.propertyAnnouncement.periodEnd >= :currentDate"
                    def query = "SELECT distinct pat.propertyAnnouncement " + baseQuery + " ORDER BY pat.propertyAnnouncement.createdDate DESC"
                    def queryParams = [propId: propId, currentDate: currentDate, roleList: roles]

                    PropertyAnnouncement.executeQuery(query, queryParams, [max: 4, offset: 0]).each { n ->
                        def detail = [title: n.title, createdDate: sdf.format(n.createdDate), periodStart: sdf.format(n.periodStart), periodEnd: sdf.format(n.periodEnd)]
                        if (ControllerUtil.hasValue(n.imageFileId)) {
                            detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'announcement', action: 'showImage', params: [id: n.imageFileId]))
                        }
                        announcement.add(detail)
                    }
                    result.announcement = announcement
                }
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"

        respond(result)
    }
}
