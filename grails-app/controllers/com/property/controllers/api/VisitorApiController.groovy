package com.property.controllers.api

import com.model.AppConstants
import com.model.general.SecurityProperty
import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.information.InboxMessage
import com.model.property.Property
import com.model.property.PropertyUnit
import com.model.property.VisitorLog
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import com.property.utils.FirebaseUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat

class VisitorApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [createVisitor:'POST',
                             getVisitorList:'POST',
                             updateVisitorStatus:'POST']

    VisitorApiController(){
        super(VisitorLog)
    }

    def createVisitor(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/createVisitor... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def propId = jsonData.propertyId
        def name = jsonData.name
        def visitDate = jsonData.date
        def phoneNo = jsonData.phoneNo
        def vehicleNo = jsonData.vehicleNo
        def picture = jsonData.picture
        def email = jsonData.email
        def unitId = jsonData.unitId

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(name) || !ControllerUtil.hasValue(visitDate) || !ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def prop = Property.get(propId)
            def userMobile = UserMobile.findByEmail(email)

            def isSecurity = isSecurity(userMobile)

            if (prop == null) {
                isSuccess = "N"
                message = "Data properti tidak ditemukan."
            } else {
                VisitorLog.withTransaction { status ->
                    try {
                        def fileId = null
                        if (ControllerUtil.hasValue(picture)) {
                            //create image file
                            fileId = UUID.randomUUID().toString().replace("-", "")
                            def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("visitor${File.separator}images${File.separator}").toString())
                            if (!destDir.exists()) {
                                destDir.mkdirs()
                            }
                            def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("visitor${File.separator}images${File.separator}").concat(fileId)

                            BASE64Decoder decoder = new BASE64Decoder()
                            byte[] decoded = decoder.decodeBuffer(picture)
                            OutputStream stream = new FileOutputStream(destFilePath)
                            stream.write(decoded)
                            stream.flush()
                            stream.close()
                        }

                        def visitor = new VisitorLog()
                        visitor.propertyId = propId
                        visitor.name = name
                        visitor.phoneNo = phoneNo
                        visitor.visitDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(visitDate)
                        visitor.vehicleNo = vehicleNo
                        visitor.image = fileId
                        visitor.logDate = new Date()
                        visitor.email = email

                        def unit = null
                        // When security add visitor, all resident on that unit will get notified
                        if (isSecurity) {
                            unit = PropertyUnit.findById(unitId);
                            visitor.status = AppConstants.VISITOR_STATUS_VISITED

                            UserMobileUnit.findAllByUnit(unit).each {umu ->
                                def inboxMessage = new InboxMessage()
                                inboxMessage.title = "Pengunjung Datang"
                                inboxMessage.content = "Pengunjung anda bernama ${visitor.name} sudah datang."
                                inboxMessage.userMobile = umu.userMobile
                                inboxMessage.save(flush:true)

                                FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, umu.userMobile.id)
                            }

                        } else {
                            unit = UserMobileUnit.findByUserMobile(UserMobile.findByEmail(email)).unit
                            visitor.status = AppConstants.VISITOR_STATUS_NEW
                        }

                        visitor.unitId = unit.id
                        visitor.unit = unit.name
                        visitor.area = unit.area.name
                        visitor.subArea = unit.subArea
                        visitor.createdBy = email
                        visitor.createdDate = new Date()

                        visitor.save(flush: true)

                        isSuccess = "Y"
                        message = "Data berhasil ditambahkan."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getVisitorList(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getVisitorList... $jsonData"

        def sdf = new SimpleDateFormat("dd MMM yy HH:mm")

        def result = [:], visitorList = [], isSuccess = "Y", message = ""
        def propId = jsonData.propertyId
        def email = jsonData.email

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            // check if user is tied to the property
            def umps = UserMobileProperty.executeQuery("FROM UserMobileProperty WHERE userMobile.email = :email AND property.id = :propId",
                    [propId:propId, email:email])
            if(umps.size()==0){
                message = "User tidak ditemukan."
                isSuccess = "N"
            } else {
                def ump = umps.get(0)

                def isSecurity = isSecurity(ump.userMobile)

                def startDate = new Date().clearTime()
                def endDate = new Date().plus(1).clearTime()
                def userQuery = "FROM VisitorLog WHERE propertyId = :propId AND visitDate >= :startDate AND status = :status AND email = :email order by visitDate asc"
                def userParam = [propId: propId, startDate:startDate, status:AppConstants.VISITOR_STATUS_NEW, email: email]

                def query = userQuery
                def param = userParam

                if (isSecurity) {
                    query = "FROM VisitorLog WHERE propertyId = :propId AND visitDate >= :startDate AND visitDate < :endDate AND status = :status  order by visitDate asc"
                    param = [propId: propId, startDate:startDate, endDate: endDate, status:AppConstants.VISITOR_STATUS_NEW]
                }


                VisitorLog.executeQuery(query, param).each { v ->
                    def detail = [recordId: v.id, name: v.name, phoneNo:v.phoneNo, date: sdf.format(v.visitDate),
                                  vehicleNo:v.vehicleNo, visitStatus: v.status, unit: v.unit, area: v.area, subArea: v.subArea]
                    if (ControllerUtil.hasValue(v.image)) {
                        detail.picture = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'visitor', action: 'showImage', params: [id: v.id]))
                    }
                    visitorList.add(detail)
                }
                result.visitors = visitorList
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def updateVisitorStatus(){
        def actionStartTime = System.currentTimeMillis()
        //:todo send notif to user that visited
        def jsonData = request.getJSON()
        log.debug "IN api/property/updateVisitorStatus... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def propId = jsonData.propertyId
        def email = jsonData.email
        def recordId = jsonData.recordId
        def visitStatus = jsonData.visitStatus

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(recordId) || !ControllerUtil.hasValue(visitStatus)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            // check if user is tied to the property
            def umps = UserMobileProperty.executeQuery("FROM UserMobileProperty WHERE userMobile.email = :email AND property.id = :propId",
                    [propId:propId, email:email])
            if(umps.size()==0){
                message = "User tidak ditemukan."
                isSuccess = "N"
            } else {
                UserMobileProperty ump = umps.get(0)

                def isSecurity = isSecurity(ump.userMobile)

                def baseQuery = "FROM VisitorLog WHERE id = :id AND propertyId = :propId "
                def queryParams = [id:recordId, propId: propId]

                if(!isSecurity){
                    baseQuery += " AND email = :email "
                    queryParams.email = email
                }

                def visitors = VisitorLog.executeQuery(baseQuery, queryParams)
                if(visitors.size()==0){
                    message = "Data tidak ditemukan"
                    isSuccess = "N"
                } else {
                    VisitorLog.withTransaction { status ->
                        try {
                            VisitorLog visitor = visitors.get(0)
                            visitor.status = visitStatus
                            visitor.updatedBy = email
                            visitor.updatedDate = new Date()
                            visitor.save(flush:true)
                            isSuccess = "Y"
                            message = "Data berhasil diperbaharui."

                            if (visitStatus.equals(AppConstants.VISITOR_STATUS_VISITED)) {
                                def inboxMessage = new InboxMessage()
                                inboxMessage.title = "Pengunjung Datang"
                                inboxMessage.content = "Pengunjung anda bernama ${visitor.name} sudah datang."
                                inboxMessage.userMobile = ump.userMobile
                                inboxMessage.save(flush:true)

                                FirebaseUtil.sendNotification(inboxMessage.title, inboxMessage.content, ump.userMobile.id)
                            }
                        } catch (Exception e){
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }
        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    boolean isSecurity(UserMobile userMobile) {
        def isSecurity = false

        if(SecurityProperty.findAllBySecurity(userMobile).size()>0){
            isSecurity = true
        }

        return isSecurity;
    }
}
