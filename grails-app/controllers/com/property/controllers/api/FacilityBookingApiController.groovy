package com.property.controllers.api

import com.model.AppConstants
import com.model.general.CodeSequence
import com.model.general.FacilityBooking
import com.model.property.Facility
import com.model.property.Property
import com.model.property.VisitorLog
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat



class FacilityBookingApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator

    def bookingService
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [createBooking:'POST',
                             getBookingList: 'POST',
                             getFacilityByPropertyId: 'POST']

    FacilityBookingApiController(){
        super(VisitorLog)
    }

    def createBooking(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/createBooking... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def email = jsonData.email
        def facilityId = jsonData.facilityId
        def bookDate = jsonData.bookDate
        def startTime = jsonData.startTime
        def endTime = jsonData.endTime
        def description = jsonData.description

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(facilityId) || !ControllerUtil.hasValue(bookDate) || !ControllerUtil.hasValue(startTime) || !ControllerUtil.hasValue(endTime)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        }

        def facility = Facility.get(facilityId)
        def user = UserMobile.findByEmail(email)
        if(user==null){
            isSuccess = "N"
            message = "User tidak ditemukan."
        } else if(facility==null){
            isSuccess = "N"
            message = "Fasilitas tidak ditemukan."
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            def sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm")
            def dtBookDate = sdf.parse(bookDate)
            def start = sdfTime.parse(bookDate+" " +startTime).getTime()
            def end = sdfTime.parse(bookDate+" " +endTime).getTime()
            //check if available
            def isAvailable = bookingService.isBookingAvailable(facilityId, dtBookDate, start, end)
            if(!isAvailable){
                isSuccess = "N"
                message = "Waktu yang anda pilih, sudah dipesan orang lain."
            } else {
                FacilityBooking.withTransaction { status ->
                    try {
                        def seq = CodeSequence.get(AppConstants.CODE_SEQUENCE_FACILITY_BOOKING)
                        if (seq == null) {
                            seq = new CodeSequence()
                            seq.id = AppConstants.CODE_SEQUENCE_FACILITY_BOOKING
                            seq.seq = 1
                            seq.save(flush: true)
                        }
                        def code = AppConstants.CODE_SEQUENCE_FACILITY_BOOKING + "-" + ControllerUtil.padLeft(5, "0", seq.seq)

                        def booking = new FacilityBooking()
                        booking.code = code
                        booking.facility = facility
                        booking.startDate = dtBookDate
                        booking.startTime = start
                        booking.endTime = end
                        booking.description = description
                        booking.status = AppConstants.BOOKING_STATUS_NEW
                        booking.createdBy = email
                        booking.save()

                        seq.seq = seq.seq + 1
                        seq.save()

                        isSuccess = "Y"
                        message = "Pesanan berhasil dengan kode booking: ${code}"
                        result.code = code
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getBookingList(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getBookingList... $jsonData"
        def result = [:], visitorList = [], isSuccess = "Y", message = ""

        def facilityId = jsonData.facilityId
        def startDate = jsonData.startDate
        def endDate = jsonData.endDate

        if(!ControllerUtil.hasValue(facilityId) || !ControllerUtil.hasValue(startDate) || !ControllerUtil.hasValue(endDate)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def facility = Facility.get(facilityId)
            if(facility == null){
                message = "Fasilitas tidak ditemukan"
                isSuccess = "N"
            } else if(!securityService.validateApiTime(jsonData)){
                message = "Koneksi bermasalah, silahkan coba lagi."
                isSuccess = "N"
            }  else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def sdfDate = new SimpleDateFormat("dd MMM yy")
                def sdfTime = new SimpleDateFormat("HH:mm")
                def sdt = sdf.parse(startDate)
                def edt = sdf.parse(endDate) + 1
                def statusList = [AppConstants.BOOKING_STATUS_NEW, AppConstants.BOOKING_STATUS_OPEN, AppConstants.BOOKING_STATUS_BOOKED, AppConstants.BOOKING_STATUS_CLOSED]
                def bookingList = []
                FacilityBooking.executeQuery("FROM FacilityBooking WHERE facility.id = :facilityId AND status in :statusList AND startDate BETWEEN :startDate AND :endDate",
                        [facilityId:facilityId, startDate:sdt, endDate:edt, statusList:statusList]).each{ obj ->
                    bookingList.add([bookDate: sdfDate.format(obj.startDate), startTime: sdfTime.format(obj.startTime), endTime:sdfTime.format(obj.endTime),
                                     code:obj.code, status:obj.status, description:obj.description])
                }
                result.bookingList = bookingList
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getFacilityByPropertyId(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getFacilityByPropertyId... $jsonData"
        def result = [:], facilityList = [], isSuccess = "Y", message = ""

        def propertyId = jsonData.propertyId

        if(!ControllerUtil.hasValue(propertyId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def prop = Property.get(propertyId)
            if(prop == null){
                message = "Data properti tidak ditemukan"
                isSuccess = "N"
            } else {
                Facility.executeQuery("FROM Facility WHERE area.property.id = :propId", [propId:propertyId]).each{ obj ->
                    def detail = [id:obj.id, name: obj.name, areaId: obj.area.id, areaName: obj.area.name, tnc: obj.tnc]
                    if (ControllerUtil.hasValue(obj.imageFileId)) {
                        detail.picture = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'facility', action: 'showImage', params: [id: obj.imageFileId]))
                    }
                    facilityList.add(detail)
                }
                result.facilityList = facilityList
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }
}
