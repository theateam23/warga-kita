package com.property.controllers.api

import com.model.AppConstants
import com.model.information.InboxMessage
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import org.apache.commons.lang.exception.ExceptionUtils

import java.text.SimpleDateFormat

class InboxMessageApiController extends RestfulController{

    static responseFormats = ['json']
    static namespace = 'api'
    def securityService

    static allowedMethods = [getMessagesByUser:'POST',
                             deleteMessageById:'POST',
                             deleteMessageByListId:'POST',
                             readMessageById:'POST',
                             readMessageByListId:'POST',
                             getMessagesSummaryByUser:'POST']

    InboxMessageApiController(){
        super(InboxMessage)
    }

    def getMessagesByUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getMessagesByUser... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def email = jsonData.email

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def user = UserMobile.findByEmail(email)
            if (user == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                def messages = [], unread = 0

                InboxMessage.findAllByUserMobileAndIsDelete(user, AppConstants.FLAG_NO,[sort: "createdDate", order: "desc"]).each { obj ->
                    messages.add([id: obj.id, title: obj.title, isRead: obj.isRead, content: obj.content, dateTime: sdf.format(obj.createdDate)])
                    if(obj.isRead == AppConstants.FLAG_NO){
                        unread++
                    }
                }
                result.messages = messages
                result.unread = unread
                isSuccess = "Y"
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getMessagesSummaryByUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getMessagesSummaryByUser... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def email = jsonData.email

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def user = UserMobile.findByEmail(email)
            if (user == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                def total = 0
                def unread = 0

                InboxMessage.findAllByUserMobileAndIsDelete(user, AppConstants.FLAG_NO).each { obj ->
                    if(obj.isRead == AppConstants.FLAG_NO){
                        unread++
                    }
                    total++
                }
                result.total = total
                result.unread = unread
                isSuccess = "Y"
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def deleteMessageById(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/deleteMessageById... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def messageId = jsonData.messageId

        if(!ControllerUtil.hasValue(messageId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def obj = InboxMessage.findById(messageId)
            if(obj==null){
                message = "Pesan tidak ditemukan."
                isSuccess = "N"
            } else {
                InboxMessage.withTransaction { status ->
                    try {
                        obj.isDelete = AppConstants.FLAG_YES
                        obj.save()
                        isSuccess = "Y"
                        message = "Pesan berhasil dihapus."
                    } catch (Exception e){
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def deleteMessageByListId(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/deleteMessageByListId... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def messageIdList = jsonData.messageIdList

        if(!ControllerUtil.hasValue(messageIdList)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            messageIdList.each { messageId ->
                def obj = InboxMessage.findById(messageId)
                if(obj==null){
                    message = "Pesan tidak ditemukan."
                    isSuccess = "N"
                } else {
                    InboxMessage.withTransaction { status ->
                        try {
                            obj.isDelete = AppConstants.FLAG_YES
                            obj.save()
                            isSuccess = "Y"
                            message = "Pesan berhasil dihapus."
                        } catch (Exception e){
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }

        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }



    def readMessageById(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/readMessageById... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def messageId = jsonData.messageId

        if(!ControllerUtil.hasValue(messageId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def obj = InboxMessage.findById(messageId)
            if(obj==null){
                message = "Pesan tidak ditemukan"
                isSuccess = "N"
            } else {
                InboxMessage.withTransaction { status ->
                    try {
                        obj.isRead = AppConstants.FLAG_YES
                        obj.save()
                        isSuccess = "Y"
                        message = "Pesan sudah dibaca."
                    } catch (Exception e){
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def readMessageByListId(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/readMessageByListId... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def messageIdList = jsonData.messageIdList

        if(!ControllerUtil.hasValue(messageIdList)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {

            def obj = InboxMessage.findById(messageId)
            if(obj==null){
                message = "Pesan tidak ditemukan"
                isSuccess = "N"
            } else {
                messageIdList.each { messageId ->
                    InboxMessage.withTransaction { status ->
                        try {
                            obj.isRead = AppConstants.FLAG_YES
                            obj.save()
                            isSuccess = "Y"
                            message = "Pesan sudah dibaca."
                        } catch (Exception e){
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }
        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

}
