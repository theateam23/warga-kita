package com.property.controllers.api

import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.maintenance.VehicleType
import com.model.property.Vehicle
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat

class VehicleApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [getVehicleByUser:'POST',
                             getVehicle:'POST',
                             createVehicle:'POST',
                             updateVehicle:'POST',
                             deleteVehicle:'POST',
                             getVehicleByPlateNo:'POST']

    VehicleApiController(){
        super(Vehicle)
    }

    def getVehicleByUser() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getVehicleByUser... $jsonData"
        def result = [:], message = "", isSuccess = "Y"

        def email = jsonData.email

        if(!ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            if(userMobile==null){
                message = "User tidak ditemukan."
                isSuccess = "N"
            } else {
                def vehicleList = []
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                Vehicle.executeQuery("SELECT distinct v FROM Vehicle v, UserMobileUnit umu WHERE v.unit.id = umu.unit.id AND umu.userMobile.email = :email ",[email:email]).each{ obj ->
                    def detail = [:]
                    detail.uid = obj.id
                    detail.unitId = obj.unit.id
                    detail.plateNo = obj.plateNo
                    if (obj.taxDueDate != null) {
                        detail.taxDueDate = sdf.format(obj.taxDueDate)
                    }
                    detail.vehicleType = obj.vehicleType?.code
                    detail.color = obj.color
                    detail.brand = obj.brand
                    if (ControllerUtil.hasValue(obj.imageFileId)) {
                        detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'vehiclePm', action: 'showImage', params: [id: obj.imageFileId]))
                    }
                    detail.imageFileName = obj.imageFileName
                    vehicleList.add(detail)
                }
                result.vehicleList = vehicleList
            }
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getVehicle(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getVehicle... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def vehicleId = jsonData.vehicleId

        if(!ControllerUtil.hasValue(vehicleId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def obj = Vehicle.get(vehicleId)
            if (obj == null) {
                isSuccess = "N"
                message = "Data tidak ditemukan."
            } else {
                def sdf = new SimpleDateFormat("yyyy-MM-dd")
                def detail = [:]
                detail.unitId = obj.unit.id
                detail.plateNo = obj.plateNo
                if (obj.taxDueDate != null) {
                    detail.taxDueDate = sdf.format(obj.taxDueDate)
                }
                detail.vehicleType = obj.vehicleType?.code
                detail.color = obj.color
                detail.brand = obj.brand
                if (ControllerUtil.hasValue(obj.imageFileId)) {
                    detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'vehiclePm', action: 'showImage', params: [id: obj.imageFileId]))
                }
                detail.imageFileName = obj.imageFileName
                result.vehicle = detail
                isSuccess = "Y"
                message = "Data ditemukan."
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def createVehicle(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/createVehicle... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def userEmail = jsonData.userEmail
        def plateNo = jsonData.plateNo
        def vehicleType = jsonData.vehicleType
        def brand = jsonData.brand
        def color = jsonData.color
        def taxDueDate = jsonData.taxDueDate
        def imageFile = jsonData.imageFile
        def imageFileName = jsonData.imageFileName

        if(!ControllerUtil.hasValue(userEmail) || !ControllerUtil.hasValue(plateNo) || !ControllerUtil.hasValue(vehicleType)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def userMobile = UserMobile.findByEmail(userEmail)
            if (userMobile == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def unit = UserMobileUnit.findByUserMobile(userMobile)?.unit
                if (unit == null) {
                    isSuccess = "N"
                    message = "Unit tidak ditemukan."
                } else {
                    Vehicle.withTransaction { status ->
                        try {
                            def sdf = new SimpleDateFormat("yyyy-MM-dd")
                            def obj = new Vehicle()
                            obj.unit = unit
                            obj.plateNo = plateNo
                            obj.plateNoSearch = String.valueOf(plateNo).replace(" ", "")
                            if (ControllerUtil.hasValue(taxDueDate)) {
                                obj.taxDueDate = sdf.parse(taxDueDate)
                            }
                            obj.brand = brand
                            obj.color = color
                            obj.vehicleType = VehicleType.findByCode(vehicleType)

                            def fileId = null
                            if (ControllerUtil.hasValue(imageFile)) {
                                //create image file
                                fileId = UUID.randomUUID().toString().replace("-", "")
                                def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").toString())
                                if(!destDir.exists()){
                                    destDir.mkdirs()
                                }
                                def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(fileId)

                                BASE64Decoder decoder = new BASE64Decoder()
                                byte[] decoded = decoder.decodeBuffer(imageFile)
                                OutputStream stream = new FileOutputStream(destFilePath)
                                stream.write(decoded)
                                stream.flush()
                                stream.close()
                            }
                            obj.imageFileId = fileId
                            obj.imageFileName = imageFileName
                            obj.save(flush: true)

                            isSuccess = "Y"
                            message = "Data berhasil ditambahkan."
                        } catch (Exception e) {
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                            isSuccess = "N"
                            message = "Server bermasalah, coba lagi beberapa saat."
                        }
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def updateVehicle(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/updateVehicle... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def vehicleId = jsonData.vehicleId
        def plateNo = jsonData.plateNo
        def vehicleType = jsonData.vehicleType
        def brand = jsonData.brand
        def color = jsonData.color
        def taxDueDate = jsonData.taxDueDate
        def imageFile = jsonData.imageFile
        def imageFileName = jsonData.imageFileName

        if(!ControllerUtil.hasValue(vehicleId) || !ControllerUtil.hasValue(plateNo) || !ControllerUtil.hasValue(vehicleType)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def obj = Vehicle.get(vehicleId)
            if (obj == null) {
                isSuccess = "N"
                message = "Data tidak ditemukan."
            } else {
                Vehicle.withTransaction { status ->
                    try {
                        def sdf = new SimpleDateFormat("yyyy-MM-dd")
                        obj.plateNo = plateNo
                        obj.plateNoSearch = String.valueOf(plateNo).replace(" ", "")
                        if (ControllerUtil.hasValue(taxDueDate)) {
                            obj.taxDueDate = sdf.parse(taxDueDate)
                        }
                        obj.brand = brand
                        obj.color = color
                        obj.vehicleType = VehicleType.findByCode(vehicleType)

                        def fileId = null
                        if (ControllerUtil.hasValue(imageFile)) {
                            //delete old image if exists
                            if(ControllerUtil.hasValue(obj.imageFileId)) {
                                def oldImage = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(obj.imageFileId))
                                if (oldImage.exists()) oldImage.delete()
                            }
                            //create image file
                            fileId = UUID.randomUUID().toString().replace("-", "")
                            def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").toString())
                            if(!destDir.exists()){
                                destDir.mkdirs()
                            }
                            def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(fileId)

                            BASE64Decoder decoder = new BASE64Decoder()
                            byte[] decoded = decoder.decodeBuffer(imageFile)
                            OutputStream stream = new FileOutputStream(destFilePath)
                            stream.write(decoded)
                            stream.flush()
                            stream.close()

                            obj.imageFileId = fileId
                            obj.imageFileName = imageFileName
                        }

                        obj.save(flush: true)

                        isSuccess = "Y"
                        message = "Data berhasil diperbaharui."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def deleteVehicle(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/deleteVehicle... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def vehicleId = jsonData.vehicleId

        if(!ControllerUtil.hasValue(vehicleId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def obj = Vehicle.get(vehicleId)
            if (obj == null) {
                isSuccess = "N"
                message = "Data tidak ditemukan."
            } else {
                //delete image if exists
                if(ControllerUtil.hasValue(obj.imageFileId)) {
                    def image = new File(grailsApplication.config.getProperty("file.upload.path").concat("vehicle${File.separator}images${File.separator}").concat(obj.imageFileId))
                    if (image.exists()) image.delete()
                }
                obj.delete(flush:true)
                isSuccess = "Y"
                message = "Data berhasil dihapus."
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getVehicleByPlateNo(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getVehicleByPlateNo... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def plateNo = jsonData.name
        def email = jsonData.email

        if(!ControllerUtil.hasValue(plateNo) || !ControllerUtil.hasValue(email)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        } else {
            def userMobile = UserMobile.findByEmail(email)
            if (userMobile == null) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def propId = UserMobileProperty.findByUserMobile(userMobile).property.id
                def plateNoSearch = String.valueOf(plateNo).replace(" ", "")
                def obj = Vehicle.executeQuery("FROM Vehicle WHERE unit.area.property.id = :propId AND UPPER(plateNoSearch) = UPPER(:plateNoSearch)",
                [propId: propId, plateNoSearch: plateNoSearch])

                if (obj == null || obj.size() == 0) {
                    isSuccess = "N"
                    message = "Data tidak ditemukan."
                } else {
                    isSuccess = "Y"
                    message = "Data ditemukan."
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

}
