package com.property.controllers.api

import com.model.AppConstants
import com.model.general.SecurityProperty
import com.model.general.UserMobileProperty
import com.model.general.UserMobileUnit
import com.model.information.InboxMessage
import com.model.maintenance.*
import com.model.property.*
import com.model.security.UserMobile
import com.property.utils.ControllerUtil
import com.property.utils.FirebaseUtil
import grails.core.GrailsApplication
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.rest.RestfulController
import grails.web.mapping.LinkGenerator
import org.apache.commons.lang.exception.ExceptionUtils
import sun.misc.BASE64Decoder

import java.text.SimpleDateFormat

class MobileApiController extends RestfulController{

    LinkGenerator grailsLinkGenerator
    def mailService
    GrailsApplication grailsApplication
    def securityService

    static responseFormats = ['json']
    static namespace = 'api'

    static allowedMethods = [
                             getProperties:'POST',
                             getPropertiesByUser:'POST',
                             getAreasByProperty:'POST',
                             getSubAreaByAreaId:'POST',
                             getUnitByPropertyAndSubArea:'POST',
                             pushNotification: 'POST',
                             getSubAreaByProperty: 'POST',
                             submitFeedback: 'POST',
                             getRequestFormList: 'POST']

    MobileApiController(){
        super(UserMobile)
    }

    def getProperties(){
        def actionStartTime = System.currentTimeMillis()
        log.debug "IN api/property/getProperties..."
        def result = [:], propList = []
        Property.findAllByIsPrivate(AppConstants.FLAG_NO).each { prop ->
            propList.add([id:prop.id, name:prop.name])
        }
        result.isSuccess="Y"
        result.property = propList
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getPropertiesByUser(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getPropertyByUser... $jsonData"
        def email = jsonData.email
        def result = [:], isSuccess = "Y", message = ""

        def userMobile = UserMobile.findByEmail(email)
        if(userMobile==null){
            isSuccess = "N"
            message = "User tidak ditemukan"
        } else {
            def prop = null
            def detail = [:]

            def ump = UserMobileProperty.findAllByUserMobile(userMobile).get(0)
            prop = ump.property

            if (!isSecurity(userMobile)) {
                def umu = UserMobileUnit.findByUserMobile(userMobile)
                if (umu == null) {
                    isSuccess = "N"
                    message = "Unit not found for this user"
                } else {
                    detail.areaName = umu.unit.area.name
                    detail.areaId = umu.unit.area.id
                    detail.unitName = umu.unit.name
                    detail.unitId = umu.unit.id
                }
            }

            detail.id = prop.id
            detail.name = prop.name
            detail.propertyType = prop.propertyType.name

//            detail.securityPhoneNumber = ControllerUtil.hasValue(prop.securityPhoneNo)?prop.securityPhoneNo.split(AppConstants.SECURITY_PHONE_DELIMITER):[]
            if (ControllerUtil.hasValue(prop.mobileBackground)) {
                detail.imageUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'propertyAdmin', action: 'showImage', params: [id: prop.mobileBackground]))
            }
            def securityList = []
            SecurityProperty.findAllByPropertyAndIsDefault(prop, AppConstants.FLAG_YES).each{ sp ->
                securityList.add([name:sp.security.name, phoneNo: sp.security.phoneNo])
            }

            detail.securityList = securityList
            result.property = detail

        }
        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getAreasByProperty() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getAreasByProperty... $jsonData"
        def result = [:], message = "", isSuccess = "Y"

        def propId = jsonData.propertyId

        if(!ControllerUtil.hasValue(propId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def areaList = []
            PropertyUnit.executeQuery("FROM PropertyArea WHERE property.id = :propId",[propId:propId]).each{ obj ->
                areaList.add([id:obj.id, name:obj.name])
            }
            result.areaList = areaList
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getUnitByPropertyAndSubArea() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getUnitByPropertyAndSubArea ... $jsonData"
        def result = [:], message = "", isSuccess = "Y"

        def propId = jsonData.propertyId
        def subArea = jsonData.subArea

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(subArea)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def unitList = []
            PropertyUnit.executeQuery("FROM PropertyUnit WHERE area.property.id = :propId AND subArea = :subArea",[propId:propId, subArea:subArea]).each{ obj ->
                unitList.add([id:obj.id, name:obj.name, unitType: obj.unitType?.name, subArea: obj.subArea])
            }
            result.unitList = unitList
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getSubAreaByAreaId() {
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/getSubAreaByAreaId... $jsonData"
        def result = [:], message = "", isSuccess = "Y"

        def areaId = jsonData.areaId

        if(!ControllerUtil.hasValue(areaId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def subAreaList = []
            PropertyUnit.executeQuery("SELECT DISTINCT subArea FROM PropertyUnit WHERE area.id = :areaId",[areaId:areaId]).each{ obj ->
                subAreaList.add(obj)
            }
            result.subAreaList = subAreaList
        }
        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def submitFeedback(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/submitFeedback... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def propId = jsonData.propertyId
        def userEmail = jsonData.email
        def title = jsonData.title
        def description = jsonData.description
        def attachment = jsonData.attachment
        def fileName = UUID.randomUUID().toString().concat(".jpg")

        if(!ControllerUtil.hasValue(propId) || !ControllerUtil.hasValue(userEmail) || !ControllerUtil.hasValue(title)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def ump = UserMobileProperty.executeQuery("FROM UserMobileProperty WHERE property.id = :propId AND userMobile.email = :userEmail",
                        [propId:propId, userEmail:userEmail])
            if (ump.size() == 0) {
                isSuccess = "N"
                message = "User tidak ditemukan."
            } else {
                def userMobile = ump.get(0)
                UserFeedback.withTransaction { status ->
                    try {
                        def obj = new UserFeedback()
                        obj.userMobileProperty = userMobile
                        obj.title = title
                        obj.description = description

                        def fileId = null
                        if (ControllerUtil.hasValue(attachment)) {
                            //create file
                            fileId = UUID.randomUUID().toString().replace("-", "")
                            def destDir = new File(grailsApplication.config.getProperty("file.upload.path").concat("feedback${File.separator}attachment${File.separator}").toString())
                            if(!destDir.exists()){
                                destDir.mkdirs()
                            }
                            def destFilePath = grailsApplication.config.getProperty("file.upload.path").concat("feedback${File.separator}attachment${File.separator}").concat(fileId)

                            BASE64Decoder decoder = new BASE64Decoder()
                            byte[] decoded = decoder.decodeBuffer(attachment)
                            OutputStream stream = new FileOutputStream(destFilePath)
                            stream.write(decoded)
                            stream.flush()
                            stream.close()
                        }
                        obj.fileId = fileId
                        obj.fileName = fileName
                        obj.save(flush: true)

                        isSuccess = "Y"
                        message = "Data berhasil ditambahkan."
                    } catch (Exception e) {
                        log.error ExceptionUtils.getStackTrace(e)
                        status.setRollbackOnly()
                        isSuccess = "N"
                        message = "Server bermasalah, coba lagi beberapa saat."
                    }
                }
            }
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def getRequestFormList(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData = request.getJSON()
        log.debug "IN api/property/getRequestFormList... $jsonData"
        def result = [:], isSuccess = "", message = ""

        def propId = jsonData.propertyId

        if(!ControllerUtil.hasValue(propId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else {
            def prop = Property.get(propId)
            def forms = []
            PropertyRequest.findAllByProperty(prop).each{ obj ->
                def detail = [:]
                detail.id = obj.id
                detail.title = obj.title
                detail.description = obj.description
                detail.fileName = obj.fileName
                if (ControllerUtil.hasValue(obj.fileId)) {
                    detail.fileUrl = grailsLinkGenerator.serverBaseURL.concat(g.createLink(controller: 'propertyRequest', action: 'downloadFile', params: [id: obj.fileId]))
                }
                forms.add(detail)
            }
            result.formList = forms
            isSuccess = "Y"
        }

        result.isSuccess = isSuccess
        result.message = message
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    def pushNotification(){
        def actionStartTime = System.currentTimeMillis()
        def jsonData =  request.getJSON()
        log.debug "IN api/property/pushNotification... $jsonData"
        def result = [:]

        def email = jsonData.email
        def propId = jsonData.propertyId
        def isSuccess = "Y", message

        if(!ControllerUtil.hasValue(email) || !ControllerUtil.hasValue(propId)){
            message = "Missing mandatory fields"
            isSuccess = "N"
        } else if(!securityService.validateApiTime(jsonData)){
            message = "Koneksi bermasalah, silahkan coba lagi."
            isSuccess = "N"
        }  else {
            def alertMessage = "", title = "Security Alert"
            def userMobile = UserMobile.findByEmail(email)
            def prop = Property.get(propId)
            def umus = UserMobileUnit.executeQuery("FROM UserMobileUnit where userMobile.email = :email AND unit.area.property.id = :propId",
                    [email:email, propId:propId]).collect{it.unit.name}.unique()
            if(umus.size()==0){
                alertMessage = "${userMobile.name} needs help"
            } else {
                alertMessage = "${userMobile.name} on unit ${umus.join(", ")} needs help"
            }

            def response = FirebaseUtil.sendNotification(title, alertMessage, "security~${propId}")

            if (!response.isSuccess) {
                message = response.message
                isSuccess = "N"
            } else {
                isSuccess = "Y"
                //create message for security
                SecurityProperty.findAllByProperty(prop).each { rjp ->
                    InboxMessage.withTransaction{ status ->
                        try {
                            def inboxMessage = new InboxMessage()
                            inboxMessage.title = title
                            inboxMessage.content = alertMessage
                            inboxMessage.userMobile = rjp.security
                            inboxMessage.save()
                        } catch (Exception e){
                            log.error ExceptionUtils.getStackTrace(e)
                            status.setRollbackOnly()
                        }
                    }
                }
            }


        }

        result.message = message
        result.isSuccess = isSuccess
        log.debug "Response time ${controllerName} - ${actionName}: ${System.currentTimeMillis() - actionStartTime} ms"
        respond(result)
    }

    boolean isSecurity(UserMobile userMobile) {
        def isSecurity = false

        if(SecurityProperty.findAllBySecurity(userMobile).size()>0){
            isSecurity = true
        }

        return isSecurity;
    }
}
