package com.property.interceptors

import com.model.AppConstants


class MenuInterceptor {

    def menuService
    def springSecurityService

    public MenuInterceptor(){
        matchAll().excludes(controller: 'auth')
                .excludes(controller: 'profile')
                .excludes(controller: 'login')
                .excludes(controller: 'logout')
                .excludes(controller: 'error')
                .excludes(controller: 'mobileApi')
                .excludes(controller: 'facilityBookingApi')
                .excludes(controller: 'homeGuardApi')
                .excludes(controller: 'memberApi')
                .excludes(controller: 'inboxMessageApi')
                .excludes(controller: 'newsAnnouncementApi')
                .excludes(controller: 'userMobileApi')
                .excludes(controller: 'vehicleApi')
                .excludes(controller: 'visitorApi')
    }

    boolean before() {
        def currentMenu = null
        if(springSecurityService.isLoggedIn()){
            def act = actionName
            if(act == null){
                act = 'index'
            }
            currentMenu = menuService.getSelectedMenuByPath(controllerName,act)
            if(currentMenu!=null){
                def principal = springSecurityService.principal
                if(principal.menuList.contains(currentMenu.id)) {
                    session.setAttribute(AppConstants.SESSION_MENU_ID, currentMenu.id)
                } else {
                    chain controller: 'error', params:[status: '404']
                    return false
                }
            } else if(session.getAttribute(AppConstants.SESSION_MENU_ID)==null){
                session.setAttribute(AppConstants.SESSION_MENU_ID,menuService.getDashboard().id)
            }
            true
        }
        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
