package warga.kita

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.ExportedUserRecord
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ListUsersPage
import com.google.firebase.auth.UserRecord
import com.model.security.Role
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserMobile
import com.model.security.UserRole
import com.property.utils.ControllerUtil
import com.property.utils.FirebaseUtil
import grails.util.Environment

class BootStrap {

    def quartzScheduler
    def grailsApplication

    def init = { servletContext ->
        println ("==============================[check] This application is running on ${Environment.getCurrent()} environment.")
        if(Role.list().size()==0) {
            Role.withTransaction { status -> 
                try {
                    def fanny = new User(username: 'fang2here@gmail.com', password: 'p@ssw0rd').save()
                    new UserInfo(name: 'Fanny Tanzil', address: 'Jakarta', phoneNo: '082111678358', user: fanny, title: '', pmId: '').save()

                    def jo = new User(username: 'joklara@yahoo.com', password: 'p@ssw0rd').save()
                    new UserInfo(name: 'Josephine Klara', address: 'Jakarta', phoneNo: '087801251987', user: jo, title: '', pmId: '').save()

                    def sall = new User(username: 'sally8386@gmail.com', password: 'p@ssw0rd').save()
                    new UserInfo(name: 'Sally Leman', address: 'Jakarta', phoneNo: '081908664110', user: sall, title: '', pmId: '').save()

                    def arif = new User(username: 'arif.rachman.h@gmail.com', password: 'p@ssw0rd').save()
                    new UserInfo(name: 'Josephine Klara', address: 'Jakarta', phoneNo: '083831888877', user: arif, title: '', pmId: '').save()

                    def oky = new User(username: 'pristantyo@', password: 'p@ssw0rd').save()
                    new UserInfo(name: 'Oky Ardian Pristantyo', address: 'Jakarta', phoneNo: '085641487915', user: oky, title: '', pmId: '').save()

                    def roleAdmin = new Role(authority: 'ROLE_ADMIN', name: 'Admin', idx: 1)
                    roleAdmin.id = 'ROLE_ADMIN'
                    roleAdmin.save()
                    def rolePM = new Role(authority: 'ROLE_PM', name: 'Property Manager', idx: 2)
                    rolePM.id = 'ROLE_PM'
                    rolePM.save()
                    def rolePA = new Role(authority: 'ROLE_PA', name: 'Property Admin', idx: 3)
                    rolePA.id = 'ROLE_PA'
                    rolePA.save()
                    def roleS = new Role(authority: 'ROLE_SALES', name: 'Property Sales', idx: 4)
                    roleS.id = 'ROLE_SALES'
                    roleS.save()

                    new UserRole(user: fanny, role: roleAdmin).save()
                    new UserRole(user: jo, role: roleAdmin).save()
                    new UserRole(user: arif, role: roleAdmin).save()
                    new UserRole(user: sall, role: roleAdmin).save()
                    new UserRole(user: oky, role: roleAdmin).save()
                } catch (Exception e){
                    status.setRollbackOnly()
                }
            }
        }

        def actionStartTime = System.currentTimeMillis()
        String configFile = "wargaku-ad2cd-firebase.json", dbUrl = "https://wargaku-ad2cd.firebaseio.com"
        //String config = "property-e4e18-firebase.json", dbUrl = "https://property-e4e18.firebaseio.com"

        URL resource = this.class.classLoader.getResource(configFile);
        File file = new File(resource.toURI());
        FileInputStream serviceAccount = new FileInputStream(file)

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl(dbUrl)
                .build();

        FirebaseApp.initializeApp(options)

        println ("==============================[check] Response time firebase initiating for : ${System.currentTimeMillis() - actionStartTime} ms\n")

        // TEST FIREBASE
        // println(FirebaseUtil.sendNotification("Fire SDK", "Yaay its works", "2c9f94d86537e20201653ccce47c0018"))


        /* def actionStartTime = System.currentTimeMillis()
        UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail("user@example.com")
                .setEmailVerified(false)
                .setPassword("secretPassword")
                .setDisabled(false);
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request)
            System.out.println("Successfully created new user: ${userRecord.getUid()} for ${System.currentTimeMillis() - actionStartTime} ms");
        } catch (com.google.firebase.auth.FirebaseAuthException e) {
            println(e.errorCode)
        } */

        /*
        ListUsersPage testPage = FirebaseAuth.getInstance().listUsers(null)
        while (testPage != null) {
            println("Total user firebase ${testPage.getValues().size()} user")
            testPage = testPage.getNextPage();
        }
        */
        // Use this if there is some UserMobile.uid value is null
        /*if(Environment.PRODUCTION == Environment.getCurrent()) {
            // delete User Firebase that not in DB
            ListUsersPage page = FirebaseAuth.getInstance().listUsers(null)
            while (page != null) {
                for (ExportedUserRecord user : page.getValues()) {
                    //System.out.println("User: ${user.getUid()} : ${user.email}");
                    def um = UserMobile.findByEmail(user.email)
                    if (um != null) {
                        um.setUid(user.getUid())
                        um.save(flush:true)
                    } else {
                        println("deleting user ${user.email}")
                        FirebaseUtil.deleteUser(user.getUid())
                    }
                }
                page = page.getNextPage();
            }
        }
        */

        quartzScheduler.start()
    }
    def destroy = {
    }
}