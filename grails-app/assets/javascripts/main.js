
$(function() {
    $.validator.addMethod('greaterThanZero', function (value, el, param) {
        if(value!=null && value!='') {
            return value > 0;
        }
        return true;
    },'Cannot be less than zero');

});