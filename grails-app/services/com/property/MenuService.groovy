package com.property

import com.model.AppConstants
import com.model.general.PropertyAdminProperty
import com.model.property.Property
import com.model.security.Menu
import com.model.security.User
import grails.gorm.transactions.Transactional

@Transactional
class MenuService {

    def getParentMenuList(roles){
        return Menu.executeQuery("SELECT DISTINCT rm.menu FROM RoleMenu rm WHERE rm.role.authority IN :roles AND rm.menu.parentMenu IS NULL",[roles:roles]).sort{it.idx}
    }

    def getSubmenuList(parentMenu,roles,userId){
        def user = User.get(userId)
        def prop = PropertyAdminProperty.findByPropertyAdmin(user)?.property
        if(prop!=null) {
            def excludedMenus = []
            if(AppConstants.PAGUYUBAN.equals(prop.propertyType.name)){
                excludedMenus.add("VISITOR_LIST")
                excludedMenus.add("MAINTAIN_FACILITY")
                excludedMenus.add("BOOKING_FACILITY")
                excludedMenus.add("MANAGE_BOOKING_FACILITY")
            } else if(AppConstants.PERUMAHAN.equals(prop.propertyType.name)){
                excludedMenus.add("MAINTAIN_FACILITY")
                excludedMenus.add("BOOKING_FACILITY")
                excludedMenus.add("MANAGE_BOOKING_FACILITY")
            }
            if(excludedMenus.size()==0) {
                return Menu.executeQuery("SELECT DISTINCT rm.menu FROM RoleMenu rm WHERE rm.role.authority IN :roles AND rm.menu.parentMenu IS NOT NULL AND rm.menu.parentMenu.id=:parentMenuId",
                        [roles: roles, parentMenuId: parentMenu.id]).sort { it.idx }
            } else {
                return Menu.executeQuery("SELECT DISTINCT rm.menu FROM RoleMenu rm WHERE rm.menu.id NOT IN :excludedMenus AND rm.role.authority IN :roles AND rm.menu.parentMenu IS NOT NULL AND rm.menu.parentMenu.id=:parentMenuId",
                        [roles: roles, parentMenuId: parentMenu.id, excludedMenus:excludedMenus]).sort { it.idx }
            }
        } else {
            return []
        }

    }

    def getMenuList(Menu selectedMenu,roles) {
        if(selectedMenu.parentMenu==null){
            return Menu.executeQuery("SELECT DISTINCT rm.menu FROM RoleMenu rm WHERE rm.role.authority IN :roles AND rm.menu.parentMenu IS NULL",[roles:roles]).sort{it.idx}
        } else {
            return Menu.executeQuery("SELECT DISTINCT rm.menu FROM RoleMenu rm WHERE rm.role.authority IN :roles AND rm.menu.parentMenu IS NOT NULL AND rm.menu.parentMenu.id=:parentMenuId",
                    [roles: roles, parentMenuId: selectedMenu.parentMenu.id]).sort { it.idx }
        }
    }

    def getSelectedMenu(String menuId){
        def selected = Menu.get(menuId)
        if(selected==null){
            selected = getDashboard()
        }
        return selected
    }

    def getDashboard(){
        return Menu.findByControllerPathAndActionName('dashboard','index')
    }

    def getSelectedMenuByPath(String controllerPath, String actionName){
        return Menu.findByControllerPathAndActionName(controllerPath,actionName)
    }
}

