package com.property

import com.model.AppConstants
import com.model.general.FacilityBooking
import com.model.property.Property
import com.model.security.User
import com.model.security.UserInfo
import grails.gorm.transactions.Transactional

@Transactional
class SalesService {

    private static Integer SILVER_THRESHOLD = 10
    private static Integer GOLD_THRESHOLD = 100

    public String getSalesRank(String salesId){
        def sales = User.get(salesId)
        def salesInfo = UserInfo.findByUser(sales)
        def dealSecured = Property.findAllBySales(salesInfo).size()
	
        if(dealSecured==0){
            return AppConstants.SALES_NONE
        } else if(dealSecured > 0 && dealSecured < SILVER_THRESHOLD){
            return AppConstants.SALES_SILVER
        } else if(dealSecured >= SILVER_THRESHOLD && dealSecured < GOLD_THRESHOLD){
            return AppConstants.SALES_GOLD
        } else if(dealSecured >= GOLD_THRESHOLD){
            return AppConstants.SALES_PLATINUM
        }
    }

    public def calculateCommission(def fee, def ranking){
        if(AppConstants.SALES_NONE.equals(ranking)){
            return 0.0
        } else if(AppConstants.SALES_SILVER.equals(ranking)){
            return fee*0.15
        } else if(AppConstants.SALES_GOLD.equals(ranking)){
            return fee*0.20
        } else if(AppConstants.SALES_PLATINUM.equals(ranking)){
            return fee*0.25
        }
    }
}

