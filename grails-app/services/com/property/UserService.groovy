package com.property

import com.model.AppConstants
import com.model.general.FacilityBooking
import com.model.security.UserInfo
import grails.gorm.transactions.Transactional

@Transactional
class UserService {

    public String getUserName(id){
        if(id==null){
            return null
        }
        def userInfo = UserInfo.executeQuery("FROM UserInfo WHERE user.id = :userId", [userId:id])
        if(userInfo.size()>0){
            return userInfo[0].name
        }
        return null
    }
}

