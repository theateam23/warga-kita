package com.property

import com.property.utils.ControllerUtil


class SecurityService {

    private static Long MODIFIER_1 = 1235724680
    private static Long MODIFIER_3 = 10000

    boolean validateApiTime(jsonData){

        def apiTime = jsonData.apiTime
        def apiKey = jsonData.apiKey
        def propertyId = jsonData.apiProperty
        def email = jsonData.apiEmail

        // Thread.sleep(300)

        if(!ControllerUtil.hasValue(apiTime) || !ControllerUtil.hasValue(apiKey) || !ControllerUtil.hasValue(propertyId) || !ControllerUtil.hasValue(email)){
            return false
        }

        Long dateTimeVal = (Long.valueOf(apiTime) + MODIFIER_1)
        Long currentDate = new Date().getTime()
//        log.debug("tolerate : " + (currentDate - dateTimeVal))
        if(currentDate - dateTimeVal <= MODIFIER_3){
            //apiKey = hashing(emailUserMobile+propertyId+datetime)
            String encrypted = "${email}${propertyId}${dateTimeVal}".toString().encodeAsSHA256()
//            log.debug("apiKey : " + (apiKey))
//            log.debug("server : " + (encrypted))
            if(encrypted.equals(apiKey)){
                return true
            }
        }

        return false
    }
}

