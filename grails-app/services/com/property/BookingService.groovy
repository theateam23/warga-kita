package com.property

import com.model.AppConstants
import com.model.general.FacilityBooking
import grails.gorm.transactions.Transactional

@Transactional
class BookingService {

    public boolean isBookingAvailable(id, startDate, start, end){
        def dt = startDate.clearTime()
        def statusList = [AppConstants.BOOKING_STATUS_OPEN, AppConstants.BOOKING_STATUS_BOOKED]
        def fbs = FacilityBooking.executeQuery("FROM FacilityBooking WHERE facility.id = :facilityId AND status in :statusList AND startDate BETWEEN :startDate AND :endDate",
                [facilityId:id, startDate:dt, endDate:dt + 1, statusList:statusList])
        def isError = false
        for(FacilityBooking fb:fbs){
            if(start == fb.startTime){
                isError = true
                break
            } else if(start > fb.startTime && start < fb.endTime){
                isError = true
                break
            } else if(end > fb.startTime && end < fb.endTime){
                isError = true
                break
            } else if(start > fb.startTime && end < fb.endTime){
                isError = true
                break
            }
        }
        return !isError
    }
}

