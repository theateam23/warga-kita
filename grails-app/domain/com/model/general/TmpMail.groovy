package com.model.general

import com.model.Updatable
import com.model.property.Facility
import com.model.security.UserMobile

class TmpMail {
    String id
    String emailFrom
    String emailTo
    String subject
    String content
    String status

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        emailFrom type: 'string', length: 100
        emailTo type: 'string', length: 100
        subject type: 'string', length: 200
        content type: 'text'
        status type: 'string', length: 20
    }

    static constraints = {
        id nullable: true
        emailFrom nullable: true
        emailTo nullable: true
        subject nullable: true
        content nullable: true
        status nullable: true
    }
}
