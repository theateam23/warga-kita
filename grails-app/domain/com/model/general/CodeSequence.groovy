package com.model.general

class CodeSequence {
    String id
    Integer seq

    static mapping = {
        id type: 'string', generator: "assigned", name: 'id'
        version false
    }

    static constraints = {
        id nullable: true
        seq nullable: true
    }
}
