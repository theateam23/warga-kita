package com.model.general

import com.model.property.PropertyUnit
import com.model.security.UserInfo

class SubUserUnit implements Serializable {

    UserInfo subUser
    PropertyUnit unit

    static mapping = {
        id composite: ['subUser', 'unit']
        version false
    }

}
