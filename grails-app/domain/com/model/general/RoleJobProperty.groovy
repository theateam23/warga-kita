package com.model.general

import com.model.maintenance.RoleMobile
import com.model.property.Property
import com.model.security.UserMobile

class RoleJobProperty implements Serializable {

    UserMobile security
    Property property
    RoleMobile roleMobile

    static mapping = {
        id composite: ['security', 'property', 'roleMobile']
        version false
    }

}
