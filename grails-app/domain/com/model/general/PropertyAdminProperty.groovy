package com.model.general

import com.model.property.Property
import com.model.security.User
import com.model.security.UserMobile

class PropertyAdminProperty implements Serializable {

    User propertyAdmin
    Property property

    static mapping = {
        id composite: ['propertyAdmin', 'property']
        version false
    }

}
