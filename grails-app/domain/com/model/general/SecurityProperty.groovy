package com.model.general

import com.model.property.Property
import com.model.security.UserMobile

class SecurityProperty implements Serializable {

    UserMobile security
    Property property
    String isDefault

    static mapping = {
        id composite: ['security', 'property']
        version false
        isDefault type: 'string', length: 1
    }

    static constraints = {
        isDefault nullable: true
    }
}
