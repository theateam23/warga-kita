package com.model.general

import com.model.Updatable
import com.model.property.Facility
import com.model.security.UserMobile

class FacilityBooking extends Updatable {
    String id
    String code
    Facility facility
    UserMobile userMobile
    Date startDate
    Long startTime
    Long endTime
    String description
    String status

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        code type: 'string', length: 200
        description type: 'string', length: 200
        status type: 'string', length: 20
    }

    static constraints = {
        id nullable: true
        code nullable: true
        facility nullable: true
        userMobile nullable: true
        startDate nullable: true
        startTime nullable: true
        endTime nullable: true
        description nullable: true
        status nullable: true
    }
}
