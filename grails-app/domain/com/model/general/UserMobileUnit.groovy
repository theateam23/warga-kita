package com.model.general

import com.model.maintenance.RoleMobile
import com.model.property.PropertyUnit
import com.model.security.UserMobile

class UserMobileUnit implements Serializable {

    UserMobile userMobile
    PropertyUnit unit
    RoleMobile roleMobile

    static mapping = {
        id composite: ['userMobile', 'unit', 'roleMobile']
        version false
    }

    static constraints = {
    }
}
