package com.model.general

class JobLog {
    String id
    String name
    Date createdDate
    String message
    String stackTrace
    String status

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        message type: 'text'
        stackTrace type: 'text'
        status type: 'string', length: 20
    }

    static constraints = {
        id nullable: true
        name nullable: true
        createdDate nullable: true
        message nullable: true
        stackTrace nullable: true
        status nullable: true
    }
}
