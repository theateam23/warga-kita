package com.model.general

import com.model.Updatable
import com.model.property.Property
import com.model.security.UserMobile

class UserMobileProperty extends Updatable {

    String id
    UserMobile userMobile
    Property property
    String isActive = 'Y'

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        isActive type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        userMobile nullable: true
        property nullable: true
        isActive nullable: true
    }
}
