package com.model.general

class JobStatus {
    String id
    String name
    Date lastUpdated

    static mapping = {
        id type: 'string', generator: "assigned", name: 'id'
        version false
        name type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        name nullable: true
        lastUpdated nullable: true
    }
}
