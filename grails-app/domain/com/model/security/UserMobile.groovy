package com.model.security

import com.model.Updatable
import com.model.maintenance.Gender

class UserMobile extends Updatable {

	String id
	String email
	String name
	String address
	String title
	String phoneNo
	String imageFileId
	String idKTP
	Date dob
	Gender gender
	String uid

	static mapping = {
		id type: 'string', generator: "uuid", name: 'id'
		email type: 'string', length: 200
		name type: 'string', length: 200
		address type: 'string', length: 200
		title type: 'string', length:20
		phoneNo type: 'string', length: 20
		imageFileId type: 'string', length: 200
		idKTP type: 'string', length: 200
		uid type: 'string', length: 100
	}

	static constraints = {
		id nullable: true
		email nullable: true
		name nullable: true
		address nullable: true
		title nullable: true
		phoneNo nullable: true
		imageFileId nullable: true
		idKTP nullable: true
		dob nullable: true
		gender nullable: true
		uid nullable: true
	}

}
