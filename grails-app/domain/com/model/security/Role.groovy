package com.model.security

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

	private static final long serialVersionUID = 1

	String id
	String authority
	String name
	Integer idx

	static constraints = {
		id type: 'string', generator: "assigned", name: 'id'
		authority blank: false, unique: true
		name blank: false
		idx nullable: true
	}

	static mapping = {
		cache true
	}
}
