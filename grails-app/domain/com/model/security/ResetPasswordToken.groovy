package com.model.security

class ResetPasswordToken {
    String id
    String email
    Long createdTime
    Long expiredTime
    String token

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        email type: 'string', length: 200
        token type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        email nullable: true
        token nullable: true
        createdTime nullable: true
        expiredTime nullable: true
    }
}
