package com.model.security

class Menu {
    String id
    String name
    String description
    String controllerPath
    String actionName
    String icon
    Menu parentMenu
    Boolean hasSubMenu = false
    Integer idx
    Boolean isDefault = false

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 100
        description type: 'string', length: 200
        controllerPath type: 'string', length: 200
        actionName type: 'string', length: 200
        icon type: 'string', length: 100
    }

    static constraints = {
        id nullable: true
        name nullable: true
        description nullable: true
        controllerPath nullable: true
        parentMenu nullable: true
        actionName nullable: true
        hasSubMenu nullable: true
        idx nullable: true
        icon nullable: true
    }

    public String toString(){
        return this.name
    }
}
