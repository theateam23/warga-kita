package com.model.security

class RoleMenu implements Serializable{
    Role role
    Menu menu
    Integer idx

    static mapping = {
        id composite: ['role', 'menu']
        version false
    }
}
