package com.model.security

import com.model.Updatable

class UserInfo extends Updatable {

	String id
	User user
	String name
	String address
	String title
	String phoneNo
	String pmId
	String imageFileId
	String imageFileName
	String bankName
	String bankAccount

	static mapping = {
		id type: 'string', generator: "uuid", name: 'id'
		name type: 'string', length: 200
		address type: 'string', length: 200
		title type: 'string', length:20
		phoneNo type: 'string', length: 20
		pmId type: 'string', length: 200
		imageFileId type: 'string', length: 200
		imageFileName type: 'string', length: 200
		bankName type: 'string', length: 200
		bankAccount type: 'string', length: 200
	}

	static constraints = {
		id nullable: true
		user nullable: true, unique: true
		name nullable: true
		address nullable: true
		title nullable: true
		phoneNo nullable: true
		pmId nullable: true
		imageFileId nullable: true
		imageFileName nullable: true
		bankName nullable: true
		bankAccount nullable: true
	}

}
