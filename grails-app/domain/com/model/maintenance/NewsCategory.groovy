package com.model.maintenance

class NewsCategory {
    String id
    String code
    String name
    String description
    String icon
    String isWargaKu
    int idx

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        description type: 'string', length: 200
        code type: 'string', length: 100
        icon type: 'string', length: 100
        isWargaKu type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        name nullable: true
        description nullable: true
        code nullable: true
        icon nullable: true
        isWargaKu nullable: true
    }

}
