package com.model.maintenance

import com.model.Updatable

class Service extends Updatable {
    String id
    String name
    String description
    String image

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        description type: 'string', length: 500
        image type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        name nullable: true
        description nullable: true
        image nullable: true
    }

}
