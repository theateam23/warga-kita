package com.model.maintenance

import com.model.Updatable

class VehicleType extends Updatable {
    String id
    String code
    String name
    String description
    String pmId

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        code type: 'string', length: 100
        name type: 'string', length: 200
        description type: 'string', length: 500
        pmId type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        code nullable: true
        name nullable: true
        description nullable: true
        pmId nullable: true
    }

    public String toString(){
        return code + " - " + name;
    }
}
