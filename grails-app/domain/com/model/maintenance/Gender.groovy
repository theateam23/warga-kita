package com.model.maintenance

import com.model.Updatable

class Gender extends Updatable {
    String id
    String code
    String name
    String description

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        code type: 'string', length: 100
        name type: 'string', length: 200
        description type: 'string', length: 500
    }

    static constraints = {
        id nullable: true
        code nullable: true
        name nullable: true
        description nullable: true
    }

    public String toString(){
        return code + " - " + name;
    }
}
