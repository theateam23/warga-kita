package com.model.maintenance

import com.model.Updatable

class RoleMobile extends Updatable {
    String id
    String name
    String description
    String roleType // 1 = tied to property, 2 = tied to unit

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        description type: 'string', length: 500
        roleType type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        name nullable: true
        description nullable: true
        roleType nullable: true
    }

}
