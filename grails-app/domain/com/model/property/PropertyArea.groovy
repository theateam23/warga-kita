package com.model.property

import com.model.Updatable


class PropertyArea extends Updatable {

    String id
    Property property
    String name
    String address
    Integer totalSubArea
    Integer totalUnit

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        address type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        property nullable: true
        name nullable: true
        totalSubArea nullable: true
        totalUnit nullable: true
        address nullable: true
    }
}
