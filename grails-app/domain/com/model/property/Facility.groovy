package com.model.property

import com.model.Updatable

class Facility extends Updatable {
    String id
    PropertyArea area
    String name
    String address
    String tnc
    String imageFileId
    String imageFileName
    String isDelete

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        address type: 'string', length: 200
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
        tnc type: 'text'
        isDelete type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        area nullable: true
        name nullable: true
        address nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
        tnc nullable: true
        isDelete nullable: true
    }
}
