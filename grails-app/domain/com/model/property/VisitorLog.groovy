package com.model.property

import com.model.Updatable


class VisitorLog extends Updatable {
    String id
    String propertyId
    String unitId
    String unit
    String area
    String subArea
    String name
    String vehicleNo
    String phoneNo
    String image
    Date logDate
    Date visitDate
    String status
    String email

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        propertyId type: 'string', length: 200
        unitId type: 'string', length: 200
        unit type: 'string', length: 200
        area type: 'string', length: 200
        subArea type: 'string', length: 200
        name type: 'string', length: 200
        image type: 'string', length: 200
        status type: 'string', length: 100
        email type: 'string', length: 200
        vehicleNo type: 'string', length: 200
        phoneNo type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        propertyId nullable: true
        unitId nullable: true
        unit nullable: true
        area nullable: true
        subArea nullable: true
        name nullable: true
        image nullable: true
        logDate nullable: true
        visitDate nullable: true
        status nullable: true
        email nullable: true
        vehicleNo nullable: true
        phoneNo nullable: true
    }

}
