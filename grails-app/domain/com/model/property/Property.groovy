package com.model.property

import com.model.AppConstants
import com.model.Updatable
import com.model.maintenance.PropertyType
import com.model.security.UserInfo

class Property extends Updatable{

    String id
    UserInfo propertyManager
    UserInfo sales
    String code
    String name
    String developer
    String address
    PropertyType propertyType
    Integer totalUnit
    String securityPhoneNo
    String securityEmails
    String mobileBackground
    String mobileBackgroundName
    String webLoginBackground
    String webLoginBackgroundName
    String logo
    String logoName
    String isPrivate
    BigDecimal fee
    Date expiryDate
    String appName


    Property() {
        isPrivate = AppConstants.FLAG_NO
        fee = BigDecimal.ZERO
        appName = AppConstants.APP_WARGAKU
    }

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        code type: 'string', length: 200
        name type: 'string', length: 200
        address type: 'string', length: 200
        developer type: 'string', length: 200
        securityPhoneNo type: 'text'
        securityEmails type: 'text'
        mobileBackground type: 'string', length: 200
        webLoginBackground type: 'string', length: 200
        logo type: 'string', length: 200
        mobileBackgroundName type: 'string', length: 200
        webLoginBackgroundName type: 'string', length: 200
        logoName type: 'string', length: 200
        isPrivate type: 'string', length: 1
        appName type: 'string', length: 50
    }

    static constraints = {
        id nullable: true
        propertyManager nullable: true
        code nullable: true
        name nullable: true
        developer nullable: true
        address nullable: true
        propertyType nullable: true
        totalUnit nullable: true
        securityPhoneNo nullable: true
        securityEmails nullable: true
        mobileBackground nullable: true
        webLoginBackground nullable: true
        logo nullable: true
        mobileBackgroundName nullable: true
        webLoginBackgroundName nullable: true
        logoName nullable: true
        sales nullable: true
        fee nullable: true
        expiryDate nullable: true
        isPrivate nullable: true
        appName nullable: true
    }

}
