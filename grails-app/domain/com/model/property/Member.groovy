package com.model.property

import com.model.AppConstants
import com.model.Updatable
import com.model.maintenance.Gender
import com.model.maintenance.MemberRelation


class Member extends Updatable {

    String id
    PropertyUnit unit
    String name
    Date dateOfBirth
    String placeOfBirth
    String profession
    String idKTP
    String email
    String address
    String status
    String education
    String hobby
    String description
    String imageFileId
    String imageFileName
    Gender gender
    MemberRelation relationship
    String isNotified = AppConstants.FLAG_NO

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        placeOfBirth type: 'string', length: 200
        profession type: 'string', length: 200
        idKTP type: 'string', length: 50
        email type: 'string', length: 200
        address type: 'string', length: 200
        status type: 'string', length: 50
        education type: 'string', length: 200
        hobby type: 'string', length: 200
        description type: 'string', length: 200
        isNotified type: 'string', length: 1
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        unit nullable: true
        name nullable: true
        dateOfBirth nullable: true
        placeOfBirth nullable: true
        profession nullable: true
        idKTP nullable: true
        email nullable: true
        address nullable: true
        status nullable: true
        education nullable: true
        hobby nullable: true
        description nullable: true
        gender nullable: true
        relationship nullable: true
        isNotified nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
    }
}
