package com.model.property

import com.model.Updatable
import com.model.maintenance.PropertyType
import com.model.security.UserInfo

class PropertyDeal extends Updatable{

    String id
    UserInfo sales
    String name
    String developer
    String address
    String description
    PropertyType propertyType
    Date startDate
    String status
    String attachment
    String attachmentName

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        address type: 'string', length: 200
        developer type: 'string', length: 200
        description type: 'text'
        status type: 'string', length: 200
        attachment type: 'string', length: 200
        attachmentName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        sales nullable: true
        name nullable: true
        developer nullable: true
        address nullable: true
        propertyType nullable: true
        status nullable: true
        description nullable: true
        attachment nullable: true
        attachmentName nullable: true
        startDate nullable: true
    }

}
