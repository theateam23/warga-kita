package com.model.property

import com.model.Updatable

class TmpUserMobile extends Updatable {

	String id
	String email
	String name
	String address
	String title
	String phoneNo
	String propertyId
	String unitId
	String role
	String idKTP

	static mapping = {
		id type: 'string', generator: "uuid", name: 'id'
		email type: 'string', length: 200
		name type: 'string', length: 200
		address type: 'string', length: 200
		title type: 'string', length:20
		phoneNo type: 'string', length: 20
		unitId type: 'string', length: 200
		propertyId type: 'string', length: 200
		idKTP type: 'string', length: 200
		role type: 'string', length: 200
	}

	static constraints = {
		id nullable: true
		email nullable: true
		name nullable: true
		address nullable: true
		title nullable: true
		phoneNo nullable: true
		unitId nullable: true
		propertyId nullable: true
		idKTP nullable: true
		role nullable: true
	}

}
