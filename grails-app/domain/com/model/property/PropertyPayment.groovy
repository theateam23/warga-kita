package com.model.property

class PropertyPayment {

    String id
    String propertyId
    String propertyName
    String salesId
    String salesName
    String description
    String attachment
    String attachmentName
    BigDecimal paymentAmount
    BigDecimal salesCommission
    Date expiryDate
    String createdBy
    Date createdDate

    def beforeInsert() {
        createdDate = new Date()
    }

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        propertyId type: 'string', length: 200
        propertyName type: 'string', length: 200
        salesId type: 'string', length: 200
        salesName type: 'string', length: 200
        description type: 'text'
        attachment type: 'string', length: 200
        attachmentName type: 'string', length: 200
        createdBy type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        propertyId nullable: true
        propertyName nullable: true
        salesId nullable: true
        salesName nullable: true
        attachment nullable: true
        attachmentName nullable: true
        paymentAmount nullable: true
        salesCommission nullable: true
        expiryDate nullable: true
        createdBy nullable: true
        createdDate nullable: true
    }

}
