package com.model.property

import com.model.general.UserMobileProperty

class UserFeedback {

    String id
    UserMobileProperty userMobileProperty
    String title
    String description
    Date createdDate
    String fileId
    String fileName

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        description type: 'text'
        fileId type: 'string', length: 200
        fileName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        description nullable: true
        title nullable: true
        createdDate nullable: true
        fileId nullable: true
        fileName nullable: true
    }

    def beforeInsert() {
        createdDate = new Date()
    }
}
