package com.model.property

import com.model.Updatable

class HomeGuardLog extends Updatable{

    String id
    String propertyId
    String unit
    String area
    String subArea
    String description
    Date guardDate
    String homeGuardId
    String imageFileId
    String imageFileName
    String status

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        propertyId type: 'string', length: 100
        unit type: 'string', length: 100
        area type: 'string', length: 100
        subArea type: 'string', length: 100
        homeGuardId type: 'string', length: 100
        description type: 'string', length: 300
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
        status type: 'string', length: 20
    }

    static constraints = {
        id nullable: true
        propertyId nullable: true
        unit nullable: true
        area nullable: true
        subArea nullable: true
        description nullable: true
        homeGuardId nullable: true
        guardDate nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
        status nullable: true
    }
}
