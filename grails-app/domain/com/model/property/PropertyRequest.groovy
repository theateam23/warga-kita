package com.model.property

import com.model.Updatable

class PropertyRequest extends Updatable{

    String id
    Property property
    String title
    String description
    String fileId
    String fileName

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        description type: 'text'
        fileId type: 'string', length: 200
        fileName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        description nullable: true
        title nullable: true
        fileId nullable: true
        fileName nullable: true
    }

}
