package com.model.property

import com.model.AppConstants
import com.model.Updatable
import com.model.maintenance.VehicleType

class Vehicle extends Updatable {

    String id
    PropertyUnit unit
    String plateNo
    String plateNoSearch
    VehicleType vehicleType
    String brand
    String color
    Date taxDueDate
    String imageFileId
    String imageFileName
    String isNotified = AppConstants.FLAG_NO

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        plateNo type: 'string', length: 200
        plateNoSearch type: 'string', length: 200
        brand type: 'string', length: 200
        color type: 'string', length: 200
        isNotified type: 'string', length: 1
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        unit nullable: true
        plateNo nullable: true
        plateNoSearch nullable: true
        vehicleType nullable: true
        brand nullable: true
        color nullable: true
        taxDueDate nullable: true
        isNotified nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
    }
}
