package com.model.property

import com.model.Updatable
import com.model.maintenance.UnitType
import com.model.security.UserMobile

class PropertyUnit extends Updatable {
    String id
    PropertyArea area
    UserMobile ownerResident
    UserMobile tenant
    String name
    String subArea
    UnitType unitType

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        subArea type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        area nullable: true
        ownerResident nullable: true
        tenant nullable: true
        name nullable: true
        subArea nullable: true
        unitType nullable: true
    }
}
