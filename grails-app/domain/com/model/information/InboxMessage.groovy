package com.model.information

import com.model.AppConstants
import com.model.Updatable
import com.model.property.Property
import com.model.security.UserMobile

class InboxMessage extends Updatable {
    String id
    UserMobile userMobile
    String title
    String content
    String isRead = AppConstants.FLAG_NO
    String isDelete = AppConstants.FLAG_NO

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        content type: 'string', length: 300
        isRead type: 'string', length: 1
        isDelete type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        userMobile nullable: true
        title nullable: true
        content nullable: true
        isRead nullable: true
        isDelete nullable: true
    }
}
