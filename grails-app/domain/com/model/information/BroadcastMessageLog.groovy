package com.model.information

import com.model.Updatable
import com.model.property.Property

class BroadcastMessageLog extends Updatable{
    String id
    String title
    String content
    Date reminderDate
    String target
    Property property

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        content type: 'string', length: 300
        target type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        title nullable: true
        content nullable: true
        reminderDate nullable: true
        target nullable: true
        property nullable: true
    }
}
