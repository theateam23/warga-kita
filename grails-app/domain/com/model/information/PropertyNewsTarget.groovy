package com.model.information

import com.model.maintenance.RoleMobile

class PropertyNewsTarget implements Serializable {
    RoleMobile roleMobile
    PropertyNews propertyNews

    static mapping = {
        id composite: ['roleMobile', 'propertyNews']
        version false
    }
}
