package com.model.information

class BroadcastReminder {
    String id
    InboxMessage inboxMessage
    Date reminderDate
    String topicId

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
    }

    static constraints = {
        id nullable: true
        inboxMessage nullable: true
        reminderDate nullable: true
        topicId nullable: true
    }
}
