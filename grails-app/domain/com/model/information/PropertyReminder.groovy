package com.model.information

import com.model.Updatable
import com.model.property.Property

class PropertyReminder extends Updatable {
    String id
    Property property
    String name
    String description
    Integer reminderDay
    String message
    String isActive

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        name type: 'string', length: 200
        description type: 'string', length: 300
        message type: 'text'
        isActive type: 'string', length: 1
    }

    static constraints = {
        id nullable: true
        property nullable: true
        name nullable: true
        description nullable: true
        reminderDay nullable: true
        message nullable: true
        isActive nullable: true
    }

    public String toString(){
        return "${name} - ${description}".toString()
    }
}
