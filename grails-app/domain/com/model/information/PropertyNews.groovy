package com.model.information

import com.model.Updatable
import com.model.maintenance.NewsCategory
import com.model.property.Property

class PropertyNews extends Updatable {
    String id
    Property property
    String title
    String subtitle
    String description
    String summary
    String imageFileId
    String imageFileName
    String email
    String phoneNo
    String sms
    String address
    NewsCategory category

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        subtitle type: 'string', length: 200
        summary type: 'string', length: 500
        description type: 'text'
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
        email type: 'string', length: 200
        phoneNo type: 'string', length: 200
        sms type: 'string', length: 200
        address type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        property nullable: true
        title nullable: true
        subtitle nullable: true
        summary nullable: true
        description nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
        category nullable: true
        email nullable: true
        phoneNo nullable: true
        sms nullable: true
        address nullable: true
    }
}
