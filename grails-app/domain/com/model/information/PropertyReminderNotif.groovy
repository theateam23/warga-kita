package com.model.information

import com.model.Updatable
import com.model.property.PropertyUnit

class PropertyReminderNotif extends Updatable {
    String id
    PropertyReminder reminder
    PropertyUnit unit
    Date deadline
    String isNotif
    String isPaid
    Date paymentDate
    String paymentMethod
    String receipt
    String receiptName

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        isNotif type: 'string', length: 1
        isPaid type: 'string', length: 1
        paymentMethod type: 'string', length: 200
        receipt type: 'string', length: 200
        receiptName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        reminder nullable: true
        unit nullable: true
        deadline nullable: true
        isNotif nullable: true
        isPaid nullable: true
        paymentDate nullable: true
        paymentMethod nullable: true
        receipt nullable: true
        receiptName nullable: true
    }
}
