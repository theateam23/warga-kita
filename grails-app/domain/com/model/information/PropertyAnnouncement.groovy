package com.model.information

import com.model.Updatable
import com.model.property.Property

class PropertyAnnouncement extends Updatable {
    String id
    Property property
    String title
    String imageFileId
    String imageFileName
    Date periodStart
    Date periodEnd

    static mapping = {
        id type: 'string', generator: "uuid", name: 'id'
        title type: 'string', length: 200
        imageFileId type: 'string', length: 200
        imageFileName type: 'string', length: 200
    }

    static constraints = {
        id nullable: true
        property nullable: true
        title nullable: true
        imageFileId nullable: true
        imageFileName nullable: true
        periodStart nullable: true
        periodEnd nullable: true
    }
}
