package com.model.information

import com.model.maintenance.RoleMobile

class PropertyAnnouncementTarget implements Serializable {
    RoleMobile roleMobile
    PropertyAnnouncement propertyAnnouncement

    static mapping = {
        id composite: ['roleMobile', 'propertyAnnouncement']
        version false
    }
}
