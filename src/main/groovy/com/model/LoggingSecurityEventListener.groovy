package com.model

/**
 * Created by user on 3/5/2018.
 */

import grails.transaction.Transactional
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

@Transactional
class LoggingSecurityEventListener extends SavedRequestAwareAuthenticationSuccessHandler  {

    void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            def authorities = authentication.principal.getAuthorities().collect{it.getAuthority().toString()}
            session.setAttribute(AppConstants.SESSION_ROLE,authorities)
        }
        //super.onAuthenticationSuccess(request, response, authentication);
        getRedirectStrategy().sendRedirect(request, response, "/dashboard");
    }
}
