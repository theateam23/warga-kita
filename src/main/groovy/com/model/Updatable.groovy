package com.model


/**
 * Created by enpee_000 on 03/03/2018.
 */
abstract class Updatable {

    Date createdDate
    String createdBy
    Date updatedDate
    String updatedBy

    def beforeInsert() {
        createdDate = new Date()
    }
    def beforeUpdate() {
        updatedDate = new Date()
    }

    static mapping = {
        createdBy type: 'string', length: 200
        updatedBy type: 'string', length: 200
    }

    static constraints = {
        createdBy nullable: true
        createdDate nullable: true
        updatedBy nullable: true
        updatedDate nullable: true
    }
}
