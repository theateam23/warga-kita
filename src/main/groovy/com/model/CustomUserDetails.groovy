package com.model

import grails.plugin.springsecurity.userdetails.GrailsUser
/**
 * Created by enpee_000 on 03/03/2018.
 */
class CustomUserDetails extends GrailsUser{

    final String fullName
    final String roleName
    final String createdDate
    final String pmId
    final List menuList

    CustomUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
            boolean accountNonLocked, Collection authorities, String id, String fullName, String roleName, String createdDate, String pmId, List menuList) {
        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities, id)
        this.fullName = fullName
        this.roleName = roleName
        this.createdDate = createdDate
        this.pmId = pmId
        this.menuList = menuList
    }
}
