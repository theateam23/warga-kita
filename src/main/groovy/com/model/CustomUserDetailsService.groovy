package com.model

import com.model.security.Role
import com.model.security.RoleMenu
import com.model.security.User
import com.model.security.UserInfo
import com.model.security.UserRole
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import grails.transaction.Transactional
import org.springframework.dao.DataAccessException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

import java.text.SimpleDateFormat

/**
 * Created by enpee_000 on 29/01/2016.
 */
class CustomUserDetailsService implements GrailsUserDetailsService {

    static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]

    @Override
    @Transactional
    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException, DataAccessException {
        User user = User.findByUsername(username)
        Role role = UserRole.findByUser(user)?.role
        UserInfo info = UserInfo.findByUser(user)
        def createdDate = new SimpleDateFormat("MMM, YYYY").format(info.createdDate)
        def authorities = user.getAuthorities().collect {new SimpleGrantedAuthority(it.authority)}

        def roleMenus = RoleMenu.executeQuery("FROM RoleMenu WHERE role.authority in :roleList",[roleList:authorities.collect{it.toString()}])
        def menuList = []
        roleMenus.each{ rm ->
            menuList.add(rm.menu.id)
        }

        return new CustomUserDetails(user.username, user.password, user.enabled,
                !user.accountExpired, !user.passwordExpired,
                !user.accountLocked, authorities ?: NO_ROLES, user.id,
                info.name,role.name,createdDate,info.pmId,menuList)
    }

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loadUserByUsername(username, true)
    }
}
