package com.model

/**
 * Created by user on 3/5/2018.
 */
class AppConstants {

    public static String SESSION_MENU_ID = "SESSION_MENU_ID"
    public static String SESSION_MENU_CONTROLLER = "SESSION_MENU_CONTROLLER"
    public static String SESSION_MENU_ACTION = "SESSION_MENU_ACTION"
    public static String SESSION_MENU_NAME = "SESSION_MENU_NAME"
    public static String SESSION_ROLE = "SESSION_ROLE"
    public static String ALL = "Semua"

    public static String PAGUYUBAN = "Paguyuban"
    public static String PERUMAHAN = "Perumahan"

    public static String ROLE_PM = "ROLE_PM"
    public static String ROLE_PA = "ROLE_PA"
    public static String ROLE_ADMIN = "ROLE_ADMIN"
    public static String ROLE_SALES = "ROLE_SALES"

    public static String ROLE_OWNER_RESIDENT = "Pemilik"
    public static String ROLE_TENANT = "Penyewa"
    public static String ROLE_SUB_USER = "Penghuni"
    public static String ROLE_SECURITY = "Keamanan"

    public static String LOGIN_NORMAL = "0"
    public static String LOGIN_GOOGLE = "1"
    public static String LOGIN_FACEBOOK = "2"
    public static String LOGIN_TWITTER = "3"
    public static String LOGIN_NORMAL_STR = "Manual"
    public static String LOGIN_GOOGLE_STR = "Google"
    public static String LOGIN_FACEBOOK_STR = "Facebook"
    public static String LOGIN_TWITTER_STR = "Twitter"

    public static String BOOKING_STATUS_NEW = "New"
    public static String BOOKING_STATUS_OPEN = "Approved"
    public static String BOOKING_STATUS_BOOKED = "Booked"
    public static String BOOKING_STATUS_CLOSED = "Closed"
    public static String BOOKING_STATUS_CANCELED = "Canceled"
    public static String BOOKING_STATUS_REJECTED = "Rejected"

    public static String CODE_SEQUENCE_FACILITY_BOOKING="FB"

    public static String ROLE_TYPE_PROPERTY = "1"
    public static String ROLE_TYPE_UNIT = "2"

    public static String VISITOR_STATUS_NEW = "New"
    public static String VISITOR_STATUS_VISITED = "Visited"
    public static String VISITOR_STATUS_CANCELED = "Canceled"
    public static String VISITOR_STATUS_EXPIRED = "Expired"

    public static String HOMEGUARD_STATUS_NEW = "New"
    public static String HOMEGUARD_STATUS_CHECK = "Checked"

    public static String FLAG_YES = "Y"
    public static String FLAG_NO = "N"

    public static String SECURITY_PHONE_DELIMITER = ";"

    public static String DEAL_STATUS_NEW = "NEW"
    public static String DEAL_STATUS_IN_PROGRESS = "IN_PROGRESS"
    public static String DEAL_STATUS_SUBMITTED = "SUBMITTED"
    public static String DEAL_STATUS_APPROVED = "APPROVED"

    public static String DEAL_STATUS_NEW_NAME = "New"
    public static String DEAL_STATUS_IN_PROGRESS_NAME = "In Progress"
    public static String DEAL_STATUS_SUBMITTED_NAME = "Submitted"
    public static String DEAL_STATUS_APPROVED_NAME = "Approved"

    public static String REMINDER_PERIODE_DAILY = "1"
    public static String REMINDER_PERIODE_WEEKLY = "2"
    public static String REMINDER_PERIODE_MONTHLY = "3"

    public static String REMINDER_PERIODE_DAILY_NAME = "Daily"
    public static String REMINDER_PERIODE_WEEKLY_NAME = "Weekly"
    public static String REMINDER_PERIODE_MONTHLY_NAME = "Monthly"

    public static String REMINDER_PAID = "Paid"
    public static String REMINDER_UNPAID = "Unpaid"

    public static String SALES_NONE = "None"
    public static String SALES_SILVER = "Silver"
    public static String SALES_GOLD = "Gold"
    public static String SALES_PLATINUM = "Platinum"

    public static String PROCESS_NEW = "NEW"
    public static String PROCESS_IN_PROGRESS = "IN_PROGRESS"

    public static String APP_WARGAKU = "Wargaku"

    public static String JOB_SUCCESS = "Success"
    public static String JOB_FAILED = "Failed"
    public static int MAX_BATCH_FIREBASE = 100

    public static String BY_SYSTEM = "System"


    public static getReminderPeriodeList(){
        [
                [id:REMINDER_PERIODE_DAILY, name:REMINDER_PERIODE_DAILY_NAME],
                [id:REMINDER_PERIODE_WEEKLY, name:REMINDER_PERIODE_WEEKLY_NAME],
                [id:REMINDER_PERIODE_MONTHLY, name:REMINDER_PERIODE_MONTHLY_NAME]
        ]
    }

    public static getDealStatusList(){
        [
                [id:DEAL_STATUS_NEW, name:DEAL_STATUS_NEW_NAME],
                [id:DEAL_STATUS_IN_PROGRESS, name:DEAL_STATUS_IN_PROGRESS_NAME],
                [id:DEAL_STATUS_SUBMITTED, name:DEAL_STATUS_SUBMITTED_NAME],
                [id:DEAL_STATUS_APPROVED, name:DEAL_STATUS_APPROVED_NAME]
        ]
    }

    public static getReminderStatusName(statusCode){
        if(statusCode == FLAG_YES){
            return REMINDER_PAID
        } else if(statusCode == FLAG_NO){
            return REMINDER_UNPAID
        }
    }

    public static getReminderPeriodeName(statusCode){
        if(statusCode == REMINDER_PERIODE_DAILY){
            return REMINDER_PERIODE_DAILY_NAME
        } else if(statusCode == REMINDER_PERIODE_WEEKLY){
            return REMINDER_PERIODE_WEEKLY_NAME
        } else if(statusCode == REMINDER_PERIODE_MONTHLY){
            return REMINDER_PERIODE_MONTHLY_NAME
        }
    }

    public static getDealStatusName(statusCode){
        if(statusCode == DEAL_STATUS_NEW){
            return DEAL_STATUS_NEW_NAME
        } else if(statusCode == DEAL_STATUS_IN_PROGRESS){
            return DEAL_STATUS_IN_PROGRESS_NAME
        } else if(statusCode == DEAL_STATUS_SUBMITTED){
            return DEAL_STATUS_SUBMITTED_NAME
        } else if(statusCode == DEAL_STATUS_APPROVED){
            return DEAL_STATUS_APPROVED_NAME
        }
    }

    public static getLoginTypeList(){
        [
                [id:LOGIN_NORMAL, name:LOGIN_NORMAL_STR],
                [id:LOGIN_GOOGLE, name:LOGIN_GOOGLE_STR],
                [id:LOGIN_FACEBOOK, name:LOGIN_FACEBOOK_STR],
                [id:LOGIN_TWITTER, name:LOGIN_TWITTER_STR]
        ]
    }

    public static getLoginTypeMap(){
        def loginTypeMap = [:]
        loginTypeMap.put(LOGIN_NORMAL,LOGIN_NORMAL_STR)
        loginTypeMap.put(LOGIN_GOOGLE,LOGIN_GOOGLE_STR)
        loginTypeMap.put(LOGIN_FACEBOOK,LOGIN_FACEBOOK_STR)
        loginTypeMap.put(LOGIN_TWITTER,LOGIN_TWITTER_STR)
        loginTypeMap
    }

    public static getFacilityBookingStatusList(){
        [
                BOOKING_STATUS_NEW,
                BOOKING_STATUS_OPEN,
                BOOKING_STATUS_BOOKED,
                BOOKING_STATUS_CANCELED,
                BOOKING_STATUS_CLOSED,
                BOOKING_STATUS_REJECTED
        ]
    }

    public static getStatusVisitor(statusCode){
        if(statusCode == VISITOR_STATUS_NEW){
            return "New"
        } else if(statusCode == VISITOR_STATUS_VISITED){
            return "Visited"
        } else if(statusCode == VISITOR_STATUS_CANCELED){
            return "Canceled"
        } else if(statusCode == VISITOR_STATUS_EXPIRED){
            return "Expired"
        }
    }
}
