INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_ADMIN', 'DASHBOARD', 0);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_ADMIN', 'MANAGE_PM', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_ADMIN', 'MANAGE_PROPERTY', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_ADMIN', 'MANAGE_SALES', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_ADMIN', 'MANAGE_DEAL', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'DASHBOARD', 0);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MANAGE_USER', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'PROPERTY_ADMIN', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MOBILE_USER', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'SECURITY_USER', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MAINTENANCE', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'UNIT_TYPE', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'AREA', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'UNIT', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MAINTAIN_FACILITY', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'DESCRIPTION_FORM', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'APPROVAL', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MOBILE_USER_APPROVAL', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MANAGE_BOOKING_FACILITY', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'ACTIVITY', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MANAGE_NEWS', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MANAGE_ANNOUNCEMENT', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MANAGE_FEEDBACK', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'BROADCAST_MESSAGE', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'BOOKING_FACILITY', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'REPORT', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'VISITOR_LIST', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'MEMBER_LIST', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'VEHICLE_LIST', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PM', 'HOME_GUARD', 4);


INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'DASHBOARD', 0);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MAINTENANCE', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'UNIT_TYPE', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'AREA', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'UNIT', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MAINTAIN_FACILITY', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'DESCRIPTION_FORM', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'APPROVAL', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MOBILE_USER_APPROVAL', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MANAGE_BOOKING_FACILITY', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'ACTIVITY', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MANAGE_NEWS', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MANAGE_ANNOUNCEMENT', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MANAGE_FEEDBACK', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'BROADCAST_MESSAGE', 4);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'BOOKING_FACILITY', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'REPORT', 5);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'VISITOR_LIST', 1);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'MEMBER_LIST', 2);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'VEHICLE_LIST', 3);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_PA', 'HOME_GUARD', 4);

INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_SALES', 'DASHBOARD', 0);
INSERT INTO `role_menu` (`role_id`, `menu_id`, `idx`) VALUES ('ROLE_SALES', 'MANAGE_DEAL', 1);