DELETE FROM `role_menu`;
DELETE FROM `menu` WHERE parent_menu_id IS NOT NULL;
DELETE FROM `menu`;
DELETE FROM `gender`;
DELETE FROM `member_relation`;
DELETE FROM `news_category`;
DELETE FROM `vehicle_type`;
DELETE FROM `role_mobile`;

INSERT INTO `gender` (`id`, `version`, `code`, `name`, `description`, `updated_by`, `updated_date`, `created_by`, `created_date`)
VALUES
	('1', 0, 'Pria', 'Pria', NULL, NULL, NULL, NULL, NULL),
	('2', 0, 'Wanita', 'Wanita', NULL, NULL, NULL, NULL, NULL),
	('3', 0, 'Lainnya', 'Lainnya', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `member_relation` (`id`, `version`, `created_date`, `code`, `name`, `updated_date`, `updated_by`, `pm_id`, `created_by`, `description`)
VALUES
	('1', 0, NULL, 'Suami', 'Suami', NULL, NULL, NULL, NULL, NULL),
	('2', 0, NULL, 'Istri', 'Istri', NULL, NULL, NULL, NULL, NULL),
	('3', 0, NULL, 'Anak', 'Anak', NULL, NULL, NULL, NULL, NULL),
	('4', 0, NULL, 'Famili', 'Famili', NULL, NULL, NULL, NULL, NULL),
	('5', 0, NULL, 'Lainnya', 'Lainnya', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `menu` (`id`, `version`, `parent_menu_id`, `icon`, `has_sub_menu`, `name`, `controller_path`, `is_default`, `idx`, `action_name`, `description`)
VALUES
	('DASHBOARD', 0, NULL, 'fa-home', 0, 'Dashboard', 'dashboard', 0, 0, 'index', 'Dashboard'),
	('MANAGE_PM', 0, NULL, 'fa-users', 0, 'Manage Property Manager', 'userAdmin', 0, 1, 'index', 'Manage Property Manager'),
	('MANAGE_PROPERTY', 0, NULL, 'fa-building', 0, 'Manage Property', 'propertyAdmin', 0, 2, 'index', 'Manage Property'),
	('MANAGE_SALES', 0, NULL, 'fa-users', 0, 'Manage Sales', 'userSales', 0, 3, 'index', 'Manage Sales'),
	('MANAGE_DEAL', 0, NULL, 'fa-gear', 0, 'Manage Deal', 'propertyDeal', 0, 4, 'index', 'Property Deal'),
	('MANAGE_USER', 0, NULL, 'fa-users', 1, 'Manage User', NULL, 0, 1, NULL, 'Manage User'),
	('MAINTENANCE', 0, NULL, 'fa-gear', 1, 'Maintenance', NULL, 0, 2, NULL, 'Maintenance'),
	('ACTIVITY', 0, NULL, 'fa-info', 1, 'Activity', NULL, 0, 3, NULL, 'Activity'),
	('REPORT', 0, NULL, 'fa-newspaper-o', 1, 'Report', NULL, 0, 5, NULL, 'Report'),
	('APPROVAL', 0, NULL, 'fa-address-book', 1, 'Approval', NULL, 0, 4, NULL, 'Approval'),
	('MOBILE_USER_APPROVAL', 0, 'APPROVAL', NULL, 0, 'Mobile User Approval', 'userApproval', 0, 3, 'index', 'Mobile User Approval'),
	('PROPERTY_ADMIN', 0, 'MANAGE_USER', NULL, 0, 'Property Admin', 'userPa', 0, 1, 'index', 'Property Admin'),
	('MOBILE_USER', 0, 'MANAGE_USER', NULL, 0, 'Mobile User', 'userMobile', 0, 2, 'index', 'Mobile User'),
	('SECURITY_USER', 0, 'MANAGE_USER', NULL, 0, 'Security User', 'securityUser', 0, 2, 'index', 'Security User'),
	('UNIT_TYPE', 0, 'MAINTENANCE', NULL, 0, 'Unit Type', 'unitType', 0, 1, 'index', 'Unit Type'),
	('AREA', 0, 'MAINTENANCE', NULL, 0, 'Area', 'buildingPm', 0, 2, 'index', 'Area'),
	('UNIT', 0, 'MAINTENANCE', NULL, 0, 'Unit', 'unitPm', 0, 3, 'index', 'Unit'),
	('MEMBER_LIST', 0, 'REPORT', NULL, 0, 'List Member', 'memberPm', 0, 4, 'index', 'List Member'),
	('VEHICLE_LIST', 0, 'REPORT', NULL, 0, 'List Vehicle', 'vehiclePm', 0, 5, 'index', 'List Vehicle'),
	('MANAGE_NEWS', 0, 'ACTIVITY', NULL, 0, 'Manage News', 'news', 0, 1, 'index', 'Manage News'),
	('MANAGE_ANNOUNCEMENT', 0, 'ACTIVITY', NULL, 0, 'Manage Announcement', 'announcement', 0, 1, 'index', 'Manage Announcement'),
	('VISITOR_LIST', 0, 'REPORT', NULL, 0, 'List Visitor', 'visitor', 0, 3, 'index', 'List Visitor'),
	('MAINTAIN_FACILITY', 0, 'MAINTENANCE', NULL, 0, 'Facility', 'facility', 0, 4, 'index', 'Maintenance Facility'),
	('MANAGE_BOOKING_FACILITY', 0, 'APPROVAL', NULL, 0, 'Manage Booking', 'manageFacilityBooking', 0, 1, 'index', 'Manage Booking'),
	('BOOKING_FACILITY', 0, 'ACTIVITY', NULL, 0, 'Booking Facility', 'facilityBooking', 0, 2, 'index', 'Booking Facility'),
	('HOME_GUARD', 0, 'REPORT', NULL, 0, 'Home Guard', 'homeGuard', 0, 1, 'index', 'Home Guard'),
	('BROADCAST_MESSAGE', 0, 'ACTIVITY', NULL, 0, 'Broadcast Message', 'broadcastMessage', 0, 2, 'index', 'Broadcast Message'),
	('MANAGE_FEEDBACK', 0, 'ACTIVITY', NULL, 0, 'Manage Feedback', 'feedback', 0, 4, 'index', 'Manage Feedback'),
	('DESCRIPTION_FORM', 0, 'MAINTENANCE', NULL, 0, 'Description Form', 'propertyRequest', 0, 5, 'index', 'Description Form');

INSERT INTO `news_category` (`id`, `version`, `code`, `icon`, `name`, `is_warga_ku`, `idx`, `description`)
VALUES
	('1', 0, 'facility', 'fa fa-building', 'Fasilitas', 'Y', 1, 'Fasilitas'),
	('2', 0, 'meals', 'ion ion-beer', 'Kuliner', 'Y', 2, 'Kuliner'),
	('3', 0, 'services', 'fa fa-address-card', 'Jasa', 'Y', 3, 'Jasa'),
	('4', 0, 'hobby', 'fa fa-bicycle', 'Hobby', 'Y', 4, 'Hobby'),
	('5', 0, 'rent', 'fa fa-dollar', 'Sewa', 'Y', 5, 'Sewa'),
	('6', 0, 'social', 'fa fa-book', 'Sosial', 'Y', 6, 'Sosial'),
	('7', 0, 'store', 'fa fa-shopping-basket', 'Toko', 'Y', 7, 'Toko'),
	('8', 0, 'others', 'fa fa-plus', 'Lain-lain', 'Y', 8, 'Lain-lain');

INSERT INTO `vehicle_type` (`id`, `version`, `created_date`, `code`, `name`, `updated_date`, `updated_by`, `pm_id`, `created_by`, `description`)
VALUES
	('1', 0, NULL, 'Sepeda', 'Sepeda', NULL, NULL, NULL, NULL, NULL),
	('2', 0, NULL, 'Sepeda Motor', 'Sepeda Motor', NULL, NULL, NULL, NULL, NULL),
	('3', 0, NULL, 'Mobil', 'Mobil', NULL, NULL, NULL, NULL, NULL),
	('4', 0, NULL, 'Truk', 'Truk', NULL, NULL, NULL, NULL, NULL),
	('5', 0, NULL, 'Lainnya', 'Lainnya', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `role_mobile` (`id`, `version`, `created_date`, `name`, `updated_date`, `updated_by`, `role_type`, `created_by`, `description`)
VALUES
	('1', 0, NULL, 'Pemilik', NULL, NULL, '2', NULL, 'Resident'),
	('2', 0, NULL, 'Penyewa', NULL, NULL, '2', NULL, 'Tenant'),
	('3', 0, NULL, 'Keamanan', NULL, NULL, '1', NULL, 'Security'),
	('4', 0, NULL, 'Penghuni', NULL, NULL, '2', NULL, 'Sub User');

INSERT INTO `property_type` (`id`, `version`, `created_date`, `name`, `updated_date`, `updated_by`, `created_by`, `description`)
VALUES
	('1', 0, NULL, 'Paguyuban', NULL, NULL, NULL, 'Paguyuban'),
	('2', 0, NULL, 'Perumahan', NULL, NULL, NULL, 'Perumahan'),
	('3', 0, NULL, 'Apartemen', NULL, NULL, NULL, 'Apartemen');